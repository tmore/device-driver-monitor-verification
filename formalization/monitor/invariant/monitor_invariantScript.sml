open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open stateTheory monitor_stateTheory nicInvariantTheory handlersTheory;
open bd_queueTheory;

val _ = new_theory "monitor_invariant";


Definition LINKED_REL_def:
  (LINKED_REL R [] = T)
  ∧ (LINKED_REL R (_ :: []) = T)
  ∧ (LINKED_REL R (a :: b :: xs) = (R a b ∧ LINKED_REL R (b :: xs)))
End


(*

BD ptr addresses may be located wherever in the CPPI_RAM_AREA but must be 4 byte
aligned, and may not be located less then 14 bytes away from the end.

*)
Definition BD_PTR_ADDRESS_def:
  BD_PTR_ADDRESS (bd_ptr : 32 word) =
   (CPPI_RAM_BYTE_PA bd_ptr ∧ WORD32_ALIGNED bd_ptr ∧ bd_ptr < (CPPI_RAM_END - 15w))
End
(*

States that the queue (list of pointers) is formed by traversing the descriptor
pointers from a certain pointer, that the queue is finite (and max length 256
(due to number of available slots) Also that all pointers formed within are
valid addresses.
*)
Definition WELL_FORMED_BD_QUEUE_def:
  WELL_FORMED_BD_QUEUE bds (bd_ptr : 32 word) (state : system_state) =
  (BD_QUEUE bds bd_ptr state.nic.regs.CPPI_RAM
   ∧ EVERY (λ p . ¬ FST (is_eop p state) ⇒ (read_ndp p state.nic.regs.CPPI_RAM) ≠ 0w) bds
   ∧ EVERY BD_PTR_ADDRESS bds
   ∧ LENGTH bds <= MAX_QUEUE_LENGTH_NUM
  )
End

(*

No two active buffer descriptors may overlap, each buffer descriptor is 16 bytes
therefore the difference between all addresses must be at least 16. This
statement implies that all elements are unique.

*)

Definition NON_OVERLAPPING_BD_QUEUES_def:
  NON_OVERLAPPING_BD_QUEUES (q1 : 32 word list) q2 =
  (∀ p1 p2 .
     (MEM p1 (q1 ++ q2))
      ∧ (MEM p2 (q1 ++ q2))
      ∧ p1 ≠ p2
      ⇒ word_abs (p1 - p2) >= 16w
      )
End

(*

If a certain memmory region is allocated in the alpha queue then the bit
corresponding to that address is set.

*)
Definition ALPHA_QUEUE_MARKED_def:
  ALPHA_QUEUE_MARKED bd_ptr alpha =
    (((alpha (alpha_index bd_ptr))
      && alpha_bitmask bd_ptr) ≠ (0w : 32 word))
End

Definition IN_ALPHA_RANGE_OF_def:
  IN_ALPHA_RANGE_OF queue_bd_ptr bd_ptr  =
    (queue_bd_ptr <= bd_ptr  ∧ bd_ptr < (queue_bd_ptr + 16w))
End

Definition IN_QUEUE_RANGE_def:
  (IN_QUEUE_RANGE _ [] = F)
  ∧ (IN_QUEUE_RANGE bd_ptr (x :: xs)  =
     ( IN_ALPHA_RANGE_OF x bd_ptr
     ∨  IN_QUEUE_RANGE bd_ptr xs))
End
(*

All ellements in the queues have their corresponding memmory region set to true.

*)
Definition BD_QUEUE_ALPHA_MARKED_def:
  BD_QUEUE_ALPHA_MARKED q1 q2 alpha =
  (∀ bd_ptr .
   (CPPI_RAM_BYTE_PA bd_ptr ∧ WORD32_ALIGNED bd_ptr) ⇒
   (IN_QUEUE_RANGE bd_ptr (q1 ++ q2) ⇔ ALPHA_QUEUE_MARKED bd_ptr alpha)
  )
End


(* CHANGE HDP POINTERS *)
Definition BD_QUEUE_INVARIANT_def:
  BD_QUEUE_INVARIANT (state : system_state) =
    ∃ tx0_queue rx0_queue .
       WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
       ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
       ∧ NON_OVERLAPPING_BD_QUEUES tx0_queue rx0_queue
       ∧ BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha
       ∧ (state.nic.tx.sop_bd_pa = 0w ∨ MEM state.nic.tx.sop_bd_pa tx0_queue)
       ∧ (state.nic.rx.sop_bd_pa = 0w ∨ MEM state.nic.rx.sop_bd_pa rx0_queue)
       ∧ (state.nic.rx.current_bd_pa = 0w ∨ MEM state.nic.rx.current_bd_pa rx0_queue)
End
(*

Invariant of the monitor. Curretly just a sketch

*)
Definition MONITOR_INVARIANT_def:
  MONITOR_INVARIANT (state : system_state) =
  ((NIC_INVARIANT state.nic ARB ARB)
  ∧ (state.monitor.initialized = (state.nic.it.state = it_initialized))
  ∧ (state.monitor.tx0_hdp_initialized = (state.nic.it.tx0_hdp_initialized))
  ∧ (state.monitor.rx0_hdp_initialized = (state.nic.it.rx0_hdp_initialized))
  ∧ (state.monitor.tx0_cp_initialized = (state.nic.it.tx0_cp_initialized))
  ∧ (state.monitor.rx0_cp_initialized = (state.nic.it.rx0_cp_initialized))

  ∧  ((¬state.monitor.initialized ∧ state.monitor.tx0_hdp_initialized)
      ⇒ state.nic.tx.sop_bd_pa = 0w )
  ∧ ((¬state.monitor.initialized ∧ state.monitor.rx0_hdp_initialized)
     ⇒ (state.nic.rx.sop_bd_pa = 0w ∧ state.nic.rx.current_bd_pa = 0w))

  ∧ ( (state.nic.td.state ≠ td_idle) ⇒ state.monitor.tx0_tearingdown)
  ∧ ( (state.nic.rd.state ≠ rd_idle) ⇒ state.monitor.rx0_tearingdown)

  ∧ (state.nic.it.state ≠ it_power_on )
  ∧ (state.monitor.initialized ⇒ state.nic.it.state = it_initialized)
  ∧ (state.nic.regs.CPDMA_SOFT_RESET = 1w ⇒ state.nic.it.state = it_reset)
  ∧ (((¬ state.monitor.initialized) ∧ (state.nic.regs.CPDMA_SOFT_RESET = 0w))
     ⇒ state.nic.it.state = it_initialize_hdp_cp)
  ∧ BD_QUEUE_INVARIANT state
  )
End




val _ = export_theory();
