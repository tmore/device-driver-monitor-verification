open HolKernel Parse listTheory wordsLib boolLib bossLib;
open state_transformerTheory;

val _ = new_theory "state_transformer_ext";


fun add_infixes n assoc =
  List.app (fn (s, t) => ( Parse.add_infix (s, n, assoc)
                         ; Parse.overload_on (s, Parse.Term t)
                         ))
Definition ifM_def:
  ifM (mb : (γ -> (bool # γ))) mt mf =
      do b <- mb;
         if b
         then mt
         else mf
      od
End



Definition andM_def:
  andM (mb1 : (γ -> (bool # γ))) (mb2 : (γ -> (bool # γ))) =
    ifM mb1 mb2 (UNIT F)
End
Definition orM_def:
  orM (mb1 : (γ -> (bool # γ))) (mb2 : (γ -> (bool # γ))) =
   ifM mb1 (UNIT T) mb2
End

Definition whenM_def:
  whenM guard ma = if guard then ma else UNIT ()
End
Definition whenMM_def:
  whenMM guard ma = ifM guard ma  (UNIT ())
End

Definition applicative_apply_def:
  applicative_apply (mf : (γ -> ((α -> β) # γ))) (mx : (γ -> (α # γ))) =
    do f <- mf;
       x <- mx;
       UNIT (f x)
    od
End


Definition functor_map_def:
  fmap (f : α -> β) (mx : (γ -> (α # γ))) =
    do x <- mx;
       UNIT (f x);
    od
End


Definition fixn_def:
  (fixn 0 _ a g = (a , g))
  ∧ (fixn (SUC n) (f : α -> γ -> (α # γ)) (a : α) (g : γ) =
     (let (a' , g') = f a g
      in fixn n f a' g'))
End

val () = add_infixes 492 HOLgrammars.RIGHT
  [("<@>" , ‘state_transformer_ext$fmap’)]
(* val () = add_infixes 491 HOLgrammars.RIGHT *)
(* [("<*>" , ‘state_transformer_ext$applicative_apply’)] *)

val _ = overload_on("APPLICATIVE_FAPPLY", “applicative_apply”);

(*
Overload "<@>" = “state_transformer_ext$fmap”
Overload "<*>" = “state_transformer_ext$applicative_apply”
val _ = send_to_back_overload "<@>" {Name = "fmap", Thy = "state_transformer_ext"}
val _ = send_to_back_overload "<*>" {Name = "applicative_apply", Thy = "state_transformer_ext"}
*)
(* Overload "APPLICATIVE_FAPPLY" = “state_transformer_ext$applicative_apply”*)
(*val _ = send_to_back_overload "APPLICATIVE_FAPPLY" {Name = "applicative_apply", Thy = "state_transformer_ext"} *)

Definition notM_def:
  notM (mb : (γ -> (bool # γ))) = ($¬ <@> mb)
End

Definition caseM_def:
  (caseM ([] : (((γ -> (bool # γ)) # ((γ -> (α # γ)))) list)) (def_ma : (γ -> (α # γ))) = def_ma)
  ∧ (caseM ((mb , ma) :: tail) def_ma =
           ifM mb ma (caseM tail def_ma))
End


val () = add_infixes 490 HOLgrammars.RIGHT
[("<∧>" , ‘state_transformer_ext$andM’)]
val () = add_infixes 489 HOLgrammars.RIGHT
[("<∨>" , ‘state_transformer_ext$orM’)]

Definition return_def:
  return = UNIT
End
val _ = export_theory();
