(*
Author: Tomas Möre 2020

Model of the handlers defined in "nic_model/monitor-linux/monitor.c"

*)
open HolKernel wordsLib boolLib bossLib;
open monitor_stateTheory;
open register_readTheory register_writeTheory address_spaceTheory schedulerTheory;

open state_transformer_extTheory;

val _ = new_theory "handlers";



(* CONSTANTS *)
val MAX_QUEUE_LENGTH_NUM_def = Define ‘MAX_QUEUE_LENGTH_NUM = 256’;
val MAX_QUEUE_LENGTH_def = Define ‘MAX_QUEUE_LENGTH = 256w’;
val SOP_def = Define ‘SOP = 1w << 31’;
val EOP_def = Define ‘EOP = 1w << 30’;
val OWNER_def = Define ‘OWNER = 1w << 29’;
val EOQ_def = Define ‘EOQ = 1w << 28’;
val TD_def = Define ‘TD = 1w << 27’;
val CRC_def = Define ‘CRC = 1w << 26’;

val RX_BL_def = Define ‘RX_BL = 0x7FFw’;
val RX_BO_def = Define ‘RX_BO = 0x77Fw << 16’;

val TX_BL_def = Define ‘TX_BL = 0xFFFFw’;
val TX_BO_def = Define ‘TX_BO = 0xFFFFw’;

val PL_def = Define ‘PL = 0x7FFw’;

val NEXT_DESCRIPTOR_POINTER_def = Define ‘NEXT_DESCRIPTOR_POINTER = 0w’;
val BUFFER_POINTER_def = Define ‘BUFFER_POINTER = 4w’;
val BOBL_def = Define ‘BOBL = 8w’;
val FLAGS_def = Define ‘FLAGS = 12w’;


(*

The following functions are written using the state_transformers.

*)

(*

In the model of the nic register writes and reads are done by special
functions. (read_register from register_readTheory) and (write_register from
register_writeTheory). These are used to simulate the reading and writing of the
physical addresses related to the nic.

*)

Definition read_nic_register_def:
  read_nic_register (pa : 32 word) (sys_state : system_state) =
    (let (nic_state' , ret_val ) = read_register ARB pa sys_state.nic
     in (ret_val , sys_state with nic := nic_state'))
End

(*

Returns only a new state with modified NIC. Would return (state # Unit) but HOL4
does not seem to have a default unit type

*)
Definition write_nic_register_def:
  write_nic_register (pa : 32 word) (value : 32 word) (sys_state : system_state) =
    ( () , sys_state with nic := write_register ARB pa value sys_state.nic)
End


(*

Runs the automaton one step. The environment is currently left arbitrary.

*)

Definition nic_transition_def:
  nic_transition (state : system_state) =
  let ( nic' , _ , _) = scheduler ARB state.nic
  in ( () , state with nic := nic')
End

Definition nic_it_transition_def:
  nic_it_transition (state : system_state) =
  (() , state with nic := it_transition state.nic)
End

(* UTILITIES as defined in montiro_hv.c *)
Definition get_next_descriptor_pointer_def:
  get_next_descriptor_pointer (pa : 32 word) =
    read_nic_register (pa + NEXT_DESCRIPTOR_POINTER)
End
Definition get_buffer_pointer_def:
  get_buffer_pointer (pa : 32 word) =
    read_nic_register (pa + BUFFER_POINTER)
End

Definition get_buffer_offset_and_length_def:
  get_buffer_offset_and_length (pa : 32 word) =
    read_nic_register (pa + BOBL)
End

Definition get_flags_def:
  get_flags (pa : 32 word) =
    read_nic_register (pa + FLAGS)
End

Definition get_rx_buffer_length_def:
  get_rx_buffer_length_def (pa : 32 word) =
    fmap ($&& RX_BL) (read_nic_register (pa + BOBL))
End
Definition get_tx_buffer_length_def:
  get_tx_buffer_length (pa : 32 word) =
    fmap ($&& TX_BL) (read_nic_register (pa + BOBL))
End
Definition get_packet_length_def:
  get_packet_length (pa : 32 word) =
    fmap ($&& PL) (read_nic_register (pa + FLAGS))
End

Definition is_sop_def:
  is_sop (pa : 32 word) =
    do val <- read_nic_register (pa + 12w);
       UNIT ((val && SOP) ≠ 0w)
    od
End
Definition is_eop_def:
  is_eop (pa : 32 word) =
    do val <- read_nic_register (pa + 12w);
       UNIT ((val && EOP) ≠ 0w)
    od
End
Definition is_released_def:
  is_released (pa : 32 word) =
    do val <- read_nic_register (pa + 12w);
       UNIT ((val && OWNER) = 0w)
    od
End

Definition is_eoq_def:
  is_eoq (pa : 32 word) =
    do val <- read_nic_register (pa + 12w);
       UNIT ((val && EOQ) ≠ 0w)
    od
End

Definition is_td_def:
  is_td (pa : 32 word) =
    do val <- read_nic_register (pa + 12w);
       UNIT ((val && TD) ≠ 0w)
    od
End

Definition is_word_aligned_def:
  is_word_aligned (address : 32 word) =
    ((address && 0x3w) = 0w)
End

val TD_INT_def = Define ‘TD_INT = 0xFFFFFFFCw : 32 word’;

val TRANSMIT_def = Define ‘TRANSMIT = T’;
val RECEIVE_def = Define ‘RECEIVE = F’;

val ADD_def = Define ‘ADD = T’;
val REMOVE_def = Define ‘REMOVE = F’;

val SOP_BD_def = Define ‘SOP_BD = T’;
val EOP_BD_def = Define ‘EOP_BD = F’;

val MAX_RHO_NIC_def = Define ‘MAX_RHO_NIC = 15w’;

Datatype:
  OVERLAP = ZEROED_NEXT_DESCRIPTOR_POINTER_OVERLAP
          | ILLEGAL_OVERLAP
          | NO_OVERLAP
End

(* UTILITY *)
Definition alpha_index_def:
  alpha_index (bd_ptr : 32 word) =
    ((bd_ptr - CPPI_RAM_START) >> 7)
End
(* UTILITY *)
Definition alpha_bitmask_def:
  alpha_bitmask (bd_ptr : 32 word) =
  ((1w : 32 word) << w2n (((bd_ptr - CPPI_RAM_START) >> 2) && (0x1Fw : 32 word)))
End

Definition IS_ACTIVE_CPPI_RAM_def:
  IS_ACTIVE_CPPI_RAM (CPPI_RAM_WORD : 32 word) =
    do alpha <- get_alpha;
    UNIT (((alpha (alpha_index CPPI_RAM_WORD))
          && alpha_bitmask CPPI_RAM_WORD) ≠ (0w : 32 word))
    od
End

Definition SET_ACTIVE_CPPI_RAM_def:
  SET_ACTIVE_CPPI_RAM (CPPI_RAM_WORD : 32 word) =
    do alpha <- get_alpha;
       let index = alpha_index CPPI_RAM_WORD;
           alpha_value = alpha index;
           or_mod = alpha_bitmask CPPI_RAM_WORD;
       in set_alpha (λ addr . if addr = index
                              then alpha_value || or_mod
                              else alpha addr)
    od
End

Definition CLEAR_ACTIVE_CPPI_RAM_def:
  CLEAR_ACTIVE_CPPI_RAM (CPPI_RAM_WORD : 32 word) =
    do alpha <- get_alpha;
       let index = alpha_index CPPI_RAM_WORD;
       in set_alpha (λ addr . if addr = index
                              then (alpha index) && (¬ alpha_bitmask CPPI_RAM_WORD)
                               else alpha addr)
    od
End

(*
INTERNAL UTILITY FUNCTION FOR LOOPING OVER BD_PTRS
first is not inteded for use elsewhere



*)

Datatype:
   LoopRes = ContinueWith 'a
           | BreakWith 'b
End

Definition breakWith_def:
  breakWith (a : β) = UNIT ((BreakWith a) : (α , β) LoopRes)
End

Definition continueWith_def:
  continueWith a = UNIT (ContinueWith a)
End

(* ((32 word) -> 'a -> ((('a , 'b) LoopRes) # system_state)) *)
Definition traverse_bd_ptr_list_loop_def:
  (traverse_bd_list_loop _ _ (BreakWith b)  _ =
    UNIT b)
  ∧ (traverse_bd_list_loop ZERO _ _ _ = return_dead ARB)
  ∧ (traverse_bd_list_loop (SUC loop_bound) (bd_ptr : 32 word) ((ContinueWith a) : (α, β) LoopRes) (f : (32 word -> α  -> system_state -> (((α , β) LoopRes) # system_state))) =
     do accum <- f bd_ptr a;
        next_ptr <- get_next_descriptor_pointer bd_ptr;
        traverse_bd_list_loop loop_bound next_ptr accum f
     od
    )
End

Definition traverse_bd_ptr_loop_def:
  (traverse_bd_list bd_ptr a f =
    traverse_bd_list_loop MAX_QUEUE_LENGTH_NUM bd_ptr (ContinueWith a) f)
End

(* EXISTS as standin but implementation does nothing sice it is ignored*)
Definition decrement_rho_nic_def:
  decrement_rho_nic (bd_ptr : 32 word) =
   UNIT ()
End

(*
Removed call to 'decrement_rho_nic' since it should be ignored

Missing line:
whenM (¬ transmit ∧ ¬ add) (decrement_rho_nic bd_ptr);

*)
Definition update_alpha_def:
  update_alpha (bd_ptr : 32 word) (add' : bool) =
      if add'
      then (do SET_ACTIVE_CPPI_RAM bd_ptr;
               SET_ACTIVE_CPPI_RAM (bd_ptr + 4w);
               SET_ACTIVE_CPPI_RAM (bd_ptr + 8w);
               SET_ACTIVE_CPPI_RAM (bd_ptr + 12w);
               UNIT ();
            od)
      else (do CLEAR_ACTIVE_CPPI_RAM bd_ptr;
               CLEAR_ACTIVE_CPPI_RAM (bd_ptr + 4w);
               CLEAR_ACTIVE_CPPI_RAM (bd_ptr + 8w);
               CLEAR_ACTIVE_CPPI_RAM (bd_ptr + 12w);
               UNIT ();
            od)
End
      (* whenM (¬ transmit ∧ ¬ add) (decrement_rho_nic bd_ptr) *)


Definition update_alpha_queue_loop_def:
  (update_alpha_queue_loop 0 bd_ptr _ =
   if bd_ptr = 0w
   then return ()
   else return_dead ARB)
  ∧
  (update_alpha_queue_loop (SUC loop_itrs) (bd_ptr : 32 word) (add' : bool) =
    if bd_ptr ≠ 0w
    then (do update_alpha bd_ptr add';
             next_bd_ptr <- get_next_descriptor_pointer bd_ptr;
             update_alpha_queue_loop loop_itrs next_bd_ptr add'
          od)
    else UNIT ())
End

Definition update_alpha_queue_def:
  update_alpha_queue (bd_ptr : 32 word) (add' : bool) =
    update_alpha_queue_loop MAX_QUEUE_LENGTH_NUM bd_ptr add'
End

Definition initialization_performed_def:
  initialization_performed =
    do tx0_active_queue <- get_tx0_active_queue;
       update_alpha_queue tx0_active_queue REMOVE;
       rx0_active_queue <- get_rx0_active_queue;
       update_alpha_queue rx0_active_queue REMOVE;
       set_tx0_active_queue 0w;
       set_rx0_active_queue 0w;
       set_initialized T;
    od
End

Definition update_active_queue_find_eop_loop_def:
  (update_active_queue_find_eop_loop 0 bd_ptr =
   ifM (is_eop bd_ptr)
       (UNIT bd_ptr)
       (return_dead ARB))
  ∧ (update_active_queue_find_eop_loop (SUC loop_itr) (bd_ptr : 32 word) =
       ifM (is_eop bd_ptr)
           (UNIT bd_ptr)
           (do update_alpha bd_ptr REMOVE;
               next_bd_ptr <- get_next_descriptor_pointer bd_ptr;
               update_active_queue_find_eop_loop loop_itr next_bd_ptr
            od))
End

Definition update_active_queue_outer_loop_def:
  (update_active_queue_outer_loop 0 released no_teardown bd_ptr _ =
   (if ¬ (released ∧ no_teardown ∧ (bd_ptr ≠ 0w))
   then UNIT bd_ptr
   else return_dead ARB))
  ∧ (update_active_queue_outer_loop (SUC loop_itr) (released : bool) (no_teardown : bool) (bd_ptr : 32 word) (tearingdown : bool) =
      (if (¬ (released ∧ no_teardown ∧ (bd_ptr ≠ 0w)))
       then UNIT bd_ptr
       else ifM (notM (is_released bd_ptr) )
                (update_active_queue_outer_loop loop_itr F no_teardown bd_ptr tearingdown)
                (ifM (is_td bd_ptr <∧> UNIT tearingdown)
                     (do update_alpha_queue bd_ptr REMOVE;
                         update_active_queue_outer_loop loop_itr released F 0w tearingdown
                      od)
                     (do bd_ptr' <- update_active_queue_find_eop_loop MAX_QUEUE_LENGTH_NUM bd_ptr;
                         update_alpha bd_ptr' REMOVE;
                         bd_ptr'' <- get_next_descriptor_pointer bd_ptr';
                         update_active_queue_outer_loop loop_itr released no_teardown bd_ptr'' tearingdown
                      od))
            ))
End
Definition update_active_queue_def:
  update_active_queue (transmit' : bool) =
    do bd_ptr <- if transmit'
                 then get_tx0_active_queue
                 else get_rx0_active_queue;
       tearingdown <- if transmit'
                      then get_tx0_tearingdown
                      else get_rx0_tearingdown;
       bd_ptr' <- update_active_queue_outer_loop MAX_QUEUE_LENGTH_NUM T T bd_ptr tearingdown;
       if transmit'
       then set_tx0_active_queue bd_ptr'
       else set_rx0_active_queue bd_ptr'
    od
End


Definition is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def:

  is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop 0 (bd_ptr : 32 word) =
    (if bd_ptr ≠ 0w
     then UNIT F
     else UNIT T)
  ∧ (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop (SUC loop_itr) (bd_ptr : 32 word) =
       if bd_ptr = 0w
       then is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop 0 bd_ptr
       else if (0x4A102000w <= bd_ptr)
              ∧ (bd_ptr < (0x4A104000w - 15w))
              ∧ (is_word_aligned bd_ptr)
            then do is_active_cppi_ram0 <- IS_ACTIVE_CPPI_RAM bd_ptr;
                    is_active_cppi_ram4 <- IS_ACTIVE_CPPI_RAM (bd_ptr + 4w);
                    is_active_cppi_ram8 <- IS_ACTIVE_CPPI_RAM (bd_ptr + 8w);
                    is_active_cppi_ram12 <- IS_ACTIVE_CPPI_RAM (bd_ptr + 12w);
                    if (is_active_cppi_ram0 ∨ is_active_cppi_ram4
                        ∨ is_active_cppi_ram8 ∨ is_active_cppi_ram12)
                    then UNIT F
                    else do bd_ptr' <- get_next_descriptor_pointer bd_ptr;
                            is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop loop_itr bd_ptr'
                         od
                 od
            else return F)
End

Definition is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_def:
  is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap (bd_ptr : 32 word) =
  is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop MAX_QUEUE_LENGTH_NUM bd_ptr
End

Definition is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop_def:
  (is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop 0 bd_ptr buffer_length_sum =
   (ifM (UNIT (bd_ptr ≠ 0w) <∧> notM (is_sop bd_ptr) <∧> notM (is_eop bd_ptr))
      (return ARB)
      (return (bd_ptr , buffer_length_sum))))
  ∧ (is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop  (SUC n) bd_ptr buffer_length_sum =
     ifM (UNIT (bd_ptr ≠ 0w) <∧> notM (is_sop bd_ptr) <∧> notM (is_eop bd_ptr))
      (do buffer_length <- get_tx_buffer_length bd_ptr;
          bd_ptr' <- get_next_descriptor_pointer bd_ptr;
          (is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop n bd_ptr' (buffer_length_sum + buffer_length))
      od)
      (return (bd_ptr , buffer_length_sum)))
End

(* This definition currently ignores the packet length checks
*)
Definition is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def:
  (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop 0 bd_ptr  =
    return (bd_ptr = 0w))
  ∧ (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop (SUC n) bd_ptr =
     if (bd_ptr = 0w)
     then return T
     else ifM (notM (is_sop bd_ptr))
              (return F)
              (do buffer_length <- get_tx_buffer_length bd_ptr;
                  packet_length <- get_packet_length bd_ptr;
                  (ifM (notM (is_eop bd_ptr))
                   (do bd_ptr' <- get_next_descriptor_pointer bd_ptr;
                       (bd_ptr'' , buffer_length_sum) <-
                          is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop
                            MAX_QUEUE_LENGTH_NUM bd_ptr' buffer_length;
                       last_buffer_length <- if bd_ptr'' ≠ 0w
                                             then get_tx_buffer_length bd_ptr''
                                             else UNIT 0w;
                       ifM ((UNIT (bd_ptr'' = 0w)) <∨> is_sop bd_ptr'')
                           (return F)
                           (if (packet_length ≠ buffer_length_sum + last_buffer_length)
                               then return F
                               else (do next_bd_ptr <- get_next_descriptor_pointer bd_ptr'';
                                        is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop n next_bd_ptr
                                     od)
                           )
                    od)
                   (if (packet_length ≠ buffer_length)
                    then return F
                    else (do
                        bd_ptr' <- get_next_descriptor_pointer bd_ptr;
                        is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop n bd_ptr'
                        od)
                   )
                  )
              od)
    )
End

Definition is_transmit_SOP_EOP_packet_length_fields_set_correctly_def:
  is_transmit_SOP_EOP_packet_length_fields_set_correctly bd_ptr =
  is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop MAX_QUEUE_LENGTH_NUM bd_ptr
End

Definition set_and_clear_word_on_sop_or_eop_def:
  (set_and_clear_word_on_sop_or_eop 0 bd_ptr _ _ _ _ =
   if bd_ptr = 0w
   then return ()
   else return ARB)
  ∧ (set_and_clear_word_on_sop_or_eop (SUC loop_itr) (bd_ptr : 32 word) (offset : 32 word) (set : 32 word) (clear : 32 word) (modify_sop_or_eop : bool) =
     if bd_ptr = 0w
     then return ()
     else (do whenMM ((is_sop bd_ptr <∧> UNIT modify_sop_or_eop)
                     <∨> (is_eop bd_ptr <∧> UNIT (¬ modify_sop_or_eop)))
                     (monad_bind
                       ((λ v . (v || set) && ¬clear) <@>  (read_nic_register (bd_ptr + offset)))
                       (write_nic_register (bd_ptr + offset)));
              bd_ptr' <- get_next_descriptor_pointer bd_ptr;
              set_and_clear_word_on_sop_or_eop loop_itr bd_ptr' offset set clear modify_sop_or_eop
           od))
End
Definition set_and_clear_word_def:
  (set_and_clear_word 0 _ _ _ _ = return_dead ARB)
  ∧ (set_and_clear_word (SUC n) (bd_ptr : 32 word) (offset : 32 word) (set : 32 word) (clear : 32 word) =
     if bd_ptr = 0w
     then return ()
     else do monad_bind
                ((λ v . (v || set) && ¬ clear) <@> read_nic_register (bd_ptr + clear))
                (write_nic_register (bd_ptr + offset));
             bd_ptr' <- get_next_descriptor_pointer bd_ptr;
             set_and_clear_word n bd_ptr' offset set clear
          od)
End

Definition no_overflow_and_updated_rho_nic_def:
  no_overflow_and_updated_rho_nic (bd_ptr : 32 word) =
    return ARB
End

Definition is_secure_linux_memory_def:
  is_secure_linux_memory (transmit' : bool) (start_bl : 32 word) (end_bl : 32 word) =
    return ARB
End
(*ifM (is_sop bd_ptr) (UNIT (((buffer_offset_length && (TX_BO : 32 word)) >> 16) : 32 word)) (UNIT (0w : 32 word))*)

(* TX_BO_def is diffrent from C due to hol4 bug
*)
Definition is_data_buffer_secure_def:
  is_data_buffer_secure (bd_ptr : 32 word) (transmit' : bool) =
  do buffer_pointer <- get_buffer_pointer bd_ptr;
     buffer_offset_length <- get_buffer_offset_and_length bd_ptr;
     buffer_length <- UNIT (if transmit'
                      then (buffer_offset_length && TX_BL)
                      else (buffer_offset_length && RX_BL));
     buffer_offset <- if transmit'
                      then (ifM (is_sop bd_ptr)
                                (UNIT (TX_BO &&  (buffer_offset_length >> 16)))
                                (UNIT (0w : 32 word)))
                      else (UNIT (0w : 32 word));
     if buffer_length = 0w
     then return F
     else if (buffer_pointer >₊ (0xFFFFFFFFw - buffer_offset - buffer_length + 1w))
     then return F
     else ifM (notM (is_secure_linux_memory transmit' (buffer_pointer >> 12) ((buffer_pointer + buffer_offset + buffer_length - 1w) >> 12)))
              (return F)
              (return T)
  od
End
Definition is_data_buffer_secure_queue_loop_def:
  (is_data_buffer_secure_queue_loop 0 bd_ptr _ =
   if bd_ptr = 0w
   then return T
   else return_dead ARB)
  ∧ (is_data_buffer_secure_queue_loop (SUC loop_itr) (bd_ptr : 32 word) (transmit' : bool) =
     if bd_ptr = 0w
     then return T
     else ifM (notM (is_data_buffer_secure bd_ptr transmit'))
              (return F)
              (do next_ptr <- get_next_descriptor_pointer bd_ptr;
                  is_data_buffer_secure_queue_loop loop_itr next_ptr transmit'
               od))
End

Definition is_data_buffer_secure_queue_def:
  is_data_buffer_secure_queue =
    is_data_buffer_secure_queue_loop MAX_QUEUE_LENGTH_NUM
End


Definition is_queue_self_overlap_inner_loop_def:
  (is_queue_self_overlap_inner_loop 0 bd_ptr other_bd_ptr =
   if other_bd_ptr = 0w
   then return F
   else return ARB)
  ∧ (is_queue_self_overlap_inner_loop (SUC n) bd_ptr other_bd_ptr =
     if other_bd_ptr = 0w
     then return F
     else if ((bd_ptr ≤₊ other_bd_ptr ∧ other_bd_ptr <₊  bd_ptr + 0x10w)
              ∨ (other_bd_ptr ≤₊ bd_ptr ∧ bd_ptr <₊  other_bd_ptr + 0x10w))
     then return T
     else do other_bd_ptr' <- get_next_descriptor_pointer other_bd_ptr;
             is_queue_self_overlap_inner_loop n bd_ptr other_bd_ptr'
          od
    )
End

Definition is_queue_self_overlap_outer_loop_def:
  (is_queue_self_overlap_outer_loop 0 bd_ptr =
   return (bd_ptr ≠ 0w))
  ∧ (is_queue_self_overlap_outer_loop (SUC n) bd_ptr =
   if bd_ptr = 0w
   then return F
   else do other_bd_ptr <- get_next_descriptor_pointer bd_ptr;
          ifM (is_queue_self_overlap_inner_loop MAX_QUEUE_LENGTH_NUM bd_ptr other_bd_ptr)
              (return T)
              (is_queue_self_overlap_outer_loop n other_bd_ptr)
        od
  )
End

Definition is_queue_self_overlap_def:
  is_queue_self_overlap (bd_ptr : 32 word) =
  is_queue_self_overlap_outer_loop MAX_QUEUE_LENGTH_NUM bd_ptr
End
(*


            if bd_ptr = 0w
            then breakWith F
            else do orig_other_bd_ptr <- get_next_descriptor_pointer bd_ptr;
                    traverse_bd_list orig_other_bd_ptr ()
                    (λ other_bd_ptr _ .
                     if ((bd_ptr <= other_bd_ptr ∧ other_bd_ptr < (bd_ptr + 0x10w))
                        ∨ (other_bd_ptr <= bd_ptr ∧ bd_ptr < (other_bd_ptr + 0x10w)))
                     then breakWith T
                     else continueWith ()
                    )
                  od

*)
Definition is_queue_secure_def:
  is_queue_secure (bd_ptr : 32 word) (transmit' : bool) =
   caseM [ (UNIT (bd_ptr = 0w)
           , return T)
         ; (notM (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap bd_ptr)
           , return F)
         ; (is_queue_self_overlap bd_ptr
           , return F)
         ; ( ((UNIT transmit') <∧> notM (is_transmit_SOP_EOP_packet_length_fields_set_correctly bd_ptr))
           , return F)
         ; ( notM (is_data_buffer_secure_queue bd_ptr transmit')
            , return F)
         ]
         (do if transmit'
             then do set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM bd_ptr FLAGS OWNER 0w SOP_BD;
                     set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM bd_ptr FLAGS 0w EOQ EOP_BD;
                     set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM bd_ptr FLAGS 0w TD SOP_BD;
                     UNIT ()
                  od
             else do set_and_clear_word MAX_QUEUE_LENGTH_NUM bd_ptr BOBL 0w RX_BO;
                     set_and_clear_word MAX_QUEUE_LENGTH_NUM bd_ptr FLAGS OWNER (SOP || EOP || EOQ || TD || CRC);
                     UNIT ()
                  od;
             return T
          od
         )
End
(*

All handlers are defined to return a tuple (bool # monitor_state). where the
left value is the return value of the procedures and right value is the state.

*)


Definition stateram_handler_def:
  stateram_handler (val : 32 word) =
  do initialized <- get_initialized;
     UNIT ¬(val ≠ 0w ∨  ¬ initialized)
  od
End

Definition dmacontrol_handler_def:
  dmacontrol_handler (val : 32 word) =
  do intitialized <- get_initialized;
     UNIT ¬ (val ≠ 0w ∨  ¬ intitialized)
  od
End


Definition cpdma_soft_reset_handler_def:
  cpdma_soft_reset_handler (val : 32 word) =
    if ((val && 1w) = 0w)
    then UNIT T
    else ifM (notM get_initialized <∨> get_tx0_tearingdown <∨> get_rx0_tearingdown)
             (UNIT F)
             (do set_initialized F;
                 set_tx0_hdp_initialized F;
                 set_rx0_hdp_initialized F;
                 set_tx0_cp_initialized F;
                 set_rx0_cp_initialized F;
                 write_nic_register CPDMA_SOFT_RESET_PA 1w;
                 UNIT T
               od)
End

Definition tx0_hdp_handler_def:
  tx0_hdp_handler (bd_ptr : 32 word) =
     ifM (notM get_initialized)
        (do cpdma_soft_reset <- read_nic_register CPDMA_SOFT_RESET_PA;
            if ((cpdma_soft_reset = 1w) ∨ bd_ptr ≠ 0w)
            then return F
            else do write_nic_register TX0_HDP_PA 0w;
                    set_tx0_hdp_initialized T;
                    whenMM (get_rx0_hdp_initialized
                            <∧> get_tx0_cp_initialized
                            <∧> get_rx0_cp_initialized)
                           initialization_performed;
                    return T
                 od
        od)
        (do tx0_hdp <- read_nic_register TX0_HDP_PA;
            tx0_tearingdown <- get_tx0_tearingdown;
            if ((tx0_hdp ≠ 0w) ∨ tx0_tearingdown)
            then return F
            else (do tx0_active_queue <- get_tx0_active_queue;
                    whenM (tx0_active_queue ≠ 0w)
                          (do update_alpha_queue tx0_active_queue REMOVE;
                              set_tx0_active_queue 0w
                          od);
                    ifM (is_queue_secure bd_ptr TRANSMIT)
                        (do set_tx0_active_queue bd_ptr;
                            update_alpha_queue bd_ptr ADD;
                            write_nic_register TX0_HDP_PA bd_ptr;
                            return T
                         od)
                        (return F)
                 od)
         od)

End

Definition rx0_hdp_handler_def:
  rx0_hdp_handler (bd_ptr : 32 word) =
     ifM (notM get_initialized)
        (do cpdma_soft_reset <- read_nic_register CPDMA_SOFT_RESET_PA;
            if ((cpdma_soft_reset = 1w) ∨ bd_ptr ≠ 0w)
            then return F
            else do write_nic_register RX0_HDP_PA 0w;
                    set_rx0_hdp_initialized T;
                    whenMM (get_tx0_hdp_initialized
                            <∧> get_tx0_cp_initialized
                            <∧> get_rx0_cp_initialized)
                           initialization_performed;
                    return T
                 od
        od)
        (do rx0_hdp <- read_nic_register RX0_HDP_PA;
            rx0_tearingdown <- get_rx0_tearingdown;
            if ((rx0_hdp ≠ 0w) ∨ rx0_tearingdown)
            then return F
            else (do rx0_active_queue <- get_rx0_active_queue;
                    whenM (rx0_active_queue ≠ 0w)
                          (do update_alpha_queue rx0_active_queue REMOVE;
                              set_rx0_active_queue 0w
                          od);
                    ifM (is_queue_secure bd_ptr TRANSMIT)
                        (do set_rx0_active_queue bd_ptr;
                            update_alpha_queue bd_ptr REMOVE;
                            write_nic_register RX0_HDP_PA bd_ptr;
                            return T
                         od)
                        (return F)
                 od)
         od)
End


Definition tx0_cp_handler_def:
  tx0_cp_handler (val : 32 word) =
  ifM (notM  get_initialized)
      (do cpdma_soft_reset <- read_nic_register CPDMA_SOFT_RESET_PA;
            if ((cpdma_soft_reset = 1w) ∨ val ≠ 0w)
            then return F
            else do write_nic_register TX0_CP_PA 0w;
                    set_tx0_cp_initialized T;
                    whenMM (get_tx0_hdp_initialized
                            <∧> get_rx0_hdp_initialized
                            <∧> get_rx0_cp_initialized)
                           initialization_performed;
                    return T
                 od
       od)
      (do ifM (notM get_tx0_tearingdown)
              (do write_nic_register TX0_CP_PA 1w;
                  return T
              od)
              (do update_active_queue TRANSMIT;
                  tx0_tearingdown <- get_tx0_tearingdown;
                  tx0_active_queue <- get_tx0_active_queue;
                  tx0_cp <- read_nic_register TX0_CP_PA;
                  tx0_hdp <- read_nic_register TX0_HDP_PA;
                  if (tx0_tearingdown ∧ tx0_active_queue = 0w ∧ tx0_cp = TD_INT ∧ tx0_hdp = 0w ∧ val = TD_INT)
                  then do write_nic_register TX0_CP_PA TD_INT;
                          set_tx0_tearingdown F;
                          return T;
                       od
                  else return F;
               od)
       od)
End

Definition rx0_cp_handler_def:
  rx0_cp_handler (val : 32 word) =
  ifM (notM  get_initialized)
      (do cpdma_soft_reset <- read_nic_register CPDMA_SOFT_RESET_PA;
            if ((cpdma_soft_reset = 1w) ∨ val ≠ 0w)
            then return F
            else do write_nic_register RX0_CP_PA 0w;
                    set_rx0_cp_initialized T;
                    whenMM (get_tx0_hdp_initialized
                            <∧> get_rx0_hdp_initialized
                            <∧> get_tx0_cp_initialized)
                           initialization_performed;
                    return T
                 od
       od)
      (do ifM (notM get_rx0_tearingdown)
              (do write_nic_register RX0_CP_PA 1w;
                  return T
              od)
              (do update_active_queue TRANSMIT;
                  rx0_tearingdown <- get_rx0_tearingdown;
                  rx0_active_queue <- get_rx0_active_queue;
                  rx0_cp <- read_nic_register RX0_CP_PA;
                  rx0_hdp <- read_nic_register RX0_HDP_PA;
                  if (rx0_tearingdown ∧ rx0_active_queue = 0w ∧ rx0_cp = TD_INT ∧ rx0_hdp = 0w ∧ val = TD_INT)
                  then do write_nic_register RX0_CP_PA TD_INT;
                          set_rx0_tearingdown F;
                          return T;
                       od
                  else return F;
               od)
       od)
End

val _ = export_theory();
