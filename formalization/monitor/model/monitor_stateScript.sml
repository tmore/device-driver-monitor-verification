(*
Author: Tomas Möre 2020

Model of the stateful variables found in "nic_model/monitor-linux/monitor.c"

*)

open HolKernel wordsLib listTheory boolLib bossLib stateTheory;

val _ = new_theory "monitor_state";


(*

minitor_state, as its name suggests, is the model of the NIC monitor state. It
contains variables that forms the internal state of the monitor, except for some
memory mappings.

*)
Datatype:
nic_monitor_state  = <|
  tx0_active_queue : 32 word;
  rx0_active_queue : 32 word;

  initialized : bool;
  tx0_hdp_initialized : bool;
  rx0_hdp_initialized : bool;
  tx0_cp_initialized  : bool;
  rx0_cp_initialized  : bool;
  tx0_tearingdown     : bool;
  rx0_tearingdown     : bool;
  alpha               : 32 word -> 32 word;
  recv_bd_nr_blocks   : 32 word -> 32 word;
  |>
End

Definition default_state_def:
default_nic_monitor_state = <|
  tx0_active_queue    := 0w;
  rx0_active_queue    := 0w;
  initialized         := F;
  tx0_hdp_initialized := F;
  rx0_hdp_initialized := F;
  tx0_cp_initialized  := F;
  rx0_cp_initialized  := F;
  tx0_tearingdown     := F;
  rx0_tearingdown     := F;
  alpha               := (λ idx . if idx < 64w then 0w else ARB);
  recv_bd_nr_blocks   := (λ idx . if idx < 2048w then 0w else ARB);
|>
End

(* Here, head is used to show that something has gone into an invalid state (suc as looping forever)*)
Datatype:
  system_state =
  <| monitor : nic_monitor_state
   ; nic     : nic_state
   ; dead    : bool
   |>
End

Definition dead_state_def:
  dead_state = (ARB : system_state) with dead := T
End

Definition return_dead_def:
  return_dead x = (λ s . (x , dead_state))
End

Definition DEAD_def:
  DEAD s = s.dead
End

(* the following are monadic style getters and settets of the state *)

Definition get_tx0_active_queue_def:
  get_tx0_active_queue (state : system_state) =
    (state.monitor.tx0_active_queue , state)
End
Definition set_tx0_active_queue_def:
  set_tx0_active_queue val (state : system_state)  =
    (() , state with monitor := state.monitor with tx0_active_queue := val)
End
Definition get_rx0_active_queue_def:
  get_rx0_active_queue (state : system_state) =
    (state.monitor.rx0_active_queue , state)
End
Definition set_rx0_active_queue_def:
  set_rx0_active_queue val (state : system_state)  =
    (() , state with monitor := state.monitor with  rx0_active_queue := val)
End
Definition get_initialized_def:
  get_initialized (state : system_state) = (state.monitor.initialized , state)
End
Definition set_initialized_def:
  set_initialized val (state : system_state)  =
    (() , state with monitor := state.monitor with  initialized := val)
End
Definition get_tx0_hdp_initialized_def:
  get_tx0_hdp_initialized (state : system_state) =
       (state.monitor.tx0_hdp_initialized , state)
End
Definition set_tx0_hdp_initialized_def:
  set_tx0_hdp_initialized val (state : system_state)  =
    (() , state with monitor := state.monitor with  tx0_hdp_initialized := val)
End
Definition get_rx0_hdp_initialized_def:
  get_rx0_hdp_initialized (state : system_state) =
       (state.monitor.rx0_hdp_initialized , state)
End
Definition set_rx0_hdp_initialized_def:
  set_rx0_hdp_initialized val (state : system_state)  =
    (() , state with monitor := state.monitor with  rx0_hdp_initialized := val)
End
Definition get_tx0_cp_initialized_def:
  get_tx0_cp_initialized (state : system_state) =
      (state.monitor.tx0_cp_initialized , state)
End
Definition set_tx0_cp_initialized_def:
  set_tx0_cp_initialized val (state : system_state)  =
    (() , state with monitor := state.monitor with  tx0_cp_initialized := val)
End
Definition get_rx0_cp_initialized_def:
  get_rx0_cp_initialized (state : system_state) =
      (state.monitor.rx0_cp_initialized , state)
End
Definition set_rx0_cp_initialized_def:
  set_rx0_cp_initialized val (state : system_state)  =
    (() , state with monitor := state.monitor with  rx0_cp_initialized := val)
End
Definition get_tx0_tearingdown_def:
  get_tx0_tearingdown (state : system_state) =
   (state.monitor.tx0_tearingdown , state)
End
Definition set_tx0_tearingdown_def:
  set_tx0_tearingdown val (state : system_state)  =
    (() , state with monitor := state.monitor with  tx0_tearingdown := val)
End
Definition get_rx0_tearingdown_def:
  get_rx0_tearingdown (state : system_state) =
   (state.monitor.rx0_tearingdown , state)
End
Definition set_rx0_tearingdown_def:
  set_rx0_tearingdown val (state : system_state)  =
    (() , state with monitor := state.monitor with  rx0_tearingdown := val)
End
Definition get_alpha_def:
  get_alpha (state : system_state) =
    (state.monitor.alpha , state)
End
Definition set_alpha_def:
  set_alpha val (state : system_state) =
    (() , state with monitor := state.monitor with  alpha := val)
End
Definition get_recv_bd_nr_blocks_def:
  get_recv_bd_nr_blocks val (state : system_state) =
    ( state.monitor.recv_bd_nr_blocks , state )
End
Definition set_recv_bd_nr_blocks_def:
  set_recv_bd_nr_blocks val (state : system_state) =
    (() , state with monitor := state.monitor with recv_bd_nr_blocks := val)
End

val _ = export_theory();
