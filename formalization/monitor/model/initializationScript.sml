(*
Author: Tomas Möre 2020

Model of the initialization of the nic

*)


open HolKernel wordsLib boolLib bossLib;
open monitor_stateTheory;
open register_readTheory register_writeTheory address_spaceTheory;

open handlersTheory state_transformer_extTheory;
open schedulerTheory;

val _ = new_theory "initialization";



Definition initialize_nic_def:
  initialize_nic =
  do write_nic_register CPDMA_SOFT_RESET_PA 1w;
     nic_it_transition;
     write_nic_register TX0_HDP_PA 0w;
     write_nic_register RX0_HDP_PA 0w;
     write_nic_register TX0_CP_PA 0w;
     write_nic_register RX0_CP_PA 0w;
     set_initialized T;
     set_tx0_hdp_initialized T;
     set_rx0_hdp_initialized T;
     set_tx0_cp_initialized T;
     set_rx0_cp_initialized T
  od
End


val _ = export_theory();
