open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;
open state_transformer_extTheory;

open helperTactics;

open initialization_performedTheory;
open monitorUtils;
open address_lemmasTheory;
open alpha_lemmasTheory;
open invariant_lemmasTheory;
open is_queue_securePropertiesTheory;


val _ = new_theory "tx0_hdp_handler_proof"

(*
The effect on the state after running tx0_hdp_handler when the nic hasn't been totaly initialized yet.
*)
Definition tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect:
  tx0_hdp_handler_non_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with tx0_hdp_initialized := T)
               ; nic := state.nic with
                <| regs := state.nic.regs with TX0_HDP := 0w;
                   it   := state.nic.it with
                             <|state := it_initialize_hdp_cp;
                             tx0_hdp_initialized := T|>;
                   tx   := state.nic.tx with
                             <|current_bd_pa := 0w; sop_bd_pa := 0w|>
                 |>
               |>)
End

(* For the case where *)
Theorem TX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ ¬ (state.monitor.rx0_hdp_initialized
       ∧ state.monitor.tx0_cp_initialized
       ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ tx0_hdp_handler 0w state =
  (T
  , tx0_hdp_handler_non_total_initialized_effect state
   )
Proof
  strip_tac
  >> strip_tac
  >> ‘¬ state.nic.dead’
      by (METIS_TAC [MON_INV_NIC_NOT_DEAD_lem])
  >> simp_st [  get_initialized_def , tx0_hdp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_tx0_hdp_def
             , address_lemmasTheory.TX0_HDP_PA_lemmas , set_tx0_hdp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]
  >> ‘state.nic.it.state = it_initialize_hdp_cp’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> ‘¬(state.nic.it.rx0_hdp_initialized
        ∧ state.nic.it.tx0_cp_initialized
        ∧ state.nic.it.rx0_cp_initialized)’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> asm_rewrite_tac [ tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
  >> simp []
  >> UNDISCH_TAC “¬(state.nic.it.rx0_hdp_initialized
                    ∧ state.nic.it.tx0_cp_initialized
                    ∧ state.nic.it.rx0_cp_initialized)”
  >> Cases_on ‘state.monitor.rx0_hdp_initialized’
  >> Cases_on ‘state.monitor.tx0_cp_initialized’
  >> Cases_on ‘state.monitor.rx0_cp_initialized’
  >> fs_st [ whenMM_def
             , get_rx0_hdp_initialized_def , get_rx0_hdp_initialized_def
             , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def
           ]
QED


Theorem TX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT:
  ∀ state .
    (MONITOR_INVARIANT state
     ∧ ¬ state.monitor.initialized
     ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w)
    ⇒ MONITOR_INVARIANT (tx0_hdp_handler_non_total_initialized_effect state)
Proof
  strip_tac >> strip_tac
  >> UNDISCH_TAC “MONITOR_INVARIANT state”
  >> rewrite_tac [MONITOR_INVARIANT_def
                  , tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect
                  , BD_QUEUE_INVARIANT_def]
  >> simp []
  >> rewrite_tac [GSYM  tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
  >> strip_tac
  >> ‘state.nic.regs.CPPI_RAM = (tx0_hdp_handler_non_total_initialized_effect state).nic.regs.CPPI_RAM’
      by (fs [ tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect])
  >> ‘state.nic.dead = (tx0_hdp_handler_non_total_initialized_effect state).nic.dead’
      by (fs [ tx0_hdp_handler_NON_TOTAL_INITIALIZED_effect])
  >> quantHeuristicsLib.QUANT_TAC [("tx0_queue", ‘tx0_queue’, [])
                                   ,("rx0_queue", ‘rx0_queue’, [])
                                  ]
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue
      (tx0_hdp_handler_non_total_initialized_effect state)’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue
      (tx0_hdp_handler_non_total_initialized_effect state)’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> asm_rewrite_tac []
  >> fs []
  >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
  >> EVAL_TAC
  >> fs []
QED

Definition tx0_hdp_handler_TOTAL_INITIALIZED_effect:
  tx0_hdp_handler_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with tx0_hdp_initialized := T)
               ; nic := state.nic with
                <| regs := state.nic.regs with TX0_HDP := 0w;
                   it   := state.nic.it with
                             <|state := it_initialized;
                             tx0_hdp_initialized := T|>;
                   tx   := state.nic.tx with
                             <|current_bd_pa := 0w; sop_bd_pa := 0w|>
                 |>
               |>)
End

Theorem TX0_HDP_HANDLER_TOTAL_INITIALIZED_lemma:
  ∀ state tx0_queue rx0_queue .
  MONITOR_INVARIANT state
  ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
  ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
  ∧ ¬ state.monitor.initialized
  ∧ (state.monitor.rx0_hdp_initialized
     ∧ state.monitor.tx0_cp_initialized
     ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ tx0_hdp_handler 0w state =
  (T
   , initailization_performed_effect tx0_queue rx0_queue (tx0_hdp_handler_total_initialized_effect state)
  )
Proof
  rpt strip_tac
  >> ‘¬state.nic.dead’
      by (fs [MON_INV_NIC_NOT_DEAD_lem])
  >> simp_st [  get_initialized_def , tx0_hdp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_tx0_hdp_def
             , address_lemmasTheory.TX0_HDP_PA_lemmas , set_tx0_hdp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]

  >> ‘state.nic.it.state = it_initialize_hdp_cp’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> ‘(state.nic.it.rx0_hdp_initialized
      ∧ state.nic.it.tx0_cp_initialized
      ∧ state.nic.it.rx0_cp_initialized)’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> Q.ABBREV_TAC ‘state' = (state with
              <|monitor := state.monitor with tx0_hdp_initialized := T;
                nic :=
                  state.nic with
                  <|regs := state.nic.regs with TX0_HDP := 0w;
                    it :=
                      state.nic.it with
                      <|state := it_initialized; tx0_hdp_initialized := T|>;
                    tx :=
                      state.nic.tx with
                      <|current_bd_pa := 0w; sop_bd_pa := 0w|> |> |>)’
  >> ‘(state.nic.dead ⇔ state'.nic.dead) ∧
         state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
         by (Q.UNABBREV_TAC ‘state'’  >> fs [ MONITOR_INVARIANT_def])
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘state.monitor.rx0_active_queue =  state'.monitor.rx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘state.monitor.tx0_active_queue =  state'.monitor.tx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘¬state'.nic.dead’
      by METIS_TAC []
  >> ‘initialization_performed state' =
             ((),
              initailization_performed_effect tx0_queue rx0_queue state')’
             by (fs [INITIALIZAITON_PERFORMED_EFFECT])
  >> fs []
  >> asm_rewrite_tac []
  >> Q.UNABBREV_TAC ‘state'’
  >> fs [initailization_performed_effect_def
        , tx0_hdp_handler_TOTAL_INITIALIZED_effect]
QED

(*

Proof that of the initialization case of the tx0_hdp_handler call

*)
Theorem TX0_HDP_HANDLER_INITIALIZATION_PRESERVES_INV_lem:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ (¬ state.monitor.initialized)
  ⇒ MONITOR_INVARIANT (SND (tx0_hdp_handler 0w state))
Proof
  strip_tac >> strip_tac
  >> ‘¬state.nic.dead’
      by METIS_TAC [MON_INV_NIC_NOT_DEAD_lem]
  >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET ≠ 0w’
  >- ( rewrite_tac [tx0_hdp_handler_def]
       >> simp_st [ get_initialized_def, read_nic_register_def
                    , register_readTheory.read_register_def
                 , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas]
       >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET’
       >> fs []
       >> Cases_on ‘n’
       >> fs []
       >> Cases_on ‘n'’
       >> fs []
       >> ‘state with nic := state.nic = state’
           by fs [monitor_stateTheory.system_state_component_equality]
       >> asm_rewrite_tac []
     )
  >> fs []
  >> Cases_on ‘(state.monitor.rx0_hdp_initialized
                ∧ state.monitor.tx0_cp_initialized
                ∧ state.monitor.rx0_cp_initialized)’
  >- ( ‘∃ tx0_queue . WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state’
       by METIS_TAC [MON_INV_HAS_TX0_QUEUE_lem]
       >> ‘∃ rx0_queue . WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state’
           by METIS_TAC [MON_INV_HAS_RX0_QUEUE_lem]
       >> ‘tx0_hdp_handler 0w state =
           (T
            , initailization_performed_effect
                tx0_queue rx0_queue
                (tx0_hdp_handler_total_initialized_effect state))’
           by (METIS_TAC [MONITOR_INVARIANT_def
                          , TX0_HDP_HANDLER_TOTAL_INITIALIZED_lemma
                         ]
              )
       >> asm_rewrite_tac [ initailization_performed_effect_def
                            , tx0_hdp_handler_TOTAL_INITIALIZED_effect]
       >> fs []
       >> Q.ABBREV_TAC ‘state' = (state with
           <|monitor :=
               state.monitor with
               <|tx0_active_queue := 0w; rx0_active_queue := 0w;
                 initialized := T; tx0_hdp_initialized := T;
                 alpha :=
                   FOLDR
                     (λbd_ptr' state'.
                          update_alpha_effect bd_ptr' REMOVE state')
                     state.monitor.alpha (tx0_queue ⧺ rx0_queue)|>;
             nic :=
               state.nic with
               <|regs := state.nic.regs with TX0_HDP := 0w;
                 it :=
                   state.nic.it with
                   <|state := it_initialized; tx0_hdp_initialized := T|>;
                 tx :=
                 state.nic.tx with <|current_bd_pa := 0w; sop_bd_pa := 0w|> |> |>)’
       >> ‘BD_QUEUE_INVARIANT state'’
           by ( rewrite_tac [BD_QUEUE_INVARIANT_def]
                >> Q.EXISTS_TAC ‘[]’
                >> Q.EXISTS_TAC ‘[]’
                >> ‘state'.monitor.tx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> ‘state'.monitor.rx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> simp [WELL_FORMED_BD_QUEUE_def
                         , bd_queueTheory.BD_QUEUE_def
                         , NON_OVERLAPPING_BD_QUEUES_def
                        ]
                >> ‘BD_QUEUE_ALPHA_MARKED [] [] state'.monitor.alpha’
                    by (Q.UNABBREV_TAC ‘state'’
                        >> simp []

                        >> METIS_TAC [BD_QUEUE_INVARIANT_UNPACK
                                      ,UPDATE_ALPHA_REMOVE_ALL_NOTHING_ALPHA_MARKED]
                       )
                >> simp []
                >> ‘state.nic.rx.sop_bd_pa = 0w ∧ state.nic.rx.current_bd_pa = 0w’
                    by (METIS_TAC [MONITOR_INVARIANT_def])
                >> ‘state'.nic.tx.sop_bd_pa = 0w
                    ∧ state'.nic.rx.sop_bd_pa = 0w
                    ∧ state'.nic.rx.current_bd_pa = 0w ’
                    by (Q.UNABBREV_TAC ‘state'’>> fs[])
                >> METIS_TAC []
              )
       >> asm_rewrite_tac [MONITOR_INVARIANT_def]
       >> asm_rewrite_tac []
       >> Q.UNABBREV_TAC ‘state'’
       >> fs [MONITOR_INVARIANT_def]
       >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
       >> fs [nicInvariantTheory.NIC_INVARIANT_def]
       >> EVAL_TAC >> fs [] >> blastLib.BBLAST_PROVE_TAC
     )
  >> ‘tx0_hdp_handler 0w state =
      (T,tx0_hdp_handler_non_total_initialized_effect state)’
      by fs [ TX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma]
  >> asm_rewrite_tac []
  >> METIS_TAC [TX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT]
QED


Definition tx0_hdp_handler_append_empty_effect_def:
  tx0_hdp_handler_append_empty_effect bd_ptr queue state =
     (state with
           <|monitor :=
               state.monitor with
               <|tx0_active_queue := bd_ptr;
                 alpha :=
                   FOLDR
                     (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state')
                     state.monitor.alpha queue|>
                ; nic := (state.nic with
           <|regs :=
               state.nic.regs with
               <|TX0_HDP :=
                   if ARB.reg_write.tx0_hdp_value = 0w then 1w
                   else ARB.reg_write.tx0_hdp_value;
                 CPPI_RAM :=
                   set_and_clear_word_on_sop_or_eop_foldr_effect queue
                     (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                        (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                           state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD)
                        FLAGS 0w EOQ EOP_BD) FLAGS 0w TD SOP_BD|>;
             tx :=
               state.nic.tx with
               <|state :=
                   if state.nic.tx.state = tx_idle then tx_fetch_next_bd
                   else state.nic.tx.state; current_bd_pa := bd_ptr;
                   expects_sop := T; sop_bd_pa := bd_ptr|> |>)|>)
End


(*

Because the previous proofs (and invariant) made a simplification of the queue
where no multi BD frames are allowed we must constrain the input to something similar in order to be capable of prooving the invaraint. This is characterized by the forall term in the assumption list, that is, if input pointer is the start of a bd queue, then every bd has its sop and eop set.

This added assumption could make some other proofs easier to make, but we here take care no ensure we do not allow it to do so.


*)

Theorem TX_HDP_HANDLER_INITIALIZED_EFFECT:
  ∀ bd_ptr state .
  MONITOR_INVARIANT state
  ∧ state.monitor.initialized
  ⇒ ∃ b queue . (
  (tx0_hdp_handler bd_ptr state)
  = (b , if b
         then (if (state.monitor.tx0_active_queue = 0w)
               then (state with
                    <|monitor :=
                        state.monitor with
                        <|tx0_active_queue := bd_ptr;
                          alpha :=
                            FOLDR
                              (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state')
                              state.monitor.alpha queue|>
                     ; nic :=
                        if bd_ptr ≠ 0w then
                          state.nic with
                          <|regs :=
                              state.nic.regs with
                              <|TX0_HDP :=
                                  if ARB.reg_write.tx0_hdp_value = 0w then 1w
                                  else ARB.reg_write.tx0_hdp_value;
                                CPPI_RAM :=
                                  set_and_clear_all_effect state.nic.regs.CPPI_RAM queue|>;
                            tx :=
                              state.nic.tx with
                              <|state :=
                                  if state.nic.tx.state = tx_idle then tx_fetch_next_bd
                                  else state.nic.tx.state; current_bd_pa := bd_ptr;
                                expects_sop := T; sop_bd_pa := bd_ptr|> |>
                        else
                          state.nic with
                          regs :=
                            state.nic.regs with
                            CPPI_RAM :=
                              set_and_clear_all_effect state.nic.regs.CPPI_RAM queue|>)
               else ARB )
         else (if ((state.nic.regs.TX0_HDP ≠ 0w ∨ state.monitor.tx0_tearingdown)
                   ∨ ¬ FST (is_queue_secure bd_ptr T state)
                  )
               then state
               else ARB
              )
    )
  ∧ (if b
     then (¬(state.nic.regs.TX0_HDP ≠ 0w ∨ state.monitor.tx0_tearingdown)
              ∧ FST (is_queue_secure bd_ptr TRANSMIT state)
              ∧ (BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
          )
     else (state.nic.regs.TX0_HDP ≠ 0w ∨ state.monitor.tx0_tearingdown)
          ∨  ¬ FST (is_queue_secure bd_ptr T state))
)
Proof
  rpt strip_tac
  >> ‘¬ state.nic.dead ’
      by METIS_TAC [MON_INV_NIC_NOT_DEAD_lem]
  >> simp_st [  get_initialized_def , tx0_hdp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_tx0_hdp_def
             , address_lemmasTheory.TX0_HDP_PA_lemmas , set_tx0_hdp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def
            , get_tx0_tearingdown_def, get_tx0_active_queue_def]
  >> ‘state with nic := state.nic = state’
      by simp [system_state_component_equality]
  >> fs_st []
  >> Cases_on ‘state.nic.regs.TX0_HDP ≠ 0w ∨ state.monitor.tx0_tearingdown’
  >- (simp_st [])
  >> fs_st []
  >> ‘∃tx0_queue rx0_queue.
      WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue
      state ∧
      WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue
      state ∧ NON_OVERLAPPING_BD_QUEUES tx0_queue rx0_queue ∧
      BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha’
      by METIS_TAC [MONITOR_INVARIANT_def, BD_QUEUE_INVARIANT_def]
  >> Cases_on ‘state.monitor.tx0_active_queue = 0w’
  >- ( simp_st []
       >> ‘∃ b queue .
            ((is_queue_secure bd_ptr T state
             = (b , if ¬ b
                    then state
                    else is_queue_secure_effect queue state
               ))
             ∧ (b ⇒ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
            )’
           by ( ‘TRANSMIT = T’ by simp [TRANSMIT_def]
                >> specl_assume_tac
                   [‘bd_ptr’, ‘TRANSMIT’, ‘state’]
                   IS_QUEUE_SECURE_EFFECT
                >> fs [TRANSMIT_def]
                >> METIS_TAC []
              )
       >> asm_rewrite_tac []
       >> Cases_on ‘¬ b’
       >- simp_st [TRANSMIT_def]
       >> simp_st [TRANSMIT_def]
       >> ‘b’ by fs []
       >> ‘BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM’
           by fs []
       >> simp_st [set_tx0_active_queue_def, is_queue_secure_effect_def]
       >> ‘state.nic.tx.sop_bd_pa = 0w’
           by ( fs [MONITOR_INVARIANT_def, BD_QUEUE_INVARIANT_def]
                >> METIS_TAC [WELL_FORMED_ZERO_START_QUEUE_IS_ZERO, MEM]
              )
       >> pat_x_rewrite_all ‘state.monitor.tx0_active_queue = 0w’
       >> ‘tx0_queue = []’
           by ( METIS_TAC [WELL_FORMED_ZERO_START_QUEUE_IS_ZERO])
       >> pat_x_rewrite_all ‘tx0_queue = []’
       >> ‘FST (is_queue_secure bd_ptr T state)’
           by fs []
       >> ‘∃ queue .( WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)
                    ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)’
           by ( METIS_TAC [IS_QUEUE_SECURE_CONSEQ] )
       >> ‘queue' = queue’
           by METIS_TAC [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
       >> pat_x_rewrite_all ‘_=_’
       >> Q.ABBREV_TAC ‘state' = (state with
                 <|monitor := state.monitor with tx0_active_queue := bd_ptr;
                   nic :=
                     state.nic with
                     regs :=
                       state.nic.regs with
                       CPPI_RAM :=
                         set_and_clear_all_effect state.nic.regs.CPPI_RAM
                         queue|>)’
       >> ‘WELL_FORMED_BD_QUEUE queue bd_ptr state'’
           by (Q.PAT_UNDISCH_TAC ‘WELL_FORMED_BD_QUEUE _ _ _’
               >> simp [is_queue_secure_effect_def]
               >> Q.UNABBREV_TAC ‘state'’
               >>  fs [WELL_FORMED_BD_QUEUE_MONITOR_INDEPENDANT]
              )
       >> ‘(update_alpha_queue bd_ptr ADD state')
           = ((),
          state' with
          monitor :=
            state'.monitor with
            alpha :=
              FOLDR
                (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state')
                state'.monitor.alpha queue)’
           by ( ‘¬ state'.nic.dead’
                by (Q.UNABBREV_TAC ‘state'’ >> fs [])
                >> METIS_TAC [UPDATE_ALPHA_QUEUE_EFFECT]
              )
       >> pat_x_rewrite_all ‘_=_’
       >> Q.UNABBREV_TAC ‘state'’
       >> simp_st []
       >> ‘state.nic.it.state = it_initialized’
           by (UNDISCH_TAC “MONITOR_INVARIANT state”
               >> rewrite_tac [MONITOR_INVARIANT_def]
               >> METIS_TAC [])
       >> simp_st []
       >> ‘state.nic.td.state = td_idle’
           by (UNDISCH_TAC “MONITOR_INVARIANT state”
               >> rewrite_tac [MONITOR_INVARIANT_def]
               >> METIS_TAC [])
       >> simp []
       >> Q.EXISTS_TAC ‘queue’
       >> simp []
     )
  >> simp_st []
  >> cheat (* to be expanded *)
QED

Theorem TX0_HDP_HANDLER_WRITE_PRESERVES_INV_lem:
  ∀ bd_ptr state .
  MONITOR_INVARIANT state
  ∧ state.monitor.initialized
  ∧ (∀ queue . BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
     ⇒ TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM)
  ⇒ MONITOR_INVARIANT (SND (tx0_hdp_handler bd_ptr state))
Proof
  rpt strip_tac
  >> specl_assume_tac
     [‘bd_ptr’, ‘state’]
     TX_HDP_HANDLER_INITIALIZED_EFFECT
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> pat_x_rewrite_all ‘_=_’
  >> Cases_on ‘¬b’
  >- (fs [] )
  >> fs [TRANSMIT_def]
  >> ‘¬ state.nic.dead’
      by METIS_TAC [MON_INV_NIC_NOT_DEAD_lem]
  >> ‘∃ queue . ((BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
             ∧ EVERY BD_PTR_ADDRESS queue
             ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
             ∧ EVERY (λp. ¬(get_alpha_bit p state.monitor.alpha
                            ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                            ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                            ∨ get_alpha_bit (p + 12w) state.monitor.alpha))
                      queue
              ∧ EVERY (λp. (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)).bl <₊ 2048w) queue
              ∧ WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)
              ∧ LENGTH queue <= MAX_QUEUE_LENGTH_NUM
                )’
      by METIS_TAC [IS_QUEUE_SECURE_CONSEQ]
  >> ‘queue' = queue’
      by (METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma])
  >> pat_x_rewrite_all ‘_=_’
  >> ‘∃tx0_queue rx0_queue.
      WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue
      state ∧
      WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue
      state ∧ NON_OVERLAPPING_BD_QUEUES tx0_queue rx0_queue ∧
      BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha’
      by METIS_TAC [MONITOR_INVARIANT_def, BD_QUEUE_INVARIANT_def]
  >> ‘state.nic.it.state = it_initialized’
      by (simp [] >> METIS_TAC [MONITOR_INVARIANT_def] )
  >> ‘state.nic.td.state = td_idle’
      by (fs [] >> METIS_TAC [MONITOR_INVARIANT_def])
  >> ‘EVERY BD_PTR_ADDRESS queue ∧ EVERY BD_PTR_ADDRESS rx0_queue’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_def]
  >> Cases_on ‘state.monitor.tx0_active_queue = 0w’
  >- ( simp []
       >> ‘state.nic.tx.sop_bd_pa = 0w’
           by ( fs [MONITOR_INVARIANT_def, BD_QUEUE_INVARIANT_def]
                >> METIS_TAC [WELL_FORMED_ZERO_START_QUEUE_IS_ZERO, MEM]
              )
       >> ‘tx0_queue = []’
           by ( METIS_TAC [WELL_FORMED_ZERO_START_QUEUE_IS_ZERO])
       >> pat_x_rewrite_all ‘tx0_queue = []’
       >> ‘∃ queue .
           ( BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
             ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue (is_queue_secure_effect queue state)
             ∧ NON_OVERLAPPING_BD_QUEUES queue rx0_queue
             ∧ (∀p1 p2 . MEM p1 queue ∧ MEM p2 rx0_queue ⇒ word_abs (p1 − p2) ≥ 16w))’
           by METIS_TAC [IS_QUEUE_SECURE_OTHER_WELL_FORMED_BD_QUEUE]
       >> ‘queue' = queue’
           by METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
       >> pat_x_rewrite_all ‘_=_’
       >> Cases_on ‘bd_ptr = 0w’
       >- ( ‘queue = []’
            by ( Cases_on ‘queue’
                 >- fs []
                 >> METIS_TAC [WELL_FORMED_BD_QUEUE_def
                               , bd_queueTheory.BD_QUEUE_def]
               )
            >> simp [set_and_clear_all_effect_def]
            >> ‘(state with
           <|monitor :=
               state.monitor with
               <|tx0_active_queue := 0w; alpha := state.monitor.alpha|>;
             nic :=
               state.nic with
               regs :=
               state.nic.regs with CPPI_RAM := state.nic.regs.CPPI_RAM|>)
                = state’
                by simp [system_state_component_equality
                        , nic_monitor_state_component_equality
                        , nic_state_component_equality
                        , nic_regs_component_equality]
            >> simp []
          )
       >> simp []
       >> Q.ABBREV_TAC ‘final_nic =
             state.nic with
               <|regs :=
                   state.nic.regs with
                   <|TX0_HDP :=
                       if ARB.reg_write.tx0_hdp_value = 0w then 1w
                       else ARB.reg_write.tx0_hdp_value;
                     CPPI_RAM :=
                       set_and_clear_all_effect state.nic.regs.CPPI_RAM queue|>;
                 tx :=
                   state.nic.tx with
                   <|state :=
                       if state.nic.tx.state = tx_idle then tx_fetch_next_bd
                       else state.nic.tx.state; current_bd_pa := bd_ptr;
                     expects_sop := T; sop_bd_pa := bd_ptr|> |>’
       >> Q.ABBREV_TAC ‘final_state =
             (state with
               <|monitor :=
                   state.monitor with
                   <|tx0_active_queue := bd_ptr;
                     alpha :=
                       FOLDR
                         (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state')
                         state.monitor.alpha queue|>; nic := final_nic|>)
                       ’
       >> ‘BD_QUEUE_ALPHA_MARKED queue rx0_queue final_state.monitor.alpha’
           by (Q.UNABBREV_TAC ‘final_state’
               >> fs []
               >> METIS_TAC [IS_QUEUE_SECURE_OTHER_QUEUE_RETAINS_BD_QUEUE_ALPHA_MARKED]
              )
       >> ‘WELL_FORMED_BD_QUEUE queue bd_ptr final_state’
           by ( UNDISCH_TAC “WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)”
                >> simp [WELL_FORMED_BD_QUEUE_def, is_queue_secure_effect_def]
                >> Q.UNABBREV_TAC ‘final_state’
                >> Q.UNABBREV_TAC ‘final_nic’
                >> fs [read_utilsTheory.IS_EOP]
                >> simp [EVERY_MEM]
                >> ntac 3 strip_tac
                >> ‘BD_PTR_ADDRESS p’
                    by METIS_TAC [EVERY_MEM]
                >> simp [read_utilsTheory.IS_EOP]
              )
       >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue final_state’
           by ( UNDISCH_TAC “WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue (is_queue_secure_effect queue state)”
                >> simp [WELL_FORMED_BD_QUEUE_def, is_queue_secure_effect_def]
                >> Q.UNABBREV_TAC ‘final_state’
                >> Q.UNABBREV_TAC ‘final_nic’
                >> fs [read_utilsTheory.IS_EOP]
                >> simp [EVERY_MEM]
                >> ntac 3 strip_tac
                >> ‘BD_PTR_ADDRESS p’
                    by METIS_TAC [EVERY_MEM]
                >> simp [read_utilsTheory.IS_EOP]
              )
       >> ‘BD_QUEUE_INVARIANT final_state’
           by ( simp [BD_QUEUE_INVARIANT_def]
                >> Q.EXISTS_TAC ‘queue’
                >> Q.EXISTS_TAC ‘rx0_queue’
                >> Q.UNABBREV_TAC ‘final_state’
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
                >> ‘MEM bd_ptr queue’
                    by ( METIS_TAC [WELL_FORMED_BD_START_MEM] )
                >> simp []
                >> fs [BD_QUEUE_INVARIANT_def]
                >> Q.PAT_UNDISCH_TAC ‘MONITOR_INVARIANT state’
                >> simp [MONITOR_INVARIANT_def, BD_QUEUE_INVARIANT_def]
                >> strip_tac
                >> METIS_TAC [WELL_FORMED_BD_QUEUE_UNIQUE]
              )
       >> RW_ASM_TAC “MONITOR_INVARIANT state” MONITOR_INVARIANT_def
       >> rewrite_tac [MONITOR_INVARIANT_def]
       >> simp []
       >> Q.UNABBREV_TAC ‘final_state’
       >> Q.UNABBREV_TAC ‘final_nic’
       >> simp []
       >> Q.ABBREV_TAC ‘final_nic =
             (state.nic with
           <|regs :=
               state.nic.regs with
               <|TX0_HDP :=
                   if ARB.reg_write.tx0_hdp_value = 0w then 1w
                   else ARB.reg_write.tx0_hdp_value;
                 CPPI_RAM :=
                   set_and_clear_all_effect state.nic.regs.CPPI_RAM queue|>;
             tx :=
               state.nic.tx with
               <|state :=
                   if state.nic.tx.state = tx_idle then tx_fetch_next_bd
                   else state.nic.tx.state; current_bd_pa := bd_ptr;
                 expects_sop := T; sop_bd_pa := bd_ptr|> |>)
               ’
       >> rewrite_tac [nicInvariantTheory.NIC_INVARIANT_def]
       >> SPLIT_ASM_TAC
       >> RW_ASM_TAC “NIC_INVARIANT state.nic ARB ARB” nicInvariantTheory.NIC_INVARIANT_def
       >> SPLIT_ASM_TAC
       >> ‘NIC_STATE_DEFINED final_nic’
           by (rewrite_tac [EVAL “NIC_STATE_DEFINED final_nic”]
               >> Q.UNABBREV_TAC ‘final_nic’
               >> simp []
              )
       >> asm_rewrite_tac []
       >> ‘tx_bd_queue final_nic = queue’
           by ( ‘BD_QUEUE queue bd_ptr
                (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)’
                by fs [WELL_FORMED_BD_QUEUE_def, is_queue_secure_effect_def]
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp [tx_bd_queueTheory.tx_bd_queue_def]
                >> METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
              )
       >> ‘∃ rx0_queue_tail .
           BD_QUEUE rx0_queue_tail state.nic.rx.sop_bd_pa
           (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
           ∧ (∀ p . MEM p rx0_queue_tail ⇒ MEM p rx0_queue)
           ∧ EVERY BD_PTR_ADDRESS rx0_queue_tail’
           by ( ‘state.nic.rx.sop_bd_pa = 0w ∨ MEM state.nic.rx.sop_bd_pa  rx0_queue’
                by METIS_TAC [BD_QUEUE_INVARIANT_def
                              , WELL_FORMED_BD_QUEUE_UNIQUE]
                >- (Q.EXISTS_TAC ‘[]’
                    >> Q.UNABBREV_TAC ‘final_nic’
                    >> fs [bd_queueTheory.BD_QUEUE_def]
                   )
                >> ‘∃queue'.
                      WELL_FORMED_BD_QUEUE queue' state.nic.rx.sop_bd_pa (is_queue_secure_effect queue state)
                    ∧ (∀ p . MEM p queue' ⇒ MEM p rx0_queue)’
                    by (METIS_TAC [WELL_FORMED_BD_QUEUE_MEM])
                >> Q.EXISTS_TAC ‘queue'’
                >> fs [is_queue_secure_effect_def, WELL_FORMED_BD_QUEUE_def]
              )
       >> ‘rx_bd_queue final_nic = rx0_queue_tail’
           by ( Q.UNABBREV_TAC ‘final_nic’
                >> fs [rx_bd_queueTheory.rx_bd_queue_def]
                >> METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
              )
       >> ‘∀p1 p2. MEM p1 queue ∧ MEM p2 rx0_queue_tail ⇒ word_abs (p1 − p2) ≥ 16w’
           by (rpt strip_tac >> fs [])
       >> ‘NON_OVERLAPPING_BD_QUEUES queue rx0_queue_tail’
           by ( fs [NON_OVERLAPPING_BD_QUEUES_def]
                >> rpt strip_tac
                >> fs []
              )
       >> ‘BD_QUEUE rx0_queue_tail state.nic.rx.sop_bd_pa state.nic.regs.CPPI_RAM’
           by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE
                           , MEM, word_abs_diff]
              )
       >> ‘QD_INVARIANT final_nic’
           by ( asm_rewrite_tac [qdInvariantTheory.QD_INVARIANT_def]
                >> strip_tac
                >> rewrite_tac [bd_queueTheory.BD_QUEUEs_DISJOINT_def]
                >> METIS_TAC [NON_OVERLAPPING_BD_QUEUES_IMPL_NO_BD_LIST_OVERLAP]
              )
       >> ‘IT_INVARIANT final_nic’
           by ( rewrite_tac [EVAL “IT_INVARIANT final_nic”]
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
              )
       >> asm_rewrite_tac []
       >> ‘TX_INVARIANT final_nic ARB’
           by (simp [txInvariantTheory.TX_INVARIANT_def]
                >> strip_tac
                >> simp [txInvariantTheory.TX_INVARIANT_MEMORY_def
                         , txInvariantWellDefinedTheory.TX_INVARIANT_WELL_DEFINED_def
                         , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_def
                        ]
                >> ‘TX_INVARIANT_NOT_DEAD final_nic’
                    by (Q.UNABBREV_TAC ‘final_nic’
                        >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_NOT_DEAD_def])
                >> ‘TX_INVARIANT_BD_QUEUE_FINITE final_nic’
                    by (Q.UNABBREV_TAC ‘final_nic’
                        >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_FINITE_def]
                        >> Q.EXISTS_TAC ‘queue’
                        >> fs [WELL_FORMED_BD_QUEUE_def]
                       )
                >> ‘TX_INVARIANT_BD_QUEUE_NO_OVERLAP queue’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_NO_OVERLAP_def]
                         >> fs  [NON_OVERLAP_IMPL_NON_BD_LIST_OVERLAP]
                       )
                >> ‘TX_INVARIANT_BD_QUEUE_SOP_EOP_MATCH queue final_nic.regs.CPPI_RAM’
                    by ( ‘TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM’
                         by METIS_TAC []
                         >> specl_assume_tac
                            [‘bd_ptr’, ‘queue’, ‘state’]
                            IS_QUEUE_SECURE_EFFECT_GIVES_TX_LINUX_BD_QUEUE_SOP_EOP_MATCH
                         >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                         >> ‘BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state).nic.regs.CPPI_RAM’
                             by ( fs [WELL_FORMED_BD_QUEUE_def
                                      , is_queue_secure_effect_def]
                                )
                         >> asm_rewrite_tac []
                         >> ‘(∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
                              word_abs (p1 − p2) ≥ 16w)’
                             by (fs [] >> METIS_TAC [])
                         >> asm_rewrite_tac []
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp [is_queue_secure_effect_def
                                  , txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_SOP_EOP_MATCH_def
                                  ]
                       )
                >> ‘TX_INVARIANT_TX_STATE_NOT_BD_QUEUE_EMPTY final_nic’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_TX_STATE_NOT_BD_QUEUE_EMPTY_def
                               , txInvariantWellDefinedTheory.NOT_TX_BD_QUEUE_EMPTY_def
                               ]
                         >> Cases_on ‘queue’
                         >- fs [bd_queueTheory.BD_QUEUE_def]
                         >> simp []
                       )
                >> ‘TX_INVARIANT_CURRENT_BD_PA_EQ_SOP_BD_PA final_nic’
                    by ( Q.UNABBREV_TAC ‘final_nic’
                         >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_CURRENT_BD_PA_EQ_SOP_BD_PA_def]
                       )
                >> ‘TX_INVARIANT_BD_QUEUE_LOCATION_DEFINED queue’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_LOCATION_DEFINED_def
                               , EVERY_MEM]
                         >> rpt strip_tac
                         >> ‘BD_PTR_ADDRESS e’
                             by METIS_TAC [EVERY_MEM]
                         >> UNDISCH_TAC “BD_PTR_ADDRESS e”
                         >> EVAL_TAC
                         >> blastLib.BBLAST_PROVE_TAC
                       )
                >> ‘TX_INVARIANT_EXPECTS_SOP_EQ_CURRENT_BD_PA_SOP_STATE final_nic’
                    by ( Q.UNABBREV_TAC ‘final_nic’
                         >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_EXPECTS_SOP_EQ_CURRENT_BD_PA_SOP_STATE_def
                                  , txTheory.TX_EXPECTS_SOP_EQ_CURRENT_BD_SOP_def
                                  , CPPI_RAMTheory.tx_read_bd_def]
                         >> simp [read_utilsTheory.READ_BD_WORD_IDX_3_EQ_READ_BD_WORD_ADD12]
                         >> ‘TX_LINUX_BD_SOP_EOP bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)’
                             by (Q.PAT_UNDISCH_TAC ‘TX_INVARIANT_BD_QUEUE_SOP_EOP_MATCH queue _’
                                 >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_SOP_EOP_MATCH_def
                                          , txInvariantWellDefinedTheory.TX_LINUX_BD_QUEUE_SOP_EOP_MATCH_def
                                          , EVERY_MEM
                                         ]
                                 >> strip_tac
                                 >> ‘MEM bd_ptr queue’
                                     by (Cases_on ‘queue’
                                         >> fs [bd_queueTheory.BD_QUEUE_def
                                                , WELL_FORMED_BD_QUEUE_def
                                               ]
                                        )
                                 >> METIS_TAC []
                                )
                         >> strip_tac
                         >> UNDISCH_TAC “TX_LINUX_BD_SOP_EOP bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)”
                         >> simp [txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_EOP_def
                                  , txInvariantWellDefinedTheory.TX_LINUX_BD_EOP_def
                                  , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_def
                                  , CPPI_RAMTheory.tx_read_bd_def
                                 ]
                         >> simp [read_utilsTheory.READ_BD_WORD_IDX_3_EQ_READ_BD_WORD_ADD12]
                       )
                >> ‘TX_INVARIANT_SOP_EOP_BD_QUEUE_NO_BUFFER_LENGTH_OVERFLOW queue final_nic.regs.CPPI_RAM’
                    by (  simp [txInvariantWellDefinedTheory.TX_INVARIANT_SOP_EOP_BD_QUEUE_NO_BUFFER_LENGTH_OVERFLOW_def
                               ]
                          >> Q.UNABBREV_TAC ‘final_nic’
                          >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_SOP_EOP_BD_NO_BUFFER_LENGTH_OVERFLOW_def]
                       )
                >> asm_rewrite_tac []
                >> ‘FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop MAX_QUEUE_LENGTH_NUM bd_ptr state)’
                    by ( specl_assume_tac
                         [‘bd_ptr’, ‘T’, ‘state’]
                         IS_QUEUE_SECURE_EFFECT
                         >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                         >> simp []
                         >> strip_tac
                         >> ‘b'’ by fs []
                         >> fs [is_transmit_SOP_EOP_packet_length_fields_set_correctly_def]
                       )
                >> ‘TX_INVARIANT_SOP_EOP_BD_QUEUE_CONSISTENT_SUM_BUFFER_LENGTH queue final_nic.regs.CPPI_RAM’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_SOP_EOP_BD_QUEUE_CONSISTENT_SUM_BUFFER_LENGTH_def
                               , txInvariantWellDefinedTheory.TX_INVARIANT_SOP_EOP_BD_CONSISTENT_SUM_BUFFER_LENGTH_def
                              ]
                         >> specl_assume_tac
                            [‘bd_ptr’
                             , ‘state’
                             , ‘MAX_QUEUE_LENGTH_NUM’
                             , ‘queue’
                            ]
                            ITSEPLFSCOL_CHEAT_EFFECT
                         >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                         >> simp [EVERY_MEM]
                         >> ntac 3 strip_tac
                         >> ‘(tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bl =
                             (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).pl’
                             by METIS_TAC []
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> ‘BD_PTR_ADDRESS bd_pa’
                             by METIS_TAC [EVERY_MEM]
                         >> ‘ALL_DISTINCT queue’
                             by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
                         >>  simp [SET_AND_CLEAR_ALL_MEM_TX_READ_BD_EFFECT]
                       )
                >> asm_rewrite_tac []
                >> simp [txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_CONFIGURATION_WELL_DEFINED_STATE_def
                         , txInvariantWellDefinedTheory.TX_INVARIANT_CURRENT_BD_STATE_def
                        ]
                >> ‘EVERY (λbd_pa. ¬TX_SOP_WITH_NOT_OWNER_SET (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ ¬TX_SOP_BO_GEQ_BL (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ TX_BL_GT_ZERO (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ ¬TX_EOP_WITH_EOQ_SET (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ TX_BUFFER_START_END_IN_RAM (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ ¬TX_BUFFER_WRAP_OVERFLOW (tx_read_bd bd_pa final_nic.regs.CPPI_RAM)
                           ∧ ¬TX_BD_LAST_NOT_EOP (tx_read_bd bd_pa final_nic.regs.CPPI_RAM))
                    queue’
                    by ( simp [EVERY_MEM]
                         >> ntac 2 strip_tac
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> ‘ALL_DISTINCT queue’
                             by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
                         >> ‘BD_PTR_ADDRESS bd_pa’
                             by METIS_TAC [EVERY_MEM]
                         >> simp [SET_AND_CLEAR_ALL_MEM_TX_READ_BD_EFFECT]
                         >> simp [txTheory.TX_SOP_WITH_NOT_OWNER_SET_def
                                  , txTheory.TX_SOP_BO_GEQ_BL_def
                                  , txTheory.TX_BL_GT_ZERO_def
                                  , txTheory.TX_EOP_WITH_EOQ_SET_def
                                  , txTheory.TX_BUFFER_START_END_IN_RAM_def
                                  , txTheory.TX_BUFFER_WRAP_OVERFLOW_def
                                  , txTheory.TX_BD_LAST_NOT_EOP_def
                                  , CPPI_RAMTheory.LAST_BD_def
                                 ]
                         >> ‘(tx_read_bd bd_pa state.nic.regs.CPPI_RAM).sop
                             ∧ (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).eop’
                             by (‘TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM’
                                 by METIS_TAC []
                                 >> Q.PAT_UNDISCH_TAC ‘TX_LINUX_BD_QUEUE_SOP_EOP_MATCH _ _’
                                 >> rewrite_tac [txInvariantWellDefinedTheory.TX_LINUX_BD_QUEUE_SOP_EOP_MATCH_def
                                          , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_EOP_def
                                          , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_def
                                          , txInvariantWellDefinedTheory.TX_LINUX_BD_EOP_def]
                                 >> simp [EVERY_MEM]
                                )
                         >> simp []
                         >> specl_assume_tac
                            [‘bd_ptr’, ‘queue’, ‘state’]
                            IS_QUEUE_SECURE_IS_DATA_BUFFER_SECURE_QUEUE_TX_EFFECT
                         >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                         >> asm_rewrite_tac []
                         >> strip_tac
                         >> ‘(tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bl >₊ 0w ∧
                             (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bp ≤₊
                             4294967295w − (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bo −
                             (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bl + 1w’
                             by ( Q.PAT_UNDISCH_TAC ‘EVERY _ _’
                                  >> rewrite_tac [EVERY_MEM]
                                  >> quantHeuristicsLib.QUANT_TAC [("e", ‘bd_pa’, [])]
                                  >> asm_rewrite_tac []
                                  >> simp []
                                )
                         >> simp []
                         >> ‘(tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bl ≤₊ 0xFFFFw’
                             by (simp [CPPI_RAMTheory.tx_read_bd_def]
                                 >> Q.ABBREV_TAC ‘x = (read_bd_word bd_pa 2w state.nic.regs.CPPI_RAM)’
                                 >> blastLib.BBLAST_PROVE_TAC)
                         >> ‘(tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bo ≤₊ 0xFFFFw’
                             by (simp [CPPI_RAMTheory.tx_read_bd_def]
                                 >> Q.ABBREV_TAC ‘x = (read_bd_word bd_pa 2w state.nic.regs.CPPI_RAM)’
                                 >> blastLib.BBLAST_PROVE_TAC)
                         >> Q.ABBREV_TAC ‘bl = (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bl’
                         >> Q.ABBREV_TAC ‘bo = (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bo’
                         >> Q.ABBREV_TAC ‘bp = (tx_read_bd bd_pa state.nic.regs.CPPI_RAM).bp’
                         >> ‘¬(bl + bo + bp + -1w <₊ bo + bp)’
                             by (Q.PAT_UNDISCH_TAC ‘_ >₊ _’
                                 >> ntac 3 (Q.PAT_UNDISCH_TAC ‘_ ≤₊ _’)
                                 >> blastLib.BBLAST_PROVE_TAC
                                )
                         >> ‘¬(bo + bp <₊ bp)’
                             by (Q.PAT_UNDISCH_TAC ‘_ >₊ _’
                                 >> Q.PAT_UNDISCH_TAC ‘¬ _’
                                 >> ntac 3 (Q.PAT_UNDISCH_TAC ‘_ ≤₊ _’)
                                 >> blastLib.BBLAST_PROVE_TAC
                                )
                         >> asm_rewrite_tac []
                         >> cheat (* LAST PROPERTIES NOT CHECKED BY MONITOR *)
                       )
                >> ‘TX_INVARIANT_COMPLETE_BD_QUEUE_CONFIGURATION_WELL_DEFINED final_nic’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_COMPLETE_BD_QUEUE_CONFIGURATION_WELL_DEFINED_def
                               , txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_CONFIGURATION_WELL_DEFINED_def
                               , txInvariantWellDefinedTheory.TX_INVARIANT_BD_CONFIGURATION_WELL_DEFINED_def
                               , txTheory.TX_CURRENT_BD_CONFIGURATION_WELL_DEFINED_def
                              ]
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_TAIL_BD_QUEUE_CONFIGURATION_WELL_DEFINED final_nic’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_COMPLETE_BD_QUEUE_CONFIGURATION_WELL_DEFINED_def
                               , txInvariantWellDefinedTheory.TX_INVARIANT_TAIL_BD_QUEUE_CONFIGURATION_WELL_DEFINED_def
                               ,txInvariantWellDefinedTheory.TX_INVARIANT_BD_QUEUE_CONFIGURATION_WELL_DEFINED_def
                               , txInvariantWellDefinedTheory.TX_INVARIANT_BD_CONFIGURATION_WELL_DEFINED_def
                               , txTheory.TX_CURRENT_BD_CONFIGURATION_WELL_DEFINED_def]
                         >> Cases_on ‘queue’
                         >- ( simp [] )
                         >> fs []
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_CURRENT_BD_EOP_STATE final_nic’
                    by ( simp [txInvariantWellDefinedTheory.TX_INVARIANT_CURRENT_BD_EOP_STATE_def
                               , tx_state_definitionsTheory.TX_STATE_NOT_BD_QUEUE_EMPTY_def
                              ]
                         >> EVAL_TAC
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> Cases_on ‘state.nic.tx.state = tx_idle’
                         >- ( simp [] >> cheat)
                         >> simp []
                         >> UNDISCH_TAC “TX_INVARIANT state.nic ARB”
                         >> rewrite_tac [txInvariantTheory.TX_INVARIANT_def ]
                         >> rewrite_tac [EVAL “TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY state.nic”]
                         >> rewrite_tac [txInvariantTheory.TX_INVARIANT_MEMORY_def
                                         , txInvariantWellDefinedTheory.TX_INVARIANT_WELL_DEFINED_def
                                         , txInvariantWellDefinedTheory.TX_INVARIANT_CURRENT_BD_STATE_def
                                         , EVAL “TX_INVARIANT_CURRENT_BD_EOP_STATE state.nic”
                                        ]
                         >> simp []
                         >> strip_tac
                         >> strip_tac
                         >> simp []
                         >> cheat  (* UNPROVABLE? *)
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_CURRENT_BD_NONZERO_NDP_EQ_CURRENT_BD_PA_NDP_STATE final_nic’
                    by ( rewrite_tac [txInvariantWellDefinedTheory.TX_INVARIANT_CURRENT_BD_NONZERO_NDP_EQ_CURRENT_BD_PA_NDP_STATE_def
                                      , txInvariantWellDefinedTheory.CURRENT_BD_NDP_EQ_CPPI_RAM_NDP_def]

                         >> rewrite_tac [EVAL “BD_QUEUE_ADVANCEMENT_DEPENDS_ON_NONZERO_NDP_STATE final_nic”]
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> COND_CASES_TAC
                         >- simp []
                         >> Q.PAT_UNDISCH_TAC ‘TX_INVARIANT _ _’
                         >> rewrite_tac [EVAL “TX_INVARIANT state.nic ARB”]
                         >> cheat
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_MEMORY_READABLE_BD_QUEUE queue ARB final_nic.regs.CPPI_RAM’
                    by ( simp [txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_BD_QUEUE_def]
                         >> simp [txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_BD_def]
                         >> cheat
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_STATE final_nic’
                    by ( simp [ txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_STATE_def]
                         >> simp [tx_state_definitionsTheory.TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST_def]
                         >> ‘ARB’
                         >> Q.PAT_UNDISCH_TAC ‘NIC_INVARIANT _ _ _’
                                                             EVAL_TAC
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> COND_CASES_TAC
                         >- (simp [])
                         >> simp []
                         >> UNDISCH_TAC “TX_INVARIANT state.nic ARB” rewrite_tac [EVAL “TX_INVARIANT state.nic ARB”]
                         >> simp [txInvariantTheory.TX_INVARIANT_def ]
                         >> rewrite_tac [EVAL “TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY state.nic”]
                         >> simp []
                         >> rewrite_tac [txInvariantTheory.TX_INVARIANT_MEMORY_def
                                         , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_def
                                         , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_STATE_def
                                         , tx_state_definitionsTheory.TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST_def
                                        ]
                         >> simp []
                       )
                >> asm_rewrite_tac []
                >> ‘TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_IMP_NO_OVERFLOW_STATE final_nic’
                    by ( simp [txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_IMP_NO_OVERFLOW_STATE_def]
                         >> rewrite_tac [EVAL “TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST final_nic”
                                         , EVAL “TX_STATE_PROCESS_MEMORY_READ_REPLY final_nic”
                                        ]
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> COND_CASES_TAC
                         >- simp []
                         >> simp []
                         >> UNDISCH_TAC “TX_INVARIANT state.nic ARB”
                         >> simp [txInvariantTheory.TX_INVARIANT_def ]
                         >> rewrite_tac [EVAL “TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY state.nic”]
                         >> rewrite_tac [txInvariantTheory.TX_INVARIANT_MEMORY_def
                                         , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_def
                                         , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_BYTES_LEFT_TO_REQUEST_IMP_NO_OVERFLOW_STATE_def
                                         , EVAL “TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST state.nic”
                                         , EVAL “TX_STATE_PROCESS_MEMORY_READ_REPLY state.nic”
                                        ]
                         >> simp []
                         >> METIS_TAC []
                       )
                >> asm_rewrite_tac []
                >> simp [txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_STATE_def]
                >> rewrite_tac [ EVAL “TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST final_nic”
                                 , EVAL “TX_STATE_PROCESS_MEMORY_READ_REPLY final_nic”
                               ]
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
                >> COND_CASES_TAC
                >- simp []
                >> UNDISCH_TAC “TX_INVARIANT state.nic ARB”
                >> simp [txInvariantTheory.TX_INVARIANT_def ]
                >> rewrite_tac [EVAL “TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY state.nic”]
                >> simp []
                >> rewrite_tac [txInvariantTheory.TX_INVARIANT_MEMORY_def
                                , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_def
                                , txInvariantMemoryReadsTheory.TX_INVARIANT_MEMORY_READABLE_STATE_def
                                , EVAL “TX_STATE_ISSUE_NEXT_MEMORY_READ_REQUEST state.nic”
                                , EVAL “TX_STATE_PROCESS_MEMORY_READ_REPLY state.nic”
                               ]
                >> simp []
                >> METIS_TAC []
              )
       >> asm_rewrite_tac []
       >> ‘RX_INVARIANT final_nic ARB’
           by ( UNDISCH_TAC “RX_INVARIANT state.nic ARB”
                >> rewrite_tac [rxInvariantTheory.RX_INVARIANT_def]
                >> rpt strip_tac
                >> ‘RX_STATE_AUTONOMOUS_TRANSITION_ENABLE state.nic’
                    by ( Q.PAT_UNDISCH_TAC ‘RX_STATE_AUTONOMOUS_TRANSITION_ENABLE _’
                         >> rewrite_tac [rx_transition_definitionsTheory.RX_STATE_AUTONOMOUS_TRANSITION_ENABLE_def
                                         , schedulerTheory.RX_ENABLE_def]
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                       )
                >> ‘RX_INVARIANT_MEMORY state.nic ARB’
                    by fs []
                >> UNDISCH_TAC “RX_INVARIANT_MEMORY state.nic ARB”
                >> rewrite_tac [rxInvariantTheory.RX_INVARIANT_MEMORY_def]
                >> strip_tac
                >> UNDISCH_TAC “RX_INVARIANT_WELL_DEFINED state.nic”
                >> rewrite_tac [rxInvariantWellDefinedTheory.RX_INVARIANT_WELL_DEFINED_def]
                >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_NOT_DEAD_def]
                >> strip_tac
                >> ‘RX_INVARIANT_BD_QUEUE_NO_OVERLAP rx0_queue_tail’
                    by ( rewrite_tac [ rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_NO_OVERLAP_def]
                         >> ‘∀q.
                             (∀p1 p2. MEM p1 rx0_queue_tail ∧ MEM p2 rx0_queue_tail ∧ p1 ≠ p2 ⇒ word_abs (p1 − p2) ≥ 16w)’
                             by fs [NON_OVERLAPPING_BD_QUEUES_def]
                         >> METIS_TAC [NON_OVERLAP_IMPL_NON_BD_LIST_OVERLAP]
                       )
                >> asm_rewrite_tac []
                >> ‘(rx_bd_queue state.nic) = rx0_queue_tail’
                    by ( rewrite_tac [rx_bd_queueTheory.rx_bd_queue_def]
                         >> METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
                       )
                >> ‘RX_INVARIANT_BD_QUEUE_LOCATION_DEFINED rx0_queue_tail’
                    by ( fs [])
                >> asm_rewrite_tac []
                >> ‘RX_INVARIANT_BD_QUEUE_FINITE final_nic’
                    by ( Q.UNABBREV_TAC ‘final_nic’
                         >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_FINITE_def]
                         >> Q.EXISTS_TAC ‘rx0_queue_tail’
                         >> simp []
                       )
                >> asm_rewrite_tac []
                >> ‘RX_INVARIANT_BD_QUEUE_STRUCTURE_SUPPORT final_nic’
                    by (UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_STRUCTURE_SUPPORT state.nic”
                        >> rewrite_tac [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_STRUCTURE_SUPPORT_def]
                        >> strip_tac
                        >> ‘RX_INVARIANT_CURRENT_BD_PA_IN_BD_QUEUE final_nic’
                            by ( UNDISCH_TAC “RX_INVARIANT_CURRENT_BD_PA_IN_BD_QUEUE state.nic”
                                 >> rewrite_tac [rxInvariantWellDefinedTheory.RX_INVARIANT_CURRENT_BD_PA_IN_BD_QUEUE_def]
                                 >> EVAL_TAC
                                 >> Q.UNABBREV_TAC ‘final_nic’
                                 >> fs [rx_bd_queueTheory.rx_bd_queue_def
                                        , bd_queueTheory.bd_queue_def
                                       ]
                               )
                        >> ‘RX_INVARIANT_STATE_WRITE_CPPI_RAM_POST_PROCESS_IMP_EOP_EQ_CURRENT_BD_PA final_nic’
                            by (UNDISCH_TAC “RX_INVARIANT_STATE_WRITE_CPPI_RAM_POST_PROCESS_IMP_EOP_EQ_CURRENT_BD_PA state.nic”
                                >> Q.UNABBREV_TAC ‘final_nic’
                                >> EVAL_TAC
                                >> simp []
                               )
                        >> simp []
                        >> ‘RX_INVARIANT_write_cp_IMP_CURRENT_BD_PA_EQ_CURRENT_BD_NDP final_nic’
                            by ( UNDISCH_TAC “RX_INVARIANT_write_cp_IMP_CURRENT_BD_PA_EQ_CURRENT_BD_NDP state.nic”
                                 >> Q.UNABBREV_TAC ‘final_nic’
                                 >> EVAL_TAC
                                 >> simp []
                               )
                        >> simp []
                        >> ‘RX_INVARIANT_idle_initialized_write_cp_IMP_CURRENT_BD_PA_EQ_SOP_BD_PA final_nic’
                            by (UNDISCH_TAC “RX_INVARIANT_idle_initialized_write_cp_IMP_CURRENT_BD_PA_EQ_SOP_BD_PA state.nic”
                                >> Q.UNABBREV_TAC ‘final_nic’
                                >> EVAL_TAC
                                >> simp []
                               )
                        >> simp []
                        >> ‘RX_INVARIANT_STATE_ISSUE_MEMORY_REQUEST_OR_WRITE_CPPI_RAM_IMP_CURRENT_BD_NDP_EQ_NDP_CURRENT_BD_PA final_nic’
                            by (UNDISCH_TAC “RX_INVARIANT_STATE_ISSUE_MEMORY_REQUEST_OR_WRITE_CPPI_RAM_IMP_CURRENT_BD_NDP_EQ_NDP_CURRENT_BD_PA state.nic”
                                >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_STATE_ISSUE_MEMORY_REQUEST_OR_WRITE_CPPI_RAM_IMP_CURRENT_BD_NDP_EQ_NDP_CURRENT_BD_PA_def
                                         , rx_state_definitionsTheory.RX_STATE_ISSUE_NEXT_MEMORY_WRITE_REQUEST_OR_WRITE_CPPI_RAM_def
                                         , rx_state_definitionsTheory.RX_STATE_ISSUE_NEXT_MEMORY_WRITE_REQUEST_def
                                         , EVAL “RX_STATE_WRITE_CPPI_RAM state.nic”
                                         , EVAL “RX_STATE_WRITE_CPPI_RAM final_nic”
                                        ]
                                >> Q.UNABBREV_TAC ‘final_nic’
                                >> simp []
                                >> strip_tac
                                >> disch_tac
                                >> ‘state.nic.rx.current_bd.ndp =
                                    read_ndp state.nic.rx.current_bd_pa state.nic.regs.CPPI_RAM’
                                    by fs []
                                >> ‘MEM state.nic.rx.current_bd_pa (rx_bd_queue state.nic)’
                                    by (UNDISCH_TAC “RX_INVARIANT_CURRENT_BD_PA_IN_BD_QUEUE state.nic”
                                        >> rewrite_tac [ EVAL “RX_INVARIANT_CURRENT_BD_PA_IN_BD_QUEUE state.nic”]
                                        >> strip_tac
                                        >> ‘MEM state.nic.rx.current_bd_pa
                                            (CHOICE
                                             {q | BD_QUEUE q state.nic.rx.sop_bd_pa state.nic.regs.CPPI_RAM})’
                                            by fs []
                                        >> rewrite_tac [ EVAL “(rx_bd_queue state.nic)”]
                                        >> simp []
                                       )
                                >> ‘MEM state.nic.rx.current_bd_pa rx0_queue_tail’
                                    by METIS_TAC []
                                >> simp []
                                >> ‘MEM state.nic.rx.current_bd_pa rx0_queue’
                                    by fs []
                                >> ‘∀ p . MEM p queue ⇒ word_abs (state.nic.rx.current_bd_pa - p) >= 16w’
                                    by ( rpt strip_tac
                                         >> METIS_TAC [word_abs_diff]
                                       )
                                >> ‘BD_PTR_ADDRESS state.nic.rx.current_bd_pa’
                                    by fs [EVERY_MEM]
                                >> specl_assume_tac
                                   [‘state.nic.rx.current_bd_pa’
                                    , ‘queue’, ‘0w’ , ‘state.nic.regs.CPPI_RAM’
                                    ]
                                   SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS
                                >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                                >> simp []
                               )
                        >> simp []
                       )
                >> simp []
                >> ‘RX_INVARIANT_RX_BUFFER_OFFSET_ZERO final_nic’
                    by ( UNDISCH_TAC “RX_INVARIANT_RX_BUFFER_OFFSET_ZERO state.nic”
                         >> EVAL_TAC
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                       )
                >> simp []
                >> ‘(rx_unseen_bd_queue final_nic) = (rx_unseen_bd_queue state.nic) ’
                    by (Q.UNABBREV_TAC ‘final_nic’
                         >> rewrite_tac [rx_bd_queueTheory.rx_unseen_bd_queue_def]
                         >> simp []
                         >> Cases_on ‘state.nic.rx.state’
                         >> simp []
                         >- (fs [rx_bd_queueTheory.rx_bd_queue_def])
                         >- (‘MEM state.nic.rx.current_bd_pa rx0_queue_tail’
                             by ( UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_STRUCTURE_SUPPORT state.nic”
                                  >> EVAL_TAC
                                  >> simp []
                                  >> simp [GSYM bd_queueTheory.bd_queue_def]
                                  >> fs [rx_bd_queueTheory.rx_bd_queue_def]
                                )
                             >> ‘∃ q . (BD_QUEUE q state.nic.rx.current_bd_pa (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue) ∧ ∀ p . MEM p q ⇒ MEM p rx0_queue_tail)’
                                 by METIS_TAC [BD_QUEUE_MEM]
                             >> ‘∀p1 p2. MEM p1 queue ∧ MEM p2 q ⇒ word_abs (p1 − p2) ≥ 16w’
                                 by simp []
                             >> ‘EVERY BD_PTR_ADDRESS q’
                                  by (METIS_TAC [EVERY_MEM])
                             >> ‘BD_QUEUE q state.nic.rx.current_bd_pa state.nic.regs.CPPI_RAM’
                                 by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE
                                                 , MEM, word_abs_diff]
                                    )
                             >> METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
                            )
                         >> TRY ( ‘MEM state.nic.rx.current_bd_pa rx0_queue_tail
                                  ∧ (state.nic.rx.current_bd.ndp =
                                     read_ndp state.nic.rx.current_bd_pa state.nic.regs.CPPI_RAM)’
                                  by ( UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_STRUCTURE_SUPPORT state.nic”
                                       >> EVAL_TAC
                                       >> METIS_TAC [bd_queueTheory.bd_queue_def
                                                     , rx_bd_queueTheory.rx_bd_queue_def ]
                                     )
                                  >> ‘∃ q . (BD_QUEUE q state.nic.rx.current_bd_pa (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue) ∧ ∀ p . MEM p q ⇒ MEM p rx0_queue_tail)’
                                      by METIS_TAC [BD_QUEUE_MEM]
                                  >> ‘∀p1 p2. MEM p1 queue ∧ MEM p2 q ⇒ word_abs (p1 − p2) ≥ 16w’
                                      by simp []
                                  >> ‘EVERY BD_PTR_ADDRESS q’
                                      by (METIS_TAC [EVERY_MEM])
                                  >> ‘BD_QUEUE q state.nic.rx.current_bd_pa state.nic.regs.CPPI_RAM’
                                      by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE
                                                      , MEM, word_abs_diff]
                                         )
                                  >> ‘BD_PTR_ADDRESS state.nic.rx.current_bd_pa’
                                      by METIS_TAC [EVERY_MEM]
                                  >> ‘state.nic.rx.current_bd_pa ≠ 0w’
                                      by (UNDISCH_TAC “BD_PTR_ADDRESS state.nic.rx.current_bd_pa”
                                          >> EVAL_TAC
                                          >> WORD_DECIDE_TAC
                                         )
                                  >> ‘(read_ndp state.nic.rx.current_bd_pa
                                       (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
                                      = (read_ndp state.nic.rx.current_bd_pa
                                         state.nic.regs.CPPI_RAM)’
                                      by ( Cases_on ‘q’
                                           >- fs [bd_queueTheory.BD_QUEUE_def]
                                           >> Cases_on ‘t’
                                           >- (fs [bd_queueTheory.BD_QUEUE_def]
                                               >> METIS_TAC []
                                              )
                                           >> fs [bd_queueTheory.BD_QUEUE_def]
                                         )
                                  >> ‘BD_QUEUE (TL q) state.nic.rx.current_bd.ndp state.nic.regs.CPPI_RAM
                                      ∧ BD_QUEUE (TL q) state.nic.rx.current_bd.ndp (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)’
                                      by ( Cases_on ‘q’
                                           >- ( ‘state.nic.rx.current_bd_pa = 0w’
                                                by (fs [bd_queueTheory.BD_QUEUE_def]
                                                   )
                                                >> METIS_TAC []
                                              )
                                           >> fs [bd_queueTheory.BD_QUEUE_def]
                                           >> METIS_TAC []
                                         )
                                  >> METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
                                )
                         >> (fs [rx_bd_queueTheory.rx_bd_queue_def])
                       )
                >> ‘∃ rx0_queue_seen . rx0_queue_seen ++ (rx_unseen_bd_queue state.nic) = rx0_queue_tail’
                    by ( UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_STRUCTURE state.nic”
                         >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_STRUCTURE_def]
                         >> strip_tac
                         >> METIS_TAC []
                       )
                >> ‘RX_INVARIANT_BD_QUEUE_STRUCTURE final_nic’
                    by ( UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_STRUCTURE state.nic”
                         >> rewrite_tac [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_STRUCTURE_def]
                         >> asm_rewrite_tac []
                         >> strip_tac
                         >> Q.EXISTS_TAC ‘rx_seen_bd_queue’
                         >> simp []
                         >> Q.PAT_UNDISCH_TAC ‘_ = _’
                       )
                >> ‘RX_INVARIANT_BD_QUEUE_WELL_DEFINED (rx_unseen_bd_queue final_nic)
                    final_nic.regs.CPPI_RAM’
                    by ( UNDISCH_TAC “RX_INVARIANT_BD_QUEUE_WELL_DEFINED (rx_unseen_bd_queue state.nic) state.nic.regs.CPPI_RAM”
                         >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_WELL_DEFINED_def]
                         >> simp [EVERY_MEM]
                         >> rpt strip_tac
                         >> ‘RX_INVARIANT_BD_WELL_DEFINED
                             (rx_read_bd bd_pa state.nic.regs.CPPI_RAM)’
                             by fs []
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                         >> Q.PAT_UNDISCH_TAC ‘RX_INVARIANT_BD_QUEUE_STRUCTURE state.nic’
                         >> simp [rxInvariantWellDefinedTheory.RX_INVARIANT_BD_QUEUE_STRUCTURE_def]
                         >> strip_tac
                         >> ‘MEM bd_pa rx0_queue_tail’
                             by fs []
                         >>  ‘(rx_read_bd bd_pa state.nic.regs.CPPI_RAM)
                              = (rx_read_bd bd_pa
                                 (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))’
                              by ( ‘BD_PTR_ADDRESS bd_pa’
                                   by (fs [] >> METIS_TAC [EVERY_MEM])
                                   >> ‘∀ p . MEM p queue ⇒ word_abs (bd_pa - p) >= 16w’
                                       by (METIS_TAC [word_abs_diff])
                                   >>  METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_RX_READ_BD]
                                 )
                         >> METIS_TAC []
                       )
                >> simp []
                >> ‘RX_INVARIANT_MEMORY_WRITABLE final_nic ARB’
                    by ( UNDISCH_TAC “RX_INVARIANT_MEMORY_WRITABLE state.nic ARB”
                         >> rewrite_tac [rxInvariantMemoryWritesTheory.RX_INVARIANT_MEMORY_WRITABLE_def]
                         >> simp []
                         >> strip_tac
                         >> ‘RX_INVARIANT_MEMORY_WRITABLE_BD_QUEUE (rx_unseen_bd_queue state.nic)
                             final_nic.regs.CPPI_RAM ARB’
                             by  (UNDISCH_TAC “RX_INVARIANT_MEMORY_WRITABLE_BD_QUEUE (rx_unseen_bd_queue state.nic) state.nic.regs.CPPI_RAM ARB”
                                  >> rewrite_tac [rxInvariantMemoryWritesTheory.RX_INVARIANT_MEMORY_WRITABLE_BD_QUEUE_def]
                                  >> simp [EVERY_MEM]
                                  >> rpt strip_tac
                                  >> Q.PAT_X_ASSUM ‘∀ bd_pa . _’ (specl_assume_tac [‘bd_pa’])
                                  >> ‘BD_ADDRESSES_WRITABLE_MEMORY bd_pa state.nic.regs.CPPI_RAM ARB’
                                      by METIS_TAC []
                                  >> UNDISCH_TAC “BD_ADDRESSES_WRITABLE_MEMORY bd_pa state.nic.regs.CPPI_RAM ARB”
                                  >> rewrite_tac  [rxInvariantMemoryWritesTheory.BD_ADDRESSES_WRITABLE_MEMORY_def]
                                  >> ‘rx_read_bd bd_pa final_nic.regs.CPPI_RAM
                                      = rx_read_bd bd_pa state.nic.regs.CPPI_RAM ’
                                      by ( ‘MEM bd_pa rx0_queue_tail’
                                           by METIS_TAC [MEM, MEM_APPEND]
                                           >> ‘BD_PTR_ADDRESS bd_pa’
                                               by METIS_TAC [EVERY_MEM]
                                           >> ‘∀ p . MEM p queue ⇒ word_abs (bd_pa - p) >= 16w’
                                               by ( rpt strip_tac >> METIS_TAC [word_abs_diff])
                                           >> Q.UNABBREV_TAC ‘final_nic’
                                           >> simp []
                                           >> METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_RX_READ_BD]
                                         )
                                  >> simp []
                                 )
                         >> simp []
                         >> ‘RX_INVARIANT_CURRENT_BUFFER_WRITABLE final_nic ARB’
                             by ( UNDISCH_TAC “RX_INVARIANT_CURRENT_BUFFER_WRITABLE state.nic ARB”
                                  >> simp [rxInvariantMemoryWritesTheory.RX_INVARIANT_CURRENT_BUFFER_WRITABLE_def]
                                  >> rewrite_tac [rx_state_definitionsTheory.RX_STATE_ISSUE_NEXT_MEMORY_WRITE_REQUEST_def]
                                  >> Q.UNABBREV_TAC ‘final_nic’
                                  >> simp []
                                )
                         >> simp []
                         >> ‘RX_INVARIANT_NEXT_MEMORY_WRITE_PA_EQ_BP_PLUS_NUMBER_OF_WRITTEN_BYTES
                             final_nic’
                             by ( UNDISCH_TAC “RX_INVARIANT_NEXT_MEMORY_WRITE_PA_EQ_BP_PLUS_NUMBER_OF_WRITTEN_BYTES state.nic”
                                  >> EVAL_TAC
                                  >> Q.UNABBREV_TAC ‘final_nic’
                                  >> simp []
                                )
                         >> simp []
                         >> ‘RX_INVARIANT_NEXT_MEMORY_WRITE_PA_IN_BUFFER final_nic’
                             by ( UNDISCH_TAC “RX_INVARIANT_NEXT_MEMORY_WRITE_PA_IN_BUFFER state.nic”
                                  >> EVAL_TAC
                                  >> Q.UNABBREV_TAC ‘final_nic’
                                  >> simp []
                                )
                       )
                >> simp []
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
              )
       >> asm_rewrite_tac []
       >> ‘TD_INVARIANT final_nic’
           by ( UNDISCH_TAC “TD_INVARIANT state.nic”
                >> ‘bd_queue state.nic.rx.sop_bd_pa (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
                    = rx0_queue_tail’
                    by METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
                >> ‘bd_queue state.nic.rx.sop_bd_pa state.nic.regs.CPPI_RAM
                    = rx0_queue_tail’
                    by METIS_TAC [bd_queueTheory.BD_QUEUE_IMP_bd_queue_lemma]
                >> EVAL_TAC
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
                >> fs [rx_bd_queueTheory.rx_bd_queue_def, bd_queueTheory.bd_queue_def]
                >> strip_tac
                >> COND_CASES_TAC
                >- fs []
                >> fs []
              )
       >> asm_rewrite_tac []
       >> ‘RD_INVARIANT final_nic’
           by ( UNDISCH_TAC “RD_INVARIANT state.nic”
                >> rewrite_tac [rdInvariantTheory.RD_INVARIANT_def]
                >> strip_tac
                >> ‘RD_INVARIANT_RX_ADVANCES_BD_QUEUE final_nic’
                    by ( UNDISCH_TAC “RD_INVARIANT_RX_ADVANCES_BD_QUEUE state.nic”
                         >> EVAL_TAC
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                       )
                >> simp []
                >> ‘RD_INVARIANT_RD_CLEARS_BD_QUEUE final_nic’
                    by ( UNDISCH_TAC “RD_INVARIANT_RD_CLEARS_BD_QUEUE state.nic”
                         >> EVAL_TAC
                         >> Q.UNABBREV_TAC ‘final_nic’
                         >> simp []
                       )
                >> simp []
                >> rewrite_tac [rdInvariantTheory.RD_INVARIANT_CURRENT_BD_PA_def
                                , EVAL “RD_STATE_WRITE_CPPI_RAM final_nic”]
                >> strip_tac
                >> Q.UNABBREV_TAC ‘final_nic’
                >> simp []
                >> ‘MEM state.nic.rx.current_bd_pa rx0_queue’
                    by ( UNDISCH_TAC “BD_QUEUE_INVARIANT state”
                         >> rewrite_tac [BD_QUEUE_INVARIANT_def]
                         >> strip_tac
                         >> fs []
                         >> METIS_TAC [WELL_FORMED_BD_QUEUE_UNIQUE]
                       )
                >> ‘BD_PTR_ADDRESS state.nic.rx.current_bd_pa’
                    by METIS_TAC [EVERY_MEM]
                >> ‘BD_IN_CPPI_RAM state.nic.rx.current_bd_pa’
                    by (UNDISCH_TAC “BD_PTR_ADDRESS state.nic.rx.current_bd_pa”
                        >> EVAL_TAC
                        >> blastLib.BBLAST_PROVE_TAC
                       )
                >> simp []
                >> rewrite_tac [bd_listTheory.BD_NOT_OVERLAP_BDs_def]
                >> simp [EVERY_MEM]
                >> strip_tac >> strip_tac
                >> rewrite_tac [bdTheory.BDs_OVERLAP_def]
                >> ‘word_abs (state.nic.rx.current_bd_pa - bd_pa2) >= 16w’
                    by ( ‘∀p1 p2. MEM p1 queue ∧ MEM p2 rx0_queue ⇒ word_abs (p1 - p2) ≥ 16w’
                         by fs []
                         >> METIS_TAC [word_abs_diff]
                       )
                >> UNDISCH_TAC “word_abs (state.nic.rx.current_bd_pa - bd_pa2) >= 16w”
                >> blastLib.BBLAST_PROVE_TAC
              )
       >> simp []
     )
  >> cheat
QED



val _ = export_theory();
