open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitorUtils;

open handlersTheory;
(*

Just random stuff about words that doesn't fit neatly anywhere else

*)

val _ = new_theory "wordExtra";


Theorem NUM_LE_32_PRESERVED:
  ∀ (n : num) (bound : num) .
  (bound < (UINT_MAX (:32)))
  ∧ (n ≤ bound)
  ⇒ (n2w n ≤₊ (n2w bound : 32 word))
Proof
  strip_tac >> strip_tac
  >> EVAL_TAC >> simp []
QED

Theorem TX_BL_OVERFLOW_CONSTRAINT:
  ∀ (n : 32 word) (m : 32 word) (max : 32 word)  .
  max <₊ 2048w
  ∧ n ≤₊ 65535w
  ∧ m ≤₊ 65535w * 256w
  ∧ max = n + m
  ⇒ max − n <₊ 2048w
Proof
  blastLib.BBLAST_PROVE_TAC
QED

Theorem QUEUE_LENGTH_SUM_FOLDR:
    ∀ (queue : 32 word list) .
    LENGTH queue <= 256
    ∧ EVERY (λ w . w ≤₊ 65535w) queue
    ∧ queue ≠ []
  ⇒ (FOLDR $+ 0w queue
     ≤₊ n2w (LENGTH queue) * 65535w)
Proof
  Induct_on ‘queue’
  >- (simp [])
  >> rpt strip_tac
  >> simp []
  >> ‘h ≤₊ 65535w ∧ EVERY (λ w . w ≤₊ 65535w) queue’
      by fs []
  >> Cases_on ‘queue = []’
  >- (simp [] >> WORD_DECIDE_TAC)
  >> ‘LENGTH queue <= 256’
      by fs []
  >> ‘FOLDR $+ 0w queue ≤₊ 65535w * n2w (LENGTH queue)’
      by fs []
  >> Q.ABBREV_TAC ‘b = FOLDR $+ 0w queue’
  >> fs []
  >> ‘n2w (SUC (LENGTH queue)) ≤₊ (256w : 32 word)’
      by (Q.PAT_UNDISCH_TAC ‘LENGTH queue ≤ 256’
          >> ‘256 < UINT_MAX (:32)’
              by EVAL_TAC
          >> METIS_TAC [NUM_LE_32_PRESERVED]
         )
  >> Cases_on ‘LENGTH queue’
  >- (simp []
      >> ‘b = 0w’
          by (Q.PAT_UNDISCH_TAC ‘b ≤₊ 65535w * 0w’
              >> blastLib.BBLAST_PROVE_TAC)
      >> simp []
     )
  >> Q.PAT_UNDISCH_TAC ‘h ≤₊ 65535w’
  >> Q.PAT_UNDISCH_TAC ‘b ≤₊ 65535w * n2w (SUC n)’
  >> Q.PAT_UNDISCH_TAC ‘SUC n <= 256’
  >> Q.PAT_UNDISCH_TAC ‘SUC (SUC n) <= 256’
  >> rpt (Cases_on_goal (find_term is_var)
          >- (WORD_EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
          >> simp [])
QED

Theorem AD_HOCK_LENGTH_MUL:
  ∀ n .
  n <= 256
  ⇒
  n2w n * 65535w ≤₊ 256w * 65535w : 32 word
Proof
  strip_tac
  >> rpt (Cases_on_goal (find_term is_var)
          >- (simp [] >> blastLib.BBLAST_PROVE_TAC)
          >> simp []
         )
QED

Theorem BD_QUEUE_TX_SUM_NO_2048_PIDGEON_HOLE:
  ∀ (max : 32 word) (queue : 32 word list ) .
  EVERY (λ w . w ≤₊ 65535w) queue
  ∧ LENGTH queue <= 256
  ∧ max <₊ 2048w
  ∧ max = FOLDR $+ 0w queue
  ⇒ EVERY (λ w . w <₊ 2048w) queue
Proof
  Induct_on ‘queue’
  >- simp []
  >> rpt strip_tac
  >> Cases_on ‘queue = []’
  >- (fs [])
  >> ‘EVERY (λw. w ≤₊ 65535w) queue’
      by fs []
  >> ‘(max = FOLDR $+ 0w (h::queue))
      = (max = h + FOLDR $+ 0w queue)’
      by fs []
  >> pat_x_rewrite_all ‘_=_’
  >> ‘LENGTH queue <= 256’
      by fs []
  >> ‘(FOLDR $+ 0w (h::queue) ≤₊ 65535w * 256w)
      = (h + (FOLDR $+ 0w queue) ≤₊ 65535w * 256w)’
      by fs []
  >> pat_x_rewrite_all ‘_=_’
  >> ‘h ≤₊ 65535w ∧ EVERY (λw. w ≤₊ 65535w) queue’
      by fs []
  >> ‘FOLDR $+ 0w queue ≤₊ (n2w (LENGTH queue) * 65535w)’
      by ( specl_assume_tac
           [‘queue’]
           QUEUE_LENGTH_SUM_FOLDR
           >> Q.PAT_UNDISCH_TAC ‘_⇒_’
           >> asm_rewrite_tac []
           >> simp []
         )
  >> ‘n2w (LENGTH queue) * (65535w : 32 word) ≤₊ 256w * (65535w : 32 word)’
      by ( METIS_TAC [AD_HOCK_LENGTH_MUL] )
  >> ‘FOLDR $+ 0w queue ≤₊ (256w * 65535w)’
      by METIS_TAC [WORD_LOWER_EQ_TRANS, WORD_MULT_COMM]
  >> ‘max - h <₊ 2048w’
      by (specl_assume_tac
          [‘h’, ‘FOLDR $+ 0w queue’, ‘max’]
          TX_BL_OVERFLOW_CONSTRAINT
          >> Q.PAT_UNDISCH_TAC ‘_⇒_’
          >> asm_rewrite_tac []
          >> once_rewrite_tac [WORD_MULT_COMM]
          >> asm_rewrite_tac []
          >> simp [])
  >> simp []
  >> ‘max - h = FOLDR $+ 0w queue’
      by fs []
  >> ‘EVERY (λw. w <₊ 2048w) queue’
      by METIS_TAC []
  >> simp []
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem PL_AND_SIZE:
  ∀ (val : 32 word) .
    (PL && val) <₊ 2048w
Proof
  strip_tac
  >> rewrite_tac [PL_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem TX_BL_AND_SIZE:
  ∀ (val : 32 word) .
    (TX_BL && val) ≤₊ 65535w
Proof
  strip_tac
  >> rewrite_tac [TX_BL_def]
  >> blastLib.BBLAST_PROVE_TAC
QED



val _ = export_theory();
