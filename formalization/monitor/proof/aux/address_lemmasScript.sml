open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;

open address_spaceTheory;
open monitor_invariantTheory;
open monitorUtils;

val _ = new_theory "address_lemmas";


Theorem CPDMA_SOFT_RESET_PA_lemmas:
  (WORD32_ALIGNED CPDMA_SOFT_RESET_PA)
  ∧ (((1 -- 0) (CPDMA_SOFT_RESET_PA : 32 word) = 0w))
  ∧ (NOT_ZERO_CHANNEL_HDP_BYTE CPDMA_SOFT_RESET_PA = F)
  ∧ ((CPDMA_SOFT_RESET_PA ≠ TX_TEARDOWN_PA))
  ∧ ((CPDMA_SOFT_RESET_PA ≠ RX_TEARDOWN_PA))
Proof
  EVAL_TAC
QED

Theorem TX0_HDP_PA_lemmas:
  (WORD32_ALIGNED TX0_HDP_PA)
  ∧ (TX0_HDP_PA ≠ TX_TEARDOWN_PA)
  ∧ (TX0_HDP_PA ≠ RX_TEARDOWN_PA)
  ∧ (TX0_HDP_PA ≠ CPDMA_SOFT_RESET_PA)
  ∧ (TX0_HDP_PA ≠ DMACONTROL_PA)
  ∧ (TX0_HDP_PA ≠ RX_BUFFER_OFFSET_PA)
  ∧ (¬ NOT_ZERO_CHANNEL_HDP_BYTE TX0_HDP_PA)
Proof
  EVAL_TAC
QED

Theorem TX0_CP_PA_lemmas:
  (WORD32_ALIGNED TX0_CP_PA)
  ∧ (TX0_CP_PA ≠ TX_TEARDOWN_PA)
  ∧ (TX0_CP_PA ≠ RX_TEARDOWN_PA)
  ∧ (TX0_CP_PA ≠ CPDMA_SOFT_RESET_PA)
  ∧ (TX0_CP_PA ≠ DMACONTROL_PA)
  ∧ (TX0_CP_PA ≠ RX_BUFFER_OFFSET_PA)
  ∧ (TX0_CP_PA ≠ TX0_HDP_PA)
  ∧ (TX0_CP_PA ≠ RX0_HDP_PA)
Proof
  EVAL_TAC
QED

Theorem RX0_CP_PA_lemmas:
  (WORD32_ALIGNED RX0_CP_PA)
  ∧ (RX0_CP_PA ≠ TX_TEARDOWN_PA)
  ∧ (RX0_CP_PA ≠ RX_TEARDOWN_PA)
  ∧ (RX0_CP_PA ≠ CPDMA_SOFT_RESET_PA)
  ∧ (RX0_CP_PA ≠ DMACONTROL_PA)
  ∧ (RX0_CP_PA ≠ RX_BUFFER_OFFSET_PA)
  ∧ (RX0_CP_PA ≠ TX0_HDP_PA)
  ∧ (RX0_CP_PA ≠ RX0_HDP_PA)
  ∧ (RX0_CP_PA ≠ TX0_CP_PA)
Proof
  EVAL_TAC
QED


Theorem RX0_HDP_PA_lemmas:
  (WORD32_ALIGNED RX0_HDP_PA)
  ∧ (RX0_HDP_PA ≠ TX_TEARDOWN_PA)
  ∧ (RX0_HDP_PA ≠ RX_TEARDOWN_PA)
  ∧ (RX0_HDP_PA ≠ CPDMA_SOFT_RESET_PA)
  ∧ (RX0_HDP_PA ≠ DMACONTROL_PA)
  ∧ (RX0_HDP_PA ≠ RX_BUFFER_OFFSET_PA)
  ∧ (RX0_HDP_PA ≠ TX0_HDP_PA)
Proof
  EVAL_TAC
QED


Theorem CPPI_RAM_BYTE_PA_lemma:
  ∀ bd_ptr .
  (CPPI_RAM_BYTE_PA bd_ptr ∧ WORD32_ALIGNED bd_ptr)  ⇒
  (   (WORD32_ALIGNED bd_ptr)
    ∧ (¬ NOT_ZERO_CHANNEL_HDP_BYTE bd_ptr)
    ∧ (bd_ptr ≠ TX_TEARDOWN_PA)
    ∧ (bd_ptr ≠ RX_TEARDOWN_PA)
    ∧ (bd_ptr ≠ CPDMA_SOFT_RESET_PA)
    ∧ (bd_ptr ≠ DMACONTROL_PA)
    ∧ (bd_ptr ≠ RX_BUFFER_OFFSET_PA)
    ∧ (bd_ptr ≠ TX0_HDP_PA)
    ∧ (bd_ptr  ≠ TX0_HDP_PA)
    ∧ (bd_ptr ≠ RX0_HDP_PA)
    ∧ (bd_ptr ≠ TX0_CP_PA)
    ∧ (bd_ptr ≠ RX0_CP_PA)
  )
Proof
  strip_tac
  >> strip_tac
  >> asm_rewrite_tac []
  >> Q.PAT_UNDISCH_TAC ‘CPPI_RAM_BYTE_PA x’
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

(* Same as above but specialized for BD_PTR_ADDRESS *)
Theorem BD_PTR_ADDRESS_lemma:
  ∀ bd_ptr .
  (BD_PTR_ADDRESS bd_ptr)  ⇒
  ( (WORD32_ALIGNED bd_ptr)
    ∧ (bd_ptr ≠ TX_TEARDOWN_PA)
    ∧ (bd_ptr ≠ RX_TEARDOWN_PA)
    ∧ (bd_ptr ≠ CPDMA_SOFT_RESET_PA)
    ∧ (bd_ptr ≠ DMACONTROL_PA)
    ∧ (bd_ptr ≠ RX_BUFFER_OFFSET_PA)
    ∧ (bd_ptr ≠ TX0_HDP_PA)
    ∧ (bd_ptr  ≠ TX0_HDP_PA)
    ∧ (bd_ptr ≠ RX0_HDP_PA)
    ∧ (bd_ptr ≠ TX0_CP_PA)
    ∧ (bd_ptr ≠ RX0_CP_PA)
  )
Proof
  strip_tac
  >> strip_tac
  >> fs [BD_PTR_ADDRESS_def, CPPI_RAM_BYTE_PA_lemma]
QED

(* Used for some reads *)
Theorem BD_PTR_ADDRESS_FLAGS:
  ∀ bd_ptr .
  BD_PTR_ADDRESS bd_ptr
  ⇒ CPPI_RAM_BYTE_PA (bd_ptr + 12w) ∧ WORD32_ALIGNED (bd_ptr + 12w)
Proof
  strip_tac
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED



Theorem BD_PTR_ADDRESS_NOT_ZERO:
  ∀ bd_ptr .
  BD_PTR_ADDRESS bd_ptr
  ⇒ bd_ptr ≠ 0w
Proof
  strip_tac
  >> simp [BD_PTR_ADDRESS_def
           , address_spaceTheory.CPPI_RAM_BYTE_PA_def
           , address_spaceTheory.CPPI_RAM_START_def
          ]
  >> WORD_DECIDE_TAC
QED



Theorem CPPI_RAM_BYTE_PA_WORD_ABS_GT_16_BREAKDOWN:
  ∀ p1 p2 .
  CPPI_RAM_BYTE_PA p1
  ∧ CPPI_RAM_BYTE_PA p2
  ∧  word_abs (p1 − p2) ≥ 16w ⇒
  word_abs ((p1 + 12w) − p2) ≥ 4w ∧ word_abs (p1 − (p2 + 8w)) ≥ 4w ∧
  word_abs (p1 − (p2 + 4w)) ≥ 4w ∧ word_abs (p1 − p2) ≥ 4w
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA p1”
                  , EVAL “CPPI_RAM_BYTE_PA p2”
                 ]
  >> blastLib.BBLAST_PROVE_TAC
QED

val _ = export_theory();

(*

  >> ‘WORD32_ALIGNED bd_ptr’ by asm_rewrite_tac []
  >> asm_rewrite_tac []
      by (Q.PAT_UNDISCH_TAC ‘CPPI_RAM_BYTE_PA x’
          >> EVAL_TAC >>
         )
      EVAL_TAC >>  PAT_X_UNDISCH_TAC “CPPI_TAM x”
  >> EVAL_TAC
        >>
  >>

  >> PAT_ASSUM “CPPI_RAM_BYTE_PA x” ASSUME_TAC
  >> Q.SPECL_THEN [‘CPPI_RAM_BYTE_PA x’ , ‘CPPI_RAM_BYTE_PA x’] ASSUME_TAC boolTheory.CONJ_SYM

*)
