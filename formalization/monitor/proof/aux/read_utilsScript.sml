(*

Author: Tomas Möre 2020



*)
open HolKernel wordsLib listTheory boolLib bossLib stateTheory;

open monitor_invariantTheory;
open monitor_stateTheory;
open monitorUtils;
open address_lemmasTheory;

open handlersTheory;
open register_readTheory;
open register_writeTheory;

val _ = new_theory "read_utils";

(* Used to get some read out of the way *)
Theorem READ_NIC_REGISTER_EXISTS:
  ∀ pa state .
  WORD32_ALIGNED pa
   ⇒ ∃ val . read_nic_register pa state = (val , state)
Proof
  NTAC 3 strip_tac
  >> simp [read_nic_register_def, register_readTheory.read_register_def]
  >> Cases_on ‘state.nic.dead’
  >> Cases_on ‘pa = TX_TEARDOWN_PA’
  >> Cases_on ‘pa = RX_TEARDOWN_PA’
  >> Cases_on ‘pa = CPDMA_SOFT_RESET_PA’
  >> Cases_on ‘pa = DMACONTROL_PA’
  >> fs [ system_state_component_equality ] (* just an optimization *)
  >> Cases_on ‘pa = RX_BUFFER_OFFSET_PA’
  >> Cases_on ‘pa = TX0_HDP_PA’
  >> Cases_on ‘pa = RX0_HDP_PA’
  >> Cases_on ‘pa = TX0_CP_PA’
  >> Cases_on ‘pa = RX0_CP_PA’
  >> Cases_on ‘CPPI_RAM_BYTE_PA pa’
  >> fs [ system_state_component_equality ]
QED
(* Utility theorem to decrease duplication later *)
Theorem RED_NIC_REGISTER_BD_PTR_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
   ⇒ ∃ val . read_nic_register pa state = (val , state)
Proof
  NTAC 3 strip_tac
  >> fs [BD_PTR_ADDRESS_def, READ_NIC_REGISTER_EXISTS]
QED

Theorem CPPI_RAM_READ:
  ∀ pa state .
  (CPPI_RAM_BYTE_PA pa ∧ WORD32_ALIGNED pa ∧ ¬ state.nic.dead)
  ⇒ read_nic_register pa state = (read_bd_word pa 0w state.nic.regs.CPPI_RAM , state)
Proof
  NTAC 3 strip_tac
  >> simp [read_nic_register_def
          , register_readTheory.read_register_def
          , CPPI_RAM_BYTE_PA_lemma
          , system_state_component_equality
          , nic_monitor_state_component_equality ]
QED

(* Slightly diffrent version of CPPI_RAM_READ *)
Theorem BD_PTR_ADDRESS_CPPI_RAM_READ:
  ∀ pa state .
  (BD_PTR_ADDRESS pa ∧ ¬ state.nic.dead)
  ⇒ read_nic_register pa state = (read_bd_word pa 0w state.nic.regs.CPPI_RAM , state)
Proof
  NTAC 3 strip_tac
  >> fs [BD_PTR_ADDRESS_def, CPPI_RAM_READ]
QED

Theorem BD_PTR_ADDRESS_CPPI_RAM_READ_N:
  ∀ pa n state .
  (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ∧ BD_PTR_ADDRESS pa ∧ ¬ state.nic.dead
  ⇒ read_nic_register (pa + n) state = (read_bd_word (pa + n) 0w state.nic.regs.CPPI_RAM , state)
Proof
  NTAC 3 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> ‘CPPI_RAM_BYTE_PA (pa + n)’
      by (UNDISCH_ALL_TAC
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + n)’
      by (UNDISCH_ALL_TAC
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> METIS_TAC [CPPI_RAM_READ]
QED

Theorem BD_PTR_ADDRESS_CPPI_RAM_READ_FLAGS:
  ∀ pa state .
  (BD_PTR_ADDRESS pa ∧ ¬ state.nic.dead)
  ⇒ read_nic_register (pa + 12w) state = (read_bd_word (pa + 12w) 0w state.nic.regs.CPPI_RAM , state)
Proof
  METIS_TAC [BD_PTR_ADDRESS_CPPI_RAM_READ_N]
QED


Theorem READ_NIC_REGISTER_CPPI_RAM_DEPENDANT:
  ∀ pa state state' .
   state.nic.dead = state'.nic.dead
  ∧ state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM
  ∧ CPPI_RAM_BYTE_PA pa
  ⇒
  FST (read_nic_register pa state) =
  FST (read_nic_register pa state')
Proof
  rpt strip_tac
  >> simp [read_nic_register_def, register_readTheory.read_register_def]
  >> Cases_on ‘state.nic.dead’
  >> fs []
  >> fs [ CPPI_RAM_BYTE_PA_lemma ]
  >> Cases_on ‘WORD32_ALIGNED pa’
  >> fs [ system_state_component_equality ]
  >> fs []
QED

Theorem IS_EOP_CPPI_RAM_DEPENDANT:
  ∀ pa state state' .
   state.nic.dead = state'.nic.dead
  ∧ state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM
  ∧ BD_PTR_ADDRESS pa
  ⇒
  FST (is_eop pa state) =
  FST (is_eop pa state')
Proof
  rpt strip_tac
  >> ‘FST (read_nic_register (pa + 12w) state)
      = FST (read_nic_register (pa + 12w) state')’
      by ( UNDISCH_TAC “BD_PTR_ADDRESS pa”
           >> fs [BD_PTR_ADDRESS_def]
           >> strip_tac
           >> ‘CPPI_RAM_BYTE_PA (pa + 12w)’
               by (UNDISCH_ALL_TAC
                   >> EVAL_TAC
                   >> blastLib.BBLAST_PROVE_TAC)
           >> fs [READ_NIC_REGISTER_CPPI_RAM_DEPENDANT]
         )
  >> simp_st [is_eop_def]
  >> Cases_on ‘(read_nic_register (pa + 12w) state)’
  >> Cases_on ‘(read_nic_register (pa + 12w) state')’
  >> fs []
QED

Theorem READ_NIC_REGISTER_MONITOR_INDEPENDANT:
  ∀ pa state monitor .
  FST (read_nic_register pa state) =
  FST (read_nic_register pa (state with monitor := monitor))
Proof
  rpt strip_tac
  >> simp [read_nic_register_def, register_readTheory.read_register_def]
  >> Cases_on ‘state.nic.dead’
  >> Cases_on ‘WORD32_ALIGNED pa’
  >> fs [ system_state_component_equality ]
  >> Cases_on ‘pa = TX_TEARDOWN_PA’
  >> Cases_on ‘pa = RX_TEARDOWN_PA’
  >> Cases_on ‘pa = CPDMA_SOFT_RESET_PA’
  >> Cases_on ‘pa = DMACONTROL_PA’
  >> fs [ system_state_component_equality ] (* just an optimization *)
  >> Cases_on ‘pa = RX_BUFFER_OFFSET_PA’
  >> Cases_on ‘pa = TX0_HDP_PA’
  >> Cases_on ‘pa = RX0_HDP_PA’
  >> Cases_on ‘pa = TX0_CP_PA’
  >> Cases_on ‘pa = RX0_CP_PA’
  >> Cases_on ‘CPPI_RAM_BYTE_PA pa’
  >> fs [ system_state_component_equality ]
QED

Theorem IS_ACTIVE_CPPI_RAM_EXISTS:
  ∀ ptr state .
  ∃ b . IS_ACTIVE_CPPI_RAM ptr state = (b , state)
Proof
  rpt strip_tac
  >> rewrite_tac [IS_ACTIVE_CPPI_RAM_def]
  >> simp_st [get_alpha_def]
QED

(* Most functions can be proven by this *)
fun is_x_exsists_tac def =
  NTAC 3 strip_tac
  >> ‘WORD32_ALIGNED (pa + 12w)’
      by (fs [BD_PTR_ADDRESS_FLAGS])
  >> ‘∃val. read_nic_register (pa + 12w) state = (val,state)’
      by specl_fs_tac [‘pa + 12w’, ‘state’] READ_NIC_REGISTER_EXISTS
  >> fs_st [def, READ_NIC_REGISTER_EXISTS, BD_PTR_ADDRESS_FLAGS];


Theorem IS_EOP_MONITOR_INDEPENDANT:
  ∀ state monitor p .
    FST (is_eop p state) = FST (is_eop p (state with monitor := monitor))
Proof
  rpt strip_tac
  >> simp_st [is_eop_def]
  >> Cases_on ‘(read_nic_register (p + 12w) state)’
  >> Cases_on ‘(read_nic_register (p + 12w) (state with monitor := monitor))’
  >> simp []
  >> ‘FST (read_nic_register (p + 12w) state)
      = FST (read_nic_register (p + 12w) (state with monitor := monitor))’
      by fs [READ_NIC_REGISTER_MONITOR_INDEPENDANT]
  >> UNDISCH_TAC “FST (read_nic_register (p + 12w) state)
                 = FST (read_nic_register (p + 12w) (state with monitor := monitor))”
  >> simp []
QED


Theorem IS_EOP_ALPHA_INDEPENDANT:
  ∀ state alpha p .
    FST (is_eop p state) = FST (is_eop p (state with monitor := state.monitor with alpha := alpha))
Proof
  fs [IS_EOP_MONITOR_INDEPENDANT]
QED

Theorem IS_RELEASED_MONITOR_INDEPENDANT:
  ∀ state monitor p .
    FST (is_released p state) = FST (is_released p (state with monitor := monitor))
Proof
  rpt strip_tac
  >> simp_st [is_released_def]
  >> Cases_on ‘(read_nic_register (p + 12w) state)’
  >> Cases_on ‘(read_nic_register (p + 12w) (state with monitor := monitor))’
  >> simp []
  >> ‘q = FST (read_nic_register (p + 12w) state)’
      by fs[]
  >> ‘q' = FST (read_nic_register (p + 12w) (state with monitor := monitor))’
      by fs[]
  >> METIS_TAC [ READ_NIC_REGISTER_MONITOR_INDEPENDANT]
QED

Theorem IS_TD_MONITOR_INDEPENDANT:
  ∀ state monitor p .
    FST (is_td p state) = FST (is_td p (state with monitor := monitor))
Proof
  rpt strip_tac
  >> simp_st [is_td_def]
  >> Cases_on ‘(read_nic_register (p + 12w) state)’
  >> Cases_on ‘(read_nic_register (p + 12w) (state with monitor := monitor))’
  >> simp []
  >> ‘q = FST (read_nic_register (p + 12w) state)’
      by fs[]
  >> ‘q' = FST (read_nic_register (p + 12w) (state with monitor := monitor))’
      by fs[]
  >> METIS_TAC [ READ_NIC_REGISTER_MONITOR_INDEPENDANT]
QED


(*

The following functions are much like a 'cases_on' but with the important
distiction that we know the state is not modified.

*)

Theorem GET_NEXT_DESCRIPTOR_POINTER_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ pa' . get_next_descriptor_pointer pa state = (pa' , state)
Proof
  rpt strip_tac
  >> ‘WORD32_ALIGNED pa’
      by (fs [BD_PTR_ADDRESS_def])
  >> ‘CPPI_RAM_BYTE_PA pa’
      by fs [BD_PTR_ADDRESS_def]
  >> simp [get_next_descriptor_pointer_def
           , NEXT_DESCRIPTOR_POINTER_def]
  >> fs [READ_NIC_REGISTER_EXISTS]
QED

Theorem IS_SOP_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . is_sop pa state = (b, state)
Proof
  is_x_exsists_tac is_sop_def
QED

Theorem IS_SOP:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ∧ ¬ state.nic.dead
  ⇒ is_sop pa state = ((tx_read_bd pa state.nic.regs.CPPI_RAM).sop, state)
Proof
  NTAC 3 strip_tac
  >> simp_st [is_sop_def]
  >> simp [BD_PTR_ADDRESS_CPPI_RAM_READ_N ]
  >> simp [CPPI_RAMTheory.tx_read_bd_def]
  >> ‘(read_bd_word pa 3w state.nic.regs.CPPI_RAM) = (read_bd_word (pa + 12w) 0w state.nic.regs.CPPI_RAM)’
      by simp [CPPI_RAMTheory.read_bd_word_def]
  >> pat_x_rewrite_all ‘_=_’
  >> simp [SOP_def]
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem IS_EOP_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . is_eop pa state = (b, state)
Proof
  is_x_exsists_tac is_eop_def
QED

Theorem IS_EOP:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ∧ ¬ state.nic.dead
  ⇒ is_eop pa state = ((tx_read_bd pa state.nic.regs.CPPI_RAM).eop, state)
Proof
  NTAC 3 strip_tac
  >> simp_st [is_eop_def]
  >> simp [BD_PTR_ADDRESS_CPPI_RAM_READ_N ]
  >> simp [CPPI_RAMTheory.tx_read_bd_def]
  >> ‘(read_bd_word pa 3w state.nic.regs.CPPI_RAM) = (read_bd_word (pa + 12w) 0w state.nic.regs.CPPI_RAM)’
      by simp [CPPI_RAMTheory.read_bd_word_def]
  >> pat_x_rewrite_all ‘_=_’
  >> simp [EOP_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem IS_RELEASED_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . is_released pa state = (b, state)
Proof
  is_x_exsists_tac is_released_def
QED

Theorem IS_EOQ_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . is_eoq pa state = (b, state)
Proof
  is_x_exsists_tac is_eoq_def
QED

Theorem IS_TD_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . is_td pa state = (b, state)
Proof
  is_x_exsists_tac is_td_def
QED

Theorem GET_BUFFER_POINTER_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . get_buffer_pointer pa state = (b, state)
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [get_buffer_pointer_def]
  >> ‘CPPI_RAM_BYTE_PA (pa + BUFFER_POINTER)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + BUFFER_POINTER)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘∃val. read_nic_register (pa + BUFFER_POINTER) state = (val,state)’
      by specl_fs_tac [‘pa + BUFFER_POINTER’, ‘state’] READ_NIC_REGISTER_EXISTS
  >> fs_st [READ_NIC_REGISTER_EXISTS, BD_PTR_ADDRESS_FLAGS]
QED

Theorem GET_BUFFER_POINTER:
  ∀ pa state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS pa
  ⇒ get_buffer_pointer pa state = ((tx_read_bd pa state.nic.regs.CPPI_RAM).bp , state)
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [get_buffer_pointer_def]
  >> ‘CPPI_RAM_BYTE_PA (pa + BUFFER_POINTER)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + BUFFER_POINTER)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> simp [CPPI_RAM_READ, BUFFER_POINTER_def
           , CPPI_RAMTheory.tx_read_bd_def
           , CPPI_RAMTheory.read_bd_word_def
          ]
QED

Theorem GET_BUFFER_OFFSET_AND_LENGTH_EXISTS:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ⇒ ∃ b . get_buffer_offset_and_length  pa state = (b, state)
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [get_buffer_offset_and_length_def ]
  >> ‘CPPI_RAM_BYTE_PA (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘∃val. read_nic_register (pa + BOBL) state = (val,state)’
      by specl_fs_tac [‘pa + BOBL’, ‘state’]
         READ_NIC_REGISTER_EXISTS
  >> fs_st [READ_NIC_REGISTER_EXISTS, BD_PTR_ADDRESS_FLAGS]
QED

Theorem GET_BUFFER_OFFSET_AND_LENGTH:
  ∀ pa state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS pa
  ⇒ get_buffer_offset_and_length  pa state
  = (read_bd_word pa 2w state.nic.regs.CPPI_RAM
     , state)
Proof

  NTAC 3 strip_tac
  >> rewrite_tac [get_buffer_offset_and_length_def ]
  >> ‘CPPI_RAM_BYTE_PA (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> simp [CPPI_RAM_READ, BOBL_def, CPPI_RAMTheory.read_bd_word_def]
QED

Theorem TX_BL_TO_READ_BD:
  ∀ bd_ptr CPPI_RAM .
  TX_BL && read_bd_word (bd_ptr + BOBL) 0w CPPI_RAM
  = (tx_read_bd bd_ptr CPPI_RAM).bl
Proof
  rpt strip_tac
  >> rewrite_tac [CPPI_RAMTheory.tx_read_bd_def]
  >> simp []
  >> ‘(read_bd_word bd_ptr 2w CPPI_RAM) =
      (read_bd_word (bd_ptr + BOBL) 0w CPPI_RAM)’
      by (simp [CPPI_RAMTheory.read_bd_word_def, BOBL_def])
  >> pat_x_rewrite_all ‘_=_’
  >> rewrite_tac [TX_BL_def]
  >> WORD_DECIDE_TAC
QED

Theorem PL_TO_READ_BD:
  ∀ bd_ptr CPPI_RAM .
  PL && read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM
  = (tx_read_bd bd_ptr CPPI_RAM).pl
Proof
  rpt strip_tac
  >> rewrite_tac [CPPI_RAMTheory.tx_read_bd_def]
  >> simp []
  >> ‘(read_bd_word bd_ptr 3w CPPI_RAM) =
      (read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM)’
      by (simp [CPPI_RAMTheory.read_bd_word_def, FLAGS_def])
  >> pat_x_rewrite_all ‘_=_’
  >> rewrite_tac [PL_def]
  >> WORD_DECIDE_TAC
QED

Theorem GET_TX_BUFFER_LENGTH:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ∧ ¬ state.nic.dead
  ⇒ get_tx_buffer_length pa state
    = ((tx_read_bd pa state.nic.regs.CPPI_RAM).bl , state)
Proof
  rpt strip_tac
  >> rewrite_tac [get_tx_buffer_length_def ]
  >> ‘CPPI_RAM_BYTE_PA (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + BOBL)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> simp_st [CPPI_RAM_READ, TX_BL_TO_READ_BD]
QED

Theorem GET_PACKET_LENGTH:
  ∀ pa state .
  BD_PTR_ADDRESS pa
  ∧ ¬ state.nic.dead
  ⇒ get_packet_length pa state
    = ((tx_read_bd pa state.nic.regs.CPPI_RAM).pl , state)
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [get_packet_length_def ]
  >> ‘CPPI_RAM_BYTE_PA (pa + FLAGS)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + FLAGS)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS pa”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> simp_st [CPPI_RAM_READ]
  >> simp [CPPI_RAMTheory.tx_read_bd_def]
  >> simp [PL_def, CPPI_RAMTheory.read_bd_word_def, FLAGS_def]
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem EOP_AND_FORM_EQ_EOP_BD_READ:
  ∀ p CPPI_RAM .
  (EOP && read_bd_word (p + 12w) 0w CPPI_RAM ≠ 0w)
  = ((tx_read_bd p CPPI_RAM).eop)
Proof
  rpt strip_tac
  >> simp [EOP_def, CPPI_RAMTheory.tx_read_bd_def, CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem SOP_AND_FORM_EQ_EOP_BD_READ:
  ∀ p CPPI_RAM .
  (SOP && read_bd_word (p + 12w) 0w CPPI_RAM ≠ 0w)
  = ((tx_read_bd p CPPI_RAM).sop)
Proof
  rpt strip_tac
  >> simp [SOP_def, CPPI_RAMTheory.tx_read_bd_def, CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem FST_IS_EOP_IMPL:
  ∀ p state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS p
  ∧ ¬ FST (is_eop p state)
  ⇒ (¬ (tx_read_bd p state.nic.regs.CPPI_RAM).eop)
Proof
  rpt strip_tac
  >> Q.PAT_UNDISCH_TAC ‘¬FST _’
  >> simp_st [IS_EOP]
QED

Theorem FST_IS_EOP_EQ:
  ∀ p state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS p
  ⇒ FST (is_eop p state)
  = (tx_read_bd p state.nic.regs.CPPI_RAM).eop
Proof
  rpt strip_tac
  >> simp_st [IS_EOP]
QED

Theorem FST_IS_SOP_IMPL:
  ∀ p state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS p
  ∧ ¬ FST (is_sop p state)
  ⇒ ¬ (tx_read_bd p state.nic.regs.CPPI_RAM).sop
Proof
  rpt strip_tac
  >> Q.PAT_UNDISCH_TAC ‘¬FST _’
  >> simp_st [IS_SOP]
QED

Theorem FST_IS_SOP_EQ:
  ∀ p state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS p
  ⇒ FST (is_sop p state) = (tx_read_bd p state.nic.regs.CPPI_RAM).sop
Proof
  rpt strip_tac
  >> simp_st [IS_SOP]
QED


Theorem READ_BD_WORD_IDX_3_EQ_READ_BD_WORD_ADD12:
  ∀ bd_ptr CPPI_RAM .
  read_bd_word bd_ptr 3w CPPI_RAM = read_bd_word (bd_ptr + 12w) 0w CPPI_RAM
Proof
  rpt strip_tac
  >> simp [CPPI_RAMTheory.read_bd_word_def]
QED

Theorem READ_BD_WORD_IDX_N_EQ_READ_BD_WORD_ADD4xN:
  ∀ bd_ptr CPPI_RAM n .
  read_bd_word bd_ptr n CPPI_RAM = read_bd_word (bd_ptr + (4w * n)) 0w CPPI_RAM
Proof
  rpt strip_tac
  >> simp [CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem TX_BO_TO_TX_READ:
  ∀ bd_ptr CPPI_RAM .
    TX_BO && read_bd_word bd_ptr 2w CPPI_RAM ≫ 16
  =  (tx_read_bd bd_ptr CPPI_RAM).bo
Proof
  rpt strip_tac
  >> simp [CPPI_RAMTheory.tx_read_bd_def, TX_BO_def]
  >> Q.ABBREV_TAC ‘x = (read_bd_word bd_ptr 2w CPPI_RAM)’
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem TX_BL_TO_TX_READ:
  ∀ bd_ptr CPPI_RAM .
  TX_BL && read_bd_word bd_ptr 2w CPPI_RAM
  = (tx_read_bd bd_ptr CPPI_RAM).bl
Proof
    rpt strip_tac
  >> simp [CPPI_RAMTheory.tx_read_bd_def, TX_BL_def]
  >> Q.ABBREV_TAC ‘x = (read_bd_word bd_ptr 2w CPPI_RAM)’
  >> blastLib.BBLAST_PROVE_TAC
QED

val _ = export_theory();
