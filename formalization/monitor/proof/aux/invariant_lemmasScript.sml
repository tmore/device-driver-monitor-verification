(*

Author: Tomas Möre 2020

Contains lots of lemmas about the invariant.

*)
open HolKernel wordsLib listTheory boolLib bossLib stateTheory;

open bd_queueTheory;
open monitor_invariantTheory;
open monitor_stateTheory;
open monitorUtils;
open address_lemmasTheory;
open handlersTheory;
open read_utilsTheory;

val _ = new_theory "invariant_lemmas";


Theorem MON_INV_NIC_NOT_DEAD_lem:
  ∀ state .
    MONITOR_INVARIANT state ⇒ ¬ state.nic.dead
Proof
  strip_tac
  >> fs [MONITOR_INVARIANT_def
         , nicInvariantTheory.NIC_INVARIANT_def
         , nicInvariantTheory.NIC_STATE_DEFINED_def
        ]
QED

Theorem MON_INV_HAS_TX0_QUEUE_lem:
  ∀ state .
  MONITOR_INVARIANT state ⇒
  ∃ tx0_queue . WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
Proof
  strip_tac
  >> fs [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def]
  >> METIS_TAC []
QED


Theorem MON_INV_HAS_RX0_QUEUE_lem:
  ∀ state .
  MONITOR_INVARIANT state ⇒
  ∃ rx0_queue . WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
Proof
  strip_tac
  >> fs [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def]
  >> METIS_TAC []
QED



Theorem WELL_FORMED_BD_QUEUE_HEAD_EQ_START:
  ∀ h q bd_ptr  state .
  WELL_FORMED_BD_QUEUE (h :: q) bd_ptr state
  ⇒ h = bd_ptr
Proof
  rpt strip_tac
  >> fs [WELL_FORMED_BD_QUEUE_def
         , bd_queueTheory.BD_QUEUE_def]
QED

Theorem WELL_FORMED_BD_QUEUE_UNIQUE:
  ∀ q q' bd_ptr state .
  WELL_FORMED_BD_QUEUE q bd_ptr state
  ∧ WELL_FORMED_BD_QUEUE q' bd_ptr state
  ⇒ q = q'
Proof
  NTAC 4 strip_tac
  >> rewrite_tac [WELL_FORMED_BD_QUEUE_def]
  >> METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
QED

Theorem WELL_FORMED_BD_START_MEM:
  ∀ bd_ptr queue state .
  bd_ptr ≠ 0w
  ∧ WELL_FORMED_BD_QUEUE queue bd_ptr state
  ⇒ MEM bd_ptr queue
Proof
  rpt strip_tac
  >> fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def]
  >> Cases_on ‘queue’
  >> fs [ bd_queueTheory.BD_QUEUE_def]
QED

Theorem IN_ALPHA_RANGE_OF_BD_PTR_ADDRESS_IS_CPPI_RAM_PA:
  ∀ p1 p2 .
    IN_ALPHA_RANGE_OF p2 p1
  ∧ BD_PTR_ADDRESS p2
  ⇒ CPPI_RAM_BYTE_PA p1
Proof
  strip_tac >> strip_tac
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem BD_QUEUE_INVARIANT_UNPACK:
  ∀ tx0_queue rx0_queue state .
    MONITOR_INVARIANT state
    ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
    ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
    ⇒ NON_OVERLAPPING_BD_QUEUES tx0_queue rx0_queue
       ∧ BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha
       ∧ (state.nic.tx.sop_bd_pa = 0w ∨ MEM state.nic.tx.sop_bd_pa tx0_queue)
       ∧ (state.nic.rx.sop_bd_pa = 0w ∨ MEM state.nic.rx.sop_bd_pa rx0_queue)
Proof
  NTAC 4 strip_tac
  >> REWRITE_ASSUMS_TAC MONITOR_INVARIANT_def
  >> REWRITE_ASSUMS_TAC BD_QUEUE_INVARIANT_def
  >> fs []
  >> ‘tx0_queue = tx0_queue'’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_UNIQUE]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘rx0_queue = rx0_queue'’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_UNIQUE]
  >> pat_x_rewrite_all ‘_=_’
  >> fs []
QED

Theorem WELL_FORMED_HEAD_BD_PTR:
  ∀ bd_ptr queue state .
  bd_ptr ≠ 0w
  ∧ WELL_FORMED_BD_QUEUE queue bd_ptr state
  ⇒ BD_PTR_ADDRESS bd_ptr
Proof
  rpt strip_tac
  >> Cases_on ‘queue’
  >- fs [bd_queueTheory.BD_QUEUE_def, WELL_FORMED_BD_QUEUE_def]
  >> fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def
         , EVERY_DEF]
  >> METIS_TAC []
QED

Theorem WELL_FORMED_ZERO_QUEUE:
  ∀ state .
    WELL_FORMED_BD_QUEUE [] 0w state
Proof
  strip_tac
  >> simp [WELL_FORMED_BD_QUEUE_def
           , bd_queueTheory.BD_QUEUE_def
           , LINKED_REL_def
          ]
QED

Theorem WELL_FORMED_ZERO_START_QUEUE_IS_ZERO:
  ∀ queue state .
  WELL_FORMED_BD_QUEUE queue 0w state
  ⇒ queue = []
Proof
  rpt strip_tac
  >> fs [WELL_FORMED_BD_QUEUE_def]
  >> Cases_on ‘queue’
  >> fs [bd_queueTheory.BD_QUEUE_def]
  >> METIS_TAC []
QED

Theorem WELL_FORMED_SINGLETON_NEXT_BD_PTR_ZERO:
  ∀ bd_ptr state .
  ¬ state.nic.dead
   ∧ WELL_FORMED_BD_QUEUE [bd_ptr] bd_ptr state
  ⇒ get_next_descriptor_pointer bd_ptr state = (0w , state)
Proof
  rpt strip_tac
  >> fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def
         , BD_PTR_ADDRESS_def]
  >> simp_st [get_next_descriptor_pointer_def
              ,  NEXT_DESCRIPTOR_POINTER_def
              , read_nic_register_def
              , register_readTheory.read_register_def
              , CPPI_RAM_BYTE_PA_lemma
              , system_state_component_equality
             ]
  >> fs [bdTheory.read_ndp_def]
QED

Theorem WELL_FORMED_MORE_THEN2_NEXT_BD_PTR:
  ∀ h h' queue state .
    ¬ state.nic.dead
  ∧ WELL_FORMED_BD_QUEUE (h::h'::queue) h state
  ⇒ get_next_descriptor_pointer h state =
  (h' , state)
Proof
  rpt strip_tac
  >> fs [WELL_FORMED_BD_QUEUE_def
         , get_next_descriptor_pointer_def
         ,  NEXT_DESCRIPTOR_POINTER_def
         , BD_PTR_ADDRESS_def
         , read_nic_register_def
         , register_readTheory.read_register_def
         , CPPI_RAM_BYTE_PA_lemma
         , system_state_component_equality
         , BD_QUEUE_def
         , bdTheory.read_ndp_def
        ]
QED


Theorem FST_get_next_descriptor_pointer_MONITOR_INDEPENDAT:
  ∀ state monitor bd_ptr .
  FST (get_next_descriptor_pointer bd_ptr state)
  = FST (get_next_descriptor_pointer bd_ptr (state with monitor := monitor))
Proof
  NTAC 3 strip_tac
  >> simp [ get_next_descriptor_pointer_def , read_nic_register_def ]
  >> Cases_on ‘(read_register ARB (bd_ptr + NEXT_DESCRIPTOR_POINTER) state.nic)’
  >> simp []
QED


Theorem WELL_FORMED_BD_QUEUE_ALPHA_INDEPENDANT:
  ∀ bd_queue bd_ptr state alpha .
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr (state with monitor := state.monitor with alpha := alpha) =
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr state
Proof
  rewrite_tac [WELL_FORMED_BD_QUEUE_def]
  >> NTAC 4 strip_tac
  >> fs []
  >> Ho_Rewrite.ONCE_REWRITE_TAC [
       FST_get_next_descriptor_pointer_MONITOR_INDEPENDAT
       , IS_EOP_ALPHA_INDEPENDANT]
  >> simp []
QED


Theorem GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE:
  ∀ bd_ptr state .
  (BD_PTR_ADDRESS bd_ptr ∧ ¬state.nic.dead)
  ⇒ (get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM, state)
Proof
  NTAC 3 strip_tac
  >> fs [ BD_PTR_ADDRESS_def
           , get_next_descriptor_pointer_def
           , NEXT_DESCRIPTOR_POINTER_def
           , read_nic_register_def
           , register_readTheory.read_register_def
           , CPPI_RAM_BYTE_PA_lemma
           , bdTheory.read_ndp_def
           , system_state_component_equality
           ]
QED

Theorem WELL_FORMED_BD_QUEUE_TAIL:
  ∀ bd_queue bd_ptr state  .
  (¬state.nic.dead ∧ WELL_FORMED_BD_QUEUE (bd_ptr :: bd_queue) bd_ptr state)
  ⇒ WELL_FORMED_BD_QUEUE bd_queue (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [ WELL_FORMED_BD_QUEUE_def
                   , bd_queueTheory.BD_QUEUE_def ]
  >> strip_tac
  >> fs []
QED



(* BD_PTR_ADDRESS is slightly stronger then needed *)
Theorem get_next_descriptor_pointer_CPPI_RAM_DEPENDANT:
  ∀ bd_ptr state state' .
  (state.nic.dead = state'.nic.dead
   ∧ state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM
   ∧ BD_PTR_ADDRESS bd_ptr)
  ⇒  FST (get_next_descriptor_pointer bd_ptr state)
     = FST (get_next_descriptor_pointer bd_ptr state')
Proof
  NTAC 4 strip_tac
  >> rewrite_tac [ get_next_descriptor_pointer_def
                  , NEXT_DESCRIPTOR_POINTER_def
                  , read_nic_register_def
                  , register_readTheory.read_register_def]
  >> asm_rewrite_tac []
  >> fs [BD_PTR_ADDRESS_def, CPPI_RAM_BYTE_PA_lemma]
  >> Cases_on ‘state'.nic.dead’
  >> fs []
QED

(*

For any two states, if one has a well_formed queue and they have the same then
so does the other

*)
Theorem WELL_FORMED_CPPI_RAM_DEPENDANT:
  ∀ bd_ptr bds state state' .
  (state.nic.dead = state'.nic.dead
   ∧ state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM
   ∧ WELL_FORMED_BD_QUEUE bds bd_ptr state)
  ⇒ WELL_FORMED_BD_QUEUE bds bd_ptr state'
Proof
  NTAC 5 strip_tac
  >> Q.PAT_UNDISCH_TAC ‘WELL_FORMED_BD_QUEUE _ _ _’
  >> rewrite_tac [WELL_FORMED_BD_QUEUE_def]
  >> simp []
  >> strip_tac
  >> UNDISCH_ALL_TAC
  >> Q.SPEC_TAC (‘bd_ptr’, ‘bd_ptr’)
  >> Induct_on ‘bds’
  >- (fs [])
  >> rpt strip_tac
  >> fs []
  >> METIS_TAC [IS_EOP_CPPI_RAM_DEPENDANT, BD_QUEUE_def, IS_EOP_CPPI_RAM_DEPENDANT]
QED

Theorem WELL_FORMED_BD_QUEUE_MONITOR_INDEPENDANT:
  ∀ bd_queue bd_ptr state monitor .
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr (state with monitor := monitor) =
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr state
Proof
  rpt strip_tac
  >> EQ_TAC
  >> strip_tac
  >> ‘state.nic.dead = (state with monitor := monitor).nic.dead’
      by fs []
  >> ‘state.nic.regs.CPPI_RAM = (state with monitor := monitor).nic.regs.CPPI_RAM’
      by fs []
  >> METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
QED


Theorem BD_QUEUE_INVARIANT_CPPI_AND_QUEUE_DEPENDANT:
  ∀ state state' .
  ¬ state.nic.dead
  ∧ ¬ state'.nic.dead
  ∧ BD_QUEUE_INVARIANT state
  ∧ state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM
  ∧ state.monitor.tx0_active_queue = state'.monitor.tx0_active_queue
  ∧ state.monitor.rx0_active_queue = state'.monitor.rx0_active_queue
  ∧ state.nic.tx.sop_bd_pa = state'.nic.tx.sop_bd_pa
  ∧ state.nic.rx.sop_bd_pa = state'.nic.rx.sop_bd_pa
  ∧ state.nic.rx.current_bd_pa = state'.nic.rx.current_bd_pa
  ∧ state.monitor.alpha = state'.monitor.alpha
  ⇒
  BD_QUEUE_INVARIANT state'
Proof
  NTAC 3 strip_tac
  >> fs [BD_QUEUE_INVARIANT_def]
  >> Q.EXISTS_TAC ‘tx0_queue’
  >> Q.EXISTS_TAC ‘rx0_queue’
  >> ‘state.nic.dead = state'.nic.dead’ by fs []
  >> specl_imp_res_tac
       [‘state'.monitor.tx0_active_queue’, ‘tx0_queue’, ‘state’, ‘state'’]
       WELL_FORMED_CPPI_RAM_DEPENDANT
  >> specl_imp_res_tac
       [‘state'.monitor.rx0_active_queue’, ‘rx0_queue’, ‘state’, ‘state'’]
       WELL_FORMED_CPPI_RAM_DEPENDANT
  >> simp []
QED

(*

Assumes the goal is BD_QUEUE_INVAIRANT called with a state, that state is used
as the target goal. It is assumed the original state is callsed 'state'


*)

Theorem IN_QUEUE_RANGE_IMP_EXISTS_QUEUE_BD_PTR:
  ∀ bd_ptr queue .
  IN_QUEUE_RANGE bd_ptr queue
  ⇒ ∃ queue_bd_ptr . MEM queue_bd_ptr queue ∧ IN_ALPHA_RANGE_OF queue_bd_ptr bd_ptr
Proof
  strip_tac
  >> Induct
  >- fs [IN_QUEUE_RANGE_def]
  >> strip_tac
  >> Cases_on ‘IN_ALPHA_RANGE_OF h bd_ptr’
  >- (fs [IN_QUEUE_RANGE_def]
      >> Q.EXISTS_TAC ‘h’
      >>  fs[]
     )
  >> fs [IN_QUEUE_RANGE_def]
  >> METIS_TAC []
QED

Theorem IN_QUEUE_RANGE_APPEND_LEFT:
  ∀ p a b .
  IN_QUEUE_RANGE p a
  ⇒ IN_QUEUE_RANGE p (a ++ b)
Proof
  Induct_on ‘a’
  >- (simp [IN_QUEUE_RANGE_def])
  >> rpt strip_tac
  >> fs [IN_QUEUE_RANGE_def]
QED

Theorem IN_QUEUE_RANGE_APPEND_RIGHT:
  ∀ p a b .
  IN_QUEUE_RANGE p b
  ⇒ IN_QUEUE_RANGE p (a ++ b)
Proof
  Induct_on ‘a’
  >- (simp [IN_QUEUE_RANGE_def])
  >> rpt strip_tac
  >> fs [IN_QUEUE_RANGE_def]
QED

Theorem IN_QUEUE_RANGE_APPEND_NOT_IN_LEFT:
  ∀ p a b .
  IN_QUEUE_RANGE p (a ++ b)
  ∧ ¬ IN_QUEUE_RANGE p a
  ⇒ IN_QUEUE_RANGE p b
Proof
  Induct_on ‘a’
  >- (simp [IN_QUEUE_RANGE_def])
  >> rpt strip_tac
  >> fs [IN_QUEUE_RANGE_def]
QED

Theorem NOT_IN_QUEUE_RANGE_APPEND_NOT_IN_EITHER:
  ∀ p a b .
  ¬ IN_QUEUE_RANGE p (a ++ b)
  ⇒ ¬ IN_QUEUE_RANGE p a
    ∧ ¬ IN_QUEUE_RANGE p b
Proof
  Induct_on ‘a’
  >- (simp [IN_QUEUE_RANGE_def])
  >> rpt strip_tac
  >> fs [IN_QUEUE_RANGE_def]
  >> METIS_TAC []
QED

Theorem WELL_FORMED_BD_QUEUE_TAIL':
  ∀ bd_ptr bd_ptr' queue state .
  WELL_FORMED_BD_QUEUE (bd_ptr :: bd_ptr' :: queue) bd_ptr state ⇒
  WELL_FORMED_BD_QUEUE (bd_ptr' :: queue) bd_ptr' state
Proof
  rpt strip_tac
  >> fs [bd_queueTheory.BD_QUEUE_def, LINKED_REL_def, WELL_FORMED_BD_QUEUE_def]
QED

Theorem WELL_FORMED_BD_QUEUE_MEM:
  ∀ start_ptr queue other_pointer state .
  WELL_FORMED_BD_QUEUE queue start_ptr state
  ∧ MEM other_pointer queue
  ⇒ ∃ queue' . WELL_FORMED_BD_QUEUE queue' other_pointer state
                ∧ (∀ p . MEM p queue' ⇒ MEM p queue)
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> fs [WELL_FORMED_BD_QUEUE_def]
     )
  >> rpt strip_tac
  >> Cases_on ‘other_pointer = start_ptr’
  >- ( Q.EXISTS_TAC ‘(h::queue)’
       >> fs []
       >> METIS_TAC []
     )
  >> Cases_on ‘queue’
  >- ( fs [WELL_FORMED_BD_QUEUE_def, LINKED_REL_def, bd_queueTheory.BD_QUEUE_def]
       >> METIS_TAC [])
  >> ‘∃ p . WELL_FORMED_BD_QUEUE (h'::t) h' state’
      by (fs [WELL_FORMED_BD_QUEUE_def, LINKED_REL_def, bd_queueTheory.BD_QUEUE_def] )
  >> ‘start_ptr = h’
      by (fs [WELL_FORMED_BD_QUEUE_def, LINKED_REL_def, bd_queueTheory.BD_QUEUE_def] )
  >> fs []
  >> ‘MEM other_pointer (h' :: t)’
      by (fs [])
  >> METIS_TAC []
QED


Theorem BD_QUEUE_MID:
  ∀ start_ptr pre ptr post CPPI_RAM .
  BD_QUEUE (pre ++ [ptr] ++ post) start_ptr CPPI_RAM
  ⇒ BD_QUEUE (ptr :: post) ptr CPPI_RAM
Proof
  Induct_on ‘pre’
  >- (rpt strip_tac >> fs [bd_queueTheory.BD_QUEUE_def])
  >> rpt strip_tac
  >> ‘BD_QUEUE (pre ++ [ptr] ++ post) (read_ndp h CPPI_RAM) CPPI_RAM’
      by fs [bd_queueTheory.BD_QUEUE_def]
  >> METIS_TAC []
QED

Theorem WELL_FORMED_BD_QUEUE_MID:
  ∀ h t q' h' t'  state .
    WELL_FORMED_BD_QUEUE (h::t) h state
    ∧ (h::t = q' ⧺ h'::t')
  ⇒ WELL_FORMED_BD_QUEUE (h'::t') h' state
Proof
  Induct_on ‘t’
  >- ( rpt strip_tac
       >> ‘h' = h’
           by  (Cases_on ‘q'’
                >> fs []
               )
       >> ‘q' = []’ by (Cases_on ‘q'’>>fs [])
       >> ‘t' = []’ by fs[]
       >> once_asm_rewrite_tac []
       >> METIS_TAC []
     )
  >> rpt strip_tac
  >> ‘WELL_FORMED_BD_QUEUE (h::t) h state’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_TAIL']
  >> Cases_on ‘q'’
  >- fs []
  >> ‘h::t = t'' ++ h''::t'’ by fs[]
  >> METIS_TAC []
QED


Theorem WELL_FORMED_HEAD_QUEUE_EMPTY:
  ∀ q h state .
  WELL_FORMED_BD_QUEUE (q ++ [h]) h state
  ⇒ q = []
Proof
  rpt strip_tac
  >> ‘ALL_DISTINCT (q ++ [h])’
      by ( fs [WELL_FORMED_BD_QUEUE_def]
           >> METIS_TAC [bd_queueTheory.BD_QUEUE_ALL_DISTINCT_lemma]
           )
  >> Cases_on ‘q’
  >- fs []
  >> ‘h' = h’ by fs [WELL_FORMED_BD_QUEUE_def
                     , bd_queueTheory.BD_QUEUE_def]
  >> fs [ALL_DISTINCT]
QED

Theorem BD_QUEUE_SEQUENTIAL_NOT_EQ:
  ∀ h h' queue CPPI_RAM .
    BD_QUEUE (h :: h' :: queue) h CPPI_RAM
    ⇒ h ≠ h'
Proof
  Induct_on ‘queue’
  >- (rpt strip_tac
      >> METIS_TAC [BD_QUEUE_def]
     )
  >> rpt strip_tac
  >> METIS_TAC [BD_QUEUE_def]
QED

Theorem BD_QUEUE_HEAD_NOT_IN_REST:
  ∀ bd_ptr queue CPPI_RAM .
  BD_QUEUE (bd_ptr::queue) bd_ptr CPPI_RAM
  ⇒
  ¬ MEM bd_ptr queue
Proof
  Induct_on ‘queue’
  >- (fs [])
  >> rpt strip_tac
  >> fs []
  >- ( METIS_TAC [BD_QUEUE_SEQUENTIAL_NOT_EQ] )
  >> ‘¬ MEM h queue’
      by METIS_TAC [BD_QUEUE_def]
  >> ‘∃ l1 l2 . queue = l1 ++ bd_ptr :: l2’
      by METIS_TAC [MEM_SPLIT]
  >> Q.PAT_X_ASSUM ‘_ = _’ REWRITE_ASSUMS_TAC
  >> ‘BD_QUEUE (bd_ptr :: l2) bd_ptr CPPI_RAM’
      by ( ‘BD_QUEUE (l1 ⧺ bd_ptr::l2) (read_ndp h CPPI_RAM) CPPI_RAM’
           by fs [BD_QUEUE_def]
           >> ‘(l1 ++  bd_ptr::l2) = (l1 ++ [bd_ptr] ++ l2)’
               by fs []
           >> Q.PAT_X_ASSUM ‘_ = _’ REWRITE_ASSUMS_TAC
           >> METIS_TAC [ BD_QUEUE_MID ]
         )
  >> ‘read_ndp bd_ptr CPPI_RAM = h’
      by fs [BD_QUEUE_def]
  >> ‘h ≠ 0w’
      by fs [BD_QUEUE_def]
  >> Cases_on ‘l2’
  >- fs [BD_QUEUE_def]
  >> ‘h' = h’
      by fs [BD_QUEUE_def]
  >> Q.PAT_X_ASSUM ‘_ = _’ REWRITE_ASSUMS_TAC
  >> ‘MEM h (l1 ⧺ bd_ptr::h::t)’
      by fs []
QED

Theorem BD_QUEUE_ALL_DISTINCT:
  ∀ queue bd_ptr CPPI_RAM .
  BD_QUEUE queue bd_ptr CPPI_RAM
  ⇒ ALL_DISTINCT queue
Proof
  Induct_on ‘queue’
  >- (fs [BD_QUEUE_def])
  >> rpt strip_tac
  >> ‘ALL_DISTINCT queue’ by (fs [bd_queueTheory.BD_QUEUE_def] >> METIS_TAC [])
  >> simp []
  >> ‘h = bd_ptr’ by fs [BD_QUEUE_def]
  >> METIS_TAC [BD_QUEUE_HEAD_NOT_IN_REST]
QED

Theorem NON_OVERLAPPING_BD_QUEUES_IMPL_NO_BD_LIST_OVERLAP:
  ∀ q1 q2 .
  (∀p1 p2. MEM p1 q1 ∧ MEM p2 q2 ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒ NO_BD_LIST_OVERLAP q1 q2
Proof
  rpt strip_tac
  >> fs [bd_listTheory.NO_BD_LIST_OVERLAP_def
         , bd_listTheory.BD_NOT_OVERLAP_BDs_def
         , NON_OVERLAPPING_BD_QUEUES_def
         , EVERY_MEM
         , bdTheory.BDs_OVERLAP_def]
  >> rpt strip_tac
  >> ‘word_abs (bd_pa2 + -1w * bd_pa) ≥ 16w’
      by (METIS_TAC [])
  >> Q.PAT_UNDISCH_TAC ‘word_abs (_) ≥ 16w’
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem NON_OVERLAP_IMPL_NON_BD_LIST_OVERLAP:
  ∀ q .
  (∀ p1 p2 . MEM p1 q ∧ MEM p2 q ∧ p1 ≠ p2 ⇒ word_abs(p1 - p2) >= 16w)
  ⇒ ¬ BD_LIST_OVERLAP q
Proof
  rpt strip_tac
  >> fs [bd_listTheory.BD_LIST_OVERLAP_def
         , bdTheory.BDs_OVERLAP_def]
  >> ‘word_abs (bd_pa1 - bd_pa2) ≥ 16w’
      by fs []
  >> UNDISCH_ALL_TAC
  >> rewrite_tac [wordsTheory.word_abs_def]
  >> COND_CASES_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BD_QUEUE_MEM:
  ∀ p bd_ptr queue cppi_ram .
  MEM p queue
  ∧ BD_QUEUE queue bd_ptr cppi_ram
  ⇒ ∃ q . (BD_QUEUE q p cppi_ram ∧ ∀ p . MEM p q ⇒ MEM p queue)
Proof
  Induct_on ‘queue’
  >- simp [bd_queueTheory.BD_QUEUE_def]
  >> rpt strip_tac
  >> fs [bd_queueTheory.BD_QUEUE_def]
  >- (Q.EXISTS_TAC ‘(h::queue)’ >> fs [bd_queueTheory.BD_QUEUE_def] >> METIS_TAC [])
  >> METIS_TAC []
QED

val _ = export_theory();
