open HolKernel  bossLib;
open helperTactics;
open computeLib;
open state_transformer_extTheory;
open state_transformerTheory;
open pairTheory;
open combinTheory;
open computeLib;

signature monitorUtils =
sig


  val conj_list :  thm list -> thm;
  val pat_assums_then :  term list -> (thm list -> tactic) -> tactic;
  val specl_assume_tac :  Q.tmquote list -> thm -> tactic;
  val specl_imp_res_tac : Q.tmquote list -> thm -> tactic;
  val specl_fs_tac :  Q.tmquote list -> thm -> tactic;
  val specl_simp_tac :  Q.tmquote list -> thm -> tactic;
  val specl_rewrite_tac :  Q.tmquote list -> thm -> tactic;
  val specl_assume_mp_tac :  Q.tmquote list -> term list -> thm -> tactic;

  val st_def_list : thm list;

  val simp_st :  thm list -> tactic;
  val fs_st :  thm list -> tactic;
  val monitor_state_simp_list : thm list;
  val goal_term : (term -> tactic) -> term list * term -> goal list * validation;
  val Cases_on_goal : (term -> term) -> term list * term -> goal list * validation;
  val is_call_to :  term -> term -> bool;
  val is_get_next_descriptor :  term -> bool;
  val find_nth_term :  int -> (term -> bool) -> term -> term;
  val is_state_monitor_upd :  term -> bool;
  val with_goal :  ('a -> 'b * 'a -> 'c) -> 'b * 'a -> 'c;
  val is_monitor_upd :  term -> bool;
  val X_RW_ASM_TAC :  term -> thm -> tactic;
  val REWRITE_ASSUMS_TAC :  thm -> goal -> goal list * validation;
  val pat_x_rewrite_all : Q.tmquote -> tactic;
  val UNDISCH_ALL_TAC :  term list * term -> goal list * validation;
  val fold_tac : ('a -> (goal, thm) gentactic) -> 'a list -> tactic;
end
