structure invariantTactics :> invariantTactics =
struct
  open helperTactics;
  open HolKernel wordsLib listTheory boolLib bossLib;

  open state_transformerTheory state_transformer_extTheory;

  open combinTheory;
  open computeLib;

  open pairTheory;
  open computeLib;
  open combinTheory;
  open invariant_lemmasTheory;
  open monitorUtils;
  open monitor_stateTheory monitor_invariantTheory handlersTheory;
  val bd_queue_invariant_tac =
       with_goal (fn t =>
                  let val target_state = snd (dest_comb t);
                      val eval_ts_eq = EVAL target_state;
                      val eval_ts = snd (dest_comb (concl eval_ts_eq))
                  in rewrite_tac [eval_ts_eq]
                     >> Q.ABBREV_TAC ‘state' = ^(eval_ts)’
                     >> ‘¬ state.nic.dead’ by METIS_TAC [ MON_INV_NIC_NOT_DEAD_lem]
                     >> ‘¬ state'.nic.dead’
                       by (Q.UNABBREV_TAC ‘state'’ >> fs [] >> METIS_TAC [])
                     >> ‘state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC >> METIS_TAC [])
                     >> ‘state.monitor.rx0_active_queue = state'.monitor.rx0_active_queue’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC >> METIS_TAC [])
                     >> ‘state.monitor.tx0_active_queue = state'.monitor.tx0_active_queue’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC >> METIS_TAC [])
                     >> ‘state.monitor.alpha = state'.monitor.alpha’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC >> METIS_TAC [])
                     >> ‘state.nic.tx.sop_bd_pa = state'.nic.tx.sop_bd_pa’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC >> METIS_TAC [])
                     >> ‘state.nic.rx.sop_bd_pa = state'.nic.rx.sop_bd_pa’
                         by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC  >> METIS_TAC [])
                     >> ‘state.nic.rx.current_bd_pa = state'.nic.rx.current_bd_pa’
                        by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC  >> METIS_TAC [])
                     >> ‘BD_QUEUE_INVARIANT state’
                         by (fs [MONITOR_INVARIANT_def] >> METIS_TAC [])
                     >> METIS_TAC [BD_QUEUE_INVARIANT_CPPI_AND_QUEUE_DEPENDANT]
                  end
                 );
end
