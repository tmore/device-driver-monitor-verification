(*

Author: Tomas Möre 2020


Contains utility functions and tactics used all over.

*)

structure monitorUtils :> monitorUtils =
struct
  open helperTactics;
  open HolKernel wordsLib listTheory boolLib bossLib;

  open state_transformerTheory state_transformer_extTheory;
  open monitor_stateTheory;
  open combinTheory;
  open computeLib;

  open pairTheory;
  open computeLib;
  open combinTheory;

  (* Takes a list of theorems and conjuncts them *)
  fun conj_list [] = raise (Fail "Can't create theorem from nothing")
    | conj_list [ x ] = x
    | conj_list (x :: xs) = CONJ x (conj_list xs)

  (* Takes a list of patterns, attempts to get then via pat assum and if succesfulls gives them as arugments to the function*)
  fun pat_assums_then [] f = raise (Fail "MUST match something!")
    | pat_assums_then (x :: xs) f =
      let fun loop thms [] = f (rev thms)
            | loop thms (p :: ps) = PAT_ASSUM p (fn t => loop (t :: thms) ps)
      in loop [] (x :: xs)
      end;

  fun specl_assume_tac l thm = Q.SPECL_THEN l assume_tac thm;
  fun specl_imp_res_tac l thm = Q.SPECL_THEN l imp_res_tac thm;
  fun specl_fs_tac l thm = Q.SPECL_THEN l (fn t => fs [t]) thm;
  fun specl_simp_tac l thm = Q.SPECL_THEN l (fn t => simp [t]) thm;
  fun specl_rewrite_tac l thm = Q.SPECL_THEN l (fn t => rewrite_tac [t]) thm;
  fun specl_assume_mp_tac l pats thm =
      Q.SPECL_THEN l (fn t => pat_assums_then pats (fn thms => assume_tac (MP t (conj_list thms) ))) thm;


  (* List of the definitions needed to sim*)
  val st_def_list = [ functor_map_def
                    , whenMM_def
                    , whenM_def
                    , notM_def
                    , andM_def
                    , orM_def
                    , ifM_def
                    , return_def
                    , IGNORE_BIND_DEF
                    , BIND_DEF
                    , UNCURRY_DEF
                    , o_DEF
                    , UNIT_DEF
                    ];

  (* Used to simplify monaic expressions *)

  fun simp_st l = simp (l @ st_def_list);
  fun fs_st l = fs (l @ st_def_list);

  val monitor_state_simp_list =
      [ get_tx0_active_queue_def
      , set_tx0_active_queue_def
      , get_rx0_active_queue_def
      , set_rx0_active_queue_def
      , get_initialized_def
      , set_initialized_def
      , get_tx0_hdp_initialized_def
      , set_tx0_hdp_initialized_def
      , get_rx0_hdp_initialized_def
      , set_rx0_hdp_initialized_def
      , get_tx0_cp_initialized_def
      , set_tx0_cp_initialized_def
      , get_rx0_cp_initialized_def
      , set_rx0_cp_initialized_def
      , get_tx0_tearingdown_def
      , set_tx0_tearingdown_def
      , get_rx0_tearingdown_def
      , set_rx0_tearingdown_def
      , get_alpha_def
      , set_alpha_def
      , get_recv_bd_nr_blocks_def
      , set_recv_bd_nr_blocks_def
      ]

  (* Fixpoint tactic using the goal term *)
  fun goal_term (f: term -> tactic) (s,g) = f g (s,g);


  fun is_call_to p term =
     if is_comb term
     then is_call_to p (fst (dest_comb term))
     else term_eq p term

  fun Cases_on_goal f (asl, w) =
      Cases_on ‘^(f w)’ (asl, w)

  fun is_get_next_descriptor t =
      is_comb t andalso
      is_comb (fst (dest_comb t)) andalso
      term_eq (fst (dest_comb (fst (dest_comb t)))) “get_next_descriptor_pointer”;

  fun find_nth_term n f t =
     let fun idx 0 (x :: _) =  x
           | idx 0 [] = raise Domain
           | idx n (_ :: xs) = idx (n - 1) xs
           | idx n [] = raise Domain
     in
         idx n (rev (find_terms f t))
     end;
  fun with_goal f (asl , w) = (f w) (asl , w)

  (* this is hacky as f *)
  fun is_state_monitor_upd t =
     is_comb t andalso
     is_comb (fst (dest_comb t)) andalso
     term_eq (fst (dest_comb (fst (dest_comb t)))) “monitor_fupd”;

  (* gets the montior term *)
  fun is_monitor_upd t =
     is_comb t andalso
     term_eq (snd (dest_comb t)) “state.monitor”;

  fun X_RW_ASM_TAC pat rw =
     PAT_X_ASSUM
       pat
       (fn thm => ASSUME_TAC (REWRITE_RULE [rw] thm));


  fun REWRITE_ASSUMS_TAC rw ((asms , g) : goal) =
      let fun loop [] = ALL_TAC
            | loop (a :: ass) =
                   TRY (PAT_X_ASSUM a (fn thm => ASSUME_TAC (REWRITE_RULE [ rw ] thm)))
                   >> loop ass
      in
          loop (rev asms) ((asms , g) : goal)
      end;

  fun pat_x_rewrite_all pat =
           Q.PAT_X_ASSUM pat (fn t => rewrite_tac [t] >> REWRITE_ASSUMS_TAC t);

  fun UNDISCH_ALL_TAC (asm,g) = (MAP_EVERY UNDISCH_TAC asm)(asm,g);

  fun fold_tac f [] = ALL_TAC
    | fold_tac f (x :: xs) = f x >> fold_tac f xs;
end
