open HolKernel wordsLib listTheory boolLib bossLib stateTheory;

open monitor_invariantTheory;
open monitor_stateTheory;
open monitorUtils;
open address_lemmasTheory;

open register_writeTheory;
open register_readTheory;
open CPPI_RAMTheory;

open handlersTheory;

val _ = new_theory "write_utils";

Theorem CPPI_RAM_WRITE:
  ∀ pa val state .
  (CPPI_RAM_BYTE_PA pa ∧ WORD32_ALIGNED pa ∧ ¬ state.nic.dead)
  ⇒ write_nic_register pa val state =
  (()
   , state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
           write_32bit_word val pa state.nic.regs.CPPI_RAM
  )
Proof
  rpt strip_tac
  >> simp [write_nic_register_def
           , write_register_def
           , CPPI_RAM_BYTE_PA_lemma
           , write_cppi_ram_def
          ]
QED

Theorem BD_PTR_ADDRESS_CPPI_RAM_WRITE_FLAGS:
  ∀ pa val state .
  (BD_PTR_ADDRESS pa ∧ ¬ state.nic.dead)
  ⇒ write_nic_register (pa + 12w) val state
  = (()
   , state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
           write_32bit_word val (pa + 12w) state.nic.regs.CPPI_RAM
  )
Proof
  rpt strip_tac
  >> ‘CPPI_RAM_BYTE_PA (pa + 12w)’
      by (UNDISCH_ALL_TAC
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘WORD32_ALIGNED (pa + 12w)’
      by (UNDISCH_ALL_TAC
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> METIS_TAC [CPPI_RAM_WRITE]
QED

Theorem CPDMA_SOFT_RESET_WRITE:
  ∀ v state .
  ¬ state.nic.dead
  ⇒
  write_nic_register CPDMA_SOFT_RESET_PA v state =
  (()
   , state with nic := write_cpdma_soft_reset v state.nic )
Proof
  NTAC 3 strip_tac
  >> simp [ write_nic_register_def
            , register_writeTheory.write_register_def
            , CPDMA_SOFT_RESET_PA_lemmas]
QED

Theorem TX0_HDP_PA_WRITE:
  ∀ v state .
  ¬ state.nic.dead
  ⇒ write_nic_register TX0_HDP_PA v state =
  (()
   , state with nic := write_tx0_hdp ARB v state.nic
  )
Proof
  NTAC 3 strip_tac
  >> ‘¬ NOT_ZERO_CHANNEL_HDP_BYTE TX0_HDP_PA’
      by (EVAL_TAC)
  >> simp [ write_nic_register_def
            , register_writeTheory.write_register_def
            , TX0_HDP_PA_lemmas]
QED

Theorem WRITE_FLAGS_DOESN_NOT_CHANGE_NEXT_PTR:
  ∀ bd_ptr val CPPI_RAM .
  (read_bd_word bd_ptr 0w (write_32bit_word val (bd_ptr + 12w) CPPI_RAM)
   = read_bd_word bd_ptr 0w CPPI_RAM)
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [ read_bd_word_def]
  >> simp []
  >> ‘(write_32bit_word val (bd_ptr + 12w)
       CPPI_RAM  (pa_to_cppi_ram_offset bd_ptr))
     = CPPI_RAM (pa_to_cppi_ram_offset bd_ptr)’
     by (rewrite_tac [write_32bit_word_def]
         >> EVAL_TAC
         >> rpt (COND_CASES_TAC
              >- (asm_rewrite_tac []
                  >> UNDISCH_ALL_TAC
                  >> blastLib.BBLAST_PROVE_TAC
                 )
              >> asm_rewrite_tac [])
        )
  >> ‘(write_32bit_word val (bd_ptr + 12w) CPPI_RAM
       (pa_to_cppi_ram_offset (bd_ptr + 1w)))
      = (CPPI_RAM (pa_to_cppi_ram_offset (bd_ptr + 1w)))’
      by (rewrite_tac [write_32bit_word_def]
         >> EVAL_TAC
         >> rpt (COND_CASES_TAC
              >- (asm_rewrite_tac []
                  >> UNDISCH_ALL_TAC
                  >> blastLib.BBLAST_PROVE_TAC
                 )
              >> asm_rewrite_tac [])
         )
  >> ‘(write_32bit_word val (bd_ptr + 12w) CPPI_RAM
       (pa_to_cppi_ram_offset (bd_ptr + 3w)))
      = (CPPI_RAM (pa_to_cppi_ram_offset (bd_ptr + 3w)))’
      by (rewrite_tac [write_32bit_word_def]
         >> EVAL_TAC
         >> rpt (COND_CASES_TAC
              >- (asm_rewrite_tac []
                  >> UNDISCH_ALL_TAC
                  >> blastLib.BBLAST_PROVE_TAC
                 )
              >> asm_rewrite_tac [])
         )
  >> ‘(write_32bit_word val (bd_ptr + 12w) CPPI_RAM
       (pa_to_cppi_ram_offset (bd_ptr + 2w)))
      = (CPPI_RAM (pa_to_cppi_ram_offset (bd_ptr + 2w)))’
      by (rewrite_tac [write_32bit_word_def]
         >> EVAL_TAC
         >> rpt (COND_CASES_TAC
              >- (asm_rewrite_tac []
                  >> UNDISCH_ALL_TAC
                  >> blastLib.BBLAST_PROVE_TAC
                 )
              >> asm_rewrite_tac [])
         )
  >> asm_rewrite_tac []
QED

Theorem WRITE_ONLY_CHANGES_FOUR_BYTES:
  ∀ write_ptr read_ptr val CPPI_RAM .
  CPPI_RAM_BYTE_PA write_ptr
  ∧ CPPI_RAM_BYTE_PA read_ptr
  ∧ write_ptr < (CPPI_RAM_END − 3w)
    ∧ read_ptr < (CPPI_RAM_END − 3w)
   ∧ (word_abs(write_ptr - read_ptr) >= 4w)
  ⇒
  (read_bd_word read_ptr 0w (write_32bit_word val write_ptr CPPI_RAM)
   = read_bd_word read_ptr 0w CPPI_RAM)
Proof
  NTAC 4 strip_tac
  >> rewrite_tac [ read_bd_word_def]
  >> strip_tac
  >> simp []
  >> ‘(write_32bit_word val write_ptr CPPI_RAM
             (pa_to_cppi_ram_offset read_ptr))
     = (CPPI_RAM (pa_to_cppi_ram_offset read_ptr))’
     by (rewrite_tac [write_32bit_word_def]
         >> simp []
         >> ‘pa_to_cppi_ram_offset (write_ptr + 3w) ≠ (pa_to_cppi_ram_offset read_ptr)
             ∧ pa_to_cppi_ram_offset (write_ptr + 2w) ≠ (pa_to_cppi_ram_offset read_ptr)
             ∧ pa_to_cppi_ram_offset (write_ptr + 1w) ≠ (pa_to_cppi_ram_offset read_ptr)
             ∧ pa_to_cppi_ram_offset write_ptr ≠ (pa_to_cppi_ram_offset read_ptr)’
             by (EVAL_TAC
                 >> UNDISCH_ALL_TAC
                 >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                 , EVAL “CPPI_RAM_END”
                                 , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                 >> blastLib.BBLAST_PROVE_TAC
                )
         >> asm_rewrite_tac [APPLY_UPDATE_THM]
        )
  >> ‘(write_32bit_word val write_ptr CPPI_RAM
             (pa_to_cppi_ram_offset (read_ptr + 1w)))
     = (CPPI_RAM (pa_to_cppi_ram_offset (read_ptr + 1w)))’
     by (rewrite_tac [write_32bit_word_def]
         >> simp []
         >> ‘pa_to_cppi_ram_offset (write_ptr + 3w) ≠ (pa_to_cppi_ram_offset (read_ptr + 1w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 2w) ≠ (pa_to_cppi_ram_offset (read_ptr + 1w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 1w) ≠ (pa_to_cppi_ram_offset (read_ptr + 1w))
             ∧ pa_to_cppi_ram_offset write_ptr ≠ (pa_to_cppi_ram_offset (read_ptr + 1w))’
             by (EVAL_TAC
                 >> UNDISCH_ALL_TAC
                 >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                 , EVAL “CPPI_RAM_END”
                                 , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                 >> blastLib.BBLAST_PROVE_TAC
                )
         >> asm_rewrite_tac [APPLY_UPDATE_THM]
        )
  >> ‘(write_32bit_word val write_ptr CPPI_RAM
             (pa_to_cppi_ram_offset (read_ptr + 2w)))
     = (CPPI_RAM (pa_to_cppi_ram_offset (read_ptr + 2w)))’
     by (rewrite_tac [write_32bit_word_def]
         >> simp []
         >> ‘pa_to_cppi_ram_offset (write_ptr + 3w) ≠ (pa_to_cppi_ram_offset (read_ptr + 2w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 2w) ≠ (pa_to_cppi_ram_offset (read_ptr + 2w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 1w) ≠ (pa_to_cppi_ram_offset (read_ptr + 2w))
             ∧ pa_to_cppi_ram_offset write_ptr ≠ (pa_to_cppi_ram_offset (read_ptr + 2w))’
             by (EVAL_TAC
                 >> UNDISCH_ALL_TAC
                 >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                 , EVAL “CPPI_RAM_END”
                                 , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                 >> blastLib.BBLAST_PROVE_TAC
                )
         >> asm_rewrite_tac [APPLY_UPDATE_THM]
        )
  >> ‘(write_32bit_word val write_ptr CPPI_RAM
             (pa_to_cppi_ram_offset (read_ptr + 3w)))
     = (CPPI_RAM (pa_to_cppi_ram_offset (read_ptr + 3w)))’
     by (rewrite_tac [write_32bit_word_def]
         >> simp []
         >> ‘pa_to_cppi_ram_offset (write_ptr + 3w) ≠ (pa_to_cppi_ram_offset (read_ptr + 3w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 2w) ≠ (pa_to_cppi_ram_offset (read_ptr + 3w))
             ∧ pa_to_cppi_ram_offset (write_ptr + 1w) ≠ (pa_to_cppi_ram_offset (read_ptr + 3w))
             ∧ pa_to_cppi_ram_offset write_ptr ≠ (pa_to_cppi_ram_offset (read_ptr + 3w))’
             by (EVAL_TAC
                 >> UNDISCH_ALL_TAC
                 >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                 , EVAL “CPPI_RAM_END”
                                 , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                 >> blastLib.BBLAST_PROVE_TAC
                )
         >> asm_rewrite_tac [APPLY_UPDATE_THM]
        )
  >> asm_rewrite_tac []
QED


(* This proof is ugly AF but this is the easiest solution i could think of *)
Theorem WRITE_32BITS_DIFF_COMM:
  ∀ p1 p2 val1 val2 CPPI_RAM .
  CPPI_RAM_BYTE_PA p1
  ∧ CPPI_RAM_BYTE_PA p2
  ∧ p1 < (CPPI_RAM_END − 3w)
  ∧ p2 < (CPPI_RAM_END − 3w)
  ∧ (word_abs(p1 - p2) >= 4w)
  ⇒
  write_32bit_word val1 p1 (write_32bit_word val2 p2 CPPI_RAM)
   = (write_32bit_word val2 p2 (write_32bit_word val1 p1 CPPI_RAM))
Proof
  rpt strip_tac
  >> rewrite_tac [ write_32bit_word_def]
  >> simp []
  >> (‘CPPI_RAM⦇
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2
        ⦈ =
        CPPI_RAM⦇
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1

        ⦈’ by ( ‘ pa_to_cppi_ram_offset (p1) ≠ (pa_to_cppi_ram_offset (p2))
                ∧ pa_to_cppi_ram_offset (p1) ≠ (pa_to_cppi_ram_offset (p2 + 1w))
                ∧ pa_to_cppi_ram_offset (p1) ≠ (pa_to_cppi_ram_offset (p2 + 2w))
                ∧ pa_to_cppi_ram_offset (p1) ≠ (pa_to_cppi_ram_offset (p2 + 3w))
                ’
                by (EVAL_TAC
                    >> UNDISCH_ALL_TAC
                    >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                    , EVAL “CPPI_RAM_END”
                                    , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                    >> blastLib.BBLAST_PROVE_TAC
                )
           >> METIS_TAC [UPDATE_COMMUTES]
        ))
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_ = _’ (fn t => ALL_TAC)
  >> (‘CPPI_RAM⦇
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1
        ⦈ =
        CPPI_RAM⦇
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1
        ⦈’ by ( ‘ pa_to_cppi_ram_offset (p1 + 1w) ≠ (pa_to_cppi_ram_offset (p2))
                ∧ pa_to_cppi_ram_offset (p1 + 1w) ≠ (pa_to_cppi_ram_offset (p2 + 1w))
                ∧ pa_to_cppi_ram_offset (p1 + 1w) ≠ (pa_to_cppi_ram_offset (p2 + 2w))
                ∧ pa_to_cppi_ram_offset (p1 + 1w) ≠ (pa_to_cppi_ram_offset (p2 + 3w))
                ’
                by (EVAL_TAC
                    >> UNDISCH_ALL_TAC
                    >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                    , EVAL “CPPI_RAM_END”
                                    , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                    >> blastLib.BBLAST_PROVE_TAC
                )
           >> METIS_TAC [UPDATE_COMMUTES]
              ))
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_ = _’ (fn t => ALL_TAC)
  >> (‘CPPI_RAM⦇
         pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1
        ⦈ =
      CPPI_RAM⦇
         pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1

        ⦈’ by ( ‘ pa_to_cppi_ram_offset (p1 + 2w) ≠ (pa_to_cppi_ram_offset (p2))
                ∧ pa_to_cppi_ram_offset (p1 + 2w) ≠ (pa_to_cppi_ram_offset (p2 + 1w))
                ∧ pa_to_cppi_ram_offset (p1 + 2w) ≠ (pa_to_cppi_ram_offset (p2 + 2w))
                ∧ pa_to_cppi_ram_offset (p1 + 2w) ≠ (pa_to_cppi_ram_offset (p2 + 3w))
                ’
                by (EVAL_TAC
                    >> UNDISCH_ALL_TAC
                    >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                    , EVAL “CPPI_RAM_END”
                                    , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                    >> blastLib.BBLAST_PROVE_TAC
                )
           >> METIS_TAC [UPDATE_COMMUTES]
              )
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_ = _’ (fn t => ALL_TAC))
  >> (‘CPPI_RAM⦇
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1
        ⦈ =
      CPPI_RAM⦇
          pa_to_cppi_ram_offset (p2 + 3w) ↦ (31n >< 24n) val2;
          pa_to_cppi_ram_offset (p2 + 2w) ↦ (23n >< 16n) val2;
          pa_to_cppi_ram_offset (p2 + 1w) ↦ (15n >< 8n) val2;
          pa_to_cppi_ram_offset p2 ↦ (7n >< 0n) val2;
          pa_to_cppi_ram_offset (p1 + 3w) ↦ (31n >< 24n) val1;
          pa_to_cppi_ram_offset (p1 + 2w) ↦ (23n >< 16n) val1;
          pa_to_cppi_ram_offset (p1 + 1w) ↦ (15n >< 8n) val1;
          pa_to_cppi_ram_offset p1 ↦ (7n >< 0n) val1

        ⦈’ by ( ‘ pa_to_cppi_ram_offset (p1 + 3w) ≠ (pa_to_cppi_ram_offset (p2))
                ∧ pa_to_cppi_ram_offset (p1 + 3w) ≠ (pa_to_cppi_ram_offset (p2 + 1w))
                ∧ pa_to_cppi_ram_offset (p1 + 3w) ≠ (pa_to_cppi_ram_offset (p2 + 2w))
                ∧ pa_to_cppi_ram_offset (p1 + 3w) ≠ (pa_to_cppi_ram_offset (p2 + 3w))
                ’
                by (EVAL_TAC
                    >> UNDISCH_ALL_TAC
                    >> rewrite_tac [EVAL “CPPI_RAM_BYTE_PA write_ptr”
                                    , EVAL “CPPI_RAM_END”
                                    , EVAL “CPPI_RAM_BYTE_PA read_ptr”]
                    >> blastLib.BBLAST_PROVE_TAC
                )
           >> METIS_TAC [UPDATE_COMMUTES]
              )
  >> asm_rewrite_tac [])
QED


Theorem READ_BD_WORD_ON_WRITE_BD_WORD:
  ∀ bd_ptr val CPPI_RAM .
  read_bd_word bd_ptr 0w (write_32bit_word val bd_ptr CPPI_RAM)
  = val
Proof
  rpt strip_tac
  >> simp [CPPI_RAMTheory.read_bd_word_def
           , CPPI_RAMTheory.write_32bit_word_def
          ]
  >> simp [APPLY_UPDATE_THM]
  >> EVAL_TAC
  >> simp []
  >> ‘(12 >< 0) (bd_ptr + 3052396547w) ≠ ((12 >< 0) (bd_ptr + 3052396544w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> ‘(12 >< 0) (bd_ptr + 3052396546w) ≠
      ((12 >< 0) (bd_ptr + 3052396544w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> ‘(12 >< 0) (bd_ptr + 3052396545w) ≠
      ((12 >< 0) (bd_ptr + 3052396544w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> ‘(12 >< 0) (bd_ptr + 3052396547w) ≠
      ((12 >< 0) (bd_ptr + 3052396546w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> ‘(12 >< 0) (bd_ptr + 3052396547w) ≠
      ((12 >< 0) (bd_ptr + 3052396545w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> ‘(12 >< 0) (bd_ptr + 3052396546w) ≠
      ((12 >< 0) (bd_ptr + 3052396545w) : 13 word)’
      by blastLib.BBLAST_PROVE_TAC
  >> asm_rewrite_tac []
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem READ_BD_WORD_ON_WRITE_BD_WORD_INDEXED:
  ∀ bd_ptr n val CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ∧ (n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w)
  ⇒
  read_bd_word bd_ptr n (write_32bit_word val (bd_ptr + (n * 4w)) CPPI_RAM)
  = val
Proof
  ntac 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> simp [CPPI_RAMTheory.read_bd_word_def
           , CPPI_RAMTheory.write_32bit_word_def
          ]
  >> simp [APPLY_UPDATE_THM]
  >> EVAL_TAC
  >> simp []
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396547w) ≠ ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396544w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396546w) ≠
      ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396544w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396545w) ≠
      ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396544w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396547w) ≠
      ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396546w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396547w) ≠
      ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396545w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> ‘(12 >< 0) (bd_ptr + (4w * n) + 3052396546w) ≠
      ((12 >< 0) (bd_ptr + (n ≪ 2) + 3052396545w) : 13 word)’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> Q.PAT_UNDISCH_TAC ‘n = 0w ∨ n = 1w ∨ n = 2w ∨ n = 3w’
          >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> asm_rewrite_tac []
  >> ‘∀ b . (12 >< 0) (bd_ptr + 4w * n + b) =
      (12 >< 0) (bd_ptr + n ≪ 2 + b)’
      by blastLib.BBLAST_PROVE_TAC
  >> fs []
  >> blastLib.BBLAST_PROVE_TAC
QED


val _ = export_theory();
