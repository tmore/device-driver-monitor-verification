open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open state_transformer_extTheory;
open address_lemmasTheory;
open alpha_lemmasTheory;

open invariant_lemmasTheory;
open read_utilsTheory;
open monitorUtils;

val _ = new_theory "update_active_queueProof";

(*

The logical function that splits the lists into what is to be garbage collected and what is to be kep in queue.

Assumes all input pointers are valid.

Horrible definition, would be better with a mutually recrisive function, but HOL's termination checker is kind of weak sooo....

*)

Definition uaq_split_at_eop_def:
  (uaq_split_at_eop [] state = ([] , []))
  ∧ (uaq_split_at_eop (ptr :: ptrs) state =
     if ¬(FST (is_eop ptr state))
     then let (to_gc , rest) = uaq_split_at_eop ptrs state
     in (ptr :: to_gc , rest)
     else ( [] , ptr ::ptrs)
    )
End

Theorem UAQ_SPLIT_IS_EOP_SPLITS:
  ∀ bds state .
  FST (uaq_split_at_eop bds state) ++ SND (uaq_split_at_eop bds state) = bds
Proof
  Induct
  >- fs [uaq_split_at_eop_def]
  >> strip_tac
  >> fs [uaq_split_at_eop_def]
  >> Cases_on ‘(¬FST (is_eop h state))’
  >> fs []
  >> Cases_on ‘(uaq_split_at_eop bds state)’
  >> fs []
  >> ‘q = FST (uaq_split_at_eop bds state)’
      by fs[]
  >> ‘r = SND (uaq_split_at_eop bds state)’
      by (once_asm_rewrite_tac []
          >> simp []
         )
  >> METIS_TAC []
QED


Theorem UAQ_SPLIT_AT_EOP_REST_SMALLER:
  ∀ ptr ptrs state .
  (¬ (FST (is_eop ptr state)))
    ⇒ LENGTH (SND (uaq_split_at_eop (ptr :: ptrs) state)) < LENGTH (ptr :: ptrs)
Proof
  Induct_on ‘ptrs’
  >- (rpt strip_tac >> fs [uaq_split_at_eop_def])
  >> rpt strip_tac
  >> once_rewrite_tac [uaq_split_at_eop_def]
  >> simp []
  >> Cases_on ‘FST (is_eop h state)’
  >- fs [uaq_split_at_eop_def]
  >> ‘LENGTH (SND (uaq_split_at_eop (h::ptrs) state)) < LENGTH (h::ptrs)’
      by fs []
  >> Cases_on ‘(uaq_split_at_eop (h::ptrs) state)’
  >> fs []
QED

val uaq_split_defn = Hol_defn "uaq_split_rec"
  ‘(uaq_split [] tearingdown state = ([] , []))
  ∧ (uaq_split (ptr :: ptrs) tearingdown state =
        if ¬ (FST (is_released ptr state))
        then ([] , ptr :: ptrs)
        else if FST (is_td ptr state) ∧ tearingdown
        then (ptr :: ptrs , [])
        else  if ¬ (FST (is_eop ptr state))
        then let (non_eops , rest) = uaq_split_at_eop (ptr :: ptrs) state
             in (if rest = []
                 then (non_eops, rest)
                 else let  (to_gc , to_keep) = uaq_split (TL rest) tearingdown state
                      in (non_eops ++ [HD rest] ++ to_gc  , to_keep)
                )
        else let (to_gc_rest , to_keep_rest) = uaq_split ptrs tearingdown state
        in (ptr :: to_gc_rest , to_keep_rest))’;

val (uaq_split_def , uaq_split_ind) =
   Defn.tprove (  uaq_split_defn ,
                  WF_REL_TAC ‘measure (λ (l, _ , _) . LENGTH l)’
                  >> fs []
                  >> (Induct
                      >- (rpt strip_tac
                          >> fs [uaq_split_at_eop_def]
                          >> ‘(non_eops,rest) = ([ptr], [])’
                              by (asm_rewrite_tac [])
                          >> fs []
                         )
                      >> rpt strip_tac
                      >- (Q.PAT_UNDISCH_TAC ‘(non_eops,rest) = uaq_split_at_eop (ptr::h::ptrs) state’
                          >> once_rewrite_tac [uaq_split_at_eop_def]
                          >> simp []
                          >> Cases_on ‘FST (is_eop h state)’
                          >- simp [uaq_split_at_eop_def]
                          >> Cases_on ‘(uaq_split_at_eop (h::ptrs) state)’
                          >> ‘LENGTH (SND (uaq_split_at_eop (h::ptrs) state)) < LENGTH (h :: ptrs)’
                              by fs [UAQ_SPLIT_AT_EOP_REST_SMALLER]
                          >> ‘(SND (uaq_split_at_eop (h::ptrs) state)) = r’
                              by fs[]
                          >>  fs []
                          >> Cases_on ‘r’
                          >> fs []
                          )
                      >> Q.PAT_UNDISCH_TAC ‘(non_eops,rest) = uaq_split_at_eop (ptr::h::ptrs) state’
                      >> once_rewrite_tac [uaq_split_at_eop_def]
                      >> simp []
                      >> Cases_on ‘FST (is_eop h state)’
                      >- simp [uaq_split_at_eop_def]
                      >> Cases_on ‘(uaq_split_at_eop (h::ptrs) state)’
                      >> ‘LENGTH (SND (uaq_split_at_eop (h::ptrs) state)) < LENGTH (h :: ptrs)’
                          by fs [UAQ_SPLIT_AT_EOP_REST_SMALLER]
                      >> ‘(SND (uaq_split_at_eop (h::ptrs) state)) = r’
                          by fs[]
                      >> Cases_on ‘r’
                      >>  fs []
                      ));


Theorem UAQ_SPLIT_RESULT_EXISTS:
  ∀ bds tearingdown state .
  ∃ to_gc to_keep .
  (uaq_split bds tearingdown state) = (to_gc , to_keep)
Proof
  Cases_on ‘(uaq_split bds tearingdown state)’
  >> Q.EXISTS_TAC ‘q’
  >> Q.EXISTS_TAC ‘r’
  >> simp []
QED

Theorem UAQ_SPLIT_NULL:
  ∀ tearingdown state .
  (uaq_split [] tearingdown state) = ([] , [])
Proof
  simp [uaq_split_def]
QED

Definition UAQ_SPLIT_SPLITS_BASE_PROP:
  UAQ_SPLIT_SPLITS_BASE_PROP bds tearingdown state =
  ((FST (uaq_split bds tearingdown state))
   ++ (SND (uaq_split bds tearingdown state))
   = bds)
End

Theorem UAQ_SPLIT_SPLITS_BASE:
  (∀tearingdown state.
   UAQ_SPLIT_SPLITS_BASE_PROP [] tearingdown state)
Proof
  fs [UAQ_SPLIT_SPLITS_BASE_PROP ,uaq_split_def]
QED
(* uaq_split_ind *)
Theorem UAQ_SPLIT_SPLITS_INDUCT:
  (∀ptr ptrs tearingdown state.
             (∀non_eops rest.
                  ¬¬FST (is_released ptr state) ∧
                  ¬(FST (is_td ptr state) ∧ tearingdown) ∧
                  ¬FST (is_eop ptr state) ∧
                  (non_eops,rest) = uaq_split_at_eop (ptr::ptrs) state ∧
                  (rest ≠ []) ⇒
                  UAQ_SPLIT_SPLITS_BASE_PROP (TL rest) tearingdown state) ∧
             (¬¬FST (is_released ptr state) ∧
              ¬(FST (is_td ptr state) ∧ tearingdown) ∧
              ¬¬FST (is_eop ptr state) ⇒
              UAQ_SPLIT_SPLITS_BASE_PROP ptrs tearingdown state) ⇒
             UAQ_SPLIT_SPLITS_BASE_PROP (ptr::ptrs) tearingdown state)
Proof
  fs [UAQ_SPLIT_SPLITS_BASE_PROP]
  >> rpt strip_tac
  >> once_asm_rewrite_tac [uaq_split_def]
  >> Cases_on ‘¬(FST (is_released ptr state))’
  >> simp []
  >> Cases_on ‘(FST (is_td ptr state) ∧ tearingdown)’
  >> simp []
  >> Cases_on ‘(¬FST (is_eop ptr state))’
  >- ( simp []
       >> Cases_on ‘(uaq_split_at_eop (ptr::ptrs) state)’
       >> simp []
       >> Cases_on ‘r’
       >- (simp []
           >> specl_assume_tac
              [ ‘(ptr::ptrs)’, ‘state’]
              UAQ_SPLIT_IS_EOP_SPLITS
           >> ‘SND (uaq_split_at_eop (ptr::ptrs) state) = []’
               by fs []
           >> ‘FST (uaq_split_at_eop (ptr::ptrs) state) = q’
               by (once_asm_rewrite_tac []
                   >> simp []
                  )
           >> fs []
          )
       >> simp []
       >> Q.PAT_ASSUM ‘∀non_eops rest. _’
                        (fn t =>
                         specl_assume_tac
                         [‘q’,‘h::t’]
                         t
                        )
       >> ‘FST (uaq_split t tearingdown state)
           ⧺ SND (uaq_split t tearingdown state)
           = t’
           by fs []
       >> Q.ABBREV_TAC ‘delay = ¬(FST (is_td ptr state) ∧ tearingdown)’
       >> Cases_on ‘(uaq_split t tearingdown state)’
       >> simp []
       >> fs[]
       >> ‘q ⧺ [h] ⧺ q' ⧺ r = q ⧺ [h] ⧺ t’ by fs []
       >> asm_rewrite_tac []
       >> ‘q ⧺ [h] ⧺ t = q ++ h::t’ by fs[]
       >> asm_rewrite_tac []
       >> ‘ptr::ptrs = q ++ h::t’
           by (specl_assume_tac
              [‘ptr::ptrs’, ‘state’]
              UAQ_SPLIT_IS_EOP_SPLITS
               >> ‘q = FST (uaq_split_at_eop (ptr::ptrs) state)’
                   by fs[]
               >> ‘h::t = SND (uaq_split_at_eop (ptr::ptrs) state)’
                   by (once_asm_rewrite_tac [] >> simp [])
               >> once_asm_rewrite_tac []
               >> simp []
              )
       >> asm_rewrite_tac []
     )
  >> Q.ABBREV_TAC ‘foo = ¬(FST (is_td ptr state) ∧ tearingdown)’
  >> fs []
  >> Cases_on ‘(uaq_split ptrs tearingdown state)’
  >> fs []
QED

Theorem UAQ_SPLIT_SPLITS:
  ∀ bds tearingdown state  .
    FST (uaq_split bds tearingdown state) ++ SND (uaq_split bds tearingdown state) = bds
Proof
  Q.SPECL_THEN
  [‘UAQ_SPLIT_SPLITS_BASE_PROP’]
  (fn t => assume_tac (MP t
                       (CONJ UAQ_SPLIT_SPLITS_BASE
                             UAQ_SPLIT_SPLITS_INDUCT
                             )
                      ))
  uaq_split_ind
  >> fs [UAQ_SPLIT_SPLITS_BASE_PROP]
QED

Theorem UAQ_SPLIT_AT_EOP_TAIL:
  ∀ h queue state q r q' r' .
  (¬ FST (is_eop h state))
  ∧ (uaq_split_at_eop (h::queue) state = (q,r))
  ∧ (uaq_split_at_eop queue state = (q',r'))
  ⇒ ((q = h :: q') ∧ (r = r'))
Proof
  rpt strip_tac
  >> UNDISCH_TAC “uaq_split_at_eop (h::queue) state = (q,r)”
  >> simp [uaq_split_at_eop_def]
QED

Theorem UAQ_SPLIT_AT_EOP_MONITOR_INDEPENDANT:
  ∀ queue state monitor .
  uaq_split_at_eop queue state
  = uaq_split_at_eop queue (state with monitor := monitor)
Proof
  Induct
  >- fs [uaq_split_at_eop_def]
  >> rpt strip_tac
  >> simp [uaq_split_at_eop_def]
  >> ‘FST (is_eop h (state with monitor := monitor)) = FST (is_eop h state)’
      by fs[ IS_EOP_MONITOR_INDEPENDANT ]
  >> simp []
  >> Cases_on ‘FST (is_eop h state)’
  >- simp []
  >> simp []
  >> METIS_TAC []
QED

Theorem UAQ_SPLIT_MONITOR_INDEPENDANT_BASE_CASE:
(∀ monitor tearingdown state.
             (λqueue t state.
                  uaq_split queue t state =
                  uaq_split queue t (state with monitor := monitor)) []
               tearingdown state)
Proof
  simp [uaq_split_def]
QED

Theorem UAQ_SPLIT_MONITOR_INDEPENDANT_STEP_CASE:
(∀ monitor ptr ptrs tearingdown state.
             (∀non_eops rest.
                  ¬¬FST (is_released ptr state) ∧
                  ¬(FST (is_td ptr state) ∧ tearingdown) ∧
                  ¬FST (is_eop ptr state) ∧
                  (non_eops,rest) = uaq_split_at_eop (ptr::ptrs) state ∧
                  rest ≠ [] ⇒
                  (λqueue t state.
                       uaq_split queue t state =
                       uaq_split queue t (state with monitor := monitor))
                    (TL rest) tearingdown state) ∧
             (¬¬FST (is_released ptr state) ∧
              ¬(FST (is_td ptr state) ∧ tearingdown) ∧
              ¬¬FST (is_eop ptr state) ⇒
              (λqueue t state.
                   uaq_split queue t state =
                   uaq_split queue t (state with monitor := monitor))
                ptrs tearingdown state) ⇒
             (λqueue t state.
                  uaq_split queue t state =
                  uaq_split queue t (state with monitor := monitor))
               (ptr::ptrs) tearingdown state)
Proof
  rpt strip_tac
  >>  simp [uaq_split_def]
  >> ‘FST (is_eop ptr (state with monitor := monitor)) = FST (is_eop ptr state)’
      by fs[ IS_EOP_MONITOR_INDEPENDANT ]
  >> ‘FST (is_td ptr (state with monitor := monitor)) = FST (is_td ptr state)’
      by fs [IS_TD_MONITOR_INDEPENDANT]
  >> ‘FST (is_released ptr (state with monitor := monitor)) = FST (is_released ptr state)’
      by fs [IS_RELEASED_MONITOR_INDEPENDANT]
  >> ‘(uaq_split_at_eop (ptr::ptrs) (state with monitor := monitor)) = (uaq_split_at_eop (ptr::ptrs) state)’
      by fs [UAQ_SPLIT_AT_EOP_MONITOR_INDEPENDANT]
  >> simp []
  >> Cases_on ‘(uaq_split_at_eop (ptr::ptrs) state)’
  >> Cases_on ‘¬FST (is_released ptr state)’
  >> simp []
  >> Cases_on ‘FST (is_td ptr state) ∧ tearingdown’
  >> simp []
  >> Cases_on ‘¬FST (is_eop ptr state)’
  >> simp []
  >> METIS_TAC []
QED

Theorem UAQ_SPLIT_MONITOR_INDEPENDANT:
  ∀ monitor queue t state  .
  uaq_split queue t state
  = uaq_split queue t (state with monitor := monitor)
Proof
  strip_tac
  >> Q.SPECL_THEN
     [‘(λ queue t state .
        uaq_split queue t state =
        uaq_split queue t (state with monitor := monitor))’]
     (fn t =>
      assume_tac (MP t (CONJ (SPEC “monitor :nic_monitor_state” UAQ_SPLIT_MONITOR_INDEPENDANT_BASE_CASE)
                        (SPEC “monitor :nic_monitor_state” UAQ_SPLIT_MONITOR_INDEPENDANT_STEP_CASE)
                     ))
     )
     uaq_split_ind
  >> METIS_TAC []
QED

Theorem UPDATE_ACITVE_QUEUE_FIND_EOP_EFFECT:
  ∀ bd_ptr queue state n .
    ¬ state.nic.dead
    ∧ n >= (LENGTH queue) ∧ n > 0
    ∧ bd_ptr ≠ 0w
    ∧ WELL_FORMED_BD_QUEUE queue bd_ptr state
    ⇒
    let (to_gc , rest ) = uaq_split_at_eop queue state
    in update_active_queue_find_eop_loop n bd_ptr state =
    (if rest = [] then 0w else HD rest
     , state with monitor := state.monitor with alpha :=
             FOLDL (λ alpha' p . update_alpha_effect p REMOVE alpha')
                   state.monitor.alpha
                   to_gc
    )
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> fs [WELL_FORMED_BD_QUEUE_def
             , bd_queueTheory.BD_QUEUE_def
             ]
     )
  >> rpt strip_tac
  >> Cases_on ‘uaq_split_at_eop (h:: queue) state’
  >> simp [uaq_split_at_eop_def]
  >> Cases_on ‘n’
  >- fs []
  >> simp_st [update_active_queue_find_eop_loop_def]
  >> ‘BD_PTR_ADDRESS h’
      by fs [WELL_FORMED_BD_QUEUE_def]
  >> ‘bd_ptr = h’
      by fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def]
  >> pat_x_rewrite_all ‘bd_ptr = h’
  >> fs []
  >> ‘∃ b . is_eop h state = (b, state)’
      by fs [ IS_EOP_EXISTS]
  >> simp []
  >> Cases_on ‘b’
  >- ( ‘r = h :: queue’ by fs [uaq_split_at_eop_def]
       >> ‘q = []’ by fs [uaq_split_at_eop_def]
       >> simp [ system_state_component_equality
                 , nic_monitor_state_component_equality]
     )
  >> simp [ UPDATE_ALPHA_EFFECT]
  >> Q.ABBREV_TAC ‘(state' =
                    (state with monitor := state.monitor with
                     alpha := update_alpha_effect h REMOVE state.monitor.alpha))’
  >> ‘¬ state'.nic.dead’
      by (Q.UNABBREV_TAC ‘state'’ >> fs[])
  >> ‘get_next_descriptor_pointer h state' = ((read_ndp h state'.nic.regs.CPPI_RAM) , state')’
      by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> ‘WELL_FORMED_BD_QUEUE queue (read_ndp h state.nic.regs.CPPI_RAM) state’
      by fs [WELL_FORMED_BD_QUEUE_TAIL]
  >> simp []
  >> ‘state'.nic.regs.CPPI_RAM = state.nic.regs.CPPI_RAM’
      by (Q.UNABBREV_TAC ‘state'’ >> fs[])
  >> simp []
  >> ‘WELL_FORMED_BD_QUEUE queue (read_ndp h state.nic.regs.CPPI_RAM) state'’
        by ( Q.UNABBREV_TAC ‘state'’
             >> METIS_TAC [WELL_FORMED_BD_QUEUE_ALPHA_INDEPENDANT]
           )
  >> ‘BD_PTR_ADDRESS (read_ndp h state.nic.regs.CPPI_RAM)’
      by ( specl_assume_tac
           [‘(read_ndp h state.nic.regs.CPPI_RAM)’, ‘queue’, ‘state'’]
           WELL_FORMED_HEAD_BD_PTR
           >> ‘(read_ndp h state.nic.regs.CPPI_RAM) ≠ 0w’
               by fs [WELL_FORMED_BD_QUEUE_def]
           >> fs []
         )
  >> ‘∃ b' . is_eop (read_ndp h state.nic.regs.CPPI_RAM) state' = (b' , state')’
      by fs [IS_EOP_EXISTS]
  >> ‘n' >= LENGTH queue’
      by fs []
  >> ‘n' > 0’
      by (fs [WELL_FORMED_BD_QUEUE_def]
          >> fs [bd_queueTheory.BD_QUEUE_def]
          >> UNDISCH_TAC “BD_QUEUE queue (read_ndp h state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM”
          >> Cases_on ‘queue’
          >- ( fs [bd_queueTheory.BD_QUEUE_def])
          >> fs []
         )
  >> ‘(read_ndp h state.nic.regs.CPPI_RAM) ≠ 0w’ by fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def]
  >> Cases_on ‘n'’
  >- fs []
  >> Cases_on ‘queue’
  >- (  fs [WELL_FORMED_BD_QUEUE_def
            , bd_queueTheory.BD_QUEUE_def]
     )
  >> ‘(read_ndp h state.nic.regs.CPPI_RAM) = h'’
      by fs [WELL_FORMED_BD_QUEUE_def
             , bd_queueTheory.BD_QUEUE_def]
  >> ‘(read_ndp h state'.nic.regs.CPPI_RAM) = h'’
      by fs [WELL_FORMED_BD_QUEUE_def
             , bd_queueTheory.BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> pat_x_rewrite_all ‘_=_’
  >> fs []
  >> Cases_on ‘b'’
  >- ( simp_st [ update_active_queue_find_eop_loop_def]
       >> ‘(q, r) = ([h],(h' ::t))’
           by ( PAT_ASSUM “uaq_split_at_eop (h::h'::t) state = (q,r)”
                (fn t => rewrite_tac [GSYM t])
                >> rewrite_tac [uaq_split_at_eop_def]
                >> ‘¬FST (is_eop h state)’
                    by fs[]
                >> ‘FST (is_eop h' state)’
                    by (‘FST (is_eop h' state')’
                            by fs []
                        >> Q.UNABBREV_TAC ‘state'’
                        >> specl_assume_tac
                                [‘state’, ‘state.monitor with
                                            alpha := update_alpha_effect h REMOVE state.monitor.alpha’
                                 , ‘h'’]
                                IS_EOP_MONITOR_INDEPENDANT
                        >> fs []
                       )
                >> asm_rewrite_tac []
                >> simp []
              )
       >> ‘q = [h]’ by fs []
       >> ‘r = h' :: t’ by fs []
       >> simp []
       >> METIS_TAC []
     )
  >> Cases_on ‘uaq_split_at_eop (h' :: t) state'’
  >> ‘update_active_queue_find_eop_loop (SUC n) h' state' =
                 (if r' = [] then 0w else HD r',
                  state' with
                  monitor :=
                    state'.monitor with
                    alpha :=
                      FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                      state'.monitor.alpha q')’
      by (fs []
          >> Q.PAT_X_ASSUM
             ‘∀bd_ptr' state' n. _’
             (fn t => (specl_assume_tac
                       [‘h'’, ‘state'’, ‘SUC n’]
                       t))
          >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
          >> simp []
         )
  >> asm_rewrite_tac []
  >> ‘q = h::q' ∧ r = r'’
      by ( ‘uaq_split_at_eop (h'::t) state' =
           uaq_split_at_eop (h'::t) state’
           by ( Q.UNABBREV_TAC ‘state'’
                >> METIS_TAC [UAQ_SPLIT_AT_EOP_MONITOR_INDEPENDANT]
              )
           >> ‘¬FST (is_eop h state)’
               by fs []
           >> ‘uaq_split_at_eop (h'::t) state = (q',r')’
               by (UNDISCH_TAC “uaq_split_at_eop (h'::t) state' = (q',r')”
                   >> asm_rewrite_tac [])
           >> METIS_TAC [UAQ_SPLIT_AT_EOP_TAIL]
         )
  >> simp []
  >> Q.UNABBREV_TAC ‘state'’
  >> simp []
QED

Theorem UAQ_SPLIT_AT_EOP_SND_NOT_EMPTY:
  ∀ h queue state .
  ¬state.nic.dead
  ∧ WELL_FORMED_BD_QUEUE (h ::queue) h state
  ∧ ¬FST (is_eop h state)
  ⇒ ∃ to_gc k to_keep . uaq_split_at_eop (h ::queue) state = ((h :: to_gc) , k :: to_keep)
Proof
  Induct_on ‘queue’
  >- (rpt strip_tac
      >> REWRITE_ASSUMS_TAC WELL_FORMED_BD_QUEUE_def
      >> SPLIT_ASM_TAC
      >> fs []
      >> METIS_TAC [bd_queueTheory.BD_QUEUE_def]
     )
  >> rpt strip_tac
  >> once_rewrite_tac [uaq_split_at_eop_def]
  >> simp []
  >> ‘WELL_FORMED_BD_QUEUE (h::queue) h state’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_TAIL']
  >> Cases_on ‘¬FST (is_eop h state)’
  >- ( ‘∃to_gc k to_keep.
       uaq_split_at_eop (h::queue) state = (h::to_gc,k::to_keep)’
       by (Q.PAT_X_ASSUM ‘∀h state._’
           (fn t =>
            specl_assume_tac
            [‘h’, ‘state’]
            t)
           >> fs []
          )
       >> fs []
     )
  >> fs[]
  >> rewrite_tac [uaq_split_at_eop_def]
  >> simp []
QED

Theorem UPDATE_ACTIVE_QUEUE_LOOP_EFFECT:
  ∀ bd_queue bd_ptr transmit' state tearingdown n to_gc to_keep.
      WELL_FORMED_BD_QUEUE bd_queue bd_ptr state
    ∧ ¬ state.nic.dead
    ∧ n >= (LENGTH bd_queue) ∧ n > 0
    ∧ (uaq_split bd_queue tearingdown  state = (to_gc , to_keep))
    ⇒ (update_active_queue_outer_loop n T T bd_ptr tearingdown state =
       ( if to_keep = [] then 0w else HD to_keep
        , state with monitor := state.monitor with alpha :=
             FOLDL (λ alpha' p . update_alpha_effect p REMOVE alpha')
                   state.monitor.alpha
                   to_gc
       ))
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> fs []
     )
  >> rpt strip_tac
  >> Cases_on ‘bd_queue'’
  >- ( fs [UAQ_SPLIT_NULL]
       >> ‘bd_ptr = 0w’ by METIS_TAC [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def]
       >> simp_st [update_active_queue_outer_loop_def
                   , system_state_component_equality
                   , nic_monitor_state_component_equality]
       >> METIS_TAC [FOLDL]
     )
  >> fs []
  >> ‘n >= LENGTH t’ by fs []
  >> ‘bd_ptr ≠ 0w’
      by (fs [WELL_FORMED_BD_QUEUE_def, bd_queueTheory.BD_QUEUE_def] >> METIS_TAC [])
  >> ‘bd_ptr = h’
      by fs [bd_queueTheory.BD_QUEUE_def, WELL_FORMED_BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘BD_PTR_ADDRESS h’
      by fs [WELL_FORMED_BD_QUEUE_def]
  >> ‘∃ b . is_released h state = (b , state)’
      by METIS_TAC [IS_RELEASED_EXISTS]
  >> Cases_on ‘¬b’
  >- ( Cases_on ‘n’
       >> simp_st [update_active_queue_outer_loop_def]
       >> ‘(to_gc,to_keep) = ([] , (h::t))’
          by (fs [uaq_split_def])
       >> ‘to_gc = []’ by fs []
       >> ‘to_keep = (h::t)’ by fs []
       >> simp_st [update_active_queue_outer_loop_def
                   , system_state_component_equality
                  , nic_monitor_state_component_equality]
     )
  >> fs []
  >> ‘∃ b . is_td h state = (b , state)’
      by fs [ IS_TD_EXISTS]
  >> Cases_on ‘b' ∧ tearingdown’
  >- ( simp_st [update_active_queue_outer_loop_def, REMOVE_def]
      >> ‘(to_gc,to_keep) = (h::t, [])’
          by fs [uaq_split_def]
      >> ‘to_gc = h::t’ by fs []
      >> ‘to_keep = []’ by fs []
      >> simp []
      >> Cases_on ‘n’
      >> simp_st [update_active_queue_outer_loop_def, REMOVE_def]
      >> specl_assume_tac
         [‘state’, ‘h’, ‘F’, ‘h::t’]
         UPDATE_ALPHA_QUEUE_EFFECT_FOLDL
      >> fs []
     )
  >> ‘((if b' then (λs. (tearingdown,s)) else (λs. (F,s))) state) = (F , state)’
      by fs_st []
  >> simp_st [update_active_queue_outer_loop_def]
  >> Q.ABBREV_TAC ‘b'_or_td = ¬(b' ∧ tearingdown)’
  >> Q.PAT_X_ASSUM ‘((if b' then (λs. (tearingdown,s)) else (λs. (F,s))) state) = (F , state)’ (fn t => ALL_TAC )
  >> Cases_on ‘uaq_split_at_eop (h :: t) state’
  >> ‘MAX_QUEUE_LENGTH_NUM >= LENGTH (h :: t)’
      by fs [WELL_FORMED_BD_QUEUE_def , MAX_QUEUE_LENGTH_NUM_def]
  >> ‘MAX_QUEUE_LENGTH_NUM > 0’
      by fs [MAX_QUEUE_LENGTH_NUM_def]
  >> ‘(update_active_queue_find_eop_loop MAX_QUEUE_LENGTH_NUM h state =
         (if r = [] then 0w else HD r,
             state with
             monitor :=
               state.monitor with
               alpha :=
                 FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                 state.monitor.alpha q))’
      by ( specl_assume_tac
                [‘h’ , ‘(h::t)’, ‘state’, ‘MAX_QUEUE_LENGTH_NUM’]
                UPDATE_ACITVE_QUEUE_FIND_EOP_EFFECT
           >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
           >> simp []
         )
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘(update_active_queue_find_eop_loop _ _ _) = _’ (fn t =>ALL_TAC)
  >> simp_st []
  >> ‘∃ b'' . is_eop h state = (b'' , state)’
      by fs [IS_EOP_EXISTS]
  >> Cases_on ‘b''’
  >- ( ‘FST (is_eop h state)’
       by fs []
       >> ‘uaq_split_at_eop (h :: t) state = ([] , (h :: t))’
           by ( rewrite_tac [uaq_split_at_eop_def]
                >> asm_rewrite_tac []
              )
       >> ‘q = [] ∧ r = h::t’
           by fs []
       >> asm_rewrite_tac []
       >> simp []
       >> fs [ UPDATE_ALPHA_EFFECT ]
       >> Q.ABBREV_TAC ‘state' = (state with
                                  monitor :=
                                  state.monitor with
                                  alpha := update_alpha_effect h REMOVE state.monitor.alpha)’
       >> ‘WELL_FORMED_BD_QUEUE (h::t) h state'’
           by (Q.UNABBREV_TAC ‘state'’
               >>  fs [WELL_FORMED_BD_QUEUE_MONITOR_INDEPENDANT]
              )
       >> ‘¬state'.nic.dead’
           by (Q.UNABBREV_TAC ‘state'’ >> fs[])
       >> Cases_on ‘t’
       >- (simp []
           >> ‘get_next_descriptor_pointer h state' = (0w , state')’
               by METIS_TAC [WELL_FORMED_SINGLETON_NEXT_BD_PTR_ZERO]
           >> simp []
           >> Cases_on ‘n’
           >> simp_st [update_active_queue_outer_loop_def]
           >> fs []
           >> ‘to_gc = [h] ∧ to_keep = []’
               by (UNDISCH_TAC “uaq_split [h] tearingdown state = (to_gc,to_keep)”
                   >> once_rewrite_tac [uaq_split_def]
                   >> ‘FST (is_released h state)’
                       by fs[]
                   >> asm_rewrite_tac []
                   >> simp [uaq_split_def]
                  )
           >> simp []
          )
       >> ‘(get_next_descriptor_pointer h state') = (h' , state')’
           by ( specl_assume_tac
                [‘h’, ‘h'’, ‘t'’, ‘state'’]
                WELL_FORMED_MORE_THEN2_NEXT_BD_PTR
                >> fs []
              )
       >> simp []
       >> Cases_on ‘(uaq_split (h'::t') tearingdown state')’
       >> Q.PAT_X_ASSUM ‘∀bd_queue bd_ptr state tearingdown. _’
                        (fn t => specl_assume_tac
                         [‘h'::t'’, ‘h'’, ‘state'’, ‘tearingdown’, ‘q'’, ‘r'’]
                         t
                        )
       >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
       >> simp []
       >> ‘WELL_FORMED_BD_QUEUE (h'::t') h' state'’
           by ( specl_assume_tac
                [‘h’, ‘h'’, ‘t'’, ‘state'’]
                WELL_FORMED_BD_QUEUE_TAIL'
                >> fs []
              )
       >> ‘n > 0’ by fs []
       >> simp []
       >> strip_tac
       >> asm_rewrite_tac []
       >> ‘(to_gc,to_keep) = (h::q',  r')’
           by ( Q.PAT_X_ASSUM ‘uaq_split (h::h'::t') tearingdown state = (to_gc,to_keep)’
                (assume_tac o GSYM)
                >> asm_rewrite_tac []
                >> once_rewrite_tac [ uaq_split_def]
                >> asm_rewrite_tac []
                >> fs []
                >> ‘uaq_split (h'::t') tearingdown state = uaq_split (h'::t') tearingdown state'’
                    by (Q.UNABBREV_TAC ‘state'’
                        >> METIS_TAC [UAQ_SPLIT_MONITOR_INDEPENDANT]
                       )
                >> ‘FST (is_released h state)’
                    by fs []
                >> ‘¬ ( b' ∧ tearingdown)’
                    by ( Q.UNABBREV_TAC ‘b'_or_td’ >> simp [])
                >> asm_rewrite_tac []
                >> simp []
              )
       >> ‘to_gc = h::q'’ by fs[]
       >> ‘to_keep = r'’ by fs []
       >> simp []
       >> Q.UNABBREV_TAC ‘state'’
       >> simp [REMOVE_def]
     )
  >> ‘¬FST (is_eop h state)’
      by fs []
  >> fs [ UPDATE_ALPHA_EFFECT ]
  >> Cases_on ‘r’
  >- ( ‘∃ to_gc' k to_keep' . uaq_split_at_eop (h::t) state = (h::to_gc',k::to_keep')’
       by (fs [UAQ_SPLIT_AT_EOP_SND_NOT_EMPTY])
       >> fs []
     )
  >> simp []
  >> ‘h::t = q ++ h' :: t'’
      by ( ‘q = FST (uaq_split_at_eop (h::t) state)’
           by fs[]
           >> ‘h'::t' = SND (uaq_split_at_eop (h::t) state)’
               by ( once_asm_rewrite_tac []
                    >> fs[])
           >> once_asm_rewrite_tac []
           >> fs [UAQ_SPLIT_IS_EOP_SPLITS]
         )
  >> ‘MEM h' (h ::t)’
      by (asm_rewrite_tac []
          >> fs []
         )
  >> ‘BD_PTR_ADDRESS h'’
      by ( fs [WELL_FORMED_BD_QUEUE_def, listTheory.EVERY_MEM]
         )
  >> Q.ABBREV_TAC ‘state' = (state with
              monitor :=
                state.monitor with
                alpha :=
                  update_alpha_effect h' REMOVE
                    (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                       state.monitor.alpha q))’
  >> ‘WELL_FORMED_BD_QUEUE (h' :: t') h' state’
      by METIS_TAC [ WELL_FORMED_BD_QUEUE_MID]
  >> ‘n >= LENGTH (h' :: t')’
      by ( ‘LENGTH (h' :: t') < LENGTH (h ::t)’
           by ( ‘h' :: t' = SND (uaq_split_at_eop (h::t) state)’
                       by (PAT_ASSUM “uaq_split_at_eop (h::t) state = (q,h'::t')”
                           (fn t => rewrite_tac [t]))
                >> PAT_ASSUM “h'::t' = SND (uaq_split_at_eop (h::t) state)”
                             (fn t => rewrite_tac [t])
                >> METIS_TAC [UAQ_SPLIT_AT_EOP_REST_SMALLER]
              )
           >> fs[]
         )
  >> ‘¬ state'.nic.dead’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘WELL_FORMED_BD_QUEUE (h' :: t') h' state'’
      by ( Q.UNABBREV_TAC ‘state'’
           >> fs [WELL_FORMED_BD_QUEUE_MONITOR_INDEPENDANT]
         )
  >> Cases_on ‘t'’
  >- ( ‘(get_next_descriptor_pointer h' state') = (0w, state')’
       by ( specl_assume_tac
            [‘h'’, ‘state'’]
            WELL_FORMED_SINGLETON_NEXT_BD_PTR_ZERO
            >> fs []
          )
       >> asm_rewrite_tac []
       >> Cases_on ‘n’
       >- ( simp_st [update_active_queue_outer_loop_def]
            >> UNDISCH_TAC “uaq_split (h::t) tearingdown state = (to_gc,to_keep)”
            >> once_rewrite_tac [uaq_split_def]
            >> simp []
            >> ‘¬(b' ∧ tearingdown)’
                by (Q.UNABBREV_TAC  ‘b'_or_td’ >> fs [])
            >> asm_rewrite_tac []
            >> Q.ABBREV_TAC ‘asdf = ¬(b' ∧ tearingdown)’
            >> fs []
          )
       >> simp_st [update_active_queue_outer_loop_def]
       >> UNDISCH_TAC “uaq_split (h::t) tearingdown state = (to_gc,to_keep)”
       >> once_rewrite_tac [uaq_split_def]
       >> ‘FST (is_released h state)’
           by fs []
       >> simp_st []
       >> ‘¬(b' ∧ tearingdown)’
           by (Q.UNABBREV_TAC ‘b'_or_td’ >> fs [])
       >> simp []
       >> Q.ABBREV_TAC ‘b'_or_td = ¬(b' ∧ tearingdown)’
       >> rewrite_tac [uaq_split_def]
       >> simp_st []
       >> strip_tac
       >> Q.UNABBREV_TAC ‘state'’
       >> Q.PAT_ASSUM ‘q ⧺ [h'] = to_gc’
                      (assume_tac o GSYM)
       >> once_asm_rewrite_tac []
       >> METIS_TAC [FOLD_UPDATE_ALPHA_CLEAR_LEFT_BACKWARDS]
     )
  >> ‘(get_next_descriptor_pointer h' state') = (h'', state')’
      by ( METIS_TAC [WELL_FORMED_MORE_THEN2_NEXT_BD_PTR] )
  >> asm_rewrite_tac []
  >> simp []
  >> Cases_on ‘uaq_split (h''::t'') tearingdown state'’
  >> Q.PAT_ASSUM ‘∀bd_queue bd_ptr state tearingdown. _’
                  (specl_assume_tac
                   [‘h''::t''’, ‘h''’, ‘state'’, ‘tearingdown’, ‘q'’, ‘r’])
  >> ‘WELL_FORMED_BD_QUEUE (h''::t'') h'' state'’
      by (specl_assume_tac
          [‘h'’, ‘h''’, ‘t''’, ‘state'’]
          WELL_FORMED_BD_QUEUE_TAIL'
          >> fs []
         )
  >> ‘n >= LENGTH (h'' :: t'')’ by fs []
  >> ‘n > 0’ by fs []
  >> simp []
  >> asm_rewrite_tac []
  >> ‘r = to_keep’
      by ( UNDISCH_TAC “uaq_split (h::t) tearingdown state = (to_gc,to_keep)”
           >> once_rewrite_tac [uaq_split_def]
           >> simp []
           >> ‘¬(b' ∧ tearingdown)’
               by (Q.UNABBREV_TAC ‘b'_or_td’ >> fs[])
           >> simp []
           >> PAT_X_ASSUM “¬(b' ∧ tearingdown)” (fn _ => ALL_TAC)
           >> ‘uaq_split (h''::t'') tearingdown state = (q',r)’
               by (Q.UNABBREV_TAC ‘state'’
                   >> METIS_TAC [UAQ_SPLIT_MONITOR_INDEPENDANT])
           >> asm_rewrite_tac []
           >> simp []
         )
  >> asm_rewrite_tac []
  >> Q.UNABBREV_TAC ‘state'’
  >> simp []
  >> ‘to_gc = q ++ [h'] ++ q'’
      by ( UNDISCH_TAC “ uaq_split (h::t) tearingdown state = (to_gc,to_keep)”
           >> once_rewrite_tac [uaq_split_def]
           >> simp []
           >> ‘¬(b' ∧ tearingdown)’
               by (Q.UNABBREV_TAC ‘b'_or_td’ >> fs[])
           >> simp []
           >> PAT_X_ASSUM “¬(b' ∧ tearingdown)” (fn _ => ALL_TAC)
           >> ‘(uaq_split (h''::t'') tearingdown state) = (q',r)’
               by (METIS_TAC [UAQ_SPLIT_MONITOR_INDEPENDANT])
           >> asm_rewrite_tac []
           >> simp []
         )
  >> simp []
  >> simp [ FOLDL_UPDATE_ALPHA_IS_FOLDL]
  >> simp [FOLD_UPDATE_ALPHA_CLEAR_MID_TO_FRONT]
  >> METIS_TAC [FOLDL_UPDATE_ALPHA_APPEND_DIST]
QED

Theorem UPDATE_ACTIVE_QUEUE_TX0_EFFECT:
  ∀ tx0_queue state to_gc to_keep .
  WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
  ∧ ¬ state.nic.dead
  ∧ uaq_split tx0_queue state.monitor.tx0_tearingdown state = (to_gc , to_keep)
  ⇒ update_active_queue T state
     = (()
        , state with monitor := state.monitor with
                <| tx0_active_queue := if to_keep = [] then 0w else HD to_keep
                 ; alpha := FOLDL (λ alpha' p . update_alpha_effect p REMOVE alpha')
                            state.monitor.alpha
                            to_gc
                 |>
       )
Proof
  rpt strip_tac
  >> simp_st [update_active_queue_def
              , get_tx0_active_queue_def
              , get_tx0_tearingdown_def]
  >> ‘MAX_QUEUE_LENGTH_NUM >= LENGTH tx0_queue’
      by fs [MAX_QUEUE_LENGTH_NUM_def, WELL_FORMED_BD_QUEUE_def]
  >> ‘MAX_QUEUE_LENGTH_NUM > 0’ by fs[MAX_QUEUE_LENGTH_NUM_def]
  >> ‘(update_active_queue_outer_loop MAX_QUEUE_LENGTH_NUM T T
             state.monitor.tx0_active_queue state.monitor.tx0_tearingdown
             state)
      = (if to_keep = [] then 0w else HD to_keep,
             state with
             monitor :=
               state.monitor with
               alpha :=
                 FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                 state.monitor.alpha to_gc)’
      by (METIS_TAC [ UPDATE_ACTIVE_QUEUE_LOOP_EFFECT])
  >> asm_rewrite_tac []
  >> simp_st [set_tx0_active_queue_def
              , system_state_component_equality]
QED

Theorem UPDATE_ACTIVE_QUEUE_RX0_EFFECT:
  ∀ rx0_queue state to_gc to_keep .
  WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
  ∧ ¬ state.nic.dead
  ∧ uaq_split rx0_queue state.monitor.rx0_tearingdown state = (to_gc , to_keep)
  ⇒ update_active_queue F state
     = (()
        , state with monitor := state.monitor with
                <| rx0_active_queue := if to_keep = [] then 0w else HD to_keep
                 ; alpha := FOLDL (λ alpha' p . update_alpha_effect p REMOVE alpha')
                            state.monitor.alpha
                            to_gc
                 |>
       )
Proof
  rpt strip_tac
  >> simp_st [update_active_queue_def
              , get_rx0_active_queue_def
              , get_rx0_tearingdown_def]
  >> ‘MAX_QUEUE_LENGTH_NUM >= LENGTH rx0_queue’
      by fs [MAX_QUEUE_LENGTH_NUM_def, WELL_FORMED_BD_QUEUE_def]
  >> ‘MAX_QUEUE_LENGTH_NUM > 0’ by fs[MAX_QUEUE_LENGTH_NUM_def]
  >> ‘(update_active_queue_outer_loop MAX_QUEUE_LENGTH_NUM T T
             state.monitor.rx0_active_queue state.monitor.rx0_tearingdown
             state)
      = (if to_keep = [] then 0w else HD to_keep,
             state with
             monitor :=
               state.monitor with
               alpha :=
                 FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                 state.monitor.alpha to_gc)’
      by (METIS_TAC [ UPDATE_ACTIVE_QUEUE_LOOP_EFFECT])
  >> asm_rewrite_tac []
  >> simp_st [set_rx0_active_queue_def
              , system_state_component_equality]
QED




val _ = export_theory();
