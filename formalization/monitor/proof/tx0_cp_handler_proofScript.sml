open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open register_writeTheory;
open address_spaceTheory;

open initialization_performedTheory;
open invariant_lemmasTheory;
open invariantTactics;
open address_lemmasTheory;
open alpha_lemmasTheory;

open monitorUtils;

val _ = new_theory "tx0_cp_handler_proof"

(*
The effect on the state after running tx0_hdp_handler when the nic hasn't been totaly initialized yet.
*)
Definition tx0_cp_handler_NON_TOTAL_INITIALIZED_effect:
  tx0_cp_handler_non_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with tx0_cp_initialized := T)
               ; nic := state.nic with
                <| interrupt :=
                         ((¬ARB.reg_write.tx0_cp_deassert_interrupt ∨
                           state.nic.rx.interrupt) ∧ state.nic.interrupt)
                 ; regs := state.nic.regs with TX0_CP := 0w
                 ; it   := state.nic.it with
                             <| state := it_initialize_hdp_cp
                              ; tx0_cp_initialized := T|>
                 ; tx   := state.nic.tx with
                               interrupt :=
                                 (¬ARB.reg_write.tx0_cp_deassert_interrupt ∧
                                  state.nic.tx.interrupt)
                 |>
               |>)
End

(* For the case where *)
Theorem TX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ ¬ (state.monitor.tx0_hdp_initialized
       ∧ state.monitor.rx0_hdp_initialized
       ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ tx0_cp_handler 0w state =
  (T
  , tx0_cp_handler_non_total_initialized_effect state
   )
Proof
  strip_tac
  >> strip_tac
  >> ‘¬ state.nic.dead’ by
      (fs [MON_INV_NIC_NOT_DEAD_lem])
  >> simp_st [  get_initialized_def , tx0_cp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_tx0_cp_def
             , address_lemmasTheory.TX0_CP_PA_lemmas , set_tx0_cp_initialized_def
            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]

  >> ‘state.nic.it.state = it_initialize_hdp_cp’ by
      (UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp [])
  >> simp []
  >> ‘¬(state.nic.it.tx0_hdp_initialized
        ∧ state.nic.it.rx0_hdp_initialized
        ∧ state.nic.it.rx0_cp_initialized)’ by
      (UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp [] >> METIS_TAC [])
  >> simp [ tx0_cp_handler_NON_TOTAL_INITIALIZED_effect ]
  >> UNDISCH_TAC “¬(state.nic.it.tx0_hdp_initialized
                    ∧ state.nic.it.rx0_hdp_initialized
                    ∧ state.nic.it.rx0_cp_initialized)”
  >> Cases_on ‘state.monitor.tx0_hdp_initialized’
  >> Cases_on ‘state.monitor.rx0_hdp_initialized’
  >> Cases_on ‘state.monitor.rx0_cp_initialized’
  >> fs_st monitor_state_simp_list
QED


Theorem TX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT:
  ∀ state .
    (MONITOR_INVARIANT state
     ∧ ¬ state.monitor.initialized
     ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w)
    ⇒ MONITOR_INVARIANT (tx0_cp_handler_non_total_initialized_effect state)
Proof
  strip_tac >> strip_tac
  >> RW_ASM_TAC  “MONITOR_INVARIANT state” MONITOR_INVARIANT_def
  >> SPLIT_ASM_TAC
  >> rewrite_tac [MONITOR_INVARIANT_def, tx0_cp_handler_NON_TOTAL_INITIALIZED_effect]
  >> simp []
  >> rewrite_tac [GSYM  tx0_cp_handler_NON_TOTAL_INITIALIZED_effect]
  >> ‘BD_QUEUE_INVARIANT (tx0_cp_handler_non_total_initialized_effect state)’
       by bd_queue_invariant_tac
  >> fs []
  >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
  >> EVAL_TAC
  >> fs []
QED

Definition tx0_cp_handler_TOTAL_INITIALIZED_effect:
  tx0_cp_handler_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with tx0_cp_initialized := T)
               ; nic := state.nic with
                <| interrupt :=
                         ((¬ARB.reg_write.tx0_cp_deassert_interrupt ∨
                           state.nic.rx.interrupt) ∧ state.nic.interrupt)
                 ; regs := state.nic.regs with TX0_CP := 0w
                 ; it   := state.nic.it with
                             <|state := it_initialized;
                             tx0_cp_initialized := T|>
                 ; tx   := state.nic.tx with
                               interrupt :=
                                 (¬ARB.reg_write.tx0_cp_deassert_interrupt ∧
                                  state.nic.tx.interrupt)
                 |>
               |>)
End


Theorem TX0_CP_HANDLER_TOTAL_INITIALIZED_lemma:
  ∀ state tx0_queue rx0_queue .
  MONITOR_INVARIANT state
  ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
  ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
  ∧ ¬ state.monitor.initialized
  ∧ (state.monitor.tx0_hdp_initialized
    ∧ state.monitor.rx0_hdp_initialized
    ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ tx0_cp_handler 0w state =
    (T
     , initailization_performed_effect tx0_queue rx0_queue (tx0_cp_handler_total_initialized_effect state)
   )
Proof
  rpt strip_tac
  >> ‘¬ state.nic.dead’ by
      (fs [MON_INV_NIC_NOT_DEAD_lem])
  >> simp_st [  get_initialized_def , tx0_cp_handler_def
          , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
          , register_readTheory.read_register_def , read_nic_register_def
          , write_nic_register_def
          , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
          , register_writeTheory.write_tx0_cp_def
          , address_lemmasTheory.TX0_CP_PA_lemmas , set_tx0_cp_initialized_def

          , get_initialized_def
          , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
          , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]
  >> ‘state.nic.it.state = it_initialize_hdp_cp’ by
      (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> ‘(state.nic.it.tx0_hdp_initialized
     ∧ state.nic.it.rx0_hdp_initialized
     ∧ state.nic.it.rx0_cp_initialized)’ by
      (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> Q.ABBREV_TAC ‘state' = (state with
              <|monitor := state.monitor with tx0_cp_initialized := T;
                nic :=
                  state.nic with
                  <|interrupt :=
                      ((¬ARB.reg_write.tx0_cp_deassert_interrupt ∨
                        state.nic.rx.interrupt) ∧ state.nic.interrupt);
                    regs := state.nic.regs with TX0_CP := 0w;
                    it :=
                      state.nic.it with
                      <|state := it_initialized; tx0_cp_initialized := T|>;
                    tx :=
                      state.nic.tx with
                      interrupt :=
                        (¬ARB.reg_write.tx0_cp_deassert_interrupt ∧
                         state.nic.tx.interrupt)|> |>)’
  >> ‘(state.nic.dead ⇔ state'.nic.dead) ∧
         state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
         by (Q.UNABBREV_TAC ‘state'’  >> fs [ MONITOR_INVARIANT_def])
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘state.monitor.rx0_active_queue =  state'.monitor.rx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘state.monitor.tx0_active_queue =  state'.monitor.tx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘¬state'.nic.dead’
      by METIS_TAC []
  >> ‘initialization_performed state' = (() , initailization_performed_effect tx0_queue rx0_queue state')’
             by (METIS_TAC [INITIALIZAITON_PERFORMED_EFFECT])
  >> fs []
  >> asm_rewrite_tac []
  >> Q.UNABBREV_TAC ‘state'’
  >> fs [initailization_performed_effect_def
        , tx0_cp_handler_TOTAL_INITIALIZED_effect]
QED

(*

Proof that of the initialization case of the tx0_hdp_handler call

*)
Theorem TX0_CP_HANDLER_INITIALIZATION_PRESERVES_INV_lem:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ (¬ state.monitor.initialized)
  ⇒ MONITOR_INVARIANT (SND (tx0_cp_handler 0w state))
Proof
  strip_tac >> strip_tac
  >> ‘¬state.nic.dead’
      by METIS_TAC [MON_INV_NIC_NOT_DEAD_lem]
  >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET ≠ 0w’
  >- ( rewrite_tac [tx0_cp_handler_def]
       >> simp_st [ get_initialized_def
                 , read_nic_register_def , register_readTheory.read_register_def
                 , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas]
       >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET’
       >> fs []
       >> Cases_on ‘n’
       >> fs []
       >> Cases_on ‘n'’
       >> fs []
       >> ‘state with nic := state.nic = state’
           by fs [monitor_stateTheory.system_state_component_equality]
       >> asm_rewrite_tac []
     )
  >> fs []
  >> Cases_on  ‘(state.monitor.tx0_hdp_initialized
                 ∧ state.monitor.rx0_hdp_initialized
                 ∧ state.monitor.rx0_cp_initialized)’
  >- ( ‘∃ tx0_queue . WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state’
       by METIS_TAC [MON_INV_HAS_TX0_QUEUE_lem]
       >> ‘∃ rx0_queue . WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state’
           by METIS_TAC [MON_INV_HAS_RX0_QUEUE_lem]
       >> ‘tx0_cp_handler 0w state =
             (T
             , initailization_performed_effect
                 tx0_queue rx0_queue
                 (tx0_cp_handler_total_initialized_effect state))’
             by (METIS_TAC [MONITOR_INVARIANT_def
                            ,TX0_CP_HANDLER_TOTAL_INITIALIZED_lemma])
       >> asm_rewrite_tac [ initailization_performed_effect_def
                            , tx0_cp_handler_TOTAL_INITIALIZED_effect]
       >> fs []
       >> Q.ABBREV_TAC ‘state' = (state with
           <|monitor :=
               state.monitor with
               <|tx0_active_queue := 0w; rx0_active_queue := 0w;
                 initialized := T; tx0_cp_initialized := T;
                 alpha :=
                   FOLDR
                     (λbd_ptr' state'.
                          update_alpha_effect bd_ptr' REMOVE state')
                     state.monitor.alpha (tx0_queue ⧺ rx0_queue)|>;
             nic :=
               state.nic with
               <|interrupt :=
                   ((¬ARB.reg_write.tx0_cp_deassert_interrupt ∨
                     state.nic.rx.interrupt) ∧ state.nic.interrupt);
                 regs := state.nic.regs with TX0_CP := 0w;
                 it :=
                   state.nic.it with
                   <|state := it_initialized; tx0_cp_initialized := T|>;
                 tx :=
                   state.nic.tx with
                   interrupt :=
                     (¬ARB.reg_write.tx0_cp_deassert_interrupt ∧
                      state.nic.tx.interrupt)|> |>)’
       >> ‘BD_QUEUE_INVARIANT state'’
           by ( rewrite_tac [BD_QUEUE_INVARIANT_def]
                >> Q.EXISTS_TAC ‘[]’
                >> Q.EXISTS_TAC ‘[]’
                >> ‘state'.monitor.tx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> ‘state'.monitor.rx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> simp [WELL_FORMED_BD_QUEUE_def
                         , bd_queueTheory.BD_QUEUE_def
                         , NON_OVERLAPPING_BD_QUEUES_def
                        ]
                >> ‘BD_QUEUE_ALPHA_MARKED [] [] state'.monitor.alpha’
                    by (Q.UNABBREV_TAC ‘state'’
                        >> simp []

                        >> METIS_TAC [BD_QUEUE_INVARIANT_UNPACK
                                      ,UPDATE_ALPHA_REMOVE_ALL_NOTHING_ALPHA_MARKED]
                       )
                >> simp []
                >> ‘state.nic.tx.sop_bd_pa = 0w
                    ∧ state.nic.rx.sop_bd_pa = 0w
                    ∧ state.nic.rx.current_bd_pa = 0w’
                    by (METIS_TAC [MONITOR_INVARIANT_def])
                >> ‘state'.nic.tx.sop_bd_pa = 0w
                    ∧ state'.nic.rx.sop_bd_pa = 0w
                    ∧ state'.nic.rx.current_bd_pa = 0w’
                    by (Q.UNABBREV_TAC ‘state'’>> fs[])
                >> METIS_TAC []
              )
       >> asm_rewrite_tac [MONITOR_INVARIANT_def]
       >> asm_rewrite_tac []
       >> Q.UNABBREV_TAC ‘state'’
       >> fs [MONITOR_INVARIANT_def]
       >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
       >> fs [nicInvariantTheory.NIC_INVARIANT_def]
       >> EVAL_TAC >> fs [] >> blastLib.BBLAST_PROVE_TAC
     )
  >> ‘tx0_cp_handler 0w state =
      (T,tx0_cp_handler_non_total_initialized_effect state)’
      by fs [ TX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma]
  >> asm_rewrite_tac []
  >> METIS_TAC [TX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT]
QED

val _ = export_theory();
