open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open state_transformer_extTheory;
open address_lemmasTheory;

open alpha_lemmasTheory;

open invariant_lemmasTheory;
open read_utilsTheory;
open write_utilsTheory;

open monitorUtils;

open wordExtraTheory;

open bd_queueTheory;

val _ = new_theory "is_queue_secureProperties";

Theorem LIFT_IS_WORD_ALIGNED:
  ∀ bd_ptr  .
  is_word_aligned bd_ptr
  ⇒ WORD32_ALIGNED bd_ptr
Proof
  strip_tac
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem IVLICRANAQOL_IMP_BD_PTR_ADDRESS_AND_NO_ALPHA_SET:
  ∀ bd_ptr state n .
  bd_ptr ≠ 0w
  ∧ (FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop (SUC n) bd_ptr state))
  ⇒ (BD_PTR_ADDRESS bd_ptr
     ∧ ¬(get_alpha_bit bd_ptr state.monitor.alpha
         ∨ get_alpha_bit (bd_ptr + 4w) state.monitor.alpha
         ∨ get_alpha_bit (bd_ptr + 8w) state.monitor.alpha
         ∨ get_alpha_bit (bd_ptr + 12w) state.monitor.alpha)
    )
Proof
  NTAC 4 strip_tac
  >> Q.PAT_UNDISCH_TAC ‘FST _’
  >> simp_st [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def]
  >> Cases_on ‘¬(1242570752w ≤ bd_ptr ∧ bd_ptr < 1242578929w ∧
              is_word_aligned bd_ptr)’
  >- (simp_st [])
  >> fs_st []
  >> ‘WORD32_ALIGNED bd_ptr’
      by METIS_TAC [LIFT_IS_WORD_ALIGNED]
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by ( simp [BD_PTR_ADDRESS_def
                 , address_spaceTheory.CPPI_RAM_BYTE_PA_def
                 , address_spaceTheory.CPPI_RAM_START_def
                 , address_spaceTheory.CPPI_RAM_END_def]
           >> WORD_DECIDE_TAC
         )
  >> strip_tac
  >> ‘¬get_alpha_bit bd_ptr state.monitor.alpha ∧
        ¬get_alpha_bit (bd_ptr + 4w) state.monitor.alpha ∧
        ¬get_alpha_bit (bd_ptr + 8w) state.monitor.alpha ∧
        ¬get_alpha_bit (bd_ptr + 12w) state.monitor.alpha’
        by ( ‘∃ b . IS_ACTIVE_CPPI_RAM bd_ptr state = (b , state)’
             by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
             >> ‘∃ b1 . IS_ACTIVE_CPPI_RAM (bd_ptr + 4w) state = (b1 , state)’
                 by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
             >> ‘∃ b2 . IS_ACTIVE_CPPI_RAM (bd_ptr + 8w) state = (b2 , state)’
                 by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
             >> ‘∃ b3 . IS_ACTIVE_CPPI_RAM (bd_ptr + 12w) state = (b3 , state)’
                 by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
             >> fs []
             >> Cases_on ‘b ∨ b1 ∨ b2 ∨ b3’
             >- (fs_st [])
             >> fs_st [get_alpha_def, IS_ACTIVE_CPPI_RAM_def
                       , get_alpha_bit_def]
           )
  >> fs []
QED

Theorem IVLICRANAQOL_NEXT:
  ∀ bd_ptr state n .
  bd_ptr ≠ 0w
  ∧ ¬ state.nic.dead
  ∧ (FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop (SUC n) bd_ptr state))
  ⇒ (FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop n
          (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state))
Proof
  rpt strip_tac
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by METIS_TAC [IVLICRANAQOL_IMP_BD_PTR_ADDRESS_AND_NO_ALPHA_SET]
  >> REWRITE_ASSUMS_TAC is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def
  >> Q.PAT_UNDISCH_TAC ‘FST _’
  >> simp []
  >> Cases_on ‘¬ (1242570752w ≤ bd_ptr ∧ bd_ptr < 1242578929w ∧ is_word_aligned bd_ptr)’
  >- (simp_st [])
  >> fs_st []
  >> ‘∃ b . IS_ACTIVE_CPPI_RAM bd_ptr state = (b , state)’
      by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
  >> ‘∃ b1 . IS_ACTIVE_CPPI_RAM (bd_ptr + 4w) state = (b1 , state)’
      by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
  >> ‘∃ b2 . IS_ACTIVE_CPPI_RAM (bd_ptr + 8w) state = (b2 , state)’
      by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
  >> ‘∃ b3 . IS_ACTIVE_CPPI_RAM (bd_ptr + 12w) state = (b3 , state)’
      by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
  >> simp_st []
  >> Cases_on ‘b ∨ b1 ∨ b2 ∨ b3’
  >- (simp_st [])
  >> simp_st []
  >> ‘(get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM, state)’
      by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> simp_st []
QED

Theorem IVLICRANAQOL_EXISTS:
  ∀ bd_ptr state n .
  ¬ state.nic.dead
  ⇒ ∃ b . is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop n bd_ptr state = (b, state)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> simp_st [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def]
      >> Cases_on ‘bd_ptr = 0w’
      >> simp_st []
     )
  >> rpt strip_tac
  >> simp_st [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def]
  >> Cases_on ‘bd_ptr = 0w’
  >- simp_st []
  >> simp_st []
  >> Cases_on ‘1242570752w ≤ bd_ptr ∧ bd_ptr < 1242578929w ∧
              is_word_aligned bd_ptr’
  >- (simp_st []
      >> ‘∃ b . IS_ACTIVE_CPPI_RAM bd_ptr state = (b , state)’
          by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
      >> ‘∃ b1 . IS_ACTIVE_CPPI_RAM (bd_ptr + 4w) state = (b1 , state)’
          by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
      >> ‘∃ b2 . IS_ACTIVE_CPPI_RAM (bd_ptr + 8w) state = (b2 , state)’
          by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
      >> ‘∃ b3 . IS_ACTIVE_CPPI_RAM (bd_ptr + 12w) state = (b3 , state)’
          by simp [IS_ACTIVE_CPPI_RAM_EXISTS]
      >> simp_st []
      >> Cases_on ‘b ∨ b1 ∨ b2 ∨ b3’
      >- simp_st []
      >> simp_st []
      >> ‘BD_PTR_ADDRESS bd_ptr’
          by (‘WORD32_ALIGNED bd_ptr’
              by METIS_TAC [LIFT_IS_WORD_ALIGNED]
              >> simp [BD_PTR_ADDRESS_def
                       , address_spaceTheory.CPPI_RAM_BYTE_PA_def
                       , address_spaceTheory.CPPI_RAM_START_def
                       , address_spaceTheory.CPPI_RAM_END_def]
              >> Q.PAT_UNDISCH_TAC ‘1242570752w ≤ bd_ptr ∧ bd_ptr < 1242578929w ∧ is_word_aligned bd_ptr’
              >> blastLib.BBLAST_PROVE_TAC
             )
      >> ‘∃ pa . get_next_descriptor_pointer bd_ptr state = (pa , state)’
          by METIS_TAC [ GET_NEXT_DESCRIPTOR_POINTER_EXISTS]
      >> simp_st []
     )
  >> simp_st []
QED

Theorem IVLICRANAQO_EXISTS:
  ∀ bd_ptr state n .
  ¬ state.nic.dead
  ⇒ ∃ b . is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap bd_ptr state = (b, state)
Proof
  rpt strip_tac
  >> rewrite_tac [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_def]
  >> METIS_TAC [IVLICRANAQOL_EXISTS]
QED

Theorem IS_VALID_LENGTH_IN_CPPI_RAM_ALIGNMENT_NO_ACTIVE_QUEUE_OVERLAP_LOOP_EFFECT:
  ∀ bd_ptr state n .
    ¬ state.nic.dead
    ∧ (FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop n bd_ptr state))
    ⇒
  ∃ bd_queue . ( BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM
                ∧ LENGTH bd_queue <= n
                ∧ EVERY BD_PTR_ADDRESS bd_queue
                ∧ EVERY (λ p . ¬ (get_alpha_bit p state.monitor.alpha
                                  ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 12w) state.monitor.alpha
                                 )
                        ) bd_queue
               )
Proof
  Induct_on ‘n’
  >- (fs [MAX_QUEUE_LENGTH_NUM_def , is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop_def]
      >> rpt strip_tac
      >> fs [BD_QUEUE_def]
      >> Cases_on ‘bd_ptr = 0w’
      >> fs_st []
     )
  >> rpt strip_tac
  >> Cases_on ‘bd_ptr = 0w’
  >- ( Q.EXISTS_TAC ‘[]’
       >> simp [BD_QUEUE_def, EVERY_DEF, LINKED_REL_def]
     )
  >> ‘FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop
                 n (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state)’
      by METIS_TAC [IVLICRANAQOL_NEXT ]
  >> ‘∃bd_queue.
           BD_QUEUE bd_queue (read_ndp bd_ptr state.nic.regs.CPPI_RAM)
              state.nic.regs.CPPI_RAM
          ∧ LENGTH bd_queue ≤ n
          ∧ EVERY BD_PTR_ADDRESS bd_queue
          ∧ EVERY (λp. ¬(get_alpha_bit p state.monitor.alpha ∨
                        get_alpha_bit (p + 4w) state.monitor.alpha ∨
                        get_alpha_bit (p + 8w) state.monitor.alpha ∨
                        get_alpha_bit (p + 12w) state.monitor.alpha))
                  bd_queue
’
            by (Q.PAT_X_ASSUM ‘∀bd_ptr state._’
                   (specl_assume_tac
                    [‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
                     , ‘state’])
                >> fs[]
                )
  >> Q.EXISTS_TAC ‘bd_ptr::bd_queue'’
  >> simp []
  >> rewrite_tac [BD_QUEUE_def]
  >> ‘BD_PTR_ADDRESS bd_ptr
      ∧ ¬(get_alpha_bit bd_ptr state.monitor.alpha ∨
          get_alpha_bit (bd_ptr + 4w) state.monitor.alpha ∨
          get_alpha_bit (bd_ptr + 8w) state.monitor.alpha ∨
          get_alpha_bit (bd_ptr + 12w) state.monitor.alpha)’
      by METIS_TAC [ IVLICRANAQOL_IMP_BD_PTR_ADDRESS_AND_NO_ALPHA_SET ]
  >> fs []
QED

Theorem IS_QUEUE_SELF_OVERLAP_INNER_LOOP_IMPL_NEXT:
  ∀ n bd_ptr other_bd_ptr queue state .
    ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS other_bd_ptr
  ∧ ¬FST (is_queue_self_overlap_inner_loop (SUC n) bd_ptr other_bd_ptr state)
  ⇒ ¬FST (is_queue_self_overlap_inner_loop n bd_ptr (read_ndp other_bd_ptr state.nic.regs.CPPI_RAM) state)
Proof
  NTAC 5 strip_tac
  >> Cases_on ‘other_bd_ptr = 0w’
  >- (strip_tac
      >> METIS_TAC [BD_PTR_ADDRESS_NOT_ZERO]
      )
  >> rewrite_tac [is_queue_self_overlap_inner_loop_def]
  >> simp_st []
  >> Cases_on ‘bd_ptr ≤₊ other_bd_ptr ∧ other_bd_ptr <₊ bd_ptr + 16w ∨
              other_bd_ptr ≤₊ bd_ptr ∧ bd_ptr <₊ other_bd_ptr + 16w’
  >- (simp_st [])
  >> simp_st []
  >> strip_tac
  >> ‘(get_next_descriptor_pointer other_bd_ptr state) = (read_ndp other_bd_ptr state.nic.regs.CPPI_RAM, state)’
      by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> Q.PAT_UNDISCH_TAC ‘¬ FST _’
  >> simp_st []
QED

Theorem IS_QUEUE_SELF_OVERLAP_INNER_LOOP_NO_OVERLAPS:
  ∀ n bd_ptr other_bd_ptr bd_queue state .
  LENGTH bd_queue <= n
  ∧ ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS bd_ptr
  ∧ BD_QUEUE bd_queue other_bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS bd_queue
  ∧ ¬ FST (is_queue_self_overlap_inner_loop n bd_ptr other_bd_ptr state)
  ⇒ (¬ (IN_QUEUE_RANGE bd_ptr bd_queue)
     ∧ EVERY (λ p . word_abs( bd_ptr - p) >= 16w) bd_queue
     )
Proof
  Induct_on ‘n’
  >- ( rpt strip_tac
       >> Cases_on ‘bd_queue'’
       >- fs [IN_QUEUE_RANGE_def]
       >> fs []
     )
  >> NTAC 5 strip_tac
  >> Cases_on ‘bd_queue'’
  >- (simp [IN_QUEUE_RANGE_def])
  >> once_rewrite_tac [IN_QUEUE_RANGE_def]
  >> ‘h = other_bd_ptr’
      by fs[BD_QUEUE_def]
  >> pat_x_rewrite_all ‘h = other_bd_ptr’
  >> ‘other_bd_ptr ≠ 0w’
      by METIS_TAC [EVERY_DEF , BD_PTR_ADDRESS_NOT_ZERO]
  >> ‘¬ IN_ALPHA_RANGE_OF other_bd_ptr bd_ptr
      ∧ word_abs (bd_ptr - other_bd_ptr) >= 16w’
      by ( Q.PAT_UNDISCH_TAC ‘¬FST _’
           >> rewrite_tac [is_queue_self_overlap_inner_loop_def]
           >> Cases_on ‘bd_ptr ≤₊ other_bd_ptr ∧ other_bd_ptr <₊ bd_ptr + 16w ∨
              other_bd_ptr ≤₊ bd_ptr ∧ bd_ptr <₊ other_bd_ptr + 16w’
           >- simp_st []
           >> simp_st []
           >> strip_tac
           >> once_rewrite_tac [IN_ALPHA_RANGE_OF_def]
           >> asm_rewrite_tac []
           >> ‘word_abs (bd_ptr + -1w * other_bd_ptr) >= 16w’
               by (Q.PAT_UNDISCH_TAC ‘¬(_ ∨ _)’
                   >> fs [EVERY_DEF]
                   >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS other_bd_ptr’
                   >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS bd_ptr’
                   >> rewrite_tac [EVAL “BD_PTR_ADDRESS other_bd_ptr”
                                   , EVAL “BD_PTR_ADDRESS bd_ptr”]
                   >> blastLib.BBLAST_PROVE_TAC
                  )
           >> Q.PAT_UNDISCH_TAC ‘¬(_ ∨ _)’
           >> RW_ASM_TAC “EVERY BD_PTR_ADDRESS _” EVERY_DEF
           >> SPLIT_ASM_TAC
           >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS other_bd_ptr’
           >> asm_rewrite_tac []
           >> simp [BD_PTR_ADDRESS_def
                    , address_spaceTheory.CPPI_RAM_BYTE_PA_def
                    , address_spaceTheory.CPPI_RAM_END_def
                    ]
           >> blastLib.BBLAST_PROVE_TAC
         )
  >> once_rewrite_tac [EVERY_DEF]
  >> ‘(λp. word_abs (bd_ptr − p) ≥ 16w) other_bd_ptr’ by fs[]
  >> asm_rewrite_tac []
  >> ‘BD_QUEUE t (read_ndp other_bd_ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
      by (RW_ASM_TAC “BD_QUEUE _ _ _” BD_QUEUE_def
          >> RW_ASM_TAC “EVERY _ _ ” EVERY_DEF
          >> SPLIT_ASM_TAC
          >> METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
         )
  >> RW_ASM_TAC “EVERY _ _” EVERY_DEF
  >> SPLIT_ASM_TAC
  >> Q.PAT_X_ASSUM ‘∀bd_ptr other_bd_ptr bd_queue state. _’
                   (specl_assume_tac
                        [‘bd_ptr’, ‘(read_ndp other_bd_ptr state.nic.regs.CPPI_RAM)’, ‘t’, ‘state’]
                   )
  >> ‘LENGTH t <= n’ by fs[]
  >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_INNER_LOOP_IMPL_NEXT]
QED

Theorem IS_QUEUE_SELF_OVERLAP_INNER_LOOP_EXISTS:
  ∀ n p1 p2 queue state .
   ¬ state.nic.dead
  ∧ BD_QUEUE (p2::queue) p2 state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS (p2::queue)
  ⇒
  ∃ b . (is_queue_self_overlap_inner_loop n p1 p2 state) = (b , state)
Proof
  Induct_on ‘n’
  >- ( rpt strip_tac
       >> simp_st [is_queue_self_overlap_inner_loop_def]
       >> METIS_TAC [EVERY_DEF , BD_PTR_ADDRESS_NOT_ZERO])
  >> NTAC 5 strip_tac
  >> rewrite_tac [is_queue_self_overlap_inner_loop_def]
  >> ‘p2 ≠ 0w’ by METIS_TAC [EVERY_DEF,BD_PTR_ADDRESS_NOT_ZERO]
  >> asm_rewrite_tac []
  >> Cases_on ‘p1 ≤₊ p2 ∧ p2 <₊ p1 + 16w ∨ p2 ≤₊ p1 ∧ p1 <₊ p2 + 16w’
  >- simp_st []
  >> simp_st []
  >> Q.PAT_X_ASSUM ‘¬_’ (fn _ => ALL_TAC)
  >> ‘get_next_descriptor_pointer p2 state = (read_ndp p2 state.nic.regs.CPPI_RAM , state)’
      by METIS_TAC [EVERY_DEF, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> Cases_on ‘queue’
  >- ( fs [BD_QUEUE_def]
       >> Cases_on ‘n’
       >> simp_st [is_queue_self_overlap_inner_loop_def]
     )
  >> simp []
  >> ‘read_ndp p2 state.nic.regs.CPPI_RAM = h’
      by fs [BD_QUEUE_def]
  >> METIS_TAC [BD_QUEUE_def , EVERY_DEF]
QED

Theorem IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_IMPL_NEXT:
  ∀ n bd_ptr queue state .
  ¬ state.nic.dead
  ∧ BD_QUEUE (bd_ptr::queue) bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS (bd_ptr::queue)
  ∧ ¬FST (is_queue_self_overlap_outer_loop (SUC n) bd_ptr state)
  ⇒ ¬FST (is_queue_self_overlap_outer_loop n (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state)
Proof
  NTAC 5 strip_tac
  >> Q.PAT_UNDISCH_TAC ‘¬FST _’
  >> rewrite_tac [is_queue_self_overlap_outer_loop_def]
  >> strip_tac
  >> ‘bd_ptr ≠ 0w’
      by (fs [BD_PTR_ADDRESS_NOT_ZERO])
  >> Q.PAT_UNDISCH_TAC ‘¬FST _’
  >> simp_st []
  >> Cases_on ‘queue’
  >- (fs [BD_QUEUE_def]
      >> ‘(get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM, state)’
          by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
      >> fs []
      >> rewrite_tac [MAX_QUEUE_LENGTH_NUM_def ]
      >> CONV_TAC (DEPTH_CONV numLib.num_CONV)
      >> simp_st [is_queue_self_overlap_inner_loop_def]
      >> simp_st [get_next_descriptor_pointer_def]
      )
  >> ‘(get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM, state)’
      by METIS_TAC [EVERY_DEF, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> asm_rewrite_tac []
  >> simp_st []
  >> ‘read_ndp bd_ptr state.nic.regs.CPPI_RAM = h’
      by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> simp []
  >> ‘∃ b . (is_queue_self_overlap_inner_loop MAX_QUEUE_LENGTH_NUM bd_ptr h
             state) = (b , state)’
      by (specl_assume_tac
          [‘MAX_QUEUE_LENGTH_NUM’, ‘bd_ptr’, ‘h’, ‘t’, ‘state’]
          IS_QUEUE_SELF_OVERLAP_INNER_LOOP_EXISTS
          >> fs []
          >> METIS_TAC [EVERY_DEF, BD_QUEUE_def])
  >> simp_st []
  >> Cases_on ‘b’
  >> simp_st []
QED

Theorem IQSOOINSO_FINAL:
  ∀ p1 p2 queue .
  EVERY (λp. word_abs (p1 − p) ≥ 16w) queue
  ∧ MEM p2 queue
  ⇒ word_abs (p1 − p2) ≥ 16w
Proof
  Induct_on ‘queue’
  >- simp [MEM]
  >> rpt strip_tac
  >> fs [EVERY_DEF]
QED

Theorem IS_QUEUE_SELF_OVERLAP_OUTERLOOP_IMPL_NO_SELF_OVERLAP:
  ∀ bd_queue bd_ptr state n .
  LENGTH bd_queue <= n
  ∧ LENGTH bd_queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS bd_queue
  ∧ ¬FST (is_queue_self_overlap_outer_loop n bd_ptr state)
  ⇒ (∀ p1 p2 . MEM p1 bd_queue ∧ MEM p2 bd_queue ∧ p1 ≠ p2
     ⇒ word_abs (p1 - p2) >= 16w)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘bd_queue'’
      >- fs [MEM]
      >> fs []
     )
  >> rpt strip_tac
  >> Cases_on ‘bd_queue'’
  >- fs [MEM]
  >> ‘h = bd_ptr ’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘¬FST (is_queue_self_overlap_outer_loop n (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state)’
      by METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_IMPL_NEXT]
  >> ‘BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
      by (RW_ASM_TAC “BD_QUEUE _ _ _” BD_QUEUE_def
          >> RW_ASM_TAC “EVERY _ _ ” EVERY_DEF
          >> SPLIT_ASM_TAC
          >> simp []
         )
  >> ‘LENGTH t <= n’ by fs []
  >> ‘∀p1 p2. MEM p1 t ∧ MEM p2 t ∧ p1 ≠ p2 ⇒
      word_abs (p1 − p2) ≥ 16w’
      by ( Q.PAT_X_ASSUM ‘∀bd_queue bd_ptr state._’
           (specl_assume_tac
                [‘t’, ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’, ‘state’]
           )
           >> fs []
         )
  >> Cases_on ‘t’
  >- (METIS_TAC [MEM])
  >> ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’
      by METIS_TAC [BD_QUEUE_def]
  >> ‘(get_next_descriptor_pointer bd_ptr state) = (h , state)’
      by ( METIS_TAC [EVERY_DEF, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE])
  >> pat_x_rewrite_all ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’
  >> ‘bd_ptr ≠ 0w’ by fs [BD_QUEUE_def]
  >> ‘¬FST (is_queue_self_overlap_inner_loop MAX_QUEUE_LENGTH_NUM bd_ptr h state)’
      by ( Q.PAT_UNDISCH_TAC ‘¬FST (is_queue_self_overlap_outer_loop (SUC n) bd_ptr state)’
           >> simp_st [is_queue_self_overlap_outer_loop_def]
           >> ‘∃ b. is_queue_self_overlap_inner_loop MAX_QUEUE_LENGTH_NUM bd_ptr h state = (b,state)’
               by ( specl_assume_tac
                    [‘MAX_QUEUE_LENGTH_NUM’
                     , ‘bd_ptr’ , ‘h’ , ‘t'’, ‘state’]
                    IS_QUEUE_SELF_OVERLAP_INNER_LOOP_EXISTS
                    >> fs []
                  )
           >> simp_st []
           >> Cases_on ‘b’
           >> simp_st []
         )
  >> ‘¬ (IN_QUEUE_RANGE bd_ptr (h::t'))
      ∧ EVERY (λp. word_abs (bd_ptr − p) ≥ 16w) (h::t')’
      by ( specl_assume_tac
           [‘MAX_QUEUE_LENGTH_NUM’
            , ‘bd_ptr’
            , ‘h’
            , ‘h::t'’
            , ‘state’
           ]
           IS_QUEUE_SELF_OVERLAP_INNER_LOOP_NO_OVERLAPS
           >> ‘LENGTH t' ≤ MAX_QUEUE_LENGTH_NUM’
               by fs []
           >> fs []
         )
  >> asm_rewrite_tac []
  >> Cases_on ‘p1 = bd_ptr’
  >- (‘MEM p2 (h::t')’ by fs[]
      >> METIS_TAC [ IQSOOINSO_FINAL]
     )
  >> Cases_on ‘p2 = bd_ptr’
  >- ( ‘MEM p1 (h::t')’ by fs[]
       >> METIS_TAC [ word_abs_diff, IQSOOINSO_FINAL]
     )
  >> ‘MEM p1 (h::t')’ by fs[]
  >> ‘MEM p2 (h::t')’ by fs[]
  >> METIS_TAC []
QED

Theorem IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_EXISTS:
  ∀ bd_queue bd_ptr state n .
  LENGTH bd_queue <= n
  ∧ LENGTH bd_queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS bd_queue
  ⇒ ∃ b . is_queue_self_overlap_outer_loop n bd_ptr state = (b , state)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> simp_st [is_queue_self_overlap_outer_loop_def]
     )
  >> rpt strip_tac
  >> simp_st [is_queue_self_overlap_outer_loop_def]
  >> Cases_on ‘bd_ptr = 0w’
  >- simp_st []
  >> simp_st []
  >> Cases_on ‘bd_queue'’
  >- (fs [BD_QUEUE_def])
  >> ‘h = bd_ptr’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘h = bd_ptr’
  >> ‘(get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM, state)’
      by METIS_TAC [EVERY_DEF, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> simp_st []
  >> Cases_on ‘t’
  >- (‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = 0w’
      by fs [BD_QUEUE_def]
      >> simp_st [MAX_QUEUE_LENGTH_NUM_def]
      >> CONV_TAC (DEPTH_CONV numLib.num_CONV)
      >> simp_st [is_queue_self_overlap_inner_loop_def]
      >> Cases_on ‘n’
      >> simp_st [is_queue_self_overlap_outer_loop_def]
     )
  >> ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’
      by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘∃b. is_queue_self_overlap_inner_loop MAX_QUEUE_LENGTH_NUM bd_ptr h state = (b,state)’
      by (specl_assume_tac
          [‘MAX_QUEUE_LENGTH_NUM’
           , ‘bd_ptr’
           , ‘h’
           , ‘t'’
           , ‘state’]
          IS_QUEUE_SELF_OVERLAP_INNER_LOOP_EXISTS
          >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
          >> simp_st []
          >> METIS_TAC [BD_QUEUE_def, EVERY_DEF]
         )
  >> simp_st []
  >> Cases_on ‘b’
  >- simp_st []
  >> simp []
  >> Q.PAT_X_ASSUM ‘∀bd_queue bd_ptr state._’
                   (specl_assume_tac
                    [‘(h::t')’, ‘h’, ‘state’])
  >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
  >> simp []
  >> ‘SUC (LENGTH t') ≤ n’
      by fs []
  >> ‘SUC (LENGTH t') ≤ MAX_QUEUE_LENGTH_NUM’
      by fs []
  >> simp []
  >> METIS_TAC [EVERY_DEF, BD_QUEUE_def]
QED

Theorem IS_QUEUE_SELF_OVERLAP_EXISTS:
  ∀ bd_queue bd_ptr state .
   LENGTH bd_queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS bd_queue
  ⇒ (∃ b . is_queue_self_overlap bd_ptr state = (b , state))
Proof
  rpt strip_tac
  >> rewrite_tac [is_queue_self_overlap_def]
  >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_EXISTS]
QED


Theorem ITSEPLFSCIL_EFFECT:
  ∀ bd_ptr state n queue start_length_sum .
  bd_ptr ≠ 0w
  ∧ ¬ state.nic.dead
  ∧ LENGTH queue <= n
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ⇒ (∃ (ptr : 32 word) buffer_length_sum .
     ((is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop n bd_ptr start_length_sum state)
      = ((ptr , buffer_length_sum) , state ))
     ∧ ((ptr = 0w)
        ∨ ( ((tx_read_bd ptr state.nic.regs.CPPI_RAM).sop
             ∨ (tx_read_bd ptr state.nic.regs.CPPI_RAM).eop)
          ∧ (∃ pre_q post_q .
             ((queue = pre_q ++ [ptr] ++ post_q)
              ∧ (buffer_length_sum = FOLDL (λ sum pa . sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl) start_length_sum pre_q)))
          )
       )
    )
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘queue’
      >> fs [BD_QUEUE_def]
     )
  >> rpt strip_tac
  >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop_def]
  >> Cases_on ‘queue’
  >- (fs [BD_QUEUE_def])
  >> ‘h = bd_ptr’
      by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> simp_st []
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by fs []
  >> simp_st [IS_SOP]
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop’
  >- (fs [] >> simp_st [IS_SOP] >> Q.EXISTS_TAC ‘[]’ >> Q.EXISTS_TAC ‘t’ >> simp [])
  >> simp_st [IS_EOP]
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop’
  >- (simp_st [IS_EOP] >> Q.EXISTS_TAC ‘[]’ >> Q.EXISTS_TAC ‘t’ >> simp [])
  >> simp_st [GET_TX_BUFFER_LENGTH]
  >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> Cases_on ‘t’
  >- (‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = 0w’
      by ( fs [BD_QUEUE_def])
      >> simp_st []
      >> Cases_on ‘n’
      >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop_def]
      >> METIS_TAC []
     )
  >> ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’
      by ( fs [BD_QUEUE_def])
  >> simp []
  >> ‘h ≠ 0w’ by fs [BD_QUEUE_def]
  >> ‘∃ ptr buffer_length_sum .
        is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop
        n h ((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl + start_length_sum) state =
                ((ptr , buffer_length_sum) ,state) ∧
                (ptr = 0w ∨
                 ( ((tx_read_bd ptr state.nic.regs.CPPI_RAM).sop
                   ∨ (tx_read_bd ptr state.nic.regs.CPPI_RAM).eop)
                 ∧ (∃pre_q post_q.
                    ((h::t') = pre_q ⧺ [ptr] ⧺ post_q)
                    ∧  (buffer_length_sum = FOLDL (λ sum pa . sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl) ((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl + start_length_sum) pre_q))))’
                by ( ‘LENGTH (h::t') <= n’ by fs []
                     >> Q.PAT_X_ASSUM ‘∀bd_ptr state queue start_length_sum ._’
                                      (specl_assume_tac
                                       [‘h’, ‘state’, ‘(h::t')’, ‘((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl + start_length_sum)’]
                                      )
                     >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                     >> asm_rewrite_tac []
                     >> fs [BD_QUEUE_def]
                     >> METIS_TAC []
                   )
  >| [Q.EXISTS_TAC ‘ptr’
      >> Q.EXISTS_TAC ‘buffer_length_sum’
      >> simp []
      >> fs []
      , Q.EXISTS_TAC ‘ptr’
        >> Q.EXISTS_TAC ‘buffer_length_sum’
        >> simp []
        >> fs []
        >> Cases_on ‘ptr = 0w’
        >- simp []
        >> simp []
        >> Q.EXISTS_TAC ‘(bd_ptr::pre_q)’
        >> Q.EXISTS_TAC ‘post_q’
        >> simp []
      , Q.EXISTS_TAC ‘ptr’
        >> Q.EXISTS_TAC ‘buffer_length_sum’
        >> simp []
        >> fs []
        >> Cases_on ‘ptr = 0w’
        >- simp []
        >> simp []
        >> Q.EXISTS_TAC ‘(bd_ptr::pre_q)’
        >> Q.EXISTS_TAC ‘post_q’
        >> simp []
      ]
QED

Theorem ITSEPLFSCOL_FINAL_UTIL:
  ∀ q1 q2 p state .
  ¬ state.nic.dead
  ∧ LENGTH q2 ≠ 0
  ∧ EVERY BD_PTR_ADDRESS q1
  ∧ BD_QUEUE (q1 ++ q2) p state.nic.regs.CPPI_RAM
  ⇒ (EVERY (λp. ¬FST (is_eop p state)
            ⇒ (read_ndp p state.nic.regs.CPPI_RAM) ≠ 0w) q1)
Proof
  Induct_on ‘q1’
  >- (fs [])
  >> rpt strip_tac
  >> fs [BD_QUEUE_def, EVERY_DEF]
  >> ‘get_next_descriptor_pointer h state =
      (read_ndp h state.nic.regs.CPPI_RAM,state)’
      by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> ‘read_ndp h state.nic.regs.CPPI_RAM ≠ 0w’
      by (Cases_on ‘q2’
          >- fs []
          >> Cases_on ‘q1’
          >> fs [BD_QUEUE_def]
          >> METIS_TAC []
         )
  >> simp []
  >> ‘read_ndp p state.nic.regs.CPPI_RAM ≠ 0w’
      by (Cases_on ‘q2’
          >- fs []
          >> Cases_on ‘q1’
          >> fs [BD_QUEUE_def]
          >> METIS_TAC []
         )
  >> simp []
  >> Q.PAT_X_ASSUM ‘h = p’ (fn t => fs [t])
  >> METIS_TAC []
QED


Theorem TX_BL_FOLD_COMM:
  ∀ bd_ptr queue start_sum CPPI_RAM .
  FOLDL (λ sum p . sum + (tx_read_bd p CPPI_RAM).bl) start_sum  (bd_ptr::queue)
  = (tx_read_bd bd_ptr CPPI_RAM).bl
  + FOLDL (λ sum p . sum + (tx_read_bd p CPPI_RAM).bl) start_sum queue
Proof
  Induct_on ‘queue’
  >- simp []
  >> rpt strip_tac
  >> fs []
QED

Theorem TX_BL_FOLDR_EQ_FOLDR:
  ∀ queue acc CPPI_RAM .
  FOLDL (λ sum p . sum + (tx_read_bd p CPPI_RAM).bl) acc queue
  = FOLDR (λ p sum . (tx_read_bd p CPPI_RAM).bl + sum) acc queue
Proof
  Induct_on ‘queue’
  >- simp []
  >> rpt strip_tac
  >> fs [TX_BL_FOLD_COMM]
QED


Theorem BD_QUEUE_TX_SUM_NO_OVERFLOW:
  ∀ queue CPPI_RAM n .
  LENGTH queue <= MAX_QUEUE_LENGTH_NUM
  ∧ queue ≠ []
  ⇒ (FOLDR (λ p sum . sum + (tx_read_bd p CPPI_RAM).bl) 0w queue
    ≤₊ n2w (LENGTH queue) * 65535w)
Proof
  Induct_on ‘queue’
  >- (simp [MAX_QUEUE_LENGTH_NUM_def])
  >> rpt strip_tac
  >> simp []
  >> ‘(tx_read_bd h CPPI_RAM).bl ≤₊ 65535w’
      by simp [GSYM read_utilsTheory.TX_BL_TO_READ_BD, TX_BL_AND_SIZE]
  >> Cases_on ‘queue = []’
  >- (simp [TX_BL_AND_SIZE] >> WORD_DECIDE_TAC)
  >> ‘LENGTH queue <= MAX_QUEUE_LENGTH_NUM’
      by fs []
  >> ‘FOLDR (λp sum. sum + (tx_read_bd p CPPI_RAM).bl)
      0w queue ≤₊ 65535w * n2w (LENGTH queue)’
      by fs []
  >> fs [MAX_QUEUE_LENGTH_NUM_def]
  >> Q.ABBREV_TAC ‘a = (tx_read_bd h CPPI_RAM).bl’
  >> Q.ABBREV_TAC ‘b = FOLDR (λp sum. sum + (tx_read_bd p CPPI_RAM).bl) 0w queue’
  >> Cases_on ‘LENGTH queue’
  >- (simp []
      >> ‘b = 0w’
          by (Q.PAT_UNDISCH_TAC ‘b ≤₊ 65535w * 0w’
              >> blastLib.BBLAST_PROVE_TAC)
      >> simp []
     )
  >> Q.PAT_UNDISCH_TAC ‘b ≤₊ _’
  >> Q.PAT_UNDISCH_TAC ‘a ≤₊ _’
  >> Q.PAT_UNDISCH_TAC ‘SUC n <= 256’
  >> Q.PAT_UNDISCH_TAC ‘SUC (SUC n) <= 256’
  >> rpt (Cases_on_goal (find_term is_var)
          >- (simp [] >> blastLib.BBLAST_PROVE_TAC)
          >> simp [])
QED

Theorem ITSEPLFSCOL_EFFECT:
  ∀ bd_ptr state n queue .
  LENGTH queue <= n
  ∧ LENGTH queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop n bd_ptr state)
  ⇒ (EVERY (λ p . ¬ FST (is_eop p state) ⇒ read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) queue
    ∧ EVERY (λ p . (tx_read_bd p state.nic.regs.CPPI_RAM).bl <₊ 2048w) queue)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘queue’
      >- ( ‘bd_ptr = 0w’ by fs [BD_QUEUE_def]
           >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
         )
      >> fs []
     )
  >> ntac 4 strip_tac
  >> Q.PAT_UNDISCH_TAC ‘FST _’
  >> rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
  >> Cases_on ‘queue’
  >- ( ‘bd_ptr = 0w’ by fs [BD_QUEUE_def]
       >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
     )
  >> ‘h = bd_ptr’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘h  = bd_ptr’
  >> fs []
  >> ‘bd_ptr ≠ 0w’ by fs [BD_QUEUE_def]
  >> simp_st [IS_SOP]
  >> simp_st [IS_EOP]
  >> COND_CASES_TAC
  >- simp_st []
  >> simp_st [GET_TX_BUFFER_LENGTH
              , GET_PACKET_LENGTH]
  >> simp_st [IS_EOP]
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop’
  >- ( simp_st []
       >> COND_CASES_TAC
       >- simp_st []
       >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
       >> strip_tac
       >> ‘BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
           by fs [ BD_QUEUE_def ]
       >> ‘LENGTH t <= MAX_QUEUE_LENGTH_NUM’ by fs []
       >> ‘LENGTH t <= n’ by fs []
       >> fs_st []
       >> ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).pl <₊ 2048w’
           by (rewrite_tac [GSYM PL_TO_READ_BD, PL_def] >> blastLib.BBLAST_PROVE_TAC)
       >> ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl <₊ 2048w’
           by (METIS_TAC [])
       >> fs_st []
       >> Q.PAT_X_ASSUM ‘∀bd_ptr state queue._’
                        (specl_assume_tac
                         [‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
                          , ‘state’
                          , ‘t’])
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> asm_rewrite_tac []
     )
  >> simp_st []
  >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> Cases_on ‘t’
  >- ( ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = 0w’ by fs [BD_QUEUE_def]
       >> rewrite_tac [MAX_QUEUE_LENGTH_NUM_def]
       >> CONV_TAC (DEPTH_CONV numLib.num_CONV)
       >> rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop_def]
       >> simp_st []
       >> ‘b’ by fs []
       >> simp_st []
       >> METIS_TAC []
     )
  >> ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘BD_QUEUE (h::t') h state.nic.regs.CPPI_RAM’ by fs [BD_QUEUE_def]
  >> ‘h ≠ 0w’ by fs [BD_QUEUE_def]
  >> ‘∃ptr buffer_length_sum.
        is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop
          MAX_QUEUE_LENGTH_NUM h (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl state =
        ((ptr,buffer_length_sum), state) ∧
        (ptr = 0w ∨
         ((tx_read_bd ptr state.nic.regs.CPPI_RAM).sop ∨
          (tx_read_bd ptr state.nic.regs.CPPI_RAM).eop) ∧
         ∃pre_q post_q.
             (h::t') = pre_q ⧺ [ptr] ⧺ post_q ∧
             buffer_length_sum =
             FOLDL
               (λsum pa.
                sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
               (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl
               pre_q)’
      by ( specl_assume_tac
           [‘h’ , ‘state’, ‘MAX_QUEUE_LENGTH_NUM’, ‘h::t'’
            , ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl’]
           ITSEPLFSCIL_EFFECT
           >> ‘LENGTH (h::t') <= MAX_QUEUE_LENGTH_NUM’ by fs[]
           >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
           >> simp []
         )
  >> fs_st []
  >| [ simp_st []
       >> ‘BD_PTR_ADDRESS ptr’
           by ( ‘EVERY BD_PTR_ADDRESS (pre_q ⧺ [ptr] ⧺ post_q)’
                by ( ‘EVERY BD_PTR_ADDRESS (h::t')’
                     by fs[]
                     >> Q.PAT_UNDISCH_TAC ‘EVERY BD_PTR_ADDRESS (h::t')’
                     >> asm_rewrite_tac []
                   )
                >> fs [EVERY_MEM]
              )
       >> ‘ptr ≠ 0w’
           by (Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS ptr’
               >> EVAL_TAC
               >> WORD_DECIDE_TAC
              )
       >> simp_st [GET_TX_BUFFER_LENGTH
                   , IS_SOP]
       , simp_st []
         >> ‘BD_PTR_ADDRESS ptr’
             by ( ‘EVERY BD_PTR_ADDRESS (pre_q ⧺ [ptr] ⧺ post_q)’
                  by ( ‘EVERY BD_PTR_ADDRESS (h::t')’
                       by fs[]
                       >> Q.PAT_UNDISCH_TAC ‘EVERY BD_PTR_ADDRESS (h::t')’
                       >> asm_rewrite_tac []
                     )
                  >> fs [EVERY_MEM]
                )
         >> ‘ptr ≠ 0w’
             by (Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS ptr’
                 >> EVAL_TAC
                 >> WORD_DECIDE_TAC
                )
         >> simp_st [GET_TX_BUFFER_LENGTH, IS_SOP]
         >> Cases_on ‘(tx_read_bd ptr state.nic.regs.CPPI_RAM).sop’
         >> simp_st []
         >> COND_CASES_TAC
         >- simp_st []
         >> simp_st []
         >> ‘BD_QUEUE (ptr::post_q) ptr state.nic.regs.CPPI_RAM’
             by METIS_TAC [BD_QUEUE_MID]
         >> ‘BD_QUEUE post_q (read_ndp ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
             by fs [bd_queueTheory.BD_QUEUE_def]
         >> ‘EVERY BD_PTR_ADDRESS (ptr::post_q)’
             by (‘EVERY BD_PTR_ADDRESS (h::t')’
                 by fs[]
                 >> ‘EVERY BD_PTR_ADDRESS (pre_q ⧺ [ptr] ⧺ post_q)’
                     by METIS_TAC []
                 >> Induct_on ‘pre_q’
                 >- simp []
                 >> strip_tac
                 >> fs []
                )
         >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
         >> strip_tac
         >> fs_st []
         >> Q.PAT_X_ASSUM ‘∀bd_ptr state queue._’
                          (specl_assume_tac
                           [‘(read_ndp ptr state.nic.regs.CPPI_RAM)’, ‘state’, ‘post_q’])
         >> ‘LENGTH post_q <= n ∧ LENGTH post_q <= MAX_QUEUE_LENGTH_NUM’
             by ( ‘LENGTH post_q <= LENGTH (pre_q ⧺ [ptr] ⧺ post_q)’
                  by simp_st  []
                  >> ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) <= n’
                      by ( ‘LENGTH (h :: t') <= n’
                           by fs[]
                           >> METIS_TAC []
                         )
                  >> ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) <= MAX_QUEUE_LENGTH_NUM’
                      by ( ‘LENGTH (h :: t') <= MAX_QUEUE_LENGTH_NUM’
                           by fs[]
                           >> METIS_TAC []
                         )
                  >> fs []
                )
         >> ‘EVERY (λp. ¬FST (is_eop p state) ⇒
                    (read_ndp p state.nic.regs.CPPI_RAM) ≠ 0w) post_q’
             by METIS_TAC []
         >> ‘EVERY BD_PTR_ADDRESS pre_q’
             by ( ‘EVERY BD_PTR_ADDRESS (h::t')’
                  by fs[]
                  >> METIS_TAC [EVERY_APPEND]
                )
         >> ‘((¬FST (is_eop h state) ⇒ read_ndp h state.nic.regs.CPPI_RAM ≠ 0w) ∧
              EVERY (λp. ¬FST (is_eop p state) ⇒
                     read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) t')’
             by (fs_st []
                 >> Cases_on ‘pre_q’
                 >- (fs [] >> simp_st [IS_EOP])
                 >> fs []
                 >> specl_assume_tac
                    [‘h' :: t’, ‘[ptr] ++ post_q’
                     , ‘h'’, ‘state’]
                    ITSEPLFSCOL_FINAL_UTIL
                 >> ‘LENGTH ([ptr] ++ post_q) ≠ 0n’
                     by fs []
                 >> ‘BD_QUEUE (h'::t ⧺ ([ptr] ⧺ post_q)) h' state.nic.regs.CPPI_RAM’
                     by (fs [])
                 >> fs [IS_EOP]
                )
         >> asm_rewrite_tac []
         >> Q.ABBREV_TAC ‘max = (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).pl’
         >> ‘max <₊ 2048w’
             by (Q.UNABBREV_TAC ‘max’
                 >> rewrite_tac [GSYM PL_TO_READ_BD]
                 >> simp [PL_AND_SIZE]
                )
         >> fs []
         >> ‘FOLDL (λsum pa. sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
             (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl
             pre_q
             =  FOLDL (λsum pa. sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
                      0w (bd_ptr :: pre_q)’
             by fs []
         >> pat_x_rewrite_all ‘_=_’
         >> Cases_on ‘pre_q’
         >- ( fs []
              >> ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl ≤₊ 65535w
                  ∧ (tx_read_bd ptr state.nic.regs.CPPI_RAM).bl ≤₊ 65535w’
                  by METIS_TAC [TX_BL_AND_SIZE
                                , TX_BL_TO_READ_BD
                               ]
              >> Q.PAT_UNDISCH_TAC ‘max = _ + _’
              >> Q.PAT_UNDISCH_TAC ‘max <₊ 2048w’
              >> Q.PAT_UNDISCH_TAC ‘_ ≤₊ 65535w’
              >> Q.PAT_UNDISCH_TAC ‘_ ≤₊ 65535w’
              >> blastLib.BBLAST_PROVE_TAC
            )
         >> ‘FOLDL (λsum pa. sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
                  0w (bd_ptr :: h' :: t)
             = FOLDR (λ pa sum . sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
             0w (bd_ptr :: h' :: t)’
             by (fs [TX_BL_FOLDR_EQ_FOLDR])
         >> pat_x_rewrite_all ‘_=_’
         >> ‘(tx_read_bd ptr state.nic.regs.CPPI_RAM).bl
             + FOLDR (λpa sum. sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
                     0w (bd_ptr::h'::t)
             = FOLDR (λpa sum. sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
                     0w (ptr :: bd_ptr::h'::t)’
             by fs []
         >> pat_x_rewrite_all ‘_=_’
         >> Q.ABBREV_TAC ‘queue = (ptr :: bd_ptr :: h' :: t)’
         >> ‘LENGTH queue <= MAX_QUEUE_LENGTH_NUM
             ∧ queue ≠ []’
             by (Q.UNABBREV_TAC ‘queue’ >> fs [])
         >> ‘FOLDR (λ pa sum . sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
                   0w queue ≤₊ n2w (LENGTH queue) * 65535w’
             by (METIS_TAC [BD_QUEUE_TX_SUM_NO_OVERFLOW])
         >> ‘FOLDR (λ pa sum . sum + (tx_read_bd pa state.nic.regs.CPPI_RAM).bl)
             0w queue
             = FOLDR $+ 0w (MAP (λ pa . (tx_read_bd pa state.nic.regs.CPPI_RAM).bl) queue)’
             by (fs [rich_listTheory.FOLDR_MAP])
         >> pat_x_rewrite_all ‘_=_’
         >> ‘EVERY (λ w . w ≤₊ 65535w) (MAP (λ pa . (tx_read_bd pa state.nic.regs.CPPI_RAM).bl) queue)’
             by (simp [listTheory.EVERY_MAP
                       , GSYM TX_BL_TO_READ_BD
                       , TX_BL_AND_SIZE
                      ]
                )
         >> Q.ABBREV_TAC ‘queue' = (MAP (λ pa . (tx_read_bd pa state.nic.regs.CPPI_RAM).bl) queue)’
         >> ‘EVERY (λ w . w <₊ 2048w) queue'’
             by ( ‘LENGTH queue' <= MAX_QUEUE_LENGTH_NUM’
                  by (Q.UNABBREV_TAC ‘queue'’ >> fs [])
                  >> METIS_TAC [BD_QUEUE_TX_SUM_NO_2048_PIDGEON_HOLE
                                , MAX_QUEUE_LENGTH_NUM_def]
                )
         >> Q.UNABBREV_TAC ‘queue'’
         >> fs [TX_BL_TO_READ_BD]
         >> Q.UNABBREV_TAC ‘queue’
         >> fs [EVERY_MAP]
     ]
QED

Theorem ITSEPLFSCOL_CHEAT_EFFECT:
  ∀ bd_ptr state n queue .
  LENGTH queue <= n
  ∧ LENGTH queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ queue . BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
     ⇒ TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM
    )
  ∧ FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop n bd_ptr state)
  ⇒ (EVERY (λ p . (tx_read_bd p state.nic.regs.CPPI_RAM).bl = (tx_read_bd p state.nic.regs.CPPI_RAM).pl) queue)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘queue’
      >- ( ‘bd_ptr = 0w’ by fs [BD_QUEUE_def]
           >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
         )
      >> fs []
     )
  >> ntac 4 strip_tac
  >> Q.PAT_UNDISCH_TAC ‘FST _’
  >> rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
  >> Cases_on ‘queue’
  >- ( ‘bd_ptr = 0w’ by fs [BD_QUEUE_def]
       >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
     )
  >> ‘h = bd_ptr’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘h  = bd_ptr’
  >> fs []
  >> ‘bd_ptr ≠ 0w’ by fs [BD_QUEUE_def]
  >> simp_st [IS_SOP]
  >> simp_st [IS_EOP]
  >> COND_CASES_TAC
  >- simp_st []
  >> simp_st [GET_TX_BUFFER_LENGTH
              , GET_PACKET_LENGTH]
  >> simp_st [IS_EOP]
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop’
  >- ( simp_st []
       >> COND_CASES_TAC
       >- simp_st []
       >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
       >> strip_tac
       >> ‘BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
           by fs [ BD_QUEUE_def ]
       >> ‘LENGTH t <= MAX_QUEUE_LENGTH_NUM’ by fs []
       >> ‘LENGTH t <= n’ by fs []
       >> fs_st []
       >> Q.PAT_X_ASSUM ‘∀bd_ptr state queue._’
                        (specl_assume_tac
                         [‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
                          , ‘state’
                          , ‘t’])
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> asm_rewrite_tac []
       >> ‘(∀queue.
              BD_QUEUE queue (read_ndp bd_ptr state.nic.regs.CPPI_RAM)
                state.nic.regs.CPPI_RAM ⇒
                TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM)’
           by ( strip_tac
                >> Q.PAT_X_ASSUM ‘∀queue._’ (specl_assume_tac [‘(bd_ptr::t)’] )
                >> ‘TX_LINUX_BD_QUEUE_SOP_EOP_MATCH (bd_ptr::t) state.nic.regs.CPPI_RAM’
                    by fs []
                >> ‘BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM)
                    state.nic.regs.CPPI_RAM’
                    by fs [BD_QUEUE_def]
                >> strip_tac
                >> ‘queue = t’
                    by METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
                >> fs [txInvariantWellDefinedTheory.TX_LINUX_BD_QUEUE_SOP_EOP_MATCH_def]
              )
       >> asm_rewrite_tac []
     )
  >> Q.PAT_X_ASSUM ‘∀queue._’ (specl_assume_tac [‘(bd_ptr::t)’] )
  >> ‘TX_LINUX_BD_QUEUE_SOP_EOP_MATCH (bd_ptr::t) state.nic.regs.CPPI_RAM’
      by fs []
  >> fs [txInvariantWellDefinedTheory.TX_LINUX_BD_QUEUE_SOP_EOP_MATCH_def
         , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_EOP_def
         , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_def
         , txInvariantWellDefinedTheory.TX_LINUX_BD_EOP_def
         ]
QED

Theorem ITSEPLFSCOL_EXISTS:
  ∀ bd_ptr state n queue .
  LENGTH queue <= n
  ∧ LENGTH queue <= MAX_QUEUE_LENGTH_NUM
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ⇒ (∃ b . (is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop n bd_ptr state)
     = (b , state))
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> fs_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
     )
  >> rpt strip_tac
  >> Cases_on ‘queue’
  >- (fs_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def
             , BD_QUEUE_def])
  >> ‘h ≠ 0w’ by fs [BD_QUEUE_def]
  >> ‘bd_ptr = h’ by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘bd_ptr = h’
  >> asm_rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_loop_def]
  >> simp_st []
  >> ‘BD_PTR_ADDRESS h’
      by fs []
  >> simp_st [IS_SOP]
  >> COND_CASES_TAC
  >- simp_st []
  >> simp_st []
  >> simp_st [GET_TX_BUFFER_LENGTH, GET_PACKET_LENGTH]
  >> simp_st [IS_EOP]
  >> Cases_on ‘(tx_read_bd h state.nic.regs.CPPI_RAM).eop’
  >- (simp_st []
      >> COND_CASES_TAC
      >> simp_st []
      >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
      >> ‘BD_QUEUE t (read_ndp h state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
          by METIS_TAC [BD_QUEUE_def]
      >> ‘LENGTH t <= MAX_QUEUE_LENGTH_NUM’
          by fs[]
      >> ‘LENGTH t <= n’
          by fs[]
      >> METIS_TAC [EVERY_DEF]
     )
  >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> Cases_on ‘t’
  >- (‘(read_ndp h state.nic.regs.CPPI_RAM) = 0w’
      by fs [BD_QUEUE_def]
      >> simp_st [MAX_QUEUE_LENGTH_NUM_def]
      >> CONV_TAC (DEPTH_CONV numLib.num_CONV)
      >> simp_st [is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop_def]
     )
  >> ‘(read_ndp h state.nic.regs.CPPI_RAM) = h'’
      by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> simp_st []
  >> ‘h' ≠ 0w’ by fs [BD_QUEUE_def]
  >> ‘BD_QUEUE (h'::t') h' state.nic.regs.CPPI_RAM’
      by fs[BD_QUEUE_def]
  >> ‘∃ptr bls . is_transmit_SOP_EOP_packet_length_fields_set_correctly_inner_loop
      MAX_QUEUE_LENGTH_NUM h' (tx_read_bd h state.nic.regs.CPPI_RAM).bl state = ((ptr, bls) ,state)
      ∧(ptr = 0w ∨
        ∃pre_q post_q.
        (h'::t') = pre_q ⧺ [ptr] ⧺ post_q ∧
        (FST (is_sop ptr state) ∨ FST (is_eop ptr state)))’
      by ( specl_assume_tac
           [‘h'’ , ‘state’, ‘MAX_QUEUE_LENGTH_NUM’, ‘h'::t'’, ‘(tx_read_bd h state.nic.regs.CPPI_RAM).bl’]
           ITSEPLFSCIL_EFFECT
           >> ‘LENGTH (h'::t') <= MAX_QUEUE_LENGTH_NUM’
               by fs[]
           >> Q.PAT_UNDISCH_TAC ‘_ ⇒ _’
           >> simp []
           >> fs []
           >> strip_tac
           >> Q.EXISTS_TAC ‘ptr’
           >> Q.EXISTS_TAC ‘buffer_length_sum’
           >> simp []
           >> ‘BD_PTR_ADDRESS ptr’
               by (‘MEM ptr (h'::t')’
                   by METIS_TAC [MEM, MEM_APPEND]
                   >> ‘EVERY BD_PTR_ADDRESS (h'::t')’
                       by fs []
                   >> METIS_TAC [EVERY_MEM]
                   )
           >> ‘ptr ≠ 0w’
               by fs [BD_PTR_ADDRESS_NOT_ZERO]
           >> simp []
           >> Q.EXISTS_TAC ‘pre_q’ >> Q.EXISTS_TAC ‘post_q’
           >> simp [IS_SOP, IS_EOP]
         )
  >> simp_st []
  >> ‘BD_PTR_ADDRESS ptr’
      by (‘MEM ptr (h'::t')’
          by METIS_TAC [MEM, MEM_APPEND]
          >> ‘EVERY BD_PTR_ADDRESS (h'::t')’
              by fs []
          >> METIS_TAC [EVERY_MEM]
         )
  >> Cases_on ‘ptr = 0w’
  >> simp_st [GET_TX_BUFFER_LENGTH, IS_SOP, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> COND_CASES_TAC
  >> simp_st []
  >> COND_CASES_TAC
  >> simp_st [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> ‘BD_QUEUE (ptr::post_q) ptr state.nic.regs.CPPI_RAM’
      by METIS_TAC [BD_QUEUE_MID]
  >> ‘EVERY BD_PTR_ADDRESS (ptr::post_q)’
      by (‘EVERY BD_PTR_ADDRESS (h::t')’
          by fs[]
          >> ‘EVERY BD_PTR_ADDRESS (pre_q ⧺ [ptr] ⧺ post_q)’
              by (METIS_TAC [EVERY_DEF])
          >> fs [EVERY_MEM]
         )
  >> ‘LENGTH (ptr :: post_q) <= n ∧ LENGTH (ptr :: post_q) <= MAX_QUEUE_LENGTH_NUM’
      by ( ‘LENGTH (ptr :: post_q) <= LENGTH (pre_q ⧺ [ptr] ⧺ post_q)’
           by simp_st  []
           >> ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) <= n’
               by ( ‘LENGTH (h' :: t') <= n’
                    by fs[]
                    >> METIS_TAC []
                  )
           >> ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) <= MAX_QUEUE_LENGTH_NUM’
               by ( ‘LENGTH (h' :: t') <= MAX_QUEUE_LENGTH_NUM’
                    by fs[]
                    >> METIS_TAC []
                  )
           >> ‘LENGTH (ptr::post_q) ≤ n’
               by (Q.PAT_UNDISCH_TAC ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) ≤ n’
                   >> fs [])
           >> Q.PAT_UNDISCH_TAC ‘LENGTH (pre_q ⧺ [ptr] ⧺ post_q) ≤ MAX_QUEUE_LENGTH_NUM’
           >> fs []
         )
  >> ‘BD_QUEUE post_q (read_ndp ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
      by fs [bd_queueTheory.BD_QUEUE_def]
  >> ‘LENGTH post_q <= MAX_QUEUE_LENGTH_NUM’
      by fs []
  >> ‘LENGTH post_q <= n’
      by fs []
  >> fs []
  >> METIS_TAC []
QED

Theorem IS_DATA_BUFFER_SECURE_EXISTS:
  ∀ bd_ptr transmit' state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS bd_ptr
  ⇒ ∃ b . is_data_buffer_secure bd_ptr transmit' state = (b, state)
Proof
  rpt strip_tac
  >> simp_st [is_data_buffer_secure_def]
  >> ‘∃ ptr . (get_buffer_pointer bd_ptr state) = (ptr,state)’
      by ( METIS_TAC [GET_BUFFER_POINTER_EXISTS]
         )
  >> ‘∃ ptr . get_buffer_offset_and_length bd_ptr state = (ptr ,state)’
      by METIS_TAC [GET_BUFFER_OFFSET_AND_LENGTH_EXISTS]
  >> simp_st []
  >> Cases_on ‘transmit'’
  >- (simp_st []
      >> ‘∃ b . is_sop bd_ptr state = (b, state)’
          by METIS_TAC [IS_SOP_EXISTS]
      >> simp_st []
      >> Cases_on ‘b’
      >- (simp_st []
          >> Cases_on ‘ptr' && TX_BL = 0w’
          >- (simp_st [])
          >> simp_st []
          >> COND_CASES_TAC
          >- simp_st []
          >> simp_st [is_secure_linux_memory_def]
          >> Cases_on ‘¬ARB’
          >- simp_st []
          >> simp_st []
         )
      >> simp_st []
      >> Cases_on ‘ptr' && TX_BL = 0w’
      >- (simp_st [])
      >> simp_st []
      >> COND_CASES_TAC
      >- simp_st []
      >> simp_st []
      >> simp_st [is_secure_linux_memory_def]
      >> Cases_on ‘¬ARB’
      >- simp_st []
      >> simp_st []
     )
  >> simp_st []
  >> Cases_on ‘ptr' && RX_BL = 0w’
  >- (simp_st [])
  >> simp_st []
  >> COND_CASES_TAC
  >- simp_st []
  >> simp_st []
  >> simp_st [is_secure_linux_memory_def]
  >> Cases_on ‘¬ARB’
  >- simp_st []
  >> simp_st []
QED


Theorem IS_DATA_BUFFER_SECURE_TX_EFFECT:
  ∀ bd_ptr transmit' state .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS bd_ptr
  ∧ FST (is_data_buffer_secure bd_ptr T state)
  ⇒ ( (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl >₊ 0w
      ∧ if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop
        then ((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bp ≤₊
              4294967295w − (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bo − (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl + 1w)
        else ((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bp ≤₊
              4294967295w − (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl + 1w)

      )
Proof
  ntac 4 strip_tac
  >> UNDISCH_TAC “FST (is_data_buffer_secure bd_ptr T state)”
  >> simp_st [is_data_buffer_secure_def]
  >> simp_st [GET_BUFFER_POINTER
              , GET_BUFFER_OFFSET_AND_LENGTH]
  >> simp [TX_BO_TO_TX_READ
           , IS_SOP]
  >> simp [TX_BL_TO_TX_READ]
  >> ‘∃ bo . ((if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop then
                 (λs. ((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bo,s))
                 else (λs. (0w,s))) state) = (bo , state)
      ∧ if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop
      then  bo  = (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bo
      else bo = 0w’
      by (COND_CASES_TAC >> simp [])
  >> ‘bo ≤₊ 0xFFFFw’
      by (Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop’
          >> fs []
          >> simp [CPPI_RAMTheory.tx_read_bd_def]
          >> blastLib.BBLAST_PROVE_TAC
         )
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl = 0w’
  >- simp []
  >> simp []
  >> COND_CASES_TAC
  >- simp []
  >> ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).bl ≤₊ 0xFFFFw’
      by (simp [GSYM TX_BL_TO_TX_READ, TX_BL_def]
          >> blastLib.BBLAST_PROVE_TAC
         )
  >> Q.ABBREV_TAC ‘m = (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM)’
  >> Q.ABBREV_TAC ‘bp = m.bp ’
  >> Q.ABBREV_TAC ‘bo = m.bo ’
  >> Q.ABBREV_TAC ‘bl = m.bl ’
  >> ‘(bl + bp + -1w ≥₊ bp)’
      by ( Q.PAT_UNDISCH_TAC ‘_ ≤₊ 65535w’ >> Q.PAT_UNDISCH_TAC ‘_ ≤₊ 65535w’
           >> Q.PAT_UNDISCH_TAC ‘¬ _’ >> Q.PAT_UNDISCH_TAC ‘bl ≠ 0w’
           >> blastLib.BBLAST_PROVE_TAC
         )
  >> Cases_on ‘m.sop’
  >> fs []
  >> WORD_DECIDE_TAC
QED





Theorem IS_DATA_BUFFER_SECURE_QUEUE_EXISTS:
  ∀ bd_ptr queue transmit' state n .
  LENGTH queue <= n
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ ¬ state.nic.dead
  ⇒ ∃ b . is_data_buffer_secure_queue_loop n bd_ptr transmit' state = (b, state)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> rewrite_tac [is_data_buffer_secure_queue_loop_def]
      >> ‘bd_ptr = 0w’
          by (Cases_on ‘queue’
              >> fs [BD_QUEUE_def]
             )
      >> simp_st []
     )
  >> rpt strip_tac
  >> Cases_on ‘queue’
  >- (‘bd_ptr = 0w’
      by fs [BD_QUEUE_def]
      >> rewrite_tac [is_data_buffer_secure_queue_loop_def]
      >> simp_st []
     )
  >> rewrite_tac [is_data_buffer_secure_queue_loop_def]
  >> ‘h = bd_ptr’
      by fs [BD_QUEUE_def]
  >> pat_x_rewrite_all ‘h = bd_ptr’
  >> ‘bd_ptr ≠ 0w’
      by fs [BD_QUEUE_def]
  >> ‘∃ b . is_data_buffer_secure bd_ptr transmit' state = (b ,state)’
      by METIS_TAC [EVERY_DEF, IS_DATA_BUFFER_SECURE_EXISTS]
  >> Cases_on ‘¬b’
  >- simp_st []
  >> simp_st []
  >> ‘(get_next_descriptor_pointer bd_ptr state) = (read_ndp bd_ptr state.nic.regs.CPPI_RAM,state)’
      by METIS_TAC [EVERY_DEF, GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> simp_st []
  >> ‘BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state.nic.regs.CPPI_RAM’
      by fs[BD_QUEUE_def]
  >> Q.PAT_X_ASSUM ‘∀bd_ptr queue transmit' state._’
                   (specl_assume_tac
                    [‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
                     , ‘t’
                     , ‘transmit'’
                     , ‘state’
                    ])
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> fs []
QED


Theorem IS_DATA_BUFFER_SECURE_QUEUE_TX_EFFECT:
  ∀ bd_ptr queue state n .
  LENGTH queue <= n
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ ¬ state.nic.dead
  ∧ FST (is_data_buffer_secure_queue_loop n bd_ptr T state)
  ⇒ EVERY (λ p . ( (tx_read_bd p state.nic.regs.CPPI_RAM).bl >₊ 0w
                   ∧ if (tx_read_bd p state.nic.regs.CPPI_RAM).sop
                   then ((tx_read_bd p state.nic.regs.CPPI_RAM).bp ≤₊
                         4294967295w − (tx_read_bd p state.nic.regs.CPPI_RAM).bo − (tx_read_bd p state.nic.regs.CPPI_RAM).bl + 1w)
                   else ((tx_read_bd p state.nic.regs.CPPI_RAM).bp ≤₊
                         4294967295w − (tx_read_bd p state.nic.regs.CPPI_RAM).bl + 1w)
                 ))
  queue
Proof
  Induct_on ‘queue’
  >- simp []
  >> rpt strip_tac
  >> Q.PAT_UNDISCH_TAC ‘FST _’
  >> Cases_on ‘n’
  >- fs [is_data_buffer_secure_queue_loop_def]
  >> simp [is_data_buffer_secure_queue_loop_def]
  >> ‘bd_ptr = h’
      by fs [bd_queueTheory.BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘h ≠ 0w’
      by fs [bd_queueTheory.BD_QUEUE_def]
  >> ‘BD_PTR_ADDRESS h’ by fs[]
  >> simp_st []
  >> ‘∃ b . (is_data_buffer_secure h T state) = (b, state)’
      by METIS_TAC [IS_DATA_BUFFER_SECURE_EXISTS, EVERY_DEF]
  >> Cases_on ‘¬b’
  >- simp_st []
  >> ‘b’ by fs []
  >> simp_st []
  >> ‘FST (is_data_buffer_secure h T state)’
      by fs []
  >> ‘LENGTH queue <= n'’
      by fs []
  >> ‘EVERY BD_PTR_ADDRESS queue’
      by fs []
  >> simp [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> RW_ASM_TAC “BD_QUEUE (h::queue) h state.nic.regs.CPPI_RAM” bd_queueTheory.BD_QUEUE_def
  >> SPLIT_ASM_TAC

  >> ‘∃ b . ((is_data_buffer_secure_queue_loop n'
             (read_ndp h state.nic.regs.CPPI_RAM) T state)) = (b , state)’
      by (METIS_TAC [IS_DATA_BUFFER_SECURE_QUEUE_EXISTS])
  >> simp []
  >> Cases_on ‘¬b'’
  >> fs []
  >> specl_assume_tac
     [‘h’, ‘T’, ‘state’]
     IS_DATA_BUFFER_SECURE_TX_EFFECT
  >> fs []
  >> METIS_TAC []
QED

Definition set_and_clear_local_effect_def:
  set_and_clear_local_effect
  (ptr : 32 word)  offset (set : 32 word)
  (clear : 32 word) modify_sop_or_eop CPPI_RAM
  =
  (let cppi_val = read_bd_word (ptr + offset) 0w CPPI_RAM;
   flags_val = read_bd_word (ptr + 12w) 0w CPPI_RAM;
   sop = (flags_val && SOP ≠ 0w);
   eop = (flags_val && EOP ≠ 0w)
   in (if ((sop ∧ modify_sop_or_eop)
           ∨ (eop ∧ ¬ modify_sop_or_eop))
       then write_32bit_word ((cppi_val || set) && ~clear) (ptr + offset) CPPI_RAM
       else CPPI_RAM
      ))
End

Definition set_and_clear_word_on_sop_or_eop_foldl_effect_def:
  set_and_clear_word_on_sop_or_eop_foldl_effect
  (queue : 32 word list) CPPI_RAM offset (set : 32 word) (clear : 32 word) modify_sop_or_eop  =
  (FOLDL (λ CPPI_RAM' p .
          set_and_clear_local_effect p offset set clear modify_sop_or_eop CPPI_RAM'
         ) CPPI_RAM
   queue)
End

Definition set_and_clear_word_on_sop_or_eop_foldr_effect_def:
  set_and_clear_word_on_sop_or_eop_foldr_effect
  (queue : 32 word list) CPPI_RAM offset (set : 32 word) (clear : 32 word) modify_sop_or_eop  =
  (FOLDR (λ p CPPI_RAM' .
          set_and_clear_local_effect p offset set clear modify_sop_or_eop CPPI_RAM'
         ) CPPI_RAM
   queue)
End

Theorem SET_AND_CLEAR_LOCAL_EFFECT_COMM_GEN:
  ∀ p1 p2 set1 clear1 msoe1 set2 clear2 msoe2  CPPI_RAM .
  word_abs (p1 - p2) >= 16w
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒
  set_and_clear_local_effect
    p1 FLAGS set1 clear1 msoe1
    (set_and_clear_local_effect p2 FLAGS set2 clear2 msoe2 CPPI_RAM)
    = set_and_clear_local_effect
      p2 FLAGS set2 clear2 msoe2
      (set_and_clear_local_effect p1 FLAGS set1 clear1 msoe1 CPPI_RAM)
Proof
  rpt strip_tac
  >> ‘word_abs ((p1 + 12w) - (p2 + 12w)) >= 4w’
      by (Q.PAT_UNDISCH_TAC ‘_ >= _’
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘word_abs ((p2 + 12w) - (p1 + 12w)) >= 4w’
      by METIS_TAC [word_abs_diff]
  >> ‘CPPI_RAM_BYTE_PA (p1 + 12w)
      ∧ (p1 + 12w) < CPPI_RAM_END − 3w’
      by (UNDISCH_TAC “BD_PTR_ADDRESS p1”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘CPPI_RAM_BYTE_PA (p2 + 12w)
      ∧ (p2 + 12w) < CPPI_RAM_END − 3w’
      by (UNDISCH_TAC “BD_PTR_ADDRESS p2”
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
  >> ‘read_bd_word (p1 + 12w) 0w
      (write_32bit_word
       (¬clear2 && (set2 ‖ read_bd_word (p2 + 12w) 0w CPPI_RAM))
       (p2 + 12w) CPPI_RAM)
      = read_bd_word (p1 + 12w) 0w CPPI_RAM’
      by METIS_TAC [WRITE_ONLY_CHANGES_FOUR_BYTES]
  >> ‘read_bd_word (p2 + 12w) 0w
      (write_32bit_word
       (¬clear1 && (set1 ‖ read_bd_word (p1 + 12w) 0w CPPI_RAM))
       (p1 + 12w) CPPI_RAM)
       = read_bd_word (p2 + 12w) 0w CPPI_RAM’
       by METIS_TAC [WRITE_ONLY_CHANGES_FOUR_BYTES]
  >> rewrite_tac [set_and_clear_local_effect_def, FLAGS_def]
  >> simp []
  >> COND_CASES_TAC
  >> simp []
  >> COND_CASES_TAC
  >> simp []
  >> COND_CASES_TAC
  >> simp []
  >> COND_CASES_TAC
  >> simp []
  >> TRY (METIS_TAC [WRITE_32BITS_DIFF_COMM])
QED

Theorem SET_AND_CLEAR_LOCAL_EFFECT_COMM:
  ∀ p1 p2 set clear modify_sop_or_eop CPPI_RAM .
  word_abs (p1 - p2) >= 16w
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒
  set_and_clear_local_effect
    p1 12w set clear modify_sop_or_eop
    (set_and_clear_local_effect p2 12w set clear modify_sop_or_eop CPPI_RAM)
    = set_and_clear_local_effect
      p2 12w set clear modify_sop_or_eop
      (set_and_clear_local_effect p1 12w set clear modify_sop_or_eop CPPI_RAM)
Proof
  METIS_TAC [SET_AND_CLEAR_LOCAL_EFFECT_COMM_GEN, FLAGS_def]
QED

Theorem FOLDR_SET_AND_CLEAR_LOCAL_EFFECT_MONIOID_COMM:
  ∀ h queue CPPI_RAM set' clear modify_sop_or_eop .
    (∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒
            word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT (h::queue)
    ∧ EVERY BD_PTR_ADDRESS (h::queue)
    ⇒ (FOLDR
          (λp CPPI_RAM'.
               set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
                 CPPI_RAM')
          ((λCPPI_RAM' p.
                set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
                  CPPI_RAM') CPPI_RAM h) queue =
        FOLDR
          (λp CPPI_RAM'.
               set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
               CPPI_RAM') CPPI_RAM (h::queue)
      )
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> fs []
     )
  >> rpt strip_tac
  >> ‘(FOLDR
             (λp CPPI_RAM'.
                  set_and_clear_local_effect p 12w set' clear
                    modify_sop_or_eop CPPI_RAM')
             (set_and_clear_local_effect h' 12w set' clear modify_sop_or_eop
              CPPI_RAM) queue)
      = (set_and_clear_local_effect h' 12w set' clear modify_sop_or_eop
             (FOLDR
                (λp CPPI_RAM'.
                     set_and_clear_local_effect p 12w set' clear
                     modify_sop_or_eop CPPI_RAM') CPPI_RAM queue))’
      by ( ‘EVERY BD_PTR_ADDRESS (h'::queue)’ by fs[]
           >>  ‘∀p1 p2.
                p1 ≠ p2 ∧ MEM p1 (h'::queue) ∧ MEM p2 (h'::queue) ⇒
                word_abs (p1 − p2) ≥ 16w’
                by METIS_TAC [MEM]
           >> ‘ALL_DISTINCT (h'::queue)’
               by fs []
           >> Q.PAT_X_ASSUM ‘∀h CPPI_RAM set' clear modify_sop_or_eop. _’
                            (specl_assume_tac
                             [‘h'’, ‘CPPI_RAM’, ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
                            )
           >> Q.PAT_UNDISCH_TAC ‘_⇒_’
           >> simp []
         )
  >> simp []
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_=_’ (fn _ => ALL_TAC)
  >> ‘word_abs (h - h') >= 16w’
      by ( Q.PAT_X_ASSUM ‘∀ p1 p2 . _’
           (specl_assume_tac
            [‘h’, ‘h'’]
           )
           >> ‘h ≠ h'’ by fs []
           >> fs []
         )
  >> ‘BD_PTR_ADDRESS h ∧ BD_PTR_ADDRESS h'’
      by fs[]
  >> METIS_TAC [SET_AND_CLEAR_LOCAL_EFFECT_COMM]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR:
  ∀ (start_ptr : 32 word) (queue : 32 word list) set clear modify_sop_or_eop CPPI_RAM .
  EVERY BD_PTR_ADDRESS queue
  ∧ ALL_DISTINCT queue
  ∧ (∀ p1 p2  .
     p1 ≠ p2
     ∧ MEM p1 queue
     ∧ MEM p2 queue
     ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒ set_and_clear_word_on_sop_or_eop_foldl_effect
        queue CPPI_RAM 12w set clear modify_sop_or_eop
        = set_and_clear_word_on_sop_or_eop_foldr_effect
        queue CPPI_RAM 12w set clear modify_sop_or_eop
Proof
  Induct_on ‘queue’
  >- (simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def
            ,set_and_clear_word_on_sop_or_eop_foldl_effect_def
           ])
  >> rpt strip_tac
  >> rewrite_tac [set_and_clear_word_on_sop_or_eop_foldr_effect_def
                  , set_and_clear_word_on_sop_or_eop_foldl_effect_def
                 ]
  >> REWRITE_ASSUMS_TAC set_and_clear_word_on_sop_or_eop_foldr_effect_def
  >> REWRITE_ASSUMS_TAC set_and_clear_word_on_sop_or_eop_foldl_effect_def
  >> rewrite_tac [FOLDL]
  >> ‘FOLDL
          (λCPPI_RAM' p.
               set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
                 CPPI_RAM')
          ((λCPPI_RAM' p.
                set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
                CPPI_RAM') CPPI_RAM h) queue
          = FOLDR
              (λp CPPI_RAM'.
                   set_and_clear_local_effect p 12w set' clear
                   modify_sop_or_eop CPPI_RAM')
              ((λCPPI_RAM' p.
                set_and_clear_local_effect p 12w set' clear modify_sop_or_eop
                CPPI_RAM') CPPI_RAM h) queue’
              by ( ‘ALL_DISTINCT queue’ by fs []
                   >> ‘EVERY BD_PTR_ADDRESS queue’ by fs[]
                   >> ‘∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w’
                       by fs []
                   >> METIS_TAC [])
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_ = _’ (fn _ => ALL_TAC)
  >> METIS_TAC [FOLDR_SET_AND_CLEAR_LOCAL_EFFECT_MONIOID_COMM]
QED

Theorem BD_PTR_WORD_ABS_GT_16_FLAGS_DIFF_GT_4:
  ∀ p1 p2 CPPI_RAM set clear modify_sop_or_eop .
  word_abs (p1  - p2) >= 16w
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒ (word_abs ((p2 + 12w) - p1) ≥ 4w
     ∧ word_abs ((p2 + 8w) - p1) ≥ 4w
     ∧ word_abs ((p2 + 4w) - p1) ≥ 4w
     ∧ word_abs (p2 - p1) ≥ 4w)
Proof
  NTAC 7 strip_tac
  >> ‘¬ IN_ALPHA_RANGE_OF p2 p1’
      by (UNDISCH_TAC “BD_PTR_ADDRESS p1”
          >> UNDISCH_TAC “BD_PTR_ADDRESS p2”
          >> EVAL_TAC
          >> UNDISCH_ALL_TAC
          >> blastLib.BBLAST_PROVE_TAC
         )
  >> ‘(word_abs (p2 + 12w − p1) ≥ 4w ∧ word_abs (p2 + 8w − p1) ≥ 4w ∧
      word_abs (p2 + 4w − p1) ≥ 4w ∧ word_abs (p2 − p1) ≥ 4w)
      = (word_abs (p1 - (p2 + 12w)) ≥ 4w ∧ word_abs (p1 - (p2 + 8w)) ≥ 4w ∧
         word_abs (p1 - (p2 + 4w)) ≥ 4w ∧ word_abs (p1 − (p2 + 0w)) ≥ 4w)’
      by METIS_TAC [word_abs_diff, WORD_ADD_0]
  >> pat_x_rewrite_all ‘_=_’
  >> METIS_TAC [BD_PTR_ADDRESS_PLUS_N_WORD_ABS, BD_PTR_ADDRESS_def]
QED


Theorem READ_NDP_ON_SET_SAME_BD_PTR_CANCEL:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop .
  BD_PTR_ADDRESS bd_ptr
  ⇒ read_ndp bd_ptr (set_and_clear_local_effect bd_ptr 12w set clear modify_sop_or_eop CPPI_RAM)
  = read_ndp bd_ptr CPPI_RAM
Proof
  rpt strip_tac
  >> simp [bdTheory.read_ndp_def, set_and_clear_local_effect_def]
  >> COND_CASES_TAC
  >- ( simp []
       >> ‘word_abs ((bd_ptr + 12w) - bd_ptr) ≥ 4w’
           by (blastLib.BBLAST_PROVE_TAC)
       >> ‘ CPPI_RAM_BYTE_PA bd_ptr
            ∧ CPPI_RAM_BYTE_PA (bd_ptr + 12w)
            ∧ bd_ptr < (CPPI_RAM_END − 3w)
            ∧ (bd_ptr + 12w) < (CPPI_RAM_END − 3w)
            ’
            by ( Q.PAT_X_ASSUM ‘_∨_’ (fn _ => ALL_TAC)
                 >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS bd_ptr’
                 >> EVAL_TAC
                 >> blastLib.BBLAST_PROVE_TAC
               )
       >> METIS_TAC [WRITE_ONLY_CHANGES_FOUR_BYTES]
     )
  >> simp []
QED

Theorem READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop n .
  BD_PTR_ADDRESS bd_ptr
  ∧  (n = 0w ∨ n = 4w ∨ n = 8w)
  ⇒ read_ndp (bd_ptr + n) (set_and_clear_local_effect bd_ptr 12w set clear modify_sop_or_eop CPPI_RAM)
  = read_ndp (bd_ptr + n) CPPI_RAM
Proof
  ntac 6 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> simp [bdTheory.read_ndp_def, set_and_clear_local_effect_def]
  >> COND_CASES_TAC
  >- ( simp []
       >> ‘word_abs ((bd_ptr + 12w) - (bd_ptr + n)) ≥ 4w’
           by (Q.PAT_UNDISCH_TAC ‘n = _ ∨ _’
               >> blastLib.BBLAST_PROVE_TAC)
       >> ‘ CPPI_RAM_BYTE_PA (bd_ptr + n)
            ∧ CPPI_RAM_BYTE_PA (bd_ptr + 12w)
            ∧ (bd_ptr + n) < (CPPI_RAM_END − 3w)
            ∧ (bd_ptr + 12w) < (CPPI_RAM_END − 3w)
            ’
            by ( Q.PAT_X_ASSUM ‘_∨_’ (fn _ => ALL_TAC)
                 >> Q.PAT_UNDISCH_TAC  ‘n = _ ∨ _’
                 >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS bd_ptr’
                 >> EVAL_TAC
                 >> blastLib.BBLAST_PROVE_TAC
               )
       >> METIS_TAC [WRITE_ONLY_CHANGES_FOUR_BYTES]
     )
  >> simp []
QED


Theorem READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL_ALT:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop n .
  BD_PTR_ADDRESS bd_ptr
  ∧  (n = 0w ∨ n = 1w ∨ n = 2w)
  ⇒ read_bd_word bd_ptr n (set_and_clear_local_effect bd_ptr 12w set clear modify_sop_or_eop CPPI_RAM)
  = read_bd_word bd_ptr n CPPI_RAM
Proof
  ntac 6 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> specl_assume_tac
     [‘bd_ptr’, ‘CPPI_RAM’, ‘set'’, ‘clear’, ‘modify_sop_or_eop’, ‘n * 4w’]
     READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL
  >> Q.ABBREV_TAC ‘hol_is_retarded = (n * 4w = 0w ∨ n * 4w = 4w ∨ n * 4w = 8w)’
  >> ‘hol_is_retarded’
      by (fs [] >> Q.UNABBREV_TAC ‘hol_is_retarded’ >> WORD_DECIDE_TAC)
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> simp [bdTheory.read_ndp_def, CPPI_RAMTheory.read_bd_word_def]
  >> fs []
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_SELF:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop .
  read_bd_word (bd_ptr + 12w) 0w
  (set_and_clear_local_effect bd_ptr FLAGS set clear modify_sop_or_eop CPPI_RAM)
  = (if SOP && read_bd_word (bd_ptr + 12w) 0w CPPI_RAM ≠ 0w ∧
            modify_sop_or_eop ∨
            EOP && read_bd_word (bd_ptr + 12w) 0w CPPI_RAM ≠ 0w ∧
            ¬modify_sop_or_eop
          then (¬clear && (set ‖ read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM))
          else read_bd_word (bd_ptr + 12w) 0w CPPI_RAM)
Proof
  rpt strip_tac
  >> simp [set_and_clear_local_effect_def]
  >> COND_CASES_TAC
  >- (simp [FLAGS_def] >> METIS_TAC [READ_BD_WORD_ON_WRITE_BD_WORD])
  >> simp []
QED

Theorem READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL:
  ∀ p1 p2 n CPPI_RAM set clear modify_sop_or_eop .
  word_abs (p1  - p2) >= 16w
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒
  read_ndp (p1 + n) (set_and_clear_local_effect p2 12w set clear modify_sop_or_eop CPPI_RAM)
  = read_ndp (p1 + n) CPPI_RAM
Proof
  ntac 7 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> simp [set_and_clear_local_effect_def]
  >> Cases_on ‘SOP && read_bd_word (p2 + 12w) 0w CPPI_RAM ≠ 0w ∧
             modify_sop_or_eop ∨
             EOP && read_bd_word (p2 + 12w) 0w CPPI_RAM ≠ 0w ∧
             ¬modify_sop_or_eop’
  >- ( simp []
       >> ‘word_abs ((p2 + 12w) - (p1 + n)) ≥ 4w’
           by (METIS_TAC [word_abs_diff, BD_PTR_ADDRESS_DIFF_GT_16_ALL_BYTE_ADDR_DIFF_GT_4])
       >> ‘CPPI_RAM_BYTE_PA (p1 + n)
           ∧ (p1 + n) < CPPI_RAM_END - 3w’
           by METIS_TAC [BD_PTR_ADDRESS_BYTE_LEMMAS]

       >> ‘ CPPI_RAM_BYTE_PA (p2 + 12w)
            ∧ (p2 + 12w) < (CPPI_RAM_END − 3w)
            ’
            by ( METIS_TAC [BD_PTR_ADDRESS_BYTE_LEMMAS])
       >> rewrite_tac [bdTheory.read_ndp_def]
       >> specl_assume_tac
                [‘p2 + 12w’, ‘p1 + n’, ‘(¬clear && (set' ‖ read_bd_word (p2 + 12w) 0w CPPI_RAM))’
                 , ‘CPPI_RAM’]
                WRITE_ONLY_CHANGES_FOUR_BYTES
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> simp []
     )
  >> simp []
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_DISJUNCT_FOLDR:
  ∀ bd_ptr queue n CPPI_RAM set clear modify_sop_or_eop .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒
  read_ndp (bd_ptr + n)
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp (bd_ptr + n) CPPI_RAM
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> fs [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
     )
  >> ntac 7 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> REWRITE_ASSUMS_TAC set_and_clear_word_on_sop_or_eop_foldr_effect_def
  >> REWRITE_ASSUMS_TAC EVERY_DEF
  >> SPLIT_ASM_TAC
  >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> once_rewrite_tac [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> ‘read_ndp (bd_ptr + n)
          (set_and_clear_local_effect h 12w set' clear modify_sop_or_eop
             (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                CPPI_RAM 12w set' clear modify_sop_or_eop))
      = read_ndp (bd_ptr + n)
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by ( ‘word_abs (bd_ptr - h) >= 16w’
               by fs []
               >> METIS_TAC [READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL]
             )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘(∀p. MEM p queue ⇒ word_abs (bd_ptr -  p) ≥ 16w)’
      by fs []
  >> REWRITE_ASSUMS_TAC (GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def)
  >> METIS_TAC []
QED


Theorem READ_PL_ON_SET_AND_CLEAR_WORD_SELF:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop .
        ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
         ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
         ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
        ⇒
        (10 -- 0) (read_bd_word (bd_ptr + 12w) 0w
                   (set_and_clear_local_effect bd_ptr FLAGS set clear modify_sop_or_eop CPPI_RAM))
        = (10 -- 0) (read_bd_word (bd_ptr + 12w) 0w
                     CPPI_RAM)
Proof
  ntac 5 strip_tac
  >> disch_tac
  >> simp [READ_NDP_ON_SET_AND_CLEAR_WORD_SELF]
  >> COND_CASES_TAC
  >- (fs [OWNER_def, SOP_BD_def, EOQ_def, EOP_def, TD_def, FLAGS_def]
      >> blastLib.BBLAST_PROVE_TAC
     )
  >> simp []
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_DISJUNCT_FOLDR_STATE:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ⇒
  read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp bd_ptr state.nic.regs.CPPI_RAM
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> fs [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
     )
  >> rpt strip_tac
  >> fs [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> rewrite_tac [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> ‘read_ndp bd_ptr
          (set_and_clear_local_effect h 12w set' clear modify_sop_or_eop
             (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
      = read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by ( ‘word_abs (bd_ptr - h) >= 16w’
               by fs []
               >> METIS_TAC [WORD_ADD_0, READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL]
             )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘(∀p. MEM p queue ⇒ word_abs (bd_ptr + -1w * p) ≥ 16w)’
      by fs []
  >> REWRITE_ASSUMS_TAC (GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def)
  >> METIS_TAC []
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_QUEUE_HEAD_FOLDR:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
    EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒
  read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp bd_ptr state.nic.regs.CPPI_RAM
Proof
  rpt strip_tac
  >> Cases_on ‘queue’
  >- (fs [BD_QUEUE_def, set_and_clear_word_on_sop_or_eop_foldr_effect_def] )
  >> ‘h = bd_ptr’
      by fs [BD_QUEUE_def]
  >> Q.PAT_X_ASSUM ‘h = bd_ptr’ (fn t => rewrite_tac [t] >> REWRITE_ASSUMS_TAC t)
  >> rewrite_tac [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> simp []
  >> rewrite_tac [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> ‘read_ndp bd_ptr
          (set_and_clear_local_effect bd_ptr 12w set' clear modify_sop_or_eop
             (set_and_clear_word_on_sop_or_eop_foldr_effect t
              state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
          = read_ndp bd_ptr (set_and_clear_word_on_sop_or_eop_foldr_effect t
                             state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by (rewrite_tac [set_and_clear_local_effect_def]
              >> simp []
              >> COND_CASES_TAC
              >- ( simp [bdTheory.read_ndp_def]
                   >> METIS_TAC [WRITE_FLAGS_DOESN_NOT_CHANGE_NEXT_PTR]
                 )
              >> simp []
             )
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘_=_’ (fn _ => ALL_TAC)
  >> ‘(∀ p . MEM p t ⇒ word_abs (bd_ptr − p) ≥ 16w)’
      by (Q.PAT_X_ASSUM ‘∀ p1 p2 . _’
          (specl_assume_tac [‘bd_ptr’])
          >> fs []
          >> strip_tac
          >> Cases_on ‘MEM p t’
          >- (fs []
              >> Q.PAT_X_ASSUM ‘∀ p2 . _’
                               (specl_assume_tac [‘p’])
              >> fs []
              >> ‘ALL_DISTINCT (bd_ptr :: t)’
                  by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
              >> fs []
              >> METIS_TAC []
             )
          >> fs []
         )
  >> fs [BD_QUEUE_def]
  >> specl_assume_tac
     [‘bd_ptr’, ‘t’, ‘state’, ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
     READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_DISJUNCT_FOLDR_STATE
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> simp []
QED



Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_MEM_FOLDR:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
   EVERY BD_PTR_ADDRESS queue
  ∧ MEM bd_ptr queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒
  read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp bd_ptr state.nic.regs.CPPI_RAM
Proof
  Induct_on ‘queue’
  >- (fs [set_and_clear_word_on_sop_or_eop_foldr_effect_def])
  >> rpt strip_tac
  >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> simp [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> Cases_on ‘h = bd_ptr’
  >- ( pat_x_rewrite_all ‘_=_’
       >> ‘BD_PTR_ADDRESS bd_ptr’
           by fs []
       >> ‘read_ndp bd_ptr
          (set_and_clear_local_effect bd_ptr 12w set' clear modify_sop_or_eop
             (set_and_clear_word_on_sop_or_eop_foldr_effect queue
              state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
          = read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by METIS_TAC [READ_NDP_ON_SET_SAME_BD_PTR_CANCEL]
       >> pat_x_rewrite_all ‘_=_’
       >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by fs []
       >> Cases_on ‘MEM bd_ptr queue’
       >- METIS_TAC [EVERY_DEF]
       >> ‘(∀p. MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)’
           by ( strip_tac
                >> Q.PAT_X_ASSUM ‘∀p1 p2. _’
                                 (specl_assume_tac
                                  [‘bd_ptr’, ‘p’]
                                 )
                >> Cases_on ‘bd_ptr = p’
                >- (fs [])
                >> fs []
              )
       >> METIS_TAC [EVERY_DEF, READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_DISJUNCT_FOLDR_STATE]
     )
  >> ‘read_ndp bd_ptr
          (set_and_clear_local_effect h 12w set' clear modify_sop_or_eop
             (set_and_clear_word_on_sop_or_eop_foldr_effect queue
              state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
          = read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by ( ‘word_abs(bd_ptr - h) >= 16w’
               by fs []
               >> METIS_TAC [EVERY_DEF, EVERY_MEM, WORD_ADD_0
                             ,READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL]
             )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by fs []
  >> METIS_TAC [MEM, EVERY_DEF]
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_MEM_FOLDL:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
   EVERY BD_PTR_ADDRESS queue
  ∧ MEM bd_ptr queue
  ∧ ALL_DISTINCT queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒
  read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldl_effect queue
             state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp bd_ptr state.nic.regs.CPPI_RAM
Proof
  rpt strip_tac
  >> simp [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR]
  >> METIS_TAC [READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_MEM_FOLDR]
QED

Theorem READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_SAME_FOLDL:
  ∀ (bd_ptr : 32 word) start_ptr (queue : 32 word list) state set clear modify_sop_or_eop .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE queue start_ptr state.nic.regs.CPPI_RAM
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ⇒
  read_ndp bd_ptr
          (set_and_clear_word_on_sop_or_eop_foldl_effect queue
             state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) =
          read_ndp bd_ptr state.nic.regs.CPPI_RAM
Proof
  rpt strip_tac
  >> ‘(set_and_clear_word_on_sop_or_eop_foldl_effect queue
       state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)
      = (set_and_clear_word_on_sop_or_eop_foldr_effect queue
         state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
      by ( ‘ALL_DISTINCT queue’
           by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
           >> METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR]
         )
  >> METIS_TAC [READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_DISJUNCT_FOLDR_STATE]
QED

Theorem BD_QUEUE_UNRELEVANT_CPPI_UPDATE_INDEPENDANT:
  ∀  other_bd_ptr bd_ptr queue CPPI_RAM set clear modify_sop_or_eop .
     BD_PTR_ADDRESS other_bd_ptr
  ∧  EVERY BD_PTR_ADDRESS queue
  ∧ (∀p . MEM p queue ⇒ word_abs (other_bd_ptr − p) ≥ 16w)
  ⇒
     BD_QUEUE queue bd_ptr
          (set_and_clear_local_effect other_bd_ptr 12w set clear modify_sop_or_eop
           CPPI_RAM)
     = BD_QUEUE queue bd_ptr CPPI_RAM
Proof
  Induct_on ‘queue’
  >- (fs [BD_QUEUE_def])
  >> rpt strip_tac
  >> simp [BD_QUEUE_def]
  >> ‘BD_PTR_ADDRESS h’
      by fs [BD_QUEUE_def]
  >> ‘(read_ndp h
             (set_and_clear_local_effect other_bd_ptr 12w set' clear
                modify_sop_or_eop CPPI_RAM))
      = read_ndp h CPPI_RAM’
      by ( ‘word_abs (h - other_bd_ptr) >= 16w’
           by ( Q.PAT_X_ASSUM ‘∀ p . _’ (specl_assume_tac [‘h’])
                >> once_rewrite_tac [word_abs_diff]
                >>  fs[]
              )
           >> METIS_TAC [WORD_ADD_0, READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL]
         )
  >> asm_rewrite_tac []
  >> ‘∀p. MEM p queue ⇒ word_abs (other_bd_ptr − p) ≥ 16w’
      by fs[]
  >> ‘EVERY BD_PTR_ADDRESS queue’
      by fs[]
  >> METIS_TAC [BD_QUEUE_def]
QED

Theorem BD_QUEUE_UPDATED_FLAGS_HEAD_CPPI_UPDATE_INDEPENDANT:
  ∀  bd_ptr queue CPPI_RAM set clear modify_sop_or_eop .
    EVERY BD_PTR_ADDRESS (bd_ptr::queue)
  ∧ (∀p1 p2. p1 ≠ p2 ∧ (MEM p1 (bd_ptr::queue)) ∧ ( MEM p2 (bd_ptr::queue))
     ⇒ word_abs (p1 + -1w * p2) ≥ 16w)
  ∧ BD_QUEUE (bd_ptr::queue) bd_ptr CPPI_RAM
  ⇒ BD_QUEUE (bd_ptr::queue) bd_ptr
          (set_and_clear_local_effect bd_ptr 12w set clear modify_sop_or_eop
           CPPI_RAM)
Proof
  rpt strip_tac
  >> simp [BD_QUEUE_def]
  >> ‘(read_ndp bd_ptr
             (set_and_clear_local_effect bd_ptr 12w set' clear
              modify_sop_or_eop CPPI_RAM))
      = read_ndp bd_ptr CPPI_RAM’
      by ( METIS_TAC [READ_NDP_ON_SET_SAME_BD_PTR_CANCEL , EVERY_DEF])
  >> ‘bd_ptr ≠ 0w’
      by fs [BD_QUEUE_def]
  >> simp []
  >> ‘(∀p. MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)’
      by (strip_tac
          >> Q.PAT_X_ASSUM ‘∀ p1 p2 . _’ (specl_assume_tac [‘bd_ptr’, ‘p’])
          >> Cases_on ‘MEM p queue’
          >- ( ‘ALL_DISTINCT (bd_ptr::queue)’
               by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
               >> fs []
               >> Cases_on ‘bd_ptr = p’
               >- (fs [])
               >> fs []
             )
          >> fs []
         )
  >> ‘BD_QUEUE queue (read_ndp bd_ptr CPPI_RAM) CPPI_RAM’
      by fs [BD_QUEUE_def]
  >> fs [BD_QUEUE_UNRELEVANT_CPPI_UPDATE_INDEPENDANT]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
  BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ⇒ BD_QUEUE queue bd_ptr
       (set_and_clear_word_on_sop_or_eop_foldr_effect
               queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop)
Proof
  Induct_on ‘queue’
  >- (rpt strip_tac
      >> fs [BD_QUEUE_def]
     )
  >> rpt strip_tac
  >> ‘h = bd_ptr’
      by fs  [BD_QUEUE_def]
  >> Q.PAT_X_ASSUM ‘h = bd_ptr’ (fn t => fs [t])
  >> ‘bd_ptr ≠ 0w’
      by fs  [BD_QUEUE_def]
  >> simp [BD_QUEUE_def]
  >> ‘(read_ndp bd_ptr
             (set_and_clear_word_on_sop_or_eop_foldr_effect (bd_ptr::queue)
              state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
      = (read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
      by ( specl_assume_tac
           [‘bd_ptr’, ‘bd_ptr::queue’, ‘state’, ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
           READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_QUEUE_HEAD_FOLDR
           >> Q.PAT_UNDISCH_TAC ‘_⇒_’
           >> simp []
           >> METIS_TAC []
         )
  >> asm_rewrite_tac []
  >> rewrite_tac [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> simp []
  >> rewrite_tac [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> Cases_on ‘queue’
  >- (fs [BD_QUEUE_def])
  >> ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM) = h’
      by (fs [BD_QUEUE_def])
  >> Q.PAT_X_ASSUM ‘_=_’ (fn t => fs [t])
  >> specl_assume_tac
     [‘bd_ptr’, ‘h’, ‘(h::t)’, ‘(set_and_clear_word_on_sop_or_eop_foldr_effect (h::t)
                state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
      , ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
     BD_QUEUE_UNRELEVANT_CPPI_UPDATE_INDEPENDANT
  >> ‘(∀p. MEM p (h::t) ⇒ word_abs (bd_ptr − p) ≥ 16w)’
      by ( strip_tac
           >> Q.PAT_X_ASSUM ‘∀ p1 p2 . _’ (specl_assume_tac [‘bd_ptr’, ‘p’])
           >> Cases_on ‘MEM p (h::t)’
           >- ( ‘ALL_DISTINCT (bd_ptr::h::t)’
                by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
                >> ‘bd_ptr ≠ h’
                    by fs[]
                >> fs[]
                >> METIS_TAC []
              )
           >> fs []
         )
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> simp []
  >> METIS_TAC [BD_QUEUE_def]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_FOLDL_PRESERVES_BD_QUEUE:
  ∀ (bd_ptr : 32 word) (queue : 32 word list) state set clear modify_sop_or_eop .
  BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ⇒ BD_QUEUE queue bd_ptr
       (set_and_clear_word_on_sop_or_eop_foldl_effect
               queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop)
Proof
  rpt strip_tac
  >> ‘BD_QUEUE queue bd_ptr
       (set_and_clear_word_on_sop_or_eop_foldr_effect
        queue state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
       by METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
  >> METIS_TAC [BD_QUEUE_ALL_DISTINCT
                ,SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_BODY_LOCAL_EFFECT:
  ∀ bd_ptr state set clear modify_sop_or_eop .
    ¬ state.nic.dead
   ∧ BD_PTR_ADDRESS bd_ptr
   ⇒ (whenMM (is_sop bd_ptr <∧> UNIT modify_sop_or_eop <∨>
            is_eop bd_ptr <∧> UNIT (¬modify_sop_or_eop))
           (monad_bind
            ((λv. (v ‖ set) && ¬clear) <@>
               read_nic_register (bd_ptr + 12w))
            (write_nic_register (bd_ptr + 12w)))) state
   = (()
      , (state with
         nic :=
           state.nic with
           regs :=
             state.nic.regs with
             CPPI_RAM := (set_and_clear_local_effect bd_ptr 12w set clear modify_sop_or_eop state.nic.regs.CPPI_RAM))
     )
Proof
  rpt strip_tac
  >> ‘CPPI_RAM_BYTE_PA bd_ptr’
      by fs [BD_PTR_ADDRESS_def]
  >> ‘WORD32_ALIGNED bd_ptr’
      by fs [BD_PTR_ADDRESS_def]
  >> ‘WORD32_ALIGNED (bd_ptr + 12w)
      ∧ CPPI_RAM_BYTE_PA (bd_ptr + 12w)’
      by (UNDISCH_ALL_TAC
          >> EVAL_TAC
          >> blastLib.BBLAST_PROVE_TAC
         )
  >> simp_st [IS_SOP, IS_EOP, whenMM_def]
  >> Cases_on ‘((tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop ∧ modify_sop_or_eop ∨
                (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop ∧ ¬modify_sop_or_eop)’
  >- ( simp_st []
       >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop’
       >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop’
       >> Cases_on ‘modify_sop_or_eop’
       >> fs_st [write_nic_register_def
                 , register_writeTheory.write_register_def
                 , register_writeTheory.write_cppi_ram_def
                 , read_nic_register_def
                 , register_readTheory.read_register_def
                 , CPPI_RAM_BYTE_PA_lemma
                 , set_and_clear_local_effect_def]
       >| [ ‘SOP && read_bd_word (bd_ptr + 12w) 0w state.nic.regs.CPPI_RAM ≠ 0w’
            by ( UNDISCH_TAC “(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop”
                 >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC
               )
            >> simp []
            , ‘EOP && read_bd_word (bd_ptr + 12w) 0w state.nic.regs.CPPI_RAM ≠ 0w’
               by ( UNDISCH_TAC “(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop”
                    >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC )
              >> simp []
            , simp [system_state_component_equality
                    , nic_state_component_equality
                    , nic_regs_component_equality
                   ]
            , simp [system_state_component_equality
                    , nic_state_component_equality
                    , nic_regs_component_equality
                   ]
          ]
       >> simp_st [IS_EOP]
       >> fs_st [write_nic_register_def
                 , register_writeTheory.write_register_def
                 , register_writeTheory.write_cppi_ram_def
                 , read_nic_register_def
                 , register_readTheory.read_register_def
                 , CPPI_RAM_BYTE_PA_lemma
                 , set_and_clear_local_effect_def
                 , system_state_component_equality
                 , nic_state_component_equality
                 , nic_regs_component_equality
                 , EOP_AND_FORM_EQ_EOP_BD_READ
                 , SOP_AND_FORM_EQ_EOP_BD_READ]
     )
  >> simp_st []
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop’
  >> Cases_on ‘(tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop’
  >> Cases_on ‘modify_sop_or_eop’
  >> fs_st [write_nic_register_def
            , register_writeTheory.write_register_def
            , register_writeTheory.write_cppi_ram_def
            , read_nic_register_def
            , register_readTheory.read_register_def
            , CPPI_RAM_BYTE_PA_lemma
            , set_and_clear_local_effect_def
            , system_state_component_equality
            , nic_state_component_equality
            , nic_regs_component_equality
            , EOP_AND_FORM_EQ_EOP_BD_READ
            , SOP_AND_FORM_EQ_EOP_BD_READ
            , IS_SOP, IS_EOP]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_EFFECT:
  ∀ bd_ptr queue state n set clear modify_sop_or_eop .
  LENGTH queue <= n
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ⇒ set_and_clear_word_on_sop_or_eop n bd_ptr 12w set clear modify_sop_or_eop state
  = (()
     , (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldl_effect
               queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop))
    )
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘queue’
      >- ( ‘bd_ptr = 0w’
           by fs [BD_QUEUE_def]
           >> simp_st [set_and_clear_word_on_sop_or_eop_def
                       , set_and_clear_word_on_sop_or_eop_foldl_effect_def
                       , system_state_component_equality
                       , nic_state_component_equality
                       , nic_regs_component_equality
                      ]
         )
      >> fs []
     )
  >> rpt strip_tac
  >> Cases_on ‘queue’
  >- ( ‘bd_ptr = 0w’
       by fs [BD_QUEUE_def]
       >> simp_st [set_and_clear_word_on_sop_or_eop_def
                   , set_and_clear_word_on_sop_or_eop_foldl_effect_def
                   , system_state_component_equality
                   , nic_state_component_equality
                   , nic_regs_component_equality
                  ]
     )
  >> ‘h = bd_ptr’
      by fs [BD_QUEUE_def]
  >> Q.PAT_X_ASSUM ‘h = bd_ptr’ (fn t => fs [t])
  >> rewrite_tac [set_and_clear_word_on_sop_or_eop_def]
  >> ‘bd_ptr ≠ 0w’
      by fs [BD_QUEUE_def]
  >> simp []
  >> ‘∃ b . is_sop bd_ptr state = (b , state)’
      by METIS_TAC [IS_SOP_EXISTS]
  >> ‘∃ b . is_eop bd_ptr state = (b , state)’
      by METIS_TAC [IS_EOP_EXISTS]
  >> rewrite_tac [IGNORE_BIND_DEF]
  >> once_rewrite_tac [ BIND_DEF]
  >> ‘(whenMM
           (is_sop bd_ptr <∧> UNIT modify_sop_or_eop <∨>
            is_eop bd_ptr <∧> UNIT (¬modify_sop_or_eop))
           (monad_bind
              ((λv. ¬clear && (set' ‖ v)) <@>
               read_nic_register (bd_ptr + 12w))
              (write_nic_register (bd_ptr + 12w)))) state
      = ((),
          state with
          nic :=
            state.nic with
            regs :=
              state.nic.regs with
              CPPI_RAM :=
                set_and_clear_local_effect bd_ptr 12w set' clear
                modify_sop_or_eop state.nic.regs.CPPI_RAM)’
      by (specl_assume_tac
          [‘bd_ptr’, ‘state’, ‘set'’, ‘clear’,‘modify_sop_or_eop’]
          SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_BODY_LOCAL_EFFECT
          >> Q.PAT_UNDISCH_TAC ‘_⇒_’
          >> simp []
         )
  >> rewrite_tac [o_DEF]
  >> simp []
  >> simp_st []
  >> Q.ABBREV_TAC ‘state' =(state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    set_and_clear_local_effect bd_ptr 12w set' clear
                    modify_sop_or_eop state.nic.regs.CPPI_RAM)’
  >> ‘BD_QUEUE (bd_ptr::t) bd_ptr state'.nic.regs.CPPI_RAM’
      by ( Q.UNABBREV_TAC ‘state'’
           >> specl_assume_tac
              [‘bd_ptr’, ‘t’, ‘state.nic.regs.CPPI_RAM’
               , ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
              BD_QUEUE_UPDATED_FLAGS_HEAD_CPPI_UPDATE_INDEPENDANT
           >> Q.PAT_UNDISCH_TAC ‘_⇒_’
           >> simp []
           >> asm_rewrite_tac []
         )
  >> ‘¬state'.nic.dead’
      by (Q.UNABBREV_TAC ‘state'’ >> fs[])
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by fs[]
  >> ‘(get_next_descriptor_pointer bd_ptr state') = (read_ndp bd_ptr state'.nic.regs.CPPI_RAM , state')’
      by METIS_TAC [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> simp []
  >> ‘∀p1 p2. p1 ≠ p2 ∧ (MEM p1 t) ∧ (MEM p2 t) ⇒
      word_abs (p1 + -1w * p2) ≥ 16w’
      by fs []
  >> ‘set_and_clear_word_on_sop_or_eop n
          (read_ndp bd_ptr state'.nic.regs.CPPI_RAM) 12w set' clear
          modify_sop_or_eop state'
          = ((),
             state' with
             nic :=
               state'.nic with
               regs :=
                 state'.nic.regs with
                 CPPI_RAM :=
                   set_and_clear_word_on_sop_or_eop_foldl_effect t
                   state'.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
          by (Q.PAT_X_ASSUM ‘∀bd_ptr' queue state' set clear modify_sop_or_eop._’
              (specl_assume_tac
               [‘(read_ndp bd_ptr state'.nic.regs.CPPI_RAM)’
                , ‘t’, ‘state'’, ‘set'’, ‘clear’, ‘modify_sop_or_eop’])
              >> Q.PAT_UNDISCH_TAC ‘_⇒_’
              >> fs [BD_QUEUE_def]
              >> METIS_TAC [BD_QUEUE_def]
                )
  >> simp []
  >> Q.UNABBREV_TAC ‘state'’
  >> simp [set_and_clear_word_on_sop_or_eop_foldl_effect_def]
QED

Theorem SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT:
  ∀ bd_ptr queue state n set clear modify_sop_or_eop .
  LENGTH queue <= n
  ∧ ¬ state.nic.dead
  ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ⇒ set_and_clear_word_on_sop_or_eop n bd_ptr 12w set clear modify_sop_or_eop state
  = (()
     , (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldr_effect
               queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop))
    )
Proof
  rpt strip_tac
  >> ‘set_and_clear_word_on_sop_or_eop n bd_ptr 12w set' clear modify_sop_or_eop state
  = (()
     , (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldl_effect
               queue state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop))
    )’ by METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_EFFECT]
  >> ‘(set_and_clear_word_on_sop_or_eop_foldl_effect
       queue state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)
      = (set_and_clear_word_on_sop_or_eop_foldr_effect
               queue state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)’
      by METIS_TAC [BD_QUEUE_ALL_DISTINCT
                    ,SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR]
  >> simp []
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_LOCAL_ON_SELF:
  ∀ ptr state set clear modify_sop_or_eop .
    ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
    ∧ BD_PTR_ADDRESS ptr
    ⇒
    FST (is_eop ptr state)
    = FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_local_effect ptr 12w set clear modify_sop_or_eop state.nic.regs.CPPI_RAM)))
Proof
  NTAC 5 strip_tac
  >> DISCH_TAC
  >> SPLIT_ASM_TAC
  >> ‘WORD32_ALIGNED (ptr + 12w)
      ∧ CPPI_RAM_BYTE_PA (ptr + 12w)’
      by  ( Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS ptr’
            >> EVAL_TAC
            >> blastLib.BBLAST_PROVE_TAC
          )
  >> simp_st [is_eop_def, read_nic_register_def
              , register_readTheory.read_register_def
              , CPPI_RAM_BYTE_PA_lemma
             ]
  >> Cases_on ‘state.nic.dead’
  >- simp_st []
  >> simp_st [set_and_clear_local_effect_def]
  >> fs []
  >> (COND_CASES_TAC
      >- ( simp_st [CPPI_RAMTheory.read_bd_word_def
                    , CPPI_RAMTheory.write_32bit_word_def]
           >> EVAL_TAC
           >> blastLib.BBLAST_PROVE_TAC
         )
      >> simp [])
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_LOCAL_ON_OTHER:
  ∀ read_ptr write_ptr state set clear modify_sop_or_eop .
    ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
    ∧ BD_PTR_ADDRESS read_ptr
    ∧ BD_PTR_ADDRESS write_ptr
    ∧ word_abs (write_ptr - read_ptr) >= 16w
    ⇒
    FST (is_eop read_ptr state)
    = FST (is_eop read_ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_local_effect write_ptr 12w set clear modify_sop_or_eop state.nic.regs.CPPI_RAM)))
Proof
  NTAC 6 strip_tac
  >> DISCH_TAC
  >> SPLIT_ASM_TAC
  >> ‘WORD32_ALIGNED (read_ptr + 12w)
     ∧ CPPI_RAM_BYTE_PA (read_ptr + 12w)
     ∧ (read_ptr + 12w) < CPPI_RAM_END - 3w’
      by  ( UNDISCH_TAC “BD_PTR_ADDRESS read_ptr”
            >> EVAL_TAC
            >> blastLib.BBLAST_PROVE_TAC
          )
  >>  ‘WORD32_ALIGNED (write_ptr + 12w)
       ∧ CPPI_RAM_BYTE_PA (write_ptr + 12w)
       ∧ (write_ptr + 12w) < CPPI_RAM_END - 3w’
       by  ( UNDISCH_TAC “BD_PTR_ADDRESS write_ptr”
             >> EVAL_TAC
             >> blastLib.BBLAST_PROVE_TAC
           )
  >> ‘word_abs ((write_ptr + 12w) - (read_ptr + 12w)) >= 4w’
      by ( ‘(write_ptr + 12w − (read_ptr + 12w)) = write_ptr - read_ptr’
           by WORD_DECIDE_TAC
           >> asm_rewrite_tac[]
           >> Q.PAT_UNDISCH_TAC ‘word_abs (write_ptr − read_ptr) ≥ 16w’
           >> blastLib.BBLAST_PROVE_TAC
         )
  >> simp_st [is_eop_def, read_nic_register_def
              , register_readTheory.read_register_def
              , CPPI_RAM_BYTE_PA_lemma
             ]
  >> Cases_on ‘state.nic.dead’
  >- simp_st []
  >> simp_st [set_and_clear_local_effect_def]
  >> COND_CASES_TAC
  >- METIS_TAC [WRITE_ONLY_CHANGES_FOUR_BYTES]
  >> simp []
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR_ON_NONMEM:
  ∀ ptr queue state CPPI_RAM set clear modify_sop_or_eop .
  ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
     word_abs (p1 − p2) ≥ 16w)
  ∧ (∀p . MEM p queue  ⇒ word_abs (p − ptr) ≥ 16w)
  ∧ BD_PTR_ADDRESS ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ⇒ FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldr_effect
              queue CPPI_RAM 12w set clear modify_sop_or_eop)))
    = FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM := CPPI_RAM))
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
     )
  >> NTAC 7 strip_tac
  >> DISCH_TAC
  >> SPLIT_ASM_TAC
  >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> simp [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> ‘FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    set_and_clear_local_effect h 12w set' clear
                      modify_sop_or_eop
                      (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))
          = FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))’
          by ( ‘word_abs (h - ptr) >= 16w’
               by fs[]
               >> ‘BD_PTR_ADDRESS h’
                   by fs []
               >> specl_assume_tac
                  [‘ptr’, ‘h’
                   , ‘(state with
                        nic :=
                        state.nic with
                        regs :=
                        state.nic.regs with
                        CPPI_RAM :=
                        set_and_clear_word_on_sop_or_eop_foldr_effect queue
                        CPPI_RAM 12w set' clear
                        modify_sop_or_eop)’
                   , ‘set'’
                   , ‘clear’
                   , ‘modify_sop_or_eop’]
                  IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_LOCAL_ON_OTHER
               >> Q.PAT_UNDISCH_TAC ‘_⇒_’
               >> simp []
             )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by fs []
  >> ‘∀p. MEM p (queue) ⇒ word_abs (p − ptr) ≥ 16w’
      by fs[]
  >> METIS_TAC [EVERY_DEF]
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR_ON_NONMEM_PURE_STATE:
  ∀ ptr queue state set clear modify_sop_or_eop .
  ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
     word_abs (p1 − p2) ≥ 16w)
  ∧ (∀p . MEM p queue  ⇒ word_abs (p − ptr) ≥ 16w)
  ∧ BD_PTR_ADDRESS ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ⇒ FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldr_effect
              queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop)))
    = FST (is_eop ptr state)
Proof
  ntac 6 strip_tac
  >> ‘state = ((state with nic := state.nic with regs := state.nic.regs with CPPI_RAM := state.nic.regs.CPPI_RAM))’
   by simp [system_state_component_equality
            , nic_state_component_equality
            , nic_regs_component_equality
           ]
  >> METIS_TAC [IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR_ON_NONMEM]
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR:
  ∀ ptr queue state set clear modify_sop_or_eop .
  ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
     word_abs (p1 − p2) ≥ 16w)
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ MEM ptr queue
  ⇒ FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldr_effect
              queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop)))
    = FST (is_eop ptr state)
Proof
  Induct_on ‘queue’
  >- ( rpt strip_tac
       >> ‘(state with
              nic :=
                state.nic with
                regs :=
                state.nic.regs with CPPI_RAM := state.nic.regs.CPPI_RAM)
           = state’
           by simp [nic_state_component_equality
                    , nic_regs_component_equality
                    , system_state_component_equality]
       >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
     )
  >> NTAC 6 strip_tac
  >> DISCH_TAC
  >> SPLIT_ASM_TAC
  >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> simp [GSYM set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> Cases_on ‘h = ptr’
  >- ( pat_x_rewrite_all ‘h = ptr’
       >> ‘FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    set_and_clear_local_effect ptr 12w set' clear
                      modify_sop_or_eop
                      (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         state.nic.regs.CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))
          = FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         state.nic.regs.CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))’
          by ( ‘BD_PTR_ADDRESS ptr’
               by fs []
               >> specl_assume_tac
                  [‘ptr’
                   , ‘(state with
                       nic :=
                       state.nic with
                       regs :=
                       state.nic.regs with
                       CPPI_RAM :=
                       (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                        state.nic.regs.CPPI_RAM 12w set' clear
                        modify_sop_or_eop))’
                   , ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
                  IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_LOCAL_ON_SELF
               >> Q.PAT_UNDISCH_TAC ‘_⇒_’
               >> simp []
             )
       >> pat_x_rewrite_all ‘_=_’
       >> ‘(∀p1 p2.
              p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)’
           by fs[]
       >> Cases_on ‘MEM ptr queue’
       >- ( METIS_TAC [MEM, EVERY_DEF]
          )
       >> ‘(∀p. MEM p queue ⇒ word_abs (p − ptr) ≥ 16w)’
           by ( strip_tac
              >> Q.PAT_X_ASSUM ‘∀ p1 p2 . p1 ≠ p2 ∧ MEM p1 (ptr::queue) ∧ MEM p2 (ptr::queue) ⇒ _’
                               (specl_assume_tac
                                [‘p’, ‘ptr’]
                               )
              >> Cases_on ‘ptr = p’
              >- (fs [])
              >>  fs []
              )
       >> METIS_TAC [EVERY_DEF, IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR_ON_NONMEM_PURE_STATE]
     )
  >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by fs[]
  >> ‘MEM ptr queue’
      by fs []
  >> ‘FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    set_and_clear_local_effect h 12w set' clear
                      modify_sop_or_eop
                      (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         state.nic.regs.CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))
          = FST
          (is_eop ptr
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         state.nic.regs.CPPI_RAM 12w set' clear
                         modify_sop_or_eop)))’
          by ( ‘BD_PTR_ADDRESS h’
               by fs []
               >> ‘word_abs (h − ptr) ≥ 16w’
                   by fs []
               >> ‘BD_PTR_ADDRESS ptr’
                   by METIS_TAC [EVERY_MEM]
               >> specl_assume_tac
                  [‘ptr’ , ‘h’
                   , ‘(state with
                       nic :=
                       state.nic with
                       regs :=
                       state.nic.regs with
                       CPPI_RAM :=
                       set_and_clear_word_on_sop_or_eop_foldr_effect queue
                       state.nic.regs.CPPI_RAM 12w set' clear
                       modify_sop_or_eop)’
                   , ‘set'’, ‘clear’, ‘modify_sop_or_eop’]
                  IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_LOCAL_ON_OTHER
               >> Q.PAT_UNDISCH_TAC ‘_⇒_’
               >> simp []
             )
  >> METIS_TAC [MEM, EVERY_DEF]
QED

Theorem IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDL:
  ∀ ptr queue state set clear modify_sop_or_eop .
  ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
     word_abs (p1 − p2) ≥ 16w)
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ ALL_DISTINCT queue
  ∧ MEM ptr queue
  ⇒ FST (is_eop ptr (state with nic := state.nic with regs := state.nic.regs with CPPI_RAM :=
             (set_and_clear_word_on_sop_or_eop_foldl_effect
              queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop)))
    = FST (is_eop ptr state)
Proof
  rpt strip_tac
  >> ‘set_and_clear_word_on_sop_or_eop_foldl_effect queue
        state.nic.regs.CPPI_RAM 12w set' clear
        modify_sop_or_eop
      =
      set_and_clear_word_on_sop_or_eop_foldr_effect queue
        state.nic.regs.CPPI_RAM 12w set' clear
        modify_sop_or_eop’
        by METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR]
  >> METIS_TAC [IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR]
QED

Theorem EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP:
  ∀ queue start_ptr state set clear modify_sop_or_eop .
     ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
      ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
      ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
              word_abs (p1 − p2) ≥ 16w)
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE queue start_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY (λp. ¬FST (is_eop p state) ⇒
           read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) queue

  ⇒ EVERY (λp. ¬FST (is_eop p (state with nic := state.nic with regs
                              := state.nic.regs with CPPI_RAM :=
                              (set_and_clear_word_on_sop_or_eop_foldr_effect
                               queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop))) ⇒
           read_ndp p
           (set_and_clear_word_on_sop_or_eop_foldr_effect
            queue state.nic.regs.CPPI_RAM 12w set clear modify_sop_or_eop) ≠ 0w)
      queue
Proof
  NTAC 6 strip_tac
  >> DISCH_TAC
  >> SPLIT_ASM_TAC
  >> rewrite_tac [EVERY_MEM]
  >> RW_ASM_TAC “EVERY (λp.
                        ¬FST (is_eop p state) ⇒
                        read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) queue”
                EVERY_MEM
  >> strip_tac
  >> Q.PAT_X_ASSUM ‘∀ e ._’
                   (specl_assume_tac [‘e’])
  >> strip_tac
  >> ‘¬FST (is_eop e state) ⇒ read_ndp e state.nic.regs.CPPI_RAM ≠ 0w’
      by fs []
  >> simp []
  >> ‘ALL_DISTINCT queue’
      by METIS_TAC [BD_QUEUE_ALL_DISTINCT]
  >> ‘ FST
          (is_eop e
             (state with
              nic :=
                state.nic with
                regs :=
                  state.nic.regs with
                  CPPI_RAM :=
                    set_and_clear_word_on_sop_or_eop_foldr_effect queue
                      state.nic.regs.CPPI_RAM 12w set' clear
                      modify_sop_or_eop))
          = FST (is_eop e state)’
          by METIS_TAC [IS_EOP_NOT_MODIFIED_BY_SET_AND_CLEAR_FOLDR]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘read_ndp e
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           state.nic.regs.CPPI_RAM 12w set' clear modify_sop_or_eop)
      = read_ndp e state.nic.regs.CPPI_RAM’
      by METIS_TAC [READ_NDP_ON_SET_AND_CLEAR_WORD_ON_SOP_MEM_FOLDR]
  >> asm_rewrite_tac []
QED

Theorem SET_AND_CLEAR_SKIP_FOLDR_MASHUP_STEP:
  ∀ h queue CPPI_RAM set1 clear1 msoe1 set2 clear2 msoe2.
    EVERY BD_PTR_ADDRESS (h::queue)
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT (h::queue)
    ⇒ (set_and_clear_word_on_sop_or_eop_foldr_effect queue
           (set_and_clear_local_effect h FLAGS set2 clear2 msoe2 CPPI_RAM)
           FLAGS set1 clear1 msoe1)
    = (set_and_clear_local_effect h FLAGS set2 clear2 msoe2
       (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             CPPI_RAM FLAGS set1 clear1 msoe1))
Proof
  Induct_on ‘queue’
  >- simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> rpt strip_tac
  >> ‘BD_PTR_ADDRESS h ∧ BD_PTR_ADDRESS h'’
      by fs []
  >> ‘word_abs (h' − h) ≥ 16w’
      by (METIS_TAC [ALL_DISTINCT, MEM])
  >> ‘(∀p1 p2. p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒
       word_abs (p1 − p2) ≥ 16w)’
      by (fs [] >> METIS_TAC [])
  >> ‘(set_and_clear_word_on_sop_or_eop_foldr_effect (h::queue)
          (set_and_clear_local_effect h' FLAGS set2 clear2 msoe2 CPPI_RAM)
          FLAGS set1 clear1 msoe1)
      =
        (set_and_clear_word_on_sop_or_eop_foldr_effect queue
         (set_and_clear_local_effect h FLAGS set1 clear1 msoe1
          (set_and_clear_local_effect h' FLAGS set2 clear2 msoe2 CPPI_RAM))
          FLAGS set1 clear1 msoe1)
         ’
      by (rewrite_tac [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
          >> METIS_TAC [FOLDR_SET_AND_CLEAR_LOCAL_EFFECT_MONIOID_COMM
                        , FLAGS_def, ALL_DISTINCT, EVERY_DEF]
         )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘(set_and_clear_local_effect h FLAGS set1 clear1 msoe1
       (set_and_clear_local_effect h' FLAGS set2 clear2 msoe2 CPPI_RAM))
       = (set_and_clear_local_effect h' FLAGS set2 clear2 msoe2
             (set_and_clear_local_effect h FLAGS set1 clear1 msoe1 CPPI_RAM))
   ’ by (METIS_TAC [SET_AND_CLEAR_LOCAL_EFFECT_COMM_GEN])
  >> pat_x_rewrite_all ‘_=_’
  >> ‘set_and_clear_word_on_sop_or_eop_foldr_effect queue
          (set_and_clear_local_effect h' FLAGS set2 clear2 msoe2
             (set_and_clear_local_effect h FLAGS set1 clear1 msoe1 CPPI_RAM))
          FLAGS set1 clear1 msoe1
    = set_and_clear_local_effect h' FLAGS set2 clear2 msoe2
                (set_and_clear_word_on_sop_or_eop_foldr_effect queue
          (set_and_clear_local_effect h FLAGS set1 clear1 msoe1 CPPI_RAM)
          FLAGS set1 clear1 msoe1)’
      by ( ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (h'::queue) ∧ MEM p2 (h'::queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by (fs [] >> METIS_TAC [])
           >> ‘ALL_DISTINCT (h'::queue)’
               by fs []
           >> ‘EVERY BD_PTR_ADDRESS (h'::queue)’
               by fs []
           >> METIS_TAC []
         )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘(set_and_clear_word_on_sop_or_eop_foldr_effect queue
             (set_and_clear_local_effect h FLAGS set1 clear1 msoe1 CPPI_RAM)
             FLAGS set1 clear1 msoe1)
      = (set_and_clear_word_on_sop_or_eop_foldr_effect (h::queue)
         CPPI_RAM FLAGS set1 clear1 msoe1)’
      by (rewrite_tac [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
          >> METIS_TAC [FOLDR_SET_AND_CLEAR_LOCAL_EFFECT_MONIOID_COMM
                        , FLAGS_def, ALL_DISTINCT, EVERY_DEF])
  >> simp []
QED



Theorem SET_AND_CLEAR_SKIP_FOLDL_MASHUP_STEP:
  ∀ h queue CPPI_RAM set1 clear1 msoe1 set2 clear2 msoe2.
    EVERY BD_PTR_ADDRESS (h::queue)
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT (h::queue)
    ⇒ (set_and_clear_word_on_sop_or_eop_foldl_effect queue
           (set_and_clear_local_effect h FLAGS set2 clear2 msoe2 CPPI_RAM)
           FLAGS set1 clear1 msoe1)
    = (set_and_clear_local_effect h FLAGS set2 clear2 msoe2
       (set_and_clear_word_on_sop_or_eop_foldl_effect queue
             CPPI_RAM FLAGS set1 clear1 msoe1))
Proof
  rpt strip_tac
  >> ‘ALL_DISTINCT queue ∧ EVERY BD_PTR_ADDRESS queue’
      by fs[]
  >> ‘∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w’
      by (fs[] >> METIS_TAC [])
  >> METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR
           , SET_AND_CLEAR_SKIP_FOLDR_MASHUP_STEP, FLAGS_def]
QED

Theorem SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP_STEP:
  ∀ h queue CPPI_RAM .
    EVERY BD_PTR_ADDRESS (h::queue)
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT (h::queue)
  ⇒ set_and_clear_word_on_sop_or_eop_foldr_effect (h::queue)
       (set_and_clear_word_on_sop_or_eop_foldr_effect (h::queue)
          (set_and_clear_word_on_sop_or_eop_foldr_effect (h::queue)
             CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
          EOQ EOP_BD) FLAGS 0w TD SOP_BD
   = (set_and_clear_local_effect h FLAGS 0w TD SOP_BD
     o set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD
     o set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD)
      (set_and_clear_word_on_sop_or_eop_foldr_effect (queue)
       (set_and_clear_word_on_sop_or_eop_foldr_effect (queue)
          (set_and_clear_word_on_sop_or_eop_foldr_effect (queue)
             CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
          EOQ EOP_BD) FLAGS 0w TD SOP_BD)
Proof
  rpt strip_tac
  >> ‘(set_and_clear_local_effect h FLAGS 0w TD SOP_BD ∘
         set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD ∘
         set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD)
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                (set_and_clear_word_on_sop_or_eop_foldr_effect queue CPPI_RAM
                   FLAGS OWNER 0w SOP_BD) FLAGS 0w EOQ EOP_BD) FLAGS 0w TD
             SOP_BD)
      =
      set_and_clear_local_effect h FLAGS 0w TD SOP_BD
      (set_and_clear_word_on_sop_or_eop_foldr_effect queue
       (set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD
                 (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                  (set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD
                   (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                         CPPI_RAM FLAGS OWNER 0w SOP_BD)) FLAGS 0w EOQ EOP_BD))
                 FLAGS 0w TD SOP_BD)’
          by (simp []
              >> METIS_TAC [SET_AND_CLEAR_SKIP_FOLDR_MASHUP_STEP]
             )
  >> pat_x_rewrite_all ‘_=_’
  >> simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
QED

Theorem SET_AND_CLEAR_WORD_OUTER_EFFECT_FOLDL_MASHUP_STEP:
  ∀ h queue CPPI_RAM .
    EVERY BD_PTR_ADDRESS (h::queue)
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 (h::queue) ∧ MEM p2 (h::queue) ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT (h::queue)
  ⇒ set_and_clear_word_on_sop_or_eop_foldl_effect (h::queue)
       (set_and_clear_word_on_sop_or_eop_foldl_effect (h::queue)
          (set_and_clear_word_on_sop_or_eop_foldl_effect (h::queue)
             CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
          EOQ EOP_BD) FLAGS 0w TD SOP_BD
   = (set_and_clear_word_on_sop_or_eop_foldl_effect (queue)
       (set_and_clear_word_on_sop_or_eop_foldl_effect (queue)
          (set_and_clear_word_on_sop_or_eop_foldl_effect (queue)
             ((set_and_clear_local_effect h FLAGS 0w TD SOP_BD
               o set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD
               o set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD)
               CPPI_RAM)
             FLAGS OWNER 0w SOP_BD)
          FLAGS 0w EOQ EOP_BD)
       FLAGS 0w TD SOP_BD)
Proof
  rpt strip_tac
  >> simp [set_and_clear_word_on_sop_or_eop_foldl_effect_def]
  >> simp [GSYM set_and_clear_word_on_sop_or_eop_foldl_effect_def]
  >> ‘EVERY BD_PTR_ADDRESS queue
      ∧ ALL_DISTINCT queue
      ∧ ∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
            word_abs (p1 − p2) ≥ 16w’
            by (fs [] >> METIS_TAC [])
  >> METIS_TAC [SET_AND_CLEAR_SKIP_FOLDL_MASHUP_STEP]
QED

Definition set_and_clear_all_effect_def:
  set_and_clear_all_effect =
  FOLDR (λ p . set_and_clear_local_effect p FLAGS 0w TD SOP_BD
              o set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
              o set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
End

Theorem SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP:
  ∀ queue CPPI_RAM .
    EVERY BD_PTR_ADDRESS queue
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT queue
  ⇒ set_and_clear_word_on_sop_or_eop_foldr_effect queue
       (set_and_clear_word_on_sop_or_eop_foldr_effect queue
          (set_and_clear_word_on_sop_or_eop_foldr_effect queue
             CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
          EOQ EOP_BD) FLAGS 0w TD SOP_BD
   = set_and_clear_all_effect CPPI_RAM queue
Proof
  rewrite_tac [set_and_clear_all_effect_def]
  >> Induct_on ‘queue’
  >- simp [set_and_clear_word_on_sop_or_eop_foldr_effect_def]
  >> rpt strip_tac
  >> simp [SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP_STEP]
  >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by (fs [] >> METIS_TAC [])
  >> METIS_TAC [EVERY_DEF, ALL_DISTINCT]
QED

Theorem SET_AND_CLEAR_WORD_OUTER_FODL_EFFECT_MASHUP:
  ∀ queue CPPI_RAM .
    EVERY BD_PTR_ADDRESS queue
    ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ ALL_DISTINCT queue
  ⇒ set_and_clear_word_on_sop_or_eop_foldl_effect queue
       (set_and_clear_word_on_sop_or_eop_foldl_effect queue
          (set_and_clear_word_on_sop_or_eop_foldl_effect queue
             CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
          EOQ EOP_BD) FLAGS 0w TD SOP_BD
   = FOLDL (λ CPPI_RAM' p .
              (set_and_clear_local_effect p FLAGS 0w TD SOP_BD
               o set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
               o set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
              CPPI_RAM'
           )
           CPPI_RAM
           queue
Proof
  Induct_on ‘queue’
  >- simp [set_and_clear_word_on_sop_or_eop_foldl_effect_def]
  >> rpt strip_tac
  >> simp [SET_AND_CLEAR_WORD_OUTER_EFFECT_FOLDL_MASHUP_STEP]
  >> ‘∀p1 p2.
            p1 ≠ p2 ∧ MEM p1 (queue) ∧ MEM p2 (queue) ⇒
            word_abs (p1 − p2) ≥ 16w’
            by (fs [] >> METIS_TAC [])
  >> ‘ALL_DISTINCT queue
      ∧ EVERY BD_PTR_ADDRESS queue’
      by fs []
  >> Q.PAT_X_ASSUM ‘∀CPPI_RAM. _’
                   (specl_assume_tac
                    [‘(set_and_clear_local_effect h FLAGS 0w TD SOP_BD
             (set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD
              (set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD CPPI_RAM)))’]
                   )
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> simp []
QED

Theorem SET_AND_CLEAR_WORD_FOLR_IS_FOLDL:
  ∀ queue CPPI_RAM .
   EVERY BD_PTR_ADDRESS queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ ALL_DISTINCT queue
  ⇒ FOLDR (λ p . set_and_clear_local_effect p FLAGS 0w TD SOP_BD
              o set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
              o set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
           CPPI_RAM
           queue
     =
     FOLDL (λ CPPI_RAM' p .
              (set_and_clear_local_effect p FLAGS 0w TD SOP_BD
               o set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
               o set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
              CPPI_RAM'
           )
           CPPI_RAM
           queue
Proof
  rpt strip_tac
  >> rewrite_tac [GSYM set_and_clear_all_effect_def]
  >> simp [GSYM SET_AND_CLEAR_WORD_OUTER_FODL_EFFECT_MASHUP]
  >> METIS_TAC [SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDL_IS_FOLDR
                , SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP
                , FLAGS_def]
QED


Definition is_queue_secure_effect_def:
  is_queue_secure_effect queue state =
   state with
        nic :=
          state.nic with
          regs :=
            state.nic.regs with
            CPPI_RAM :=
            set_and_clear_all_effect state.nic.regs.CPPI_RAM queue
End





Theorem EXISTING_QUEUE_DIFFS_FROM_NEW_QUEUE:
  ∀ queue bd_ptr other_queue state .
  BD_QUEUE other_queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS other_queue
  ∧ EVERY BD_PTR_ADDRESS queue
    ∧ EVERY
      (λp.
       ¬(get_alpha_bit p state.monitor.alpha ∨
         get_alpha_bit (p + 4w) state.monitor.alpha ∨
         get_alpha_bit (p + 8w) state.monitor.alpha ∨
         get_alpha_bit (p + 12w) state.monitor.alpha)) queue
    ∧ (∀p. (CPPI_RAM_BYTE_PA p ∧ WORD32_ALIGNED p)
       ⇒ (IN_QUEUE_RANGE p other_queue ⇒
       ALPHA_QUEUE_MARKED p state.monitor.alpha))
    ⇒ ∀ p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs(p1 - p2) >= 16w
Proof
  rpt strip_tac
  >> ‘BD_PTR_ADDRESS p1
      ∧ BD_PTR_ADDRESS p2’
      by METIS_TAC [EVERY_MEM]
  >> ‘get_alpha_bit p2 state.monitor.alpha
      ∧ get_alpha_bit (p2 + 4w) state.monitor.alpha
      ∧ get_alpha_bit (p2 + 8w) state.monitor.alpha
      ∧ get_alpha_bit (p2 + 12w) state.monitor.alpha’
      by ( ‘IN_QUEUE_RANGE p2 other_queue
           ∧ IN_QUEUE_RANGE (p2 + 4w) other_queue
           ∧ IN_QUEUE_RANGE (p2 + 8w) other_queue
           ∧ IN_QUEUE_RANGE (p2 + 12w) other_queue’
           by ( METIS_TAC [MEM_IN_QUEUE_RANGE_BYTES] )
           >> simp []
           >> ‘ALPHA_QUEUE_MARKED p2 state.monitor.alpha
               ∧ ALPHA_QUEUE_MARKED (p2 + 4w) state.monitor.alpha
               ∧ ALPHA_QUEUE_MARKED (p2 + 8w) state.monitor.alpha
               ∧ ALPHA_QUEUE_MARKED (p2 + 12w) state.monitor.alpha’
               by METIS_TAC [BD_PTR_ADDRESS_def, BD_PTR_ADDRESS_BYTE_LEMMAS]
           >> REWRITE_ASSUMS_TAC ALPHA_QUEUE_MARKED_def
           >> METIS_TAC [get_alpha_bit_def]
         )
  >> ‘¬(get_alpha_bit p1 state.monitor.alpha ∨
        get_alpha_bit (p1 + 4w) state.monitor.alpha ∨
        get_alpha_bit (p1 + 8w) state.monitor.alpha ∨
        get_alpha_bit (p1 + 12w) state.monitor.alpha)’
      by (REWRITE_ASSUMS_TAC EVERY_MEM
          >> METIS_TAC []
         )
  >> METIS_TAC [NO_ALPHA_BIT_SAME_DIFF_GT_16, word_abs_diff]
QED

Theorem SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS:
  ∀ bd_ptr queue n CPPI_RAM .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ read_ndp (bd_ptr + n) (set_and_clear_all_effect CPPI_RAM queue)
  = read_ndp (bd_ptr + n) CPPI_RAM
Proof
  rewrite_tac [set_and_clear_all_effect_def]
  >> Induct_on ‘queue’
  >- simp []
  >> ntac 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> ‘∀p. MEM p (queue) ⇒ word_abs (bd_ptr − p) ≥ 16w’
      by fs []
  >> ‘EVERY BD_PTR_ADDRESS (queue)’
      by fs []
  >> ‘word_abs (bd_ptr - h) >= 16w’
      by (fs [] >> METIS_TAC [])
  >> ‘BD_PTR_ADDRESS h’
      by fs []
  >> simp []
  >> METIS_TAC [READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL, FLAGS_def]
QED

Theorem SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_RX_READ_BD:
  ∀ bd_ptr queue CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ⇒ (rx_read_bd bd_ptr (set_and_clear_all_effect CPPI_RAM queue)
     = rx_read_bd bd_ptr CPPI_RAM)
Proof
  rpt strip_tac
  >> rewrite_tac [CPPI_RAMTheory.rx_read_bd_def]
  >> ‘read_ndp (bd_ptr + 0w) (set_and_clear_all_effect CPPI_RAM queue)
      = read_ndp (bd_ptr + 0w) CPPI_RAM
      ∧ read_ndp (bd_ptr + 4w) (set_and_clear_all_effect CPPI_RAM queue)
      = read_ndp (bd_ptr + 4w) CPPI_RAM
      ∧ read_ndp (bd_ptr + 8w) (set_and_clear_all_effect CPPI_RAM queue)
      = read_ndp (bd_ptr + 8w) CPPI_RAM
      ∧ read_ndp (bd_ptr + 12w) (set_and_clear_all_effect CPPI_RAM queue)
      = read_ndp (bd_ptr + 12w) CPPI_RAM’
      by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
         )
  >> fs [bdTheory.read_ndp_def, CPPI_RAMTheory.read_bd_word_def]
QED

Theorem IS_QUEUE_SECURE_EFFECT_DOESNT_CHANGE_OTHERS:
  ∀ bd_ptr queue n state .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ read_ndp (bd_ptr + n) (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
  = read_ndp (bd_ptr + n) state.nic.regs.CPPI_RAM
Proof
  ntac 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> simp [is_queue_secure_effect_def]
  >> METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
QED

Theorem IS_QUEUE_SECURE_EFFECT_DOESNT_CHANGE_OTHERS:
  ∀ bd_ptr queue n state .
    BD_PTR_ADDRESS bd_ptr
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ read_ndp (bd_ptr + n) (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
  = read_ndp (bd_ptr + n) state.nic.regs.CPPI_RAM
Proof
  ntac 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> simp [is_queue_secure_effect_def]
  >> METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
QED

Theorem IS_QUEUE_SECURE_EFFECT_SELF_CHANGE:
  ∀ bd_ptr queue n state .
  EVERY BD_PTR_ADDRESS (bd_ptr::queue)
  ∧ ALL_DISTINCT (bd_ptr::queue)
  ∧ (∀ p1 p2 . p1 ≠ p2 ∧ MEM p1 (bd_ptr::queue) ∧ MEM p2 (bd_ptr::queue) ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ read_ndp (bd_ptr + n) (is_queue_secure_effect (bd_ptr::queue) state).nic.regs.CPPI_RAM
  = read_ndp (bd_ptr + n)
             ((set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD
               ∘ set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD
               ∘ set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD)
              state.nic.regs.CPPI_RAM)
Proof
  rewrite_tac [is_queue_secure_effect_def]
  >> ntac 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> rewrite_tac [set_and_clear_all_effect_def]
  >> ‘FOLDR (λp. set_and_clear_local_effect p FLAGS 0w TD SOP_BD
             ∘ set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
             ∘ set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
      state.nic.regs.CPPI_RAM (bd_ptr::queue)
      =
      FOLDL (λCPPI_RAM' p. (set_and_clear_local_effect p FLAGS 0w TD SOP_BD
                            ∘ set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
                            ∘ set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD)
             CPPI_RAM')
      state.nic.regs.CPPI_RAM (bd_ptr::queue)’
      by METIS_TAC [SET_AND_CLEAR_WORD_FOLR_IS_FOLDL]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘ALL_DISTINCT queue
      ∧ EVERY BD_PTR_ADDRESS queue
      ∧ (∀p. MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)’
      by (fs [] >> METIS_TAC [])
  >> simp []
  >> ‘(FOLDL (λCPPI_RAM' p.
                  set_and_clear_local_effect p FLAGS 0w TD SOP_BD
                    (set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD
                       (set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD
                          CPPI_RAM')))
             (set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD
                (set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD
                   (set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD
                    state.nic.regs.CPPI_RAM))) queue)
      = (FOLDR
             (λ p . (set_and_clear_local_effect p FLAGS 0w TD SOP_BD ∘
                         set_and_clear_local_effect p FLAGS 0w EOQ EOP_BD ∘
                         set_and_clear_local_effect p FLAGS OWNER 0w SOP_BD))
             (set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD
                (set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD
                   (set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD
                      state.nic.regs.CPPI_RAM))) queue)’
      by ( simp [SET_AND_CLEAR_WORD_FOLR_IS_FOLDL])
  >> pat_x_rewrite_all ‘_=_’
  >> rewrite_tac [GSYM set_and_clear_all_effect_def]
  >> ‘BD_PTR_ADDRESS bd_ptr’ by fs[]
  >> METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
QED


Theorem IS_QUEUE_SECURE_EFFECT_MEM_CHANGE:
  ∀ bd_ptr queue n state .
  EVERY BD_PTR_ADDRESS queue
  ∧ ALL_DISTINCT queue
  ∧ MEM bd_ptr queue
  ∧ (∀ p1 p2 . p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ read_ndp (bd_ptr + n) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
  = read_ndp (bd_ptr + n)
             ((set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD
               ∘ set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD
               ∘ set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD)
              state.nic.regs.CPPI_RAM)
Proof
  Induct_on ‘queue’
  >- fs []
  >> NTAC 4 strip_tac
  >> disch_tac
  >> SPLIT_ASM_TAC
  >> REWRITE_ASSUMS_TAC (EVAL “MEM bd_ptr (h::queue)”)
  >> Cases_on ‘bd_ptr = h’
  >- ( pat_x_rewrite_all ‘_=_’
       >> specl_assume_tac
          [‘h’, ‘queue’, ‘n’, ‘state’]
          IS_QUEUE_SECURE_EFFECT_SELF_CHANGE
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> simp []
       >> ‘(∀p1 p2.
              p1 ≠ p2 ∧ (p1 = h ∨ MEM p1 queue) ∧ (p2 = h ∨ MEM p2 queue) ⇒
              word_abs (p1 + -1w * p2) ≥ 16w)’
           by (fs [] >> METIS_TAC [])
       >> asm_rewrite_tac []
       >> simp [is_queue_secure_effect_def]
     )
  >> rewrite_tac [set_and_clear_all_effect_def]
  >> simp []
  >> rewrite_tac [GSYM set_and_clear_all_effect_def]
  >> ‘word_abs (bd_ptr - h) >= 16w’
      by (fs [] >> METIS_TAC [])
  >> ‘ALL_DISTINCT queue ∧ EVERY BD_PTR_ADDRESS queue’
      by fs []
  >> ‘BD_PTR_ADDRESS h’
      by fs []
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by fs [EVERY_MEM]
  >> ‘read_ndp (bd_ptr + n)
          (set_and_clear_local_effect h FLAGS 0w TD SOP_BD
             (set_and_clear_local_effect h FLAGS 0w EOQ EOP_BD
                (set_and_clear_local_effect h FLAGS OWNER 0w SOP_BD
                 (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))))
          = read_ndp (bd_ptr + n) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)’
          by METIS_TAC [READ_NDP_ON_SET_DIFFRENT_BD_PTR_CANCEL, word_abs_diff, FLAGS_def]
  >> asm_rewrite_tac []
  >> ‘MEM bd_ptr queue’
      by fs []
  >> ‘(∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒
                 word_abs (p1 − p2) ≥ 16w)’
      by (fs [] >> METIS_TAC [])
  >> Q.PAT_ASSUM ‘∀bd_ptr n state._’
                          (specl_assume_tac [‘bd_ptr’, ‘n’, ‘state’])
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> fs []
  >> asm_rewrite_tac []
QED


Theorem SET_AND_CLEAR_ALL_EFFECT_PREVERVES_EVERY_TX_BL_LENGTH_LT_2048:
  ∀ queue state .
  EVERY BD_PTR_ADDRESS queue
  ∧ EVERY (λ p . (tx_read_bd p state.nic.regs.CPPI_RAM).bl <₊ 2048w) queue
  ∧ ALL_DISTINCT queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒ EVERY (λ p . (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)).bl <₊ 2048w) queue
Proof
  rpt strip_tac
  >> rewrite_tac [EVERY_MEM]
  >> rpt strip_tac
  >> simp [CPPI_RAMTheory.tx_read_bd_def]
  >> ‘(read_bd_word e 2w
       (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
      = (read_bd_word (e + 8w) 0w
         (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))’
      by (simp [CPPI_RAMTheory.read_bd_word_def])
  >> pat_x_rewrite_all ‘_=_’
  >> simp [GSYM bdTheory.read_ndp_def]
  >> ‘BD_PTR_ADDRESS e’
      by fs[EVERY_MEM]
  >> simp [IS_QUEUE_SECURE_EFFECT_MEM_CHANGE]
  >> simp [READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL, FLAGS_def]
  >> ‘(tx_read_bd e state.nic.regs.CPPI_RAM).bl <₊ 2048w’
      by fs [EVERY_MEM]
  >> fs [CPPI_RAMTheory.tx_read_bd_def
         , CPPI_RAMTheory.read_bd_word_def
         , bdTheory.read_ndp_def
         ]
QED

Theorem IS_QUEUE_SECURE_EFFECT_ON_DIFFRENT_KEEPS_BD_QUEUE:
  ∀ bd_ptr other_queue queue state .
   EVERY BD_PTR_ADDRESS other_queue
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ BD_QUEUE other_queue bd_ptr state.nic.regs.CPPI_RAM
  ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs(p1 - p2) >= 16w)
  ⇒ BD_QUEUE other_queue bd_ptr (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
Proof
  Induct_on ‘other_queue’
  >- (fs [BD_QUEUE_def])
  >> rpt strip_tac
  >> simp [BD_QUEUE_def]
  >> REWRITE_ASSUMS_TAC BD_QUEUE_def
  >> SPLIT_ASM_TAC
  >> asm_rewrite_tac []
  >> pat_x_rewrite_all ‘h = bd_ptr’
  >> REWRITE_ASSUMS_TAC EVERY_DEF
  >> SPLIT_ASM_TAC
  >> ‘(∀p. MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)’
      by (strip_tac >> strip_tac
          >> Q.PAT_X_ASSUM ‘∀ p1 p2 . _’
                           (specl_assume_tac
                            [‘p’, ‘bd_ptr’]
                           )
          >> Q.PAT_UNDISCH_TAC ‘_⇒_’
          >> asm_rewrite_tac []
          >> METIS_TAC [MEM, word_abs_diff]
         )
  >> ‘(read_ndp bd_ptr
       (is_queue_secure_effect queue state).nic.regs.CPPI_RAM)
      = read_ndp bd_ptr state.nic.regs.CPPI_RAM’
      by METIS_TAC [WORD_ADD_0, IS_QUEUE_SECURE_EFFECT_DOESNT_CHANGE_OTHERS]
  >> asm_rewrite_tac []
  >> Q.PAT_X_ASSUM ‘∀ bd_ptr queue state. _’
                   (specl_assume_tac
                    [‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’, ‘queue’
                     , ‘state’
                    ])
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> simp []
QED


Theorem IS_QUEUE_SECURE_EFFECT_ON_DIFFRENT_KEEPS_EVERY_NON_EOP_NOT_LAST:
    ∀ bd_ptr other_queue queue state .
   EVERY BD_PTR_ADDRESS other_queue
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs(p1 - p2) >= 16w)
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ EVERY (λp. ¬FST (is_eop p state) ⇒ read_ndp p state.nic.regs.CPPI_RAM ≠ 0w)
          other_queue
  ⇒ EVERY (λp. ¬FST (is_eop p (is_queue_secure_effect queue state)) ⇒ read_ndp p (is_queue_secure_effect queue state).nic.regs.CPPI_RAM ≠ 0w)
          other_queue
Proof
  rpt strip_tac
  >> RW_ASM_TAC “EVERY (λ p . _) other_queue” EVERY_MEM
  >> rewrite_tac [EVERY_MEM]
  >> strip_tac >> strip_tac
  >> simp [is_queue_secure_effect_def]
  >> Q.PAT_X_ASSUM ‘∀e. MEM e other_queue ⇒ _’
                   (specl_assume_tac [‘e’])
  >> ‘¬FST (is_eop e state) ⇒ read_ndp e state.nic.regs.CPPI_RAM ≠ 0w’
      by fs []
  >> strip_tac
  >> ‘(∀p. MEM p queue ⇒ word_abs (e − p) ≥ 16w)’
      by (strip_tac >> strip_tac
          >> Q.PAT_X_ASSUM ‘∀ p1 p2 . _’ (specl_assume_tac [‘p’, ‘e’])
          >> Q.PAT_UNDISCH_TAC ‘_⇒_’
          >> asm_rewrite_tac []
          >> METIS_TAC [MEM, word_abs_diff]
         )
  >> ‘BD_PTR_ADDRESS e’
      by METIS_TAC [EVERY_MEM]
  >> ‘read_ndp e (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
      = read_ndp e state.nic.regs.CPPI_RAM’
      by  METIS_TAC [word_abs_diff, WORD_ADD_0
                     , IS_QUEUE_SECURE_EFFECT_DOESNT_CHANGE_OTHERS]
  >> pat_x_rewrite_all ‘_=_’
  >> ‘FST (is_eop e (is_queue_secure_effect queue state))
      = FST (is_eop e state)’
      by ( rewrite_tac [is_queue_secure_effect_def
                        , is_eop_def]
           >> ‘WORD32_ALIGNED (e + 12w) ∧ CPPI_RAM_BYTE_PA (e + 12w)’
               by METIS_TAC [BD_PTR_ADDRESS_FLAGS]
           >> simp_st [is_eop_def
                       , read_nic_register_def
                       , register_readTheory.read_register_def
                       , CPPI_RAM_BYTE_PA_lemma
                      ]
           >> COND_CASES_TAC
           >- (simp_st [])
           >> simp_st []
           >> ‘∀ p . MEM p queue ⇒ word_abs (e - p) >= 16w’
               by (fs [] >> METIS_TAC [])
           >> METIS_TAC[bdTheory.read_ndp_def, word_abs_def
                        , SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
         )
  >> ‘read_ndp e (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
      = read_ndp e state.nic.regs.CPPI_RAM’
      by METIS_TAC [WORD_ADD_0, SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS]
  >> pat_x_rewrite_all ‘_=_’
  >> fs [is_queue_secure_effect_def]
QED

Theorem IS_QUEUE_SECURE_EFFECT_PRESERVES_WELL_FORMED_BD_QUEUE:
  ∀ bd_ptr other_queue queue state .
    EVERY BD_PTR_ADDRESS queue
  ∧ (∀ p . MEM p queue ⇒ word_abs (bd_ptr − p) ≥ 16w)
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ (∀p1 p2. MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ∧ WELL_FORMED_BD_QUEUE other_queue bd_ptr state
  ⇒ WELL_FORMED_BD_QUEUE other_queue bd_ptr (is_queue_secure_effect queue state)
Proof
  rpt strip_tac
  >> REWRITE_ASSUMS_TAC WELL_FORMED_BD_QUEUE_def
  >> SPLIT_ASM_TAC
  >> simp [WELL_FORMED_BD_QUEUE_def]
  >> METIS_TAC [IS_QUEUE_SECURE_EFFECT_ON_DIFFRENT_KEEPS_BD_QUEUE
                , IS_QUEUE_SECURE_EFFECT_ON_DIFFRENT_KEEPS_EVERY_NON_EOP_NOT_LAST]
QED

Theorem IS_QUEUE_SECURE_OTHER_QUEUE_RETAINS_CPPI_PROPERTIES:
  ∀ other_queue_start other_queue queue_start queue state .
  WELL_FORMED_BD_QUEUE other_queue other_queue_start state
  ∧ WELL_FORMED_BD_QUEUE queue queue_start (is_queue_secure_effect queue state)
  ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 queue ∧ p1 ≠ p2
     ⇒ word_abs (p1 - p2) >= 16w)
  ∧ EVERY (λ p . ¬ (get_alpha_bit p state.monitor.alpha
                    ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                    ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                    ∨ get_alpha_bit (p + 12w) state.monitor.alpha
                   )) queue
  ∧ (∀p. (CPPI_RAM_BYTE_PA p ∧ WORD32_ALIGNED p)
       ⇒ (IN_QUEUE_RANGE p other_queue ⇒
       ALPHA_QUEUE_MARKED p state.monitor.alpha))
  ∧ (∀p1 p2. MEM p1 other_queue ∧ MEM p2 other_queue ∧ p1 ≠ p2 ⇒
             word_abs (p1 − p2) ≥ 16w)
  ⇒ NON_OVERLAPPING_BD_QUEUES queue other_queue
    ∧ (∀p1 p2. MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs (p1 − p2) ≥ 16w)
    ∧ WELL_FORMED_BD_QUEUE other_queue other_queue_start (is_queue_secure_effect queue state)
Proof
  ntac 6 strip_tac
  >> RW_ASM_TAC “WELL_FORMED_BD_QUEUE other_queue other_queue_start state”
                WELL_FORMED_BD_QUEUE_def
  >> RW_ASM_TAC “WELL_FORMED_BD_QUEUE queue queue_start (is_queue_secure_effect queue state)”
                WELL_FORMED_BD_QUEUE_def
  >> SPLIT_ASM_TAC
  >> ‘∀p1 p2. MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs (p1 − p2) ≥ 16w’
      by METIS_TAC [EXISTING_QUEUE_DIFFS_FROM_NEW_QUEUE]
  >> ‘NON_OVERLAPPING_BD_QUEUES queue other_queue’
       by (rewrite_tac [NON_OVERLAPPING_BD_QUEUES_def]
           >> rpt strip_tac
           >> REWRITE_ASSUMS_TAC MEM_APPEND
           >> FULL_SIMP_TAC bool_ss []
           >- (METIS_TAC [word_abs_diff])
           >> METIS_TAC [word_abs_diff]
          )
  >> asm_rewrite_tac []
  >> Cases_on ‘other_queue’
  >- (fs [WELL_FORMED_BD_QUEUE_def, BD_QUEUE_def])
  >> ‘other_queue_start = h’
      by fs [WELL_FORMED_BD_QUEUE_def, BD_QUEUE_def]
  >> pat_x_rewrite_all ‘other_queue_start = h’
  >> ‘(∀p. MEM p queue ⇒ word_abs (h - p) ≥ 16w)’
      by ( rpt strip_tac
           >> METIS_TAC [MEM, word_abs_diff]
         )
  >> specl_assume_tac
     [‘h’, ‘(h :: t)’, ‘queue’, ‘state’]
     IS_QUEUE_SECURE_EFFECT_PRESERVES_WELL_FORMED_BD_QUEUE
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> simp []
QED

Theorem IS_QUEUE_SECURE_OTHER_QUEUE_RETAINS_BD_QUEUE_ALPHA_MARKED:
  ∀ queue other_queue alpha .
  BD_QUEUE_ALPHA_MARKED [] other_queue alpha
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ EVERY BD_PTR_ADDRESS other_queue
  ⇒
  BD_QUEUE_ALPHA_MARKED queue other_queue
    (FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state')
     alpha queue)
Proof
  fs [BD_QUEUE_ALPHA_MARKED_def]
  >> rpt strip_tac
  >> fs [ALPHA_QUEUE_MARKED_def]
  >> ‘(alpha_bitmask bd_ptr &&
        FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' ADD state') alpha
          queue (alpha_index bd_ptr) ≠ 0w)
          = get_alpha_bit bd_ptr (FOLDR (λbd_ptr' alpha'. update_alpha_effect bd_ptr' ADD alpha') alpha queue)’
          by simp [get_alpha_bit_def]
  >> pat_x_rewrite_all ‘_=_’
  >> Cases_on ‘IN_QUEUE_RANGE bd_ptr (queue ⧺ other_queue)’
  >- (simp []
       >> Cases_on ‘IN_QUEUE_RANGE bd_ptr queue’
       >- ( specl_assume_tac
            [‘bd_ptr’, ‘ADD’, ‘queue’, ‘alpha’]
            IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_EFFECT_EQ_FORAL_MEM_B
            >> Q.PAT_UNDISCH_TAC ‘_⇒_’
            >> fs [ADD_def]
          )
       >> specl_assume_tac
          [‘bd_ptr’, ‘ADD’, ‘queue’, ‘alpha’]
          NOT_IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_OLD_VALUE
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> asm_rewrite_tac []
       >> fs [ADD_def]
       >> strip_tac
       >> simp [get_alpha_bit_def]
       >> Q.PAT_X_ASSUM ‘∀bd_ptr'. _’
                        (specl_assume_tac [‘bd_ptr’])
       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
       >> simp []
       >> ‘IN_QUEUE_RANGE bd_ptr other_queue’
           by (specl_assume_tac
               [‘bd_ptr’, ‘queue’, ‘other_queue’]
               IN_QUEUE_RANGE_APPEND_NOT_IN_LEFT
               >> fs []
              )
       >> simp []
     )
  >> ‘¬ IN_QUEUE_RANGE bd_ptr queue’
      by METIS_TAC [NOT_IN_QUEUE_RANGE_APPEND_NOT_IN_EITHER, APPEND]
  >> ‘¬ IN_QUEUE_RANGE bd_ptr other_queue’
      by METIS_TAC [NOT_IN_QUEUE_RANGE_APPEND_NOT_IN_EITHER, APPEND]
  >> simp []
  >> specl_assume_tac
     [‘bd_ptr’, ‘ADD’, ‘queue’, ‘alpha’]
     NOT_IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_OLD_VALUE
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> simp []
  >> strip_tac
  >> fs [get_alpha_bit_def]
  >> Q.PAT_X_ASSUM ‘∀bd_ptr'. _’
                   (specl_assume_tac [‘bd_ptr’])
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> simp []
QED

Theorem SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE:
  ∀ queue other_queue_start other_queue state .
  EVERY BD_PTR_ADDRESS queue
  ∧ EVERY BD_PTR_ADDRESS other_queue
  ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs(p1 - p2) >= 16w)
  ∧ BD_QUEUE other_queue other_queue_start (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
  ⇒ BD_QUEUE other_queue other_queue_start state.nic.regs.CPPI_RAM
Proof
  Induct_on ‘other_queue’
  >- (simp [set_and_clear_all_effect_def, bd_queueTheory.BD_QUEUE_def])
  >> rpt strip_tac
  >> ‘other_queue_start = h’
      by fs [bd_queueTheory.BD_QUEUE_def]
  >> pat_x_rewrite_all ‘_=_’
  >> RW_ASM_TAC “BD_QUEUE _ _ _” bd_queueTheory.BD_QUEUE_def
  >> SPLIT_ASM_TAC
  >> simp [bd_queueTheory.BD_QUEUE_def]
  >> ‘BD_PTR_ADDRESS h ∧ EVERY BD_PTR_ADDRESS other_queue’
      by fs[]
  >> ‘(∀p. MEM p queue ⇒ word_abs (h − p) ≥ 16w)’
      by (rpt strip_tac
          >>  METIS_TAC [MEM, word_abs_diff]
          )
  >> ‘(read_ndp h
       (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
      = (read_ndp h state.nic.regs.CPPI_RAM)’
      by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_DOESNT_CHANGE_OTHERS
                      , WORD_ADD_0, word_abs_diff]
         )
  >> pat_x_rewrite_all ‘_=_’
  >> ‘∀p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs (p1 − p2) ≥ 16w’
      by (fs [] >> METIS_TAC [])
  >> METIS_TAC []
QED

Theorem SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE_EQ:
  ∀ queue other_queue_start other_queue state .
  EVERY BD_PTR_ADDRESS queue
  ∧ EVERY BD_PTR_ADDRESS other_queue
  ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs(p1 - p2) >= 16w)
  ⇒ BD_QUEUE other_queue other_queue_start (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
  = BD_QUEUE other_queue other_queue_start state.nic.regs.CPPI_RAM
Proof
  rpt strip_tac
  >> EQ_TAC
  >- (METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_ON_UNRELATED_BD_QUEUE])
  >> specl_assume_tac
     [‘other_queue_start’, ‘other_queue’, ‘queue’, ‘state’]
     IS_QUEUE_SECURE_EFFECT_ON_DIFFRENT_KEEPS_BD_QUEUE
  >> fs [is_queue_secure_effect_def]
QED


Theorem SET_AND_CLEAR_DOESNT_TOUCH_EOP_SOP:
  ∀ bd_pa CPPI_RAM set clear msoe .
    ((set = OWNER ∧ clear = 0w ∧ msoe = SOP_BD)
   ∨ (set = 0w ∧ clear = EOQ ∧ msoe = EOP_BD)
   ∨ (set = 0w ∧ clear = TD ∧ msoe = SOP_BD))
    ⇒
  ((31 -- 31)
          (read_bd_word (bd_pa + 12w) 0w
             (set_and_clear_local_effect bd_pa FLAGS set clear msoe
                CPPI_RAM))
          = (31 -- 31)
          (read_bd_word (bd_pa + 12w) 0w CPPI_RAM))
  ∧ ((30 -- 30)
          (read_bd_word (bd_pa + 12w) 0w
             (set_and_clear_local_effect bd_pa FLAGS set clear msoe
                CPPI_RAM))
          = (30 -- 30)
          (read_bd_word (bd_pa + 12w) 0w CPPI_RAM))
Proof
  rpt strip_tac
  >> simp [READ_NDP_ON_SET_AND_CLEAR_WORD_SELF]
  >> (COND_CASES_TAC
      >- (UNDISCH_ALL_TAC
          >> fs [TD_def, SOP_def, EOP_def, SOP_BD_def, FLAGS_def
                 , EOQ_def, OWNER_def]
          >> UNDISCH_ALL_TAC
          >> blastLib.BBLAST_PROVE_TAC)
      >> simp [])
QED

(* The assumption is really just a cheat, since the nic model is not capable of handling  *)
Theorem IS_QUEUE_SECURE_EFFECT_GIVES_TX_LINUX_BD_QUEUE_SOP_EOP_MATCH:
  ∀ bd_ptr queue state .
  BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
    ⇒ TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue (is_queue_secure_effect queue state).nic.regs.CPPI_RAM
Proof
  rpt strip_tac
  >> UNDISCH_TAC “TX_LINUX_BD_QUEUE_SOP_EOP_MATCH queue state.nic.regs.CPPI_RAM”
  >> simp [txInvariantWellDefinedTheory.TX_LINUX_BD_QUEUE_SOP_EOP_MATCH_def]
  >> simp [EVERY_MEM]
  >> strip_tac
  >> strip_tac >> strip_tac
  >> ‘TX_LINUX_BD_SOP_EOP bd_pa state.nic.regs.CPPI_RAM’
      by fs []
  >> UNDISCH_TAC “TX_LINUX_BD_SOP_EOP bd_pa state.nic.regs.CPPI_RAM”
  >> rewrite_tac [txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_EOP_def
                  , txInvariantWellDefinedTheory.TX_LINUX_BD_SOP_def
                  , txInvariantWellDefinedTheory.TX_LINUX_BD_EOP_def
                  , is_queue_secure_effect_def
                 ]
  >> simp [CPPI_RAMTheory.tx_read_bd_def]
  >> ‘((read_bd_word bd_pa 3w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
      = (read_bd_word (bd_pa + 12w) 0w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)))
      ∧ ((read_bd_word bd_pa 3w state.nic.regs.CPPI_RAM)
         = (read_bd_word (bd_pa + 12w) 0w state.nic.regs.CPPI_RAM))’
      by ( METIS_TAC [read_utilsTheory.READ_BD_WORD_IDX_3_EQ_READ_BD_WORD_ADD12])
  >> simp []
  >> ‘(read_bd_word (bd_pa + 12w) 0w
       (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
       = read_ndp (bd_pa + 12w)
        ((set_and_clear_local_effect bd_pa FLAGS 0w TD SOP_BD ∘
          set_and_clear_local_effect bd_pa FLAGS 0w EOQ EOP_BD ∘
          set_and_clear_local_effect bd_pa FLAGS OWNER 0w SOP_BD)
           state.nic.regs.CPPI_RAM)’
        by ( rewrite_tac [GSYM bdTheory.read_ndp_def]
             >> ‘ALL_DISTINCT queue’
                 by (METIS_TAC [BD_QUEUE_ALL_DISTINCT])
             >> METIS_TAC [ IS_QUEUE_SECURE_EFFECT_MEM_CHANGE]
           )
  >> asm_rewrite_tac []
  >> simp [bdTheory.read_ndp_def]
  >> METIS_TAC [SET_AND_CLEAR_DOESNT_TOUCH_EOP_SOP]
QED


(* NOT complete, only does the transmit part.
*)
Theorem IS_QUEUE_SECURE_EFFECT:
  ∀ bd_ptr transmit' state .
  ¬ state.nic.dead
  ∧ transmit' = T
  ⇒ (∃ b queue .
        (is_queue_secure bd_ptr transmit' state
            = (b , if ¬ b
                      then state
                      else if transmit'
                      then is_queue_secure_effect queue state
                      else ARB)
            ∧ (b ⇒ (BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
                    ∧ (BD_QUEUE queue bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
                    ∧ ( bd_ptr = 0w
                           ∨ ((bd_ptr ≠ 0w)
                              ∧ FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap bd_ptr state)
                              ∧ ¬ FST (is_queue_self_overlap bd_ptr state)
                             ∧ FST (is_data_buffer_secure_queue bd_ptr transmit' state)
                             ∧ (transmit' ⇒ FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly bd_ptr state)))
                             ∧ (BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
                         )
                    ))
    )
Proof
  rpt strip_tac
  >> rewrite_tac [is_queue_secure_def]
  >> Cases_on ‘bd_ptr = 0w’
  >- (simp_st [caseM_def]
      >> Q.EXISTS_TAC ‘[]’
      >> simp [WELL_FORMED_ZERO_QUEUE
               , is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def
               ]
     )
  >> ‘∃ b . is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap
      bd_ptr state = (b , state)’
      by (METIS_TAC [IVLICRANAQO_EXISTS])
  >> Cases_on ‘¬b’
  >- (simp_st [caseM_def]
      >> Q.EXISTS_TAC ‘[]’
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def]
     )
  >> ‘∃bd_queue.
             BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM ∧
             LENGTH bd_queue ≤ MAX_QUEUE_LENGTH_NUM
             ∧ EVERY BD_PTR_ADDRESS bd_queue
             ∧ EVERY (λ p . ¬ (get_alpha_bit p state.monitor.alpha
                                  ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 12w) state.monitor.alpha
                                 )
                        ) bd_queue’
             by ( Q.PAT_UNDISCH_TAC ‘_ = (b,state)’
                  >> rewrite_tac [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_def]
                  >> strip_tac
                  >> ‘FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop
                           MAX_QUEUE_LENGTH_NUM bd_ptr state)’
                      by fs []
                  >> METIS_TAC [IS_VALID_LENGTH_IN_CPPI_RAM_ALIGNMENT_NO_ACTIVE_QUEUE_OVERLAP_LOOP_EFFECT]
                )
  >> ‘∃ b . is_queue_self_overlap bd_ptr state = (b , state)’
      by (rewrite_tac [is_queue_self_overlap_def]
          >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_EXISTS]
         )
  >> Cases_on ‘b'’
  >- (simp_st [caseM_def]
      >> Q.EXISTS_TAC ‘[]’
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def]
     )
  >> ‘∃ b . is_transmit_SOP_EOP_packet_length_fields_set_correctly
      bd_ptr state = (b , state)’
      by ( rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_def]
           >> METIS_TAC [ITSEPLFSCOL_EXISTS]
         )
  >> Cases_on ‘transmit' ∧ ¬b'’
  >- (simp_st [caseM_def]
      >> Q.EXISTS_TAC ‘[]’
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def])
  >> ‘((if transmit' then
                  (λx.
                       (λ(x,s). (¬x,s))
                         (is_transmit_SOP_EOP_packet_length_fields_set_correctly
                            bd_ptr x))
                else (λs. (F,s))) state) = (F, state)’
      by (Cases_on ‘transmit'’
          >- ( simp_st [] >> fs [] )
          >> fs [] >> simp_st []
         )
  >> ‘∃ b . is_data_buffer_secure_queue bd_ptr T state = (b , state)’
      by ( rewrite_tac [is_data_buffer_secure_queue_def]
           >> METIS_TAC [IS_DATA_BUFFER_SECURE_QUEUE_EXISTS]
         )
  >> ‘b'’ by METIS_TAC [] (* Only for the current special case *)
  >> Cases_on ‘¬b''’
  >- ( simp_st [caseM_def]
       >> Q.EXISTS_TAC ‘[]’
       >> simp [is_queue_secure_effect_def
                , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def])
  >> simp_st [caseM_def]
  >> ‘∀p1 p2. p1 ≠ p2 ∧ MEM p1 bd_queue' ∧ MEM p2 bd_queue' ⇒
      word_abs (p1 − p2) ≥ 16w’
      by  ( ‘¬FST (is_queue_self_overlap_outer_loop MAX_QUEUE_LENGTH_NUM bd_ptr state)’
            by fs [is_queue_self_overlap_def]
            >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTERLOOP_IMPL_NO_SELF_OVERLAP]
          )

  >> simp_st []
  >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS OWNER 0w SOP_BD state)
           = ((),
          state with
          nic :=
            state.nic with
            regs :=
              state.nic.regs with
              CPPI_RAM :=
                set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                  state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD)
           ’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
  >> pat_x_rewrite_all ‘_=_’
  >> Q.ABBREV_TAC ‘state' = (state with
                     nic :=
                       state.nic with
                       regs :=
                         state.nic.regs with
                         CPPI_RAM :=
                           set_and_clear_word_on_sop_or_eop_foldr_effect
                             bd_queue' state.nic.regs.CPPI_RAM FLAGS OWNER 0w
                             SOP_BD)’
  >> ‘BD_QUEUE bd_queue' bd_ptr state'.nic.regs.CPPI_RAM’
           by ( Q.UNABBREV_TAC ‘state'’
                >> simp []
                >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
              )
  >> ‘¬state'.nic.dead’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS 0w EOQ EOP_BD state')
           = ((),
          state' with
          nic :=
            state'.nic with
            regs :=
              state'.nic.regs with
              CPPI_RAM :=
                set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                state'.nic.regs.CPPI_RAM FLAGS 0w EOQ EOP_BD)’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
  >> Q.ABBREV_TAC ‘state'' = (state' with
                     nic :=
                       state'.nic with
                       regs :=
                         state'.nic.regs with
                         CPPI_RAM :=
                           set_and_clear_word_on_sop_or_eop_foldr_effect
                             bd_queue' state'.nic.regs.CPPI_RAM FLAGS 0w EOQ
                             EOP_BD)’
  >> ‘¬state''.nic.dead’
      by (Q.UNABBREV_TAC ‘state''’>>fs [])
  >> ‘BD_QUEUE bd_queue' bd_ptr state''.nic.regs.CPPI_RAM’
      by ( Q.UNABBREV_TAC ‘state''’
           >> simp []
           >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
         )
  >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS 0w TD SOP_BD state'')
           = ((),
              state'' with
                     nic :=
                     state''.nic with
                     regs :=
                     state''.nic.regs with
                     CPPI_RAM :=
                     set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                     state''.nic.regs.CPPI_RAM FLAGS 0w TD SOP_BD)’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
  >> Q.ABBREV_TAC ‘state'''
                  = (state'' with
                     nic :=
                     state''.nic with
                     regs :=
                     state''.nic.regs with
                     CPPI_RAM :=
                     set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                     state''.nic.regs.CPPI_RAM FLAGS 0w TD SOP_BD)’
  >> ‘¬state'''.nic.dead’
      by (Q.UNABBREV_TAC ‘state'''’>>fs [])
  >> ‘BD_QUEUE bd_queue' bd_ptr state'''.nic.regs.CPPI_RAM’
      by ( Q.UNABBREV_TAC ‘state'''’
           >> simp []
           >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
         )
  >> Q.EXISTS_TAC ‘T’
  >> Q.EXISTS_TAC ‘bd_queue'’
  >> Q.UNABBREV_TAC ‘state'’
  >> Q.UNABBREV_TAC ‘state''’
  >> Q.UNABBREV_TAC ‘state'''’
  >> simp [is_queue_secure_effect_def]
  >> ‘set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
      (set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
       (set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
        state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
       EOQ EOP_BD) FLAGS 0w TD SOP_BD
      = set_and_clear_all_effect state.nic.regs.CPPI_RAM bd_queue'’
      by ( ‘ALL_DISTINCT bd_queue'’
           by (METIS_TAC[BD_QUEUE_ALL_DISTINCT])
           >> METIS_TAC [SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP]
         )
  >> fs []
QED



Theorem IS_QUEUE_SECURE_CONSEQ:
  ∀ bd_ptr state .
  ¬ state.nic.dead
  ∧ FST (is_queue_secure bd_ptr T state)
  ⇒ ∃ queue . ((BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM)
               ∧ EVERY BD_PTR_ADDRESS queue
               ∧ LENGTH queue ≤ MAX_QUEUE_LENGTH_NUM
               ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
             ∧ EVERY (λp. ¬(get_alpha_bit p state.monitor.alpha
                            ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                            ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                            ∨ get_alpha_bit (p + 12w) state.monitor.alpha))
                      queue
              ∧ EVERY (λp. (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)).bl <₊ 2048w) queue
              ∧ WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)

              )
Proof
  rpt strip_tac
  >> ‘∃b queue.
       BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM ∧
        (bd_ptr = 0w ∨
         (bd_ptr ≠ 0w
          ∧ FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap
               bd_ptr state)
          ∧ ¬FST (is_queue_self_overlap bd_ptr state)
          ∧ FST (is_data_buffer_secure_queue bd_ptr T state)
          ∧ (FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly
                  bd_ptr state))
          ∧ BD_QUEUE queue bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)))’
               by ( specl_assume_tac
                   [‘bd_ptr’, ‘T’, ‘state’]
                   IS_QUEUE_SECURE_EFFECT
                   >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                   >> simp []
                   >> strip_tac
                   >> Q.EXISTS_TAC ‘queue’
                   >> ‘b = T’ by fs []
                   >> fs []
                  )
  >- ( Q.EXISTS_TAC ‘[]’
       >> fs [bd_queueTheory.BD_QUEUE_def
              , WELL_FORMED_BD_QUEUE_def]
     )
  >> Q.EXISTS_TAC ‘queue’
  >> asm_rewrite_tac []
  >>  ‘∃bd_queue.
       BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM ∧
       LENGTH bd_queue ≤ MAX_QUEUE_LENGTH_NUM ∧ EVERY BD_PTR_ADDRESS bd_queue ∧
       EVERY (λp. ¬(get_alpha_bit p state.monitor.alpha ∨
                    get_alpha_bit (p + 4w) state.monitor.alpha ∨
                    get_alpha_bit (p + 8w) state.monitor.alpha ∨
                    get_alpha_bit (p + 12w) state.monitor.alpha)) bd_queue’
       by METIS_TAC [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_def
                     , IS_VALID_LENGTH_IN_CPPI_RAM_ALIGNMENT_NO_ACTIVE_QUEUE_OVERLAP_LOOP_EFFECT]
  >> ‘bd_queue' = queue’
      by METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
  >> pat_x_rewrite_all ‘_=_’
  >> asm_rewrite_tac []
  >> ‘(∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)’
      by ( specl_assume_tac
           [‘queue’, ‘bd_ptr’, ‘state’, ‘MAX_QUEUE_LENGTH_NUM’ ]
           IS_QUEUE_SELF_OVERLAP_OUTERLOOP_IMPL_NO_SELF_OVERLAP
           >> Q.PAT_UNDISCH_TAC ‘_⇒_’
           >> asm_rewrite_tac [GSYM is_queue_self_overlap_def]
           >> fs []
         )
  >> asm_rewrite_tac []
  >> ‘EVERY (λp . ¬FST (is_eop p state) ⇒ read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) queue
      ∧ EVERY (λp. (tx_read_bd p state.nic.regs.CPPI_RAM).bl <₊ 2048w) queue’
      by (specl_assume_tac
          [‘bd_ptr’ , ‘state’, ‘MAX_QUEUE_LENGTH_NUM’, ‘queue’]
          ITSEPLFSCOL_EFFECT
          >> fs [is_transmit_SOP_EOP_packet_length_fields_set_correctly_def]
         )
  >> ‘ALL_DISTINCT queue’
      by (METIS_TAC [BD_QUEUE_ALL_DISTINCT])
  >> ‘EVERY (λp . (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)).
             bl <₊ 2048w) queue’
      by ( METIS_TAC [SET_AND_CLEAR_ALL_EFFECT_PREVERVES_EVERY_TX_BL_LENGTH_LT_2048] )
  >> asm_rewrite_tac []
  >> rewrite_tac [WELL_FORMED_BD_QUEUE_def]
  >> asm_rewrite_tac []
  >> ‘EVERY (λp . ¬FST (is_eop p (is_queue_secure_effect queue state)) ⇒
             read_ndp p
             (is_queue_secure_effect queue state).nic.regs.CPPI_RAM ≠ 0w)
      queue’
      by (rpt strip_tac >> simp []
          >> simp [is_queue_secure_effect_def]
          >> simp [GSYM SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP]
          >> Q.ABBREV_TAC ‘state1 =
                 (state with
                     nic :=
                       state.nic with
                       regs :=
                         state.nic.regs with
                         CPPI_RAM :=
                           (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                            state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD))’
          >> ‘BD_QUEUE queue bd_ptr state1.nic.regs.CPPI_RAM’
              by ( Q.UNABBREV_TAC ‘state1’
                   >> simp []
                   >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
                 )
          >> Q.ABBREV_TAC ‘state2 =
                 (state1 with
                     nic :=
                       state1.nic with
                       regs :=
                         state1.nic.regs with
                         CPPI_RAM :=
                           (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                            state1.nic.regs.CPPI_RAM FLAGS 0w EOQ EOP_BD))’
          >> ‘BD_QUEUE queue bd_ptr state2.nic.regs.CPPI_RAM’
              by ( Q.UNABBREV_TAC ‘state2’
                   >> simp []
                   >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
                 )
           >> Q.ABBREV_TAC ‘state3 =
                 (state2 with
                     nic :=
                       state2.nic with
                       regs :=
                         state2.nic.regs with
                         CPPI_RAM :=
                           (set_and_clear_word_on_sop_or_eop_foldr_effect queue
                            state2.nic.regs.CPPI_RAM FLAGS 0w TD SOP_BD))’
          >> ‘BD_QUEUE queue bd_ptr state3.nic.regs.CPPI_RAM’
              by ( Q.UNABBREV_TAC ‘state3’
                   >> simp []
                   >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
                 )
          >> ‘EVERY (λp. ¬FST (is_eop p state1) ⇒
                    read_ndp p state1.nic.regs.CPPI_RAM ≠ 0w) queue’
                   by (Q.UNABBREV_TAC ‘state1’
                       >> specl_assume_tac
                          [‘queue’, ‘bd_ptr’, ‘state’, ‘OWNER’, ‘0w’, ‘SOP_BD’]
                          EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
                       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                       >> simp [FLAGS_def]
                      )
          >> ‘EVERY (λp. ¬FST (is_eop p state2) ⇒
                          read_ndp p state2.nic.regs.CPPI_RAM ≠ 0w) queue’
              by (Q.UNABBREV_TAC ‘state2’
                  >> specl_assume_tac
                     [‘queue’, ‘bd_ptr’, ‘state1’, ‘0w’, ‘EOQ’, ‘EOP_BD’]
                     EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
                  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                  >> simp [FLAGS_def]
                 )
          >> ‘EVERY (λp. ¬FST (is_eop p state3) ⇒
                          read_ndp p state3.nic.regs.CPPI_RAM ≠ 0w) queue’
              by (Q.UNABBREV_TAC ‘state3’
                  >> specl_assume_tac
                     [‘queue’, ‘bd_ptr’, ‘state2’, ‘0w’, ‘TD’, ‘SOP_BD’]
                     EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
                  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                  >> simp [FLAGS_def]
                 )
          >> Q.UNABBREV_TAC ‘state1’
          >> Q.UNABBREV_TAC ‘state2’
          >> Q.UNABBREV_TAC ‘state3’
          >> fs []
         )
  >> asm_rewrite_tac []
  >> simp [is_queue_secure_effect_def]
QED


Theorem IS_QUEUE_SECURE_OTHER_WELL_FORMED_BD_QUEUE:
  ∀ bd_ptr other_queue_start other_queue state .
  ¬ state.nic.dead
    ∧ WELL_FORMED_BD_QUEUE other_queue other_queue_start state
    ∧ FST (is_queue_secure bd_ptr T state)
    ∧ NON_OVERLAPPING_BD_QUEUES [] other_queue
    ∧ BD_QUEUE_ALPHA_MARKED [] other_queue state.monitor.alpha
    ⇒ ∃ queue .
     ( BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
    ∧ WELL_FORMED_BD_QUEUE other_queue other_queue_start (is_queue_secure_effect queue state)
    ∧ NON_OVERLAPPING_BD_QUEUES queue other_queue
    ∧ (∀p1 p2 . MEM p1 queue ∧ MEM p2 other_queue ⇒ word_abs (p1 − p2) ≥ 16w))
Proof
  rpt strip_tac
  >> ‘∃ queue. (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
      ∧ EVERY (λp. ¬(get_alpha_bit p state.monitor.alpha ∨
                     get_alpha_bit (p + 4w) state.monitor.alpha ∨
                     get_alpha_bit (p + 8w) state.monitor.alpha ∨
                     get_alpha_bit (p + 12w) state.monitor.alpha)) queue
      ∧ WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)
      ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM’
      by ( METIS_TAC [IS_QUEUE_SECURE_CONSEQ] )
  >> Q.EXISTS_TAC ‘queue’
  >> asm_rewrite_tac []
  >> ‘(∀p. (CPPI_RAM_BYTE_PA p ∧ WORD32_ALIGNED p)
       ⇒ (IN_QUEUE_RANGE p other_queue ⇒
          ALPHA_QUEUE_MARKED p state.monitor.alpha))’
      by (METIS_TAC [BD_QUEUE_ALPHA_MARKED_def
                     , IN_QUEUE_RANGE_APPEND_RIGHT]
         )
  >> ‘(∀p1 p2. MEM p1 other_queue ∧ MEM p2 other_queue ∧ p1 ≠ p2 ⇒ word_abs (p1 − p2) ≥ 16w)’
      by ( fs [NON_OVERLAPPING_BD_QUEUES_def] )
  >> ‘(∀p1 p2. MEM p1 queue ∧ MEM p2 queue ∧ p1 ≠ p2 ⇒ word_abs (p1 − p2) ≥ 16w)’
      by ( fs [NON_OVERLAPPING_BD_QUEUES_def] )
  >> specl_assume_tac
     [‘other_queue_start’, ‘other_queue’, ‘bd_ptr’, ‘queue’, ‘state’]
     IS_QUEUE_SECURE_OTHER_QUEUE_RETAINS_CPPI_PROPERTIES
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> asm_rewrite_tac []
QED



Theorem IS_QUEUE_SECURE_IS_DATA_BUFFER_SECURE_QUEUE_TX_EFFECT:
  ∀ bd_ptr queue state .
  ¬ state.nic.dead
    ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
    ∧ FST (is_queue_secure bd_ptr T state)
    ⇒ EVERY (λp.
                (tx_read_bd p state.nic.regs.CPPI_RAM).bl >₊ 0w ∧
                if (tx_read_bd p state.nic.regs.CPPI_RAM).sop then
                  (tx_read_bd p state.nic.regs.CPPI_RAM).bp ≤₊
                  4294967295w − (tx_read_bd p state.nic.regs.CPPI_RAM).bo −
                  (tx_read_bd p state.nic.regs.CPPI_RAM).bl + 1w
                else
                  (tx_read_bd p state.nic.regs.CPPI_RAM).bp ≤₊
                  4294967295w − (tx_read_bd p state.nic.regs.CPPI_RAM).bl +
                  1w) queue
Proof
  rpt strip_tac
  >> Cases_on ‘bd_ptr = 0w’
  >- (‘queue = []’
      by (Cases_on ‘queue’
          >- simp []
          >> fs [bd_queueTheory.BD_QUEUE_def]
          >> METIS_TAC []
         )
      >> simp []
     )
  >> specl_assume_tac
     [‘bd_ptr’, ‘T’, ‘state’]
     IS_QUEUE_SECURE_EFFECT
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> ‘b’ by fs []
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> ‘queue' = queue’
      by METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
  >> pat_x_rewrite_all ‘_=_’
  >> specl_assume_tac
     [‘bd_ptr’, ‘state’]
     IS_QUEUE_SECURE_CONSEQ
  >> Q.PAT_UNDISCH_TAC ‘_⇒_’
  >> asm_rewrite_tac []
  >> strip_tac
  >> ‘queue' = queue’
      by METIS_TAC [bd_queueTheory.EQ_START_EQ_QUEUEs_lemma]
  >> pat_x_rewrite_all ‘_=_’
  >> METIS_TAC [IS_DATA_BUFFER_SECURE_QUEUE_TX_EFFECT, is_data_buffer_secure_queue_def]
QED

(*
Theorem IS_QUEUE_SECURE_EFFECT:
  ∀ bd_ptr transmit' state .
  ¬ state.nic.dead
  ∧ FST
  ⇒
    ⇒ (∃ b queue . is_queue_secure bd_ptr transmit' state
              = (b , is_queue_secure_effect queue state )
              ∧ (if b then WELL_FORMED_BD_QUEUE queue bd_ptr (is_queue_secure_effect queue state)
                      ∧ (∀ p1 p2 . MEM p1 queue ∧ MEM p2 queue ∧ p1 ≠ p2
                         ⇒ word_abs (p1 - p2) >= 16w)
                      ∧ EVERY (λp. (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)).bl <₊ 2048w) queue
                      ∧ EVERY (λ p . ¬ (get_alpha_bit p state.monitor.alpha
                                        ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                                        ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                                        ∨ get_alpha_bit (p + 12w) state.monitor.alpha
                                       )) queue
                      ∧ BD_QUEUE queue bd_ptr state.nic.regs.CPPI_RAM
                 else queue = []
                )
      )
Proof
  rpt strip_tac
  >> rewrite_tac [is_queue_secure_def]
  >> Cases_on ‘bd_ptr = 0w’
  >- (simp_st [caseM_def]
      >> Q.EXISTS_TAC ‘[]’
      >> simp [WELL_FORMED_ZERO_QUEUE
               , is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def
               ]
      >> METIS_TAC []
     )
  >> ‘∃ b . is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap
      bd_ptr state = (b , state)’
      by (METIS_TAC [IVLICRANAQO_EXISTS])
  >> Cases_on ‘¬b’
  >- (simp_st [caseM_def]
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def]
     )
  >> ‘∃bd_queue.
             BD_QUEUE bd_queue bd_ptr state.nic.regs.CPPI_RAM ∧
             LENGTH bd_queue ≤ MAX_QUEUE_LENGTH_NUM
             ∧ EVERY BD_PTR_ADDRESS bd_queue
             ∧ EVERY (λ p . ¬ (get_alpha_bit p state.monitor.alpha
                                  ∨ get_alpha_bit (p + 4w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 8w) state.monitor.alpha
                                  ∨ get_alpha_bit (p + 12w) state.monitor.alpha
                                 )
                        ) bd_queue’
             by ( Q.PAT_UNDISCH_TAC ‘_ = (b,state)’
                  >> rewrite_tac [is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_def]
                  >> strip_tac
                  >> ‘FST (is_valid_length_in_cppi_ram_alignment_no_active_queue_overlap_loop
                           MAX_QUEUE_LENGTH_NUM bd_ptr state)’
                      by fs []
                  >> METIS_TAC [IS_VALID_LENGTH_IN_CPPI_RAM_ALIGNMENT_NO_ACTIVE_QUEUE_OVERLAP_LOOP_EFFECT]
                )
  >> ‘∃ b . is_queue_self_overlap bd_ptr state = (b , state)’
      by (rewrite_tac [is_queue_self_overlap_def]
          >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTER_LOOP_EXISTS]
         )
  >> Cases_on ‘b'’
  >- (simp_st [caseM_def]
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def]
     )
  >> ‘∃ b . is_transmit_SOP_EOP_packet_length_fields_set_correctly
      bd_ptr state = (b , state)’
      by ( rewrite_tac [is_transmit_SOP_EOP_packet_length_fields_set_correctly_def]
           >> METIS_TAC [ITSEPLFSCOL_EXISTS]
         )
  >> Cases_on ‘transmit' ∧ ¬b'’
  >- (simp_st [caseM_def]
      >> simp [is_queue_secure_effect_def
               , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def])
  >> ‘((if transmit' then
                  (λx.
                       (λ(x,s). (¬x,s))
                         (is_transmit_SOP_EOP_packet_length_fields_set_correctly
                            bd_ptr x))
                else (λs. (F,s))) state) = (F, state)’
      by (Cases_on ‘transmit'’
          >- ( simp_st [] >> fs [] )
          >> fs [] >> simp_st []
         )
  >> ‘∃ b . is_data_buffer_secure_queue bd_ptr T state = (b , state)’
      by ( rewrite_tac [is_data_buffer_secure_queue_def]
           >> METIS_TAC [IS_DATA_BUFFER_SECURE_QUEUE_EXISTS]
         )
  >> ‘b'’ by METIS_TAC [] (* Only for the current special case *)
  >> Cases_on ‘¬b''’
  >- ( simp_st [caseM_def]
       >> simp [is_queue_secure_effect_def
                , set_and_clear_all_effect_def
               , set_and_clear_word_on_sop_or_eop_foldr_effect_def
               , system_state_component_equality
               , nic_state_component_equality
               , nic_regs_component_equality
               , bd_queueTheory.BD_QUEUE_def])
  >> simp_st [caseM_def]
  >> ‘∀p1 p2. p1 ≠ p2 ∧ MEM p1 bd_queue' ∧ MEM p2 bd_queue' ⇒
      word_abs (p1 − p2) ≥ 16w’
      by  ( ‘¬FST (is_queue_self_overlap_outer_loop MAX_QUEUE_LENGTH_NUM bd_ptr state)’
            by fs [is_queue_self_overlap_def]
            >> METIS_TAC [IS_QUEUE_SELF_OVERLAP_OUTERLOOP_IMPL_NO_SELF_OVERLAP]
          )
  >> Cases_on ‘transmit'’
  >- ( simp_st []
       >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS OWNER 0w SOP_BD state)
           = ((),
          state with
          nic :=
            state.nic with
            regs :=
              state.nic.regs with
              CPPI_RAM :=
                set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                  state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD)
           ’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
       >> simp_st []
       >> Q.PAT_X_ASSUM ‘_=_’ (fn _ => ALL_TAC)
       >> Q.ABBREV_TAC ‘state' = (state with
                     nic :=
                       state.nic with
                       regs :=
                         state.nic.regs with
                         CPPI_RAM :=
                           set_and_clear_word_on_sop_or_eop_foldr_effect
                             bd_queue' state.nic.regs.CPPI_RAM FLAGS OWNER 0w
                             SOP_BD)’
       >> ‘BD_QUEUE bd_queue' bd_ptr state'.nic.regs.CPPI_RAM’
           by ( Q.UNABBREV_TAC ‘state'’
                >> simp []
                >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
              )
       >> ‘¬state'.nic.dead’
           by (Q.UNABBREV_TAC ‘state'’ >> fs [])
       >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS 0w EOQ EOP_BD state')
           = ((),
          state' with
          nic :=
            state'.nic with
            regs :=
              state'.nic.regs with
              CPPI_RAM :=
                set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                state'.nic.regs.CPPI_RAM FLAGS 0w EOQ EOP_BD)’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
       >> Q.ABBREV_TAC ‘state'' = (state' with
                     nic :=
                       state'.nic with
                       regs :=
                         state'.nic.regs with
                         CPPI_RAM :=
                           set_and_clear_word_on_sop_or_eop_foldr_effect
                             bd_queue' state'.nic.regs.CPPI_RAM FLAGS 0w EOQ
                             EOP_BD)’
       >> ‘¬state''.nic.dead’
           by (Q.UNABBREV_TAC ‘state''’>>fs [])
       >> ‘BD_QUEUE bd_queue' bd_ptr state''.nic.regs.CPPI_RAM’
           by ( Q.UNABBREV_TAC ‘state''’
                >> simp []
                >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
              )
       >> ‘(set_and_clear_word_on_sop_or_eop MAX_QUEUE_LENGTH_NUM
            bd_ptr FLAGS 0w TD SOP_BD state'')
           = ((),
              state'' with
                     nic :=
                     state''.nic with
                     regs :=
                     state''.nic.regs with
                     CPPI_RAM :=
                     set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                     state''.nic.regs.CPPI_RAM FLAGS 0w TD SOP_BD)’
           by (METIS_TAC [FLAGS_def
                          , SET_AND_CLEAR_WORD_ON_SOP_OR_EOP_FOLDR_EFFECT])
       >> Q.ABBREV_TAC ‘state'''
                       = (state'' with
                          nic :=
                          state''.nic with
                          regs :=
                          state''.nic.regs with
                          CPPI_RAM :=
                          set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                          state''.nic.regs.CPPI_RAM FLAGS 0w TD SOP_BD)’
       >> ‘¬state'''.nic.dead’
           by (Q.UNABBREV_TAC ‘state'''’>>fs [])
       >> ‘BD_QUEUE bd_queue' bd_ptr state'''.nic.regs.CPPI_RAM’
           by ( Q.UNABBREV_TAC ‘state'''’
                >> simp []
                >> METIS_TAC [FLAGS_def, SET_AND_CLEAR_WORD_ON_SOP_FOLDR_PRESERVES_BD_QUEUE]
              )
       >> simp_st []
       >> ‘EVERY (λp. ¬FST (is_eop p state) ⇒
                  read_ndp p state.nic.regs.CPPI_RAM ≠ 0w) bd_queue'
           ∧ EVERY (λp. (tx_read_bd p state.nic.regs.CPPI_RAM).bl <₊ 2048w) bd_queue'’
           by (‘FST (is_transmit_SOP_EOP_packet_length_fields_set_correctly bd_ptr state)’
               by fs []
               >> METIS_TAC [ITSEPLFSCOL_EFFECT
                             , is_transmit_SOP_EOP_packet_length_fields_set_correctly_def]
              )
       >> ‘EVERY (λp. (tx_read_bd p (set_and_clear_all_effect state.nic.regs.CPPI_RAM bd_queue')).bl <₊ 2048w) bd_queue'’
           by ( METIS_TAC [ SET_AND_CLEAR_ALL_EFFECT_PREVERVES_EVERY_TX_BL_LENGTH_LT_2048
                            , BD_QUEUE_ALL_DISTINCT] )
       >> ‘WELL_FORMED_BD_QUEUE bd_queue' bd_ptr state'''’
           by (simp [WELL_FORMED_BD_QUEUE_def]
               >> ‘EVERY (λp. ¬FST (is_eop p state') ⇒
                    read_ndp p state'.nic.regs.CPPI_RAM ≠ 0w) bd_queue'’
                   by (Q.UNABBREV_TAC ‘state'’
                       >> specl_assume_tac
                          [‘bd_queue'’, ‘bd_ptr’, ‘state’, ‘OWNER’, ‘0w’, ‘SOP_BD’]
                          EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
                       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                       >> simp [FLAGS_def]
                      )
               >> ‘EVERY (λp. ¬FST (is_eop p state'') ⇒
                          read_ndp p state''.nic.regs.CPPI_RAM ≠ 0w) bd_queue'’
                   by (Q.UNABBREV_TAC ‘state''’
                       >> specl_assume_tac
                          [‘bd_queue'’, ‘bd_ptr’, ‘state'’, ‘0w’, ‘EOQ’, ‘EOP_BD’]
                          EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
                       >> Q.PAT_UNDISCH_TAC ‘_⇒_’
                       >> simp [FLAGS_def]
                      )
               >> Q.UNABBREV_TAC ‘state'''’
               >> specl_assume_tac
                  [‘bd_queue'’, ‘bd_ptr’, ‘state''’, ‘0w’, ‘TD’, ‘SOP_BD’]
                          EOP_SET_CORRENTLY_PRESEREVED_BY_SET_AND_CLEAR_ON_SOP_OR_EOP
               >> Q.PAT_UNDISCH_TAC ‘_⇒_’
               >> simp [FLAGS_def]
              )
       >> Q.EXISTS_TAC ‘bd_queue'’
       >> Q.UNABBREV_TAC ‘state'’
       >> Q.UNABBREV_TAC ‘state''’
       >> Q.UNABBREV_TAC ‘state'''’
       >> simp [is_queue_secure_effect_def]
       >> ‘set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                (set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                   (set_and_clear_word_on_sop_or_eop_foldr_effect bd_queue'
                      state.nic.regs.CPPI_RAM FLAGS OWNER 0w SOP_BD) FLAGS 0w
                   EOQ EOP_BD) FLAGS 0w TD SOP_BD
                = set_and_clear_all_effect state.nic.regs.CPPI_RAM bd_queue'’
                by ( ‘ALL_DISTINCT bd_queue'’
                     by (METIS_TAC[BD_QUEUE_ALL_DISTINCT])
                     >> METIS_TAC [SET_AND_CLEAR_WORD_OUTER_EFFECT_MASHUP]
                   )
       >> fs []
     )
  >> METIS_TAC []
QED
*)


Theorem SET_AND_CLEAR_DOESNT_CHANGE_SOP_AND_EOP:
  ∀ bd_ptr CPPI_RAM set clear modify_sop_or_eop .
        ((set = OWNER ∧ clear = 0w ∧ modify_sop_or_eop = SOP_BD)
         ∨ (set = 0w ∧ clear = EOQ ∧ modify_sop_or_eop = EOP_BD)
         ∨ (set = 0w ∧ clear = TD ∧ modify_sop_or_eop = SOP_BD))
        ⇒
        (SOP && (read_bd_word (bd_ptr + 12w) 0w
                   (set_and_clear_local_effect bd_ptr FLAGS set clear modify_sop_or_eop CPPI_RAM))
        = SOP && (read_bd_word (bd_ptr + 12w) 0w
                  CPPI_RAM))
        ∧ (EOP && (read_bd_word (bd_ptr + 12w) 0w
                   (set_and_clear_local_effect bd_ptr FLAGS set clear modify_sop_or_eop CPPI_RAM))
        = EOP && (read_bd_word (bd_ptr + 12w) 0w
                     CPPI_RAM))
Proof
  ntac 5 strip_tac
  >> disch_tac
  >> simp [READ_NDP_ON_SET_AND_CLEAR_WORD_SELF]
  >> COND_CASES_TAC
  >- (fs [OWNER_def, SOP_BD_def, EOQ_def, EOP_def, TD_def, FLAGS_def, SOP_def]
      >> blastLib.BBLAST_PROVE_TAC
     )
  >> simp []
QED

Theorem SET_AN_CLEAR_LOCAL_TD_SOP_BD_EFFECT:
  ∀ bd_ptr CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ⇒
  (tx_read_bd bd_ptr (set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD CPPI_RAM))
  = (tx_read_bd bd_ptr CPPI_RAM) with td := (if (tx_read_bd bd_ptr CPPI_RAM).sop
                                              then F
                                              else (tx_read_bd bd_ptr CPPI_RAM).td)
Proof
  rpt strip_tac
  >> simp [set_and_clear_local_effect_def]
  >> Cases_on ‘SOP && read_bd_word (bd_ptr + 12w) 0w CPPI_RAM ≠ 0w’
  >- ( ‘(tx_read_bd bd_ptr CPPI_RAM).sop’
       by METIS_TAC [read_utilsTheory.SOP_AND_FORM_EQ_EOP_BD_READ]
       >> simp [SOP_BD_def]
       >> simp [CPPI_RAMTheory.tx_read_bd_def]
       >> ‘write_32bit_word
               (¬TD && read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM)
               (bd_ptr + FLAGS) CPPI_RAM
               = (set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD CPPI_RAM)’
               by simp [set_and_clear_local_effect_def, SOP_BD_def]
       >> simp [READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL_ALT, FLAGS_def]
       >> simp [set_and_clear_local_effect_def, SOP_BD_def]
       >> simp []
       >> ‘(12w : 32 word)  = 3w * 4w’
           by fs []
       >> pat_x_rewrite_all ‘_=_’
       >> simp [READ_BD_WORD_ON_WRITE_BD_WORD_INDEXED]
       >> simp [TD_def]
       >> ‘read_bd_word (bd_ptr + 12w) 0w CPPI_RAM = read_bd_word bd_ptr 3w CPPI_RAM’
           by simp [CPPI_RAMTheory.read_bd_word_def]
       >> pat_x_rewrite_all ‘_=_’
       >> simp []
       >> rpt strip_tac
       >> TRY blastLib.BBLAST_PROVE_TAC
       >> fs [CPPI_RAMTheory.tx_read_bd_def]
       >> Q.PAT_UNDISCH_TAC ‘_=_’
       >> blastLib.BBLAST_PROVE_TAC
     )
  >> ‘¬(tx_read_bd bd_ptr CPPI_RAM).sop’
      by METIS_TAC [read_utilsTheory.SOP_AND_FORM_EQ_EOP_BD_READ]
  >> simp [SOP_BD_def]
  >> simp [SOP_BD_def, CPPI_RAMTheory.tx_read_bd_def]
  >> simp [SOP_def, CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem SET_AND_CLEAR_LOCAL_EOQ_EOP_BD_EFFECT:
  ∀ bd_ptr CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ⇒
  (tx_read_bd bd_ptr (set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD CPPI_RAM))
  = (tx_read_bd bd_ptr CPPI_RAM) with eoq := (if (tx_read_bd bd_ptr CPPI_RAM).eop
                                              then F
                                              else (tx_read_bd bd_ptr CPPI_RAM).eoq)
Proof
  rpt strip_tac
  >> simp [set_and_clear_local_effect_def]
  >> Cases_on ‘EOP && read_bd_word (bd_ptr + 12w) 0w CPPI_RAM ≠ 0w’
  >- ( ‘(tx_read_bd bd_ptr CPPI_RAM).eop’
       by METIS_TAC [read_utilsTheory.EOP_AND_FORM_EQ_EOP_BD_READ]
       >> simp [EOP_BD_def]
       >> simp [CPPI_RAMTheory.tx_read_bd_def]
       >> ‘write_32bit_word
               (¬EOQ && read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM)
               (bd_ptr + FLAGS) CPPI_RAM
               = (set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD CPPI_RAM)’
               by simp [set_and_clear_local_effect_def, EOP_BD_def]
       >> simp [READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL_ALT, FLAGS_def]
       >> simp [set_and_clear_local_effect_def, EOP_BD_def]
       >> simp []
       >> ‘(12w : 32 word)  = 3w * 4w’
           by fs []
       >> pat_x_rewrite_all ‘_=_’
       >> simp [READ_BD_WORD_ON_WRITE_BD_WORD_INDEXED]
       >> simp [EOQ_def]
       >> ‘read_bd_word (bd_ptr + 12w) 0w CPPI_RAM = read_bd_word bd_ptr 3w CPPI_RAM’
           by simp [CPPI_RAMTheory.read_bd_word_def]
       >> pat_x_rewrite_all ‘_=_’
       >> simp []
       >> rpt strip_tac
       >> TRY blastLib.BBLAST_PROVE_TAC
       >> fs [CPPI_RAMTheory.tx_read_bd_def]
       >> Q.PAT_UNDISCH_TAC ‘_=_’
       >> blastLib.BBLAST_PROVE_TAC
     )
  >> ‘¬(tx_read_bd bd_ptr CPPI_RAM).eop’
      by METIS_TAC [read_utilsTheory.EOP_AND_FORM_EQ_EOP_BD_READ]
  >> simp [EOP_BD_def]
  >> simp [SOP_BD_def, CPPI_RAMTheory.tx_read_bd_def]
  >> simp [SOP_def, CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem SET_AN_CLEAR_LOCAL_OWNER_SOP_BD_EFFECT:
  ∀ bd_ptr CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ⇒
  (tx_read_bd bd_ptr (set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD CPPI_RAM))
  = (tx_read_bd bd_ptr CPPI_RAM) with own := (if (tx_read_bd bd_ptr CPPI_RAM).sop
                                              then T
                                              else (tx_read_bd bd_ptr CPPI_RAM).own)
Proof
  rpt strip_tac
  >> simp [set_and_clear_local_effect_def]
  >> Cases_on ‘SOP && read_bd_word (bd_ptr + 12w) 0w CPPI_RAM ≠ 0w’
  >- ( simp [SOP_BD_def]
       >> simp [CPPI_RAMTheory.tx_read_bd_def]
       >> ‘write_32bit_word
               (OWNER ‖ read_bd_word (bd_ptr + FLAGS) 0w CPPI_RAM)
               (bd_ptr + FLAGS) CPPI_RAM
               = (set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD CPPI_RAM)’
               by simp [set_and_clear_local_effect_def, SOP_BD_def]
       >> simp [READ_NDP_ADD_N_ON_SET_SAME_BD_PTR_CANCEL_ALT, FLAGS_def]
       >> simp [set_and_clear_local_effect_def, SOP_BD_def]
       >> simp []
       >> ‘(12w : 32 word)  = 3w * 4w’
           by fs []
       >> pat_x_rewrite_all ‘_=_’
       >> simp [READ_BD_WORD_ON_WRITE_BD_WORD_INDEXED]
       >> simp [OWNER_def]
       >> ‘read_bd_word (bd_ptr + 12w) 0w CPPI_RAM = read_bd_word bd_ptr 3w CPPI_RAM’
           by simp [CPPI_RAMTheory.read_bd_word_def]
       >> pat_x_rewrite_all ‘_=_’
       >> ‘(29 -- 29) (read_bd_word bd_ptr 3w CPPI_RAM ‖ 536870912w) = 1w’
           by blastLib.BBLAST_PROVE_TAC
       >> ‘(31 -- 31) (read_bd_word bd_ptr 3w CPPI_RAM) = 1w’
           by (Q.PAT_UNDISCH_TAC ‘_ ≠ 0w’
               >> simp [SOP_def] >> simp [CPPI_RAMTheory.read_bd_word_def]
               >> blastLib.BBLAST_PROVE_TAC)
       >> simp []
       >> rpt strip_tac
       >> TRY blastLib.BBLAST_PROVE_TAC
       >> Q.PAT_UNDISCH_TAC ‘_ ≠ _’
       >> simp [SOP_def, CPPI_RAMTheory.read_bd_word_def]
       >> blastLib.BBLAST_PROVE_TAC
     )
  >> simp [SOP_BD_def, CPPI_RAMTheory.tx_read_bd_def]
  >> Q.PAT_UNDISCH_TAC ‘¬ _’
  >> simp [SOP_def, CPPI_RAMTheory.read_bd_word_def]
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem SET_AND_CLEAR_LOCAL_ALL_TX_READ_BD_EFFECT:
  ∀ bd_ptr CPPI_RAM .
  BD_PTR_ADDRESS bd_ptr
  ⇒
  (tx_read_bd bd_ptr ((set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD ∘
                      set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD ∘
                      set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD) CPPI_RAM))
  = (tx_read_bd bd_ptr CPPI_RAM) with
  <| eoq := (if (tx_read_bd bd_ptr CPPI_RAM).eop
             then F
             else (tx_read_bd bd_ptr CPPI_RAM).eoq)
  ; td := (if (tx_read_bd bd_ptr CPPI_RAM).sop
           then F
           else (tx_read_bd bd_ptr CPPI_RAM).td)
  ; own := (if (tx_read_bd bd_ptr CPPI_RAM).sop
            then T
            else (tx_read_bd bd_ptr CPPI_RAM).own)
  |>
Proof
  rpt strip_tac
  >> simp [ SET_AN_CLEAR_LOCAL_OWNER_SOP_BD_EFFECT
         , SET_AND_CLEAR_LOCAL_EOQ_EOP_BD_EFFECT
         , SET_AN_CLEAR_LOCAL_TD_SOP_BD_EFFECT
       ]
QED


Theorem SET_AND_CLEAR_ALL_MEM_TX_READ_BD_EFFECT:
  ∀ bd_ptr queue state .
  BD_PTR_ADDRESS bd_ptr
  ∧ MEM bd_ptr queue
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ ALL_DISTINCT queue
  ∧ (∀p1 p2. p1 ≠ p2 ∧ MEM p1 queue ∧ MEM p2 queue ⇒ word_abs (p1 − p2) ≥ 16w)
  ⇒
  (tx_read_bd bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue))
  = (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM) with
  <| eoq := (if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eop
             then F
             else (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).eoq)
  ; td := (if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop
           then F
           else (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).td)
  ; own := (if (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).sop
            then T
            else (tx_read_bd bd_ptr state.nic.regs.CPPI_RAM).own)
  |>
Proof
  rpt strip_tac
  >> ‘tx_read_bd bd_ptr (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
      = (tx_read_bd bd_ptr ((set_and_clear_local_effect bd_ptr FLAGS 0w TD SOP_BD ∘
                             set_and_clear_local_effect bd_ptr FLAGS 0w EOQ EOP_BD ∘
                             set_and_clear_local_effect bd_ptr FLAGS OWNER 0w SOP_BD)
                             state.nic.regs.CPPI_RAM))’
      by ( rewrite_tac [CPPI_RAMTheory.tx_read_bd_def]
           >> ‘read_bd_word bd_ptr 0w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               = read_ndp (bd_ptr + 0w) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
                ∧ read_bd_word bd_ptr 1w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               = read_ndp (bd_ptr + 4w) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               ∧ read_bd_word bd_ptr 2w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               = read_ndp (bd_ptr + 8w) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               ∧ read_bd_word bd_ptr 3w (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)
               = read_ndp (bd_ptr + 12w) (set_and_clear_all_effect state.nic.regs.CPPI_RAM queue)’
               by (simp [bdTheory.read_ndp_def, CPPI_RAMTheory.read_bd_word_def])
           >> asm_rewrite_tac []
           >> simp [IS_QUEUE_SECURE_EFFECT_MEM_CHANGE]
           >> simp [bdTheory.read_ndp_def, CPPI_RAMTheory.read_bd_word_def]
         )
  >> simp [SET_AND_CLEAR_LOCAL_ALL_TX_READ_BD_EFFECT]
QED



(*

   = <| ndp := (tx_read_bd bd_ptr CPPI_RAM).ndp
   ;  bp :=  (tx_read_bd bd_ptr CPPI_RAM).bp
   ;  bl := (tx_read_bd bd_ptr CPPI_RAM).bl
   ;  bo := (tx_read_bd bd_ptr CPPI_RAM).bo
   ;  pl := (tx_read_bd bd_ptr CPPI_RAM).pl
   ;  pass_crc := F
   ; td
     |>
     set_and_clear_all_effect_def
        set_and_clear_local_effect_def

*)
val _ = export_theory();
