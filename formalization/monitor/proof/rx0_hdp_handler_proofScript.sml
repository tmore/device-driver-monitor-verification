open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open state_transformerTheory;
open state_transformer_extTheory;

open monitorUtils;
open invariant_lemmasTheory;
open address_lemmasTheory;

open register_writeTheory;
open address_spaceTheory;

open alpha_lemmasTheory;

open initialization_performedTheory;


val _ = new_theory "rx0_hdp_handler_proof"


Definition rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect:
  rx0_hdp_handler_non_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with rx0_hdp_initialized := T)
               ; nic := state.nic with
                <| regs := state.nic.regs with RX0_HDP := 0w;
                   it   := state.nic.it with
                             <|state := it_initialize_hdp_cp;
                             rx0_hdp_initialized := T|>;
                   rx   := state.nic.rx with
                             <|current_bd_pa := 0w; sop_bd_pa := 0w|>
                 |>
               |>)
End

(*

Basically the same as the tx0 variant

*)
Theorem RX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ ¬ (state.monitor.tx0_hdp_initialized
       ∧ state.monitor.tx0_cp_initialized
       ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ rx0_hdp_handler 0w state =
  (T
  , rx0_hdp_handler_non_total_initialized_effect state
   )
Proof
  strip_tac
  >> strip_tac
  >> ‘¬ state.nic.dead’ by
      (UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp [])
  >> rewrite_tac [rx0_hdp_handler_def]
  >> asm_rewrite_tac [ UNCURRY_DEF , BIND_DEF , o_DEF , ifM_def
                       , notM_def , get_initialized_def
                       , functor_map_def , UNIT_DEF
                       , whenMM_def , andM_def]
  >> asm_rewrite_tac [ read_nic_register_def
                      , register_readTheory.read_register_def
                      , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
                      ]
  >> simp []
  >> asm_rewrite_tac [ handlersTheory.write_nic_register_def
                          , register_writeTheory.write_register_def
                          , address_lemmasTheory.RX0_HDP_PA_lemmas
                          , register_writeTheory.write_rx0_hdp_def
                          , set_rx0_hdp_initialized_def
                          , UNCURRY_DEF , BIND_DEF , o_DEF , IGNORE_BIND_DEF
                          , write_nic_register_def
                          , handlersTheory.write_nic_register_def

                     ]
  >> simp [ UNCURRY_DEF , BIND_DEF , o_DEF , ifM_def
                       , notM_def , get_initialized_def
                       , functor_map_def , UNIT_DEF
                       , whenMM_def , andM_def]
  >> simp [ whenMM_def , andM_def , ifM_def , BIND_DEF , UNCURRY_DEF
            , UNIT_DEF , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def , get_tx0_cp_initialized_def
            , get_rx0_cp_initialized_def , return_def, o_DEF ]
  >> ‘state.nic.it.state = it_initialize_hdp_cp’ by
      (UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp [])
  >> simp []
  >> ‘¬(state.nic.it.tx0_hdp_initialized ∧
        state.nic.it.tx0_cp_initialized ∧
        state.nic.it.rx0_cp_initialized)’ by
      (UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp [] >> METIS_TAC [])
  >> asm_rewrite_tac [ rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
  >> simp []

  >> UNDISCH_TAC “¬(state.nic.it.tx0_hdp_initialized ∧
                    state.nic.it.tx0_cp_initialized ∧ state.nic.it.rx0_cp_initialized)”
  >> Cases_on ‘state.monitor.tx0_hdp_initialized’
  >> Cases_on ‘state.monitor.tx0_cp_initialized’
  >> Cases_on ‘state.monitor.rx0_cp_initialized’
  >> fs [ whenMM_def , andM_def , ifM_def , BIND_DEF , UNCURRY_DEF
          , UNIT_DEF , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def , get_tx0_cp_initialized_def
          , get_rx0_cp_initialized_def , return_def, o_DEF ]
QED


(*

Basically the same as the tx0 variant

*)
Theorem  RX0_HDP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT:
  ∀ state .
    MONITOR_INVARIANT state
    ∧ ¬ state.monitor.initialized
    ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
    ⇒ MONITOR_INVARIANT (rx0_hdp_handler_non_total_initialized_effect state)
Proof
  strip_tac >> strip_tac
  >> ‘¬state.nic.dead’
      by (METIS_TAC [MON_INV_NIC_NOT_DEAD_lem])
  >> UNDISCH_TAC “MONITOR_INVARIANT state”
  >> rewrite_tac [MONITOR_INVARIANT_def, rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
  >> simp []
  >> rewrite_tac [GSYM  rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
  >> strip_tac
  >> ‘state.nic.regs.CPPI_RAM = (rx0_hdp_handler_non_total_initialized_effect state).nic.regs.CPPI_RAM’
      by (fs [ rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect])
  >> ‘state.nic.dead = (rx0_hdp_handler_non_total_initialized_effect state).nic.dead’
      by (fs [ rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect])
  >> ‘BD_QUEUE_INVARIANT
      (rx0_hdp_handler_non_total_initialized_effect state)’
      by ( fs [BD_QUEUE_INVARIANT_def]
           >> Q.EXISTS_TAC ‘tx0_queue’
           >> Q.EXISTS_TAC ‘rx0_queue’
           >>  simp [rx0_hdp_handler_NON_TOTAL_INITIALIZED_effect]
           >> Q.ABBREV_TAC ‘state' = (state with
           <|monitor := state.monitor with rx0_hdp_initialized := T;
             nic :=
               state.nic with
               <|regs := state.nic.regs with RX0_HDP := 0w;
                 it :=
                   state.nic.it with
                   <|state := it_initialize_hdp_cp;
                     rx0_hdp_initialized := T|>;
                 rx :=
                 state.nic.rx with <|current_bd_pa := 0w; sop_bd_pa := 0w|> |> |>)’
           >> ‘¬state'.nic.dead’
               by (Q.UNABBREV_TAC ‘state'’ >> fs [])
           >> ‘state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
               by (Q.UNABBREV_TAC ‘state'’ >> simp [])
           >> ‘state.nic.dead = state'.nic.dead’
               by fs []
           >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state'’
               by (METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT])
           >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state'’
               by (METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT])
           >> simp []
         )
  >> asm_rewrite_tac []
  >> fs []
  >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
  >> EVAL_TAC
  >> fs []
QED

Definition rx0_hdp_handler_TOTAL_INITIALIZED_effect:
  rx0_hdp_handler_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with rx0_hdp_initialized := T)
               ; nic := state.nic with
                <| regs := state.nic.regs with RX0_HDP := 0w;
                   it   := state.nic.it with
                             <|state := it_initialized;
                             rx0_hdp_initialized := T|>;
                   rx   := state.nic.rx with
                             <|current_bd_pa := 0w; sop_bd_pa := 0w|>
                 |>
               |>)
End

Theorem RX0_HDP_HANDLER_TOTAL_INITIALIZED_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ (state.monitor.tx0_hdp_initialized
     ∧ state.monitor.tx0_cp_initialized
     ∧ state.monitor.rx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ ∃ tx0_queue rx0_queue .
     rx0_hdp_handler 0w state =
  (T
  , initailization_performed_effect tx0_queue rx0_queue (rx0_hdp_handler_total_initialized_effect state)
   )
Proof
  strip_tac
  >> strip_tac
  >> ‘(∃ tx0_queue . WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state)’
      by (METIS_TAC [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def])
  >> ‘(∃rx0_queue. WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state)’
        by (METIS_TAC [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def])
  >> Q.EXISTS_TAC ‘tx0_queue’
  >> Q.EXISTS_TAC ‘rx0_queue’
  >> ‘¬ state.nic.dead’
      by (METIS_TAC [MON_INV_NIC_NOT_DEAD_lem])
  >> rewrite_tac [rx0_hdp_handler_def]
  >> simp_st [  get_initialized_def , rx0_hdp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_rx0_hdp_def
             , address_lemmasTheory.RX0_HDP_PA_lemmas , set_rx0_hdp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]
  >> ‘state.nic.it.state = it_initialize_hdp_cp’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> ‘(state.nic.it.tx0_hdp_initialized
     ∧ state.nic.it.rx0_cp_initialized
     ∧ state.nic.it.tx0_cp_initialized)’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> Q.ABBREV_TAC ‘state' = (state with
              <|monitor := state.monitor with rx0_hdp_initialized := T;
                nic :=
                  state.nic with
                  <|regs := state.nic.regs with RX0_HDP := 0w;
                    it :=
                      state.nic.it with
                      <|state := it_initialized; rx0_hdp_initialized := T|>;
                    rx :=
                      state.nic.rx with
                      <|current_bd_pa := 0w; sop_bd_pa := 0w|> |> |>)’
  >> ‘(state.nic.dead ⇔ state'.nic.dead) ∧
         state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
         by (Q.UNABBREV_TAC ‘state'’  >> fs [ MONITOR_INVARIANT_def])
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘state'.monitor.rx0_active_queue =  state.monitor.rx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘state'.monitor.tx0_active_queue =  state.monitor.tx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘initialization_performed state' =
             ((),
              initailization_performed_effect tx0_queue rx0_queue state')’
             by (fs [INITIALIZAITON_PERFORMED_EFFECT])
  >> fs []
  >> asm_rewrite_tac []
  >> Q.UNABBREV_TAC ‘state'’
  >> fs [initailization_performed_effect_def
        , rx0_hdp_handler_TOTAL_INITIALIZED_effect]
QED

val _ = export_theory();
