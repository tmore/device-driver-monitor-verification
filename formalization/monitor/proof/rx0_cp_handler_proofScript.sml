open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open state_transformerTheory;
open state_transformer_extTheory;

open monitorUtils;
open invariant_lemmasTheory;
open address_lemmasTheory;

open register_writeTheory;
open address_spaceTheory;

open alpha_lemmasTheory;

open initialization_performedTheory;

val _ = new_theory "rx0_cp_handler_proof"

(*
The effect on the state after running tx0_hdp_handler when the nic hasn't been totaly initialized yet.
*)
Definition rx0_cp_handler_NON_TOTAL_INITIALIZED_effect:
  rx0_cp_handler_non_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with rx0_cp_initialized := T)
               ; nic := state.nic with
                <| interrupt :=
                         ((¬ARB.reg_write.rx0_cp_deassert_interrupt ∨
                           state.nic.tx.interrupt) ∧ state.nic.interrupt)
                 ; regs := state.nic.regs with RX0_CP := 0w
                 ; it   := state.nic.it with
                             <| state := it_initialize_hdp_cp
                              ; rx0_cp_initialized := T|>
                 ; rx   := state.nic.rx with
                               interrupt :=
                                 (¬ARB.reg_write.rx0_cp_deassert_interrupt ∧
                                  state.nic.rx.interrupt)
                 |>
               |>)
End

(* For the case where *)
Theorem RX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ ¬ (state.monitor.tx0_hdp_initialized
       ∧ state.monitor.rx0_hdp_initialized
       ∧ state.monitor.tx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ rx0_cp_handler 0w state =
  (T
  , rx0_cp_handler_non_total_initialized_effect state
   )
Proof
  strip_tac
  >> strip_tac
  >> ‘¬ state.nic.dead’
      by (METIS_TAC [MON_INV_NIC_NOT_DEAD_lem])
  >> simp_st [  get_initialized_def , rx0_cp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_rx0_cp_def
             , address_lemmasTheory.RX0_CP_PA_lemmas , set_rx0_cp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]

  >> ‘state.nic.it.state = it_initialize_hdp_cp’
      by fs [MONITOR_INVARIANT_def]
  >> simp []
  >> ‘¬(state.nic.it.tx0_hdp_initialized
        ∧ state.nic.it.rx0_hdp_initialized
        ∧ state.nic.it.tx0_cp_initialized)’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> asm_rewrite_tac [ rx0_cp_handler_NON_TOTAL_INITIALIZED_effect]
  >> simp []
  >> UNDISCH_TAC “¬(state.nic.it.tx0_hdp_initialized
                   ∧ state.nic.it.rx0_hdp_initialized
                   ∧ state.nic.it.tx0_cp_initialized)”
  >> Cases_on ‘state.monitor.tx0_hdp_initialized’
  >> Cases_on ‘state.monitor.rx0_hdp_initialized’
  >> Cases_on ‘state.monitor.tx0_cp_initialized’
  >> fs_st [ whenMM_def
             , get_rx0_hdp_initialized_def , get_rx0_hdp_initialized_def
             , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def
           ]
QED


Theorem  RX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT:
  ∀ state .
    (MONITOR_INVARIANT state
     ∧ ¬ state.monitor.initialized
     ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w)
    ⇒ MONITOR_INVARIANT (rx0_cp_handler_non_total_initialized_effect state)
Proof
  rpt strip_tac
  >> UNDISCH_TAC “MONITOR_INVARIANT state”
  >> rewrite_tac [MONITOR_INVARIANT_def
                  , rx0_cp_handler_NON_TOTAL_INITIALIZED_effect
                  , BD_QUEUE_INVARIANT_def
                  ]
  >> simp []
  >> rewrite_tac [GSYM  rx0_cp_handler_NON_TOTAL_INITIALIZED_effect]
  >> strip_tac
  >> ‘state.nic.regs.CPPI_RAM = (rx0_cp_handler_non_total_initialized_effect state).nic.regs.CPPI_RAM’
      by (fs [ rx0_cp_handler_NON_TOTAL_INITIALIZED_effect])
  >> ‘state.nic.dead = (rx0_cp_handler_non_total_initialized_effect state).nic.dead’
      by (fs [ rx0_cp_handler_NON_TOTAL_INITIALIZED_effect])
  >> quantHeuristicsLib.QUANT_TAC [("tx0_queue", ‘tx0_queue’, [])
                                   ,("rx0_queue", ‘rx0_queue’, [])
                                  ]
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue
      (rx0_cp_handler_non_total_initialized_effect state)’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue
      (rx0_cp_handler_non_total_initialized_effect state)’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> asm_rewrite_tac []
  >> fs []
  >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
  >> EVAL_TAC
  >> fs []
QED

Definition rx0_cp_handler_TOTAL_INITIALIZED_effect:
  rx0_cp_handler_total_initialized_effect (state : system_state) =
  (state with <| monitor := (state.monitor with rx0_cp_initialized := T)
               ; nic := state.nic with
                <| interrupt :=
                         ((¬ARB.reg_write.rx0_cp_deassert_interrupt ∨
                           state.nic.tx.interrupt) ∧ state.nic.interrupt)
                 ; regs := state.nic.regs with RX0_CP := 0w
                 ; it   := state.nic.it with
                             <|state := it_initialized;
                             rx0_cp_initialized := T|>
                 ; rx   := state.nic.rx with
                               interrupt :=
                                 (¬ARB.reg_write.rx0_cp_deassert_interrupt ∧
                                  state.nic.rx.interrupt)
                 |>
               |>)
End


Theorem RX0_CP_HANDLER_TOTAL_INITIALIZED_lemma:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ ¬ state.monitor.initialized
  ∧ (state.monitor.tx0_hdp_initialized
    ∧ state.monitor.rx0_hdp_initialized
    ∧ state.monitor.tx0_cp_initialized)
  ∧ state.nic.regs.CPDMA_SOFT_RESET = 0w
  ⇒ (∃ tx0_queue rx0_queue . (rx0_cp_handler 0w state =
                              (T
                               , initailization_performed_effect tx0_queue rx0_queue (rx0_cp_handler_total_initialized_effect state)
                              ))
     ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
     ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
    )
Proof
  strip_tac
  >> strip_tac
  >> ‘(∃ tx0_queue . WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state)’
      by (METIS_TAC [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def])
  >> ‘(∃rx0_queue. WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state)’
        by (METIS_TAC [MONITOR_INVARIANT_def , BD_QUEUE_INVARIANT_def])
  >> Q.EXISTS_TAC ‘tx0_queue’
  >> Q.EXISTS_TAC ‘rx0_queue’
  >> ‘¬ state.nic.dead’
      by (METIS_TAC [MON_INV_NIC_NOT_DEAD_lem])
  >> rewrite_tac [rx0_cp_handler_def]
  >> simp_st [  get_initialized_def , rx0_cp_handler_def
             , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas
             , register_readTheory.read_register_def , read_nic_register_def
             , write_nic_register_def
             , handlersTheory.write_nic_register_def , register_writeTheory.write_register_def
             , register_writeTheory.write_rx0_cp_def
             , address_lemmasTheory.RX0_CP_PA_lemmas , set_rx0_cp_initialized_def

            , get_initialized_def
            , get_tx0_hdp_initialized_def , get_rx0_hdp_initialized_def
            , get_rx0_cp_initialized_def , get_tx0_cp_initialized_def]
  >> ‘state.nic.it.state = it_initialize_hdp_cp’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> ‘(state.nic.it.tx0_hdp_initialized
     ∧ state.nic.it.rx0_hdp_initialized
     ∧ state.nic.it.tx0_cp_initialized)’
      by (METIS_TAC [MONITOR_INVARIANT_def])
  >> simp []
  >> Q.ABBREV_TAC ‘state' = (state with
              <|monitor := state.monitor with rx0_cp_initialized := T;
                nic :=
                  state.nic with
                  <|interrupt :=
                      ((¬ARB.reg_write.rx0_cp_deassert_interrupt ∨
                        state.nic.tx.interrupt) ∧ state.nic.interrupt);
                    regs := state.nic.regs with RX0_CP := 0w;
                    it :=
                      state.nic.it with
                      <|state := it_initialized; rx0_cp_initialized := T|>;
                    rx :=
                      state.nic.rx with
                      interrupt :=
                        (¬ARB.reg_write.rx0_cp_deassert_interrupt ∧
                         state.nic.rx.interrupt)|> |>)’
  >> ‘(state.nic.dead ⇔ state'.nic.dead) ∧
         state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
         by (Q.UNABBREV_TAC ‘state'’  >> fs [ MONITOR_INVARIANT_def])
  >> ‘WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state'’
      by METIS_TAC [WELL_FORMED_CPPI_RAM_DEPENDANT]
  >> ‘state'.monitor.rx0_active_queue =  state.monitor.rx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘state'.monitor.tx0_active_queue =  state.monitor.tx0_active_queue’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘initialization_performed state' =
             ((),
              initailization_performed_effect tx0_queue rx0_queue state')’
             by (fs [INITIALIZAITON_PERFORMED_EFFECT])
  >> fs []
  >> asm_rewrite_tac []
  >> Q.UNABBREV_TAC ‘state'’
  >> fs [initailization_performed_effect_def
        , rx0_cp_handler_TOTAL_INITIALIZED_effect]
QED


(*

Proof that of the initialization case of the tx0_hdp_handler call

*)
Theorem RX0_CP_HANDLER_INITIALIZATION_PRESERVES_INV_lem:
  ∀ state .
  MONITOR_INVARIANT state
  ∧ (¬ state.monitor.initialized)
  ⇒ MONITOR_INVARIANT (SND (rx0_cp_handler 0w state))
Proof
  strip_tac >> strip_tac
  >> ‘¬state.nic.dead’
      by METIS_TAC [MON_INV_NIC_NOT_DEAD_lem]
  >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET ≠ 0w’
  >- ( rewrite_tac [rx0_cp_handler_def]
       >> simp_st [ get_initialized_def
                 , read_nic_register_def , register_readTheory.read_register_def
                 , address_lemmasTheory.CPDMA_SOFT_RESET_PA_lemmas]
       >> Cases_on ‘state.nic.regs.CPDMA_SOFT_RESET’
       >> fs []
       >> Cases_on ‘n’
       >> fs []
       >> Cases_on ‘n'’
       >> fs []
       >> ‘state with nic := state.nic = state’
           by fs [monitor_stateTheory.system_state_component_equality]
       >> asm_rewrite_tac []
     )
  >> fs []
  >> Cases_on  ‘(state.monitor.tx0_hdp_initialized
                 ∧ state.monitor.rx0_hdp_initialized
                 ∧ state.monitor.tx0_cp_initialized)’
  >- ( ‘∃tx0_queue rx0_queue.
             rx0_cp_handler 0w state =
             (T,
              initailization_performed_effect tx0_queue rx0_queue
                                              (rx0_cp_handler_total_initialized_effect state))
             ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue
             state
             ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue
               state
             ’
             by (METIS_TAC [ RX0_CP_HANDLER_TOTAL_INITIALIZED_lemma])
       >> asm_rewrite_tac [ initailization_performed_effect_def
                            , rx0_cp_handler_TOTAL_INITIALIZED_effect]
       >> fs []
       >> Q.ABBREV_TAC ‘state' = (state with
           <|monitor :=
               state.monitor with
               <|tx0_active_queue := 0w; rx0_active_queue := 0w;
                 initialized := T; rx0_cp_initialized := T;
                 alpha :=
                   FOLDR
                     (λ bd_ptr' state' .
                          update_alpha_effect bd_ptr' REMOVE state')
                     state.monitor.alpha (tx0_queue ⧺ rx0_queue)|>;
             nic :=
               state.nic with
               <|interrupt :=
                   ((¬ARB.reg_write.rx0_cp_deassert_interrupt ∨
                     state.nic.tx.interrupt) ∧ state.nic.interrupt);
                 regs := state.nic.regs with RX0_CP := 0w;
                 it :=
                   state.nic.it with
                   <|state := it_initialized; rx0_cp_initialized := T|>;
                 rx :=
                   state.nic.rx with
                   interrupt :=
                     (¬ARB.reg_write.rx0_cp_deassert_interrupt ∧
                      state.nic.rx.interrupt)|> |>)’
       >> ‘BD_QUEUE_INVARIANT state'’
           by ( rewrite_tac [BD_QUEUE_INVARIANT_def]
                >> Q.EXISTS_TAC ‘[]’
                >> Q.EXISTS_TAC ‘[]’
                >> ‘state'.monitor.tx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> ‘state'.monitor.rx0_active_queue = 0w’
                    by (Q.UNABBREV_TAC ‘state'’ >> fs[])
                >> simp [WELL_FORMED_BD_QUEUE_def
                         , bd_queueTheory.BD_QUEUE_def
                         , NON_OVERLAPPING_BD_QUEUES_def
                        ]
                >> ‘BD_QUEUE_ALPHA_MARKED [] [] state'.monitor.alpha’
                    by (Q.UNABBREV_TAC ‘state'’
                        >> simp []

                        >> METIS_TAC [BD_QUEUE_INVARIANT_UNPACK
                                      ,UPDATE_ALPHA_REMOVE_ALL_NOTHING_ALPHA_MARKED]
                       )
                >> asm_rewrite_tac []
                >> simp []
                >> ‘state.nic.tx.sop_bd_pa = 0w ∧ state.nic.rx.sop_bd_pa = 0w
                    ∧ state.nic.rx.current_bd_pa = 0w’
                    by (METIS_TAC [MONITOR_INVARIANT_def])
                >> ‘state'.nic.tx.sop_bd_pa = 0w ∧ state'.nic.rx.sop_bd_pa = 0w
                    ∧ state'.nic.rx.current_bd_pa = 0w’
                    by (Q.UNABBREV_TAC ‘state'’>> fs[])
                >> METIS_TAC []
              )
       >> rewrite_tac [MONITOR_INVARIANT_def]
       >> asm_rewrite_tac []
       >> Q.UNABBREV_TAC ‘state'’
       >> fs [MONITOR_INVARIANT_def]
       >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
       >> fs [nicInvariantTheory.NIC_INVARIANT_def]
       >> EVAL_TAC >> fs [] >> blastLib.BBLAST_PROVE_TAC
     )
  >> ‘rx0_cp_handler 0w state =
      (T,rx0_cp_handler_non_total_initialized_effect state)’
      by fs [ RX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_lemma]
  >> asm_rewrite_tac []
  >> METIS_TAC [RX0_CP_HANDLER_NON_TOTAL_INITIALIZED_EFFECT_PRESERVES_INVARIANT]
QED

val _ = export_theory();
