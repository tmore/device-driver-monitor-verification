open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open read_utilsTheory;
open address_lemmasTheory;

open monitorUtils;
open invariant_lemmasTheory;


val _ = new_theory "alpha_lemmas";


Theorem X_AND_Y_LT_Y:
  ∀ (w : 32 word) . ((w && 31w) < 32w)
Proof
  strip_tac >> blastLib.BBLAST_PROVE_TAC
QED

Theorem A_BOR_B_BAND_B_EQ_B:
  ∀ (a : 32 word) b .
    (a || b) && b = b
Proof
  NTAC 2 strip_tac
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem SHIFTL_1_NOT_0:
  ∀ s . s < 32 ⇒ ((1w : 32 word) << s ≠ 0w)
Proof
  Cases
  >> fs []
  >> rpt (goal_term (fn tm => Cases_on ‘^(find_term is_var tm)’) >> fs [])
QED

Theorem WORD_ADD_LEFT_ZERO:
  ∀ a .
    0w && a = 0w
Proof
  strip_tac
  >> WORD_DECIDE_TAC
QED

Theorem WORD_ADD_RIGHT_ZERO:
  ∀ a .
    a && 0w = 0w
Proof
  strip_tac
  >> WORD_DECIDE_TAC
QED


Theorem ONE_SHIFT_LT_32_INJ:
  ∀ a b .
      (a < 32)
      ∧ (b < 32)
      ⇒ ((1w : 32 word) <<  a = 1w << b) = (a = b)
Proof
  rpt strip_tac
  >> NTAC 32 (Cases_on_goal (find_term is_var) >- (NTAC 32 (Cases_on_goal (find_term is_var) >- fs []) >> fs []))
  >> fs []
QED

Theorem WORD32_SHIFT_BIT_SET_EFFECT:
  ∀ (b : 32 word) s1 s2 .
    (s1 < 32 ∧ s2 < 32 ∧ s1 ≠ s2) ⇒
    ((b || (1w << s1)) && (1w << s2)) = (b && (1w << s2))
Proof
  NTAC 4 strip_tac
  >> NTAC 32 (Cases_on_goal (find_nth_term 1 is_var)
           >- (NTAC 32 (Cases_on_goal (find_nth_term 1 is_var) >- (WORD_DECIDE_TAC >> fs[])) >> fs []))
  >> fs []
QED

Theorem WORD32_SHIFT_BIT_CLEAR_EFFECT:
  ∀ (b : 32 word) s1 s2 .
    (s1 < 32 ∧ s2 < 32 ∧ s1 ≠ s2) ⇒
    ((b && (~(1w << s1))) && (1w << s2)) = (b && (1w << s2))
Proof
  NTAC 4 strip_tac
  >> NTAC 32 (Cases_on_goal (find_nth_term 1 is_var)
           >- (NTAC 32 (Cases_on_goal (find_nth_term 1 is_var) >- (WORD_DECIDE_TAC >> fs[])) >> fs []))
  >> fs []
QED

Theorem BD_PTR_ADDRESS_PLUS_N_WORD_ABS:
  ∀ p1 p2 n .
    (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ∧ CPPI_RAM_BYTE_PA p1 ∧ WORD32_ALIGNED p1
  ∧ BD_PTR_ADDRESS p2
  ∧ ¬ IN_ALPHA_RANGE_OF p2 p1
  ⇒ (word_abs (p1 - (p2 + n)) >= 4w)
Proof
  NTAC 3 strip_tac
  >> EVAL_TAC
  >> Cases_on ‘p1 + -(p2 + n) < 0w’
  >> asm_rewrite_tac []
  >- (Q.PAT_UNDISCH_TAC ‘p1 + -(p2 + n) < 0w’
      >> blastLib.BBLAST_PROVE_TAC
     )
  >> Q.PAT_UNDISCH_TAC ‘¬(p1 + -(p2 + n) < 0w)’
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BD_PTR_ADDRESS_WORD_ABS:
  ∀ p1 p2 .
  CPPI_RAM_BYTE_PA p1 ∧ WORD32_ALIGNED p1
  ∧ BD_PTR_ADDRESS p2
  ∧ ¬ IN_ALPHA_RANGE_OF p2 p1
  ⇒ (word_abs (p1 - p2) >= 4w)
Proof
  METIS_TAC [WORD_ADD_0 ,  BD_PTR_ADDRESS_PLUS_N_WORD_ABS]
QED

Definition alpha_bitmask_index_def:
  alpha_bitmask_index p = w2n ((p − CPPI_RAM_START) ≫ 2 && 31w)
End

Definition get_alpha_bit_def:
  get_alpha_bit bd_ptr alpha =
     (((alpha (alpha_index bd_ptr))
          && alpha_bitmask bd_ptr) ≠ (0w : 32 word))
End

Definition set_cppi_ram_effect_def:
  set_cppi_ram_effect (bd_ptr : 32 word) (alpha : 32 word -> 32 word) =
    (λ addr . if addr = alpha_index bd_ptr
              then alpha (alpha_index bd_ptr) || alpha_bitmask bd_ptr
              else alpha addr)
End

Definition clear_cppi_ram_effect_def:
  clear_cppi_ram_effect (bd_ptr : 32 word) (alpha : 32 word -> 32 word) =
     (λ addr . if addr = alpha_index bd_ptr
                  then alpha (alpha_index bd_ptr) && ¬ (alpha_bitmask bd_ptr)
                  else alpha addr)
End

Theorem SET_CPPI_RAM_EFFECT_COMM:
  ∀ p1 p2 alpha .
  set_cppi_ram_effect p1 (set_cppi_ram_effect p2 alpha)
  = set_cppi_ram_effect p2 (set_cppi_ram_effect p1 alpha)
Proof
  rpt strip_tac
  >> EVAL_TAC
  >> simp [FUN_EQ_THM]
  >> strip_tac
  >> COND_CASES_TAC
  >> COND_CASES_TAC
  >> COND_CASES_TAC
  >> simp []
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem CLEAR_CPPI_RAM_EFFECT_COMM:
  ∀ p1 p2 alpha .
  clear_cppi_ram_effect p1 (clear_cppi_ram_effect p2 alpha)
  = clear_cppi_ram_effect p2 (clear_cppi_ram_effect p1 alpha)
Proof
  rpt strip_tac
  >> EVAL_TAC
  >> simp [FUN_EQ_THM]
  >> strip_tac
  >> COND_CASES_TAC
  >> COND_CASES_TAC
  >> COND_CASES_TAC
  >> simp []
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED


Theorem BITMASK_INDEX_LESS_THEN_32:
  ∀ (ptr : 32 word) .
    alpha_bitmask_index ptr < 32
Proof
  strip_tac
  >> EVAL_TAC
  >> ‘((ptr + 3052396544w) ≫ 2 && 31w) < 32w’
          by fs [ X_AND_Y_LT_Y]
  >> specl_assume_tac [‘((ptr + 3052396544w) ≫ 2 && 31w)’
                       , ‘32w’]
                      WORD_LO
  >> blastLib.FULL_BBLAST_TAC
QED

Theorem ALPHA_BITMASK_NOT_ZERO:
  ∀ (ptr : 32 word) .
  alpha_bitmask ptr ≠ 0w
Proof
  strip_tac
  >> rewrite_tac [alpha_bitmask_def]
  >> rewrite_tac [GSYM alpha_bitmask_index_def]
  >> ‘alpha_bitmask_index ptr < 32’
      by fs [ BITMASK_INDEX_LESS_THEN_32 ]
  >> Cases_on ‘alpha_bitmask_index ptr’
  >- fs []
  >> rpt (Cases_on_goal (find_term is_var) >> fs [])
QED

Theorem ALPHA_BITMASK_IS_POW:
  ∀ (p : 32 word) .
  ∃ n . n < 32 ∧ alpha_bitmask p = 1w << n
Proof
  strip_tac
  >> rewrite_tac [alpha_bitmask_def]
  >> rewrite_tac [GSYM alpha_bitmask_index_def]
  >> ‘alpha_bitmask_index p < 32’
      by fs [ BITMASK_INDEX_LESS_THEN_32 ]
  >> Q.EXISTS_TAC ‘alpha_bitmask_index p’
  >> Cases_on ‘alpha_bitmask_index p’
  >- fs []
  >> rpt (Cases_on_goal (find_term is_var) >> fs [])
QED

Theorem ALPHA_BITMASK_NOT_EQ_IMP_ZERO:
  ∀ p1 (p2 : 32 word) .
  alpha_bitmask p1 ≠ alpha_bitmask p2
  ⇒ alpha_bitmask p1 && alpha_bitmask p2  = 0w
Proof
  strip_tac >> strip_tac
  >> rewrite_tac [alpha_bitmask_def]
  >> rewrite_tac [GSYM alpha_bitmask_index_def]
  >> ‘alpha_bitmask_index p1 < 32’
      by fs [ BITMASK_INDEX_LESS_THEN_32 ]
  >> ‘alpha_bitmask_index p2 < 32’
      by fs [ BITMASK_INDEX_LESS_THEN_32 ]
  >> Cases_on ‘alpha_bitmask_index p1’
  >- (Cases_on ‘alpha_bitmask_index p2’
      >- fs []
      >> rpt (Cases_on_goal (find_term is_var) >> fs [])
     )
  >> Cases_on ‘alpha_bitmask_index p2’
  >- rpt (Cases_on_goal (find_term is_var) >> fs [])
  >> (rpt (Cases_on_goal (find_term is_var) >- (rpt (Cases_on_goal (find_term is_var) >> fs [])) >> fs []))
QED

Theorem BITMASK_INDEX_WORD_NOT_EQ:
  ∀ p1 (p2 : 32 word) .
    (word_abs(p1 - p2) >= 4w)
    ∧ (alpha_index p1 = alpha_index p2)
    ⇒ ((p1 − CPPI_RAM_START) ≫ 2 && 31w) ≠  ((p2 − CPPI_RAM_START) ≫ 2 && 31w)
Proof
  strip_tac >> strip_tac
  >> EVAL_TAC
  >> Cases_on ‘p1 + -p2 < 0w’
  >> asm_rewrite_tac []
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BITMASK_INDEX_NOT_EQ:
  ∀ p1 (p2 : 32 word) .
    (word_abs(p1 - p2) >= 4w)
    ∧ (alpha_index p1 = alpha_index p2)
    ⇒ alpha_bitmask_index p1 ≠ alpha_bitmask_index p2
Proof
  strip_tac >> strip_tac
  >> strip_tac
  >> rewrite_tac [alpha_bitmask_index_def]
  >> ‘((p1 − CPPI_RAM_START) ≫ 2 && 31w) ≠ ((p2 − CPPI_RAM_START) ≫ 2 && 31w)’
      by METIS_TAC [BITMASK_INDEX_WORD_NOT_EQ]
  >> WORD_DECIDE_TAC
QED

Theorem BD_PTR_NOT_IN_RANGE_SAME_ALPHA_INDEX_NOT_SAME_BITMASK_INDEX_WORD:
  ∀ p1 (p2 : 32 word) .
    BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ∧ ¬ IN_ALPHA_RANGE_OF p2 p1
  ∧ (alpha_index p1 = alpha_index p2)
  ⇒ ((p1 − CPPI_RAM_START) ≫ 2 && 31w) ≠  ((p2 − CPPI_RAM_START) ≫ 2 && 31w)
Proof
  strip_tac >> strip_tac
  >> EVAL_TAC
  >> Cases_on ‘p1 + -p2 < 0w’
  >> UNDISCH_ALL_TAC
  >> asm_rewrite_tac []
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BD_PTR_NOT_IN_RANGE_SAME_ALPHA_INDEX_NOT_SAME_BITMASK_INDEX:
  ∀ p1 (p2 : 32 word) .
    BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ∧ ¬ IN_ALPHA_RANGE_OF p2 p1
  ∧ (alpha_index p1 = alpha_index p2)
  ⇒ w2n ((p1 − CPPI_RAM_START) ≫ 2 && 31w) ≠  w2n ((p2 − CPPI_RAM_START) ≫ 2 && 31w)
Proof
  NTAC 3 strip_tac
  >> ‘((p1 − CPPI_RAM_START) ≫ 2 && 31w) ≠ ((p2 − CPPI_RAM_START) ≫ 2 && 31w)’
      by METIS_TAC [BD_PTR_NOT_IN_RANGE_SAME_ALPHA_INDEX_NOT_SAME_BITMASK_INDEX_WORD]
  >> METIS_TAC [w2n_11]
QED


Theorem IN_ALPHA_RANGE_EQ_BITMASK_INDEX:
  ∀ p1 (p2 : 32 word) .
  IN_ALPHA_RANGE_OF p2 p1
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒ (alpha_bitmask_index p1 = alpha_bitmask_index p2
     ∨ alpha_bitmask_index p1 = alpha_bitmask_index (p2 + 4w)
     ∨ alpha_bitmask_index p1 = alpha_bitmask_index (p2 + 8w)
     ∨ alpha_bitmask_index p1 = alpha_bitmask_index (p2 + 12w)
    )
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> EVAL_TAC
  >> CCONTR_TAC
  >> fs []
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem OR_NON_ZERO_EQ_NONZERO:
  ∀ a (b : 32 word) .
  a ≠ 0w
  ⇒ a || b ≠ 0w
Proof
  strip_tac >> strip_tac
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem ALPHA_INDEX_NOT_EQ_BITMASK_INDEX_NOT_EQ:
  ∀ n p1 p2 .
      (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
      ∧ IN_ALPHA_RANGE_OF p2 p1
      ∧ alpha_index p1 ≠ alpha_index (p2 + n)
    ⇒ alpha_bitmask_index p1 ≠ alpha_bitmask_index (p2 + n)
Proof
  NTAC 4 strip_tac
  >> ‘(((p1 + 3052396544w) ≫ 2 && 31w) ≠ ((p2 + n + 3052396544w) ≫ 2 && 31w))’
      by (UNDISCH_ALL_TAC >> EVAL_TAC >> blastLib.BBLAST_PROVE_TAC)
  >> EVAL_TAC
  >> METIS_TAC [w2n_11]
QED


Theorem ALPHA_INDEX_NOT_EQ_BITMASK_NOT_EQ:
  ∀ n p1 p2 .
      (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
      ∧ IN_ALPHA_RANGE_OF p2 p1
      ∧ alpha_index p1 ≠ alpha_index (p2 + n)
    ⇒ alpha_bitmask p1 ≠ alpha_bitmask (p2 + n)
Proof
  METIS_TAC [ BITMASK_INDEX_LESS_THEN_32
              , alpha_bitmask_def
              , GSYM alpha_bitmask_index_def
              , ALPHA_INDEX_NOT_EQ_BITMASK_INDEX_NOT_EQ
              , ONE_SHIFT_LT_32_INJ]
QED

Theorem IN_ALPHA_RANGE_EQ_BITMASK:
  ∀ p1 (p2 : 32 word) .
  IN_ALPHA_RANGE_OF p2 p1
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒ (alpha_bitmask p1 = alpha_bitmask p2
     ∨ alpha_bitmask p1 = alpha_bitmask (p2 + 4w)
     ∨ alpha_bitmask p1 = alpha_bitmask (p2 + 8w)
     ∨ alpha_bitmask p1 = alpha_bitmask (p2 + 12w)
    )
Proof
  rpt strip_tac
  >> rewrite_tac [alpha_bitmask_def]
  >> rewrite_tac [GSYM alpha_bitmask_index_def]
  >> METIS_TAC [IN_ALPHA_RANGE_EQ_BITMASK_INDEX]
QED

Theorem IN_ALPHA_RANGE_EQ_INDEX:
  ∀ p1 (p2 : 32 word) .
  IN_ALPHA_RANGE_OF p2 p1
  ∧ BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ⇒ (alpha_index p1 = alpha_index p2
     ∨ alpha_index p1 = alpha_index (p2 + 4w)
     ∨ alpha_index p1 = alpha_index (p2 + 8w)
     ∨ alpha_index p1 = alpha_index (p2 + 12w)
    )
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> EVAL_TAC
  >> CCONTR_TAC
  >> fs []
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem IN_RANGE_OF_EQS:
  ∀ p1 p2 .
  WORD32_ALIGNED p1
  ∧ WORD32_ALIGNED p2
  ∧ IN_ALPHA_RANGE_OF p2 p1
  ⇒ p1 = p2 ∨ p1 = p2 + 4w ∨ p1 = p2 + 8w ∨ p1 = p2 + 12w
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem GET_AND_SET_ON_SAME:
  ∀ p alpha .
    get_alpha_bit p (set_cppi_ram_effect p alpha) = T
Proof
  rpt strip_tac
  >> rewrite_tac [get_alpha_bit_def, set_cppi_ram_effect_def]
  >> simp [WORD_LEFT_AND_OVER_OR, ALPHA_BITMASK_NOT_ZERO]
QED

Theorem NOT_EQ_DIFF_GT4:
  ∀ p1 p2 .
  WORD32_ALIGNED p1
  ∧ CPPI_RAM_BYTE_PA p1
  ∧ WORD32_ALIGNED p2
  ∧ CPPI_RAM_BYTE_PA p2
  ∧ p1 ≠ p2
  ⇒ word_abs(p1 - p2) >= 4w
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> EVAL_TAC
  >> COND_CASES_TAC
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem WORD_DIFF_GT_16_NO_4_BIT_EQUAL:
  ∀ (p1 : 32 word) p2 n1 n2 .
  word_abs(p1 - p2) >= 16w
  ∧ (n1 = 0w ∨ n1 = 4w ∨ n1 = 8w ∨ n1 = 12w)
  ∧ (n2 = 0w ∨ n2 = 4w ∨ n2 = 8w ∨ n2 = 12w)
  ⇒ p1 + n1 ≠ p2 + n2
Proof
  NTAC 4 strip_tac
  >> UNDISCH_ALL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BD_PTR_ADDRESS_BYTE_LEMMAS:
  ∀ p n .
  BD_PTR_ADDRESS p
  ∧ (n = 0w ∨ n = 4w ∨ n = 8w ∨ n = 12w)
  ⇒ WORD32_ALIGNED (p + n)
  ∧ CPPI_RAM_BYTE_PA (p + n)
  ∧ (p + n) < (CPPI_RAM_END − 3w)
Proof
  rpt strip_tac
  >> UNDISCH_ALL_TAC
  >> EVAL_TAC
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem BD_PTR_ADDRESS_DIFF_GT_16_ALL_BYTE_ADDR_DIFF_GT_4:
  ∀ p1 p2 n1 n2 .
  BD_PTR_ADDRESS p1
  ∧ BD_PTR_ADDRESS p2
  ∧ word_abs(p1 - p2) >= 16w
  ∧ (n1 = 0w ∨ n1 = 4w ∨ n1 = 8w ∨ n1 = 12w)
  ∧ (n2 = 0w ∨ n2 = 4w ∨ n2 = 8w ∨ n2 = 12w)
  ⇒ word_abs((p1 + n1) - (p2 + n2)) >= 4w
Proof
  NTAC 4 strip_tac
  >> METIS_TAC [WORD_DIFF_GT_16_NO_4_BIT_EQUAL, NOT_EQ_DIFF_GT4, BD_PTR_ADDRESS_BYTE_LEMMAS]
QED

Theorem MEM_IN_QUEUE_RANGE:
  ∀ bd_ptr queue .
    MEM bd_ptr queue
    ∧ EVERY BD_PTR_ADDRESS queue
    ⇒ IN_QUEUE_RANGE bd_ptr queue
Proof
  Induct_on ‘queue’
  >- (simp [])
  >> rpt strip_tac
  >> Cases_on ‘bd_ptr = h’
  >- ( pat_x_rewrite_all ‘bd_ptr = h’
       >> fs []
       >> simp [IN_QUEUE_RANGE_def]
       >> rewrite_tac [IN_ALPHA_RANGE_OF_def]
       >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS h’
       >> rewrite_tac [EVAL “BD_PTR_ADDRESS h”]
       >> blastLib.BBLAST_PROVE_TAC
     )
  >> fs [IN_QUEUE_RANGE_def]
QED

Theorem MEM_IN_QUEUE_RANGE_BYTES:
  ∀ bd_ptr queue .
    MEM bd_ptr queue
    ∧ EVERY BD_PTR_ADDRESS queue
    ⇒ IN_QUEUE_RANGE bd_ptr queue
    ∧ IN_QUEUE_RANGE (bd_ptr + 4w) queue
    ∧ IN_QUEUE_RANGE (bd_ptr + 8w) queue
    ∧ IN_QUEUE_RANGE (bd_ptr + 12w) queue
Proof
  Induct_on ‘queue’
  >- (simp [])
  >> NTAC 3 strip_tac
  >> Cases_on ‘bd_ptr = h’
  >- ( pat_x_rewrite_all ‘bd_ptr = h’
       >> fs []
       >> simp [IN_QUEUE_RANGE_def]
       >> rewrite_tac [IN_ALPHA_RANGE_OF_def]
       >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS h’
       >> rewrite_tac [EVAL “BD_PTR_ADDRESS h”]
       >> blastLib.BBLAST_PROVE_TAC
     )
  >> fs [IN_QUEUE_RANGE_def]
QED

Theorem NO_ALPHA_BIT_SAME_DIFF_GT_16:
  ∀ p1 p2 alpha .
    BD_PTR_ADDRESS p1 ∧ BD_PTR_ADDRESS p2
  ∧ get_alpha_bit p1 alpha
  ∧ get_alpha_bit (p1 + 4w) alpha
  ∧ get_alpha_bit (p1 + 8w) alpha
  ∧ get_alpha_bit (p1 + 12w) alpha
  ∧ ¬ get_alpha_bit p2 alpha
  ∧ ¬ get_alpha_bit (p2 + 4w) alpha
  ∧ ¬ get_alpha_bit (p2 + 8w) alpha
  ∧ ¬ get_alpha_bit (p2 + 12w) alpha
  ⇒ word_abs (p1 - p2) >= 16w
Proof
  rpt strip_tac
  >> fs[]
  >> ‘WORD32_ALIGNED p1’
      by fs [BD_PTR_ADDRESS_def]
  >> ‘WORD32_ALIGNED p2’
      by fs [BD_PTR_ADDRESS_def]
  >> Cases_on ‘IN_ALPHA_RANGE_OF p2 p1’
  >- ( ‘p1 = p2 ∨ p1 = p2 + 4w ∨ p1 = p2 + 8w ∨ p1 = p2 + 12w’
       by METIS_TAC [IN_RANGE_OF_EQS]
       >> METIS_TAC []
     )
  >> Cases_on ‘IN_ALPHA_RANGE_OF p1 p2’
  >- ( ‘p2 = p1 ∨ p2 = p1 + 4w ∨ p2 = p1 + 8w ∨ p2 = p1 + 12w’
       by METIS_TAC [IN_RANGE_OF_EQS]
       >> METIS_TAC []
     )
  >> Q.PAT_UNDISCH_TAC ‘¬ _ _ _’
  >> Q.PAT_UNDISCH_TAC ‘¬ _ _ _’
  >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS p1’
  >> Q.PAT_UNDISCH_TAC ‘BD_PTR_ADDRESS p2’
  >> rewrite_tac [EVAL “¬IN_ALPHA_RANGE_OF p2 p1”
                  , EVAL “¬IN_ALPHA_RANGE_OF p1 p2”
                  , EVAL “BD_PTR_ADDRESS p1”
                  , EVAL “BD_PTR_ADDRESS p2”
                  ]
  >> blastLib.BBLAST_PROVE_TAC
QED

Theorem GET_AND_CLEAR_ON_SAME:
  ∀ p alpha .
    get_alpha_bit p (clear_cppi_ram_effect p alpha) = F
Proof
  rpt strip_tac
  >> rewrite_tac [get_alpha_bit_def, clear_cppi_ram_effect_def]
  >> simp []
  >> once_rewrite_tac [WORD_AND_ASSOC]
  >> rewrite_tac [WORD_AND_COMP]
  >> simp [WORD_AND_COMP]
QED

Theorem SET_AND_GET_DIFFRENT_ALPHA:
  ∀ get_ptr set_ptr (alpha : 32 word -> 32 word) .
  (word_abs(get_ptr - set_ptr) >= 4w)
  ⇒ get_alpha_bit get_ptr (set_cppi_ram_effect set_ptr alpha) = get_alpha_bit get_ptr alpha
Proof
  NTAC 4 strip_tac
  >> simp [get_alpha_bit_def , set_cppi_ram_effect_def]
  >> ‘alpha_bitmask_index get_ptr < 32’
      by (fs [ BITMASK_INDEX_LESS_THEN_32 ] )
  >> ‘alpha_bitmask_index set_ptr < 32’
      by (fs [ BITMASK_INDEX_LESS_THEN_32 ] )
  >> Cases_on ‘alpha_index get_ptr = alpha_index set_ptr’
  >- (asm_rewrite_tac []
      >> ‘alpha_bitmask_index get_ptr ≠ alpha_bitmask_index set_ptr’
          by fs [BITMASK_INDEX_NOT_EQ]
      >> rewrite_tac [alpha_bitmask_def, GSYM alpha_bitmask_index_def]
      >> once_rewrite_tac [WORD_AND_COMM]
      >> specl_assume_tac
         [‘alpha (alpha_index set_ptr)’
          , ‘alpha_bitmask_index set_ptr’
          , ‘alpha_bitmask_index get_ptr’]
         WORD32_SHIFT_BIT_SET_EFFECT
      >> fs []
     )
  >> asm_rewrite_tac []
  >> METIS_TAC [WORD_AND_COMM]
QED

Theorem CLEAR_AND_GET_DIFFRENT_ALPHA:
  ∀ get_ptr set_ptr (alpha : 32 word -> 32 word) .
  (word_abs(get_ptr - set_ptr) >= 4w)
  ⇒ get_alpha_bit get_ptr (clear_cppi_ram_effect set_ptr alpha) = get_alpha_bit get_ptr alpha
Proof
  NTAC 4 strip_tac
  >> simp [get_alpha_bit_def , clear_cppi_ram_effect_def]
  >> ‘alpha_bitmask_index get_ptr < 32’
      by (fs [ BITMASK_INDEX_LESS_THEN_32 ] )
  >> ‘alpha_bitmask_index set_ptr < 32’
      by (fs [ BITMASK_INDEX_LESS_THEN_32 ] )
  >> Cases_on ‘alpha_index get_ptr = alpha_index set_ptr’
  >- (asm_rewrite_tac []
      >> ‘alpha_bitmask_index get_ptr ≠ alpha_bitmask_index set_ptr’
          by fs [BITMASK_INDEX_NOT_EQ]
      >> rewrite_tac [alpha_bitmask_def, GSYM alpha_bitmask_index_def]
      >> once_rewrite_tac [WORD_AND_COMM]
      >> specl_assume_tac
         [‘alpha (alpha_index set_ptr)’
          , ‘alpha_bitmask_index set_ptr’
          , ‘alpha_bitmask_index get_ptr’]
         WORD32_SHIFT_BIT_CLEAR_EFFECT
      >> fs []
     )
  >> asm_rewrite_tac []
  >> METIS_TAC [WORD_AND_COMM]
QED

Theorem SET_ACTIVE_CPPI_RAM_EFFECT:
  ∀ state bd_ptr .
  SET_ACTIVE_CPPI_RAM bd_ptr state =
  ( ()
  , state with monitor := state.monitor with alpha := set_cppi_ram_effect bd_ptr state.monitor.alpha)
Proof
  EVAL_TAC >> blastLib.FULL_BBLAST_TAC
QED

Theorem CLEAR_ACTIVE_CPPI_RAM_EFFECT:
  ∀ state bd_ptr .
  (CLEAR_ACTIVE_CPPI_RAM bd_ptr state) =
  ( ()
    , state with monitor :=
            state.monitor with alpha :=
                  clear_cppi_ram_effect bd_ptr state.monitor.alpha)
Proof
  EVAL_TAC >> blastLib.FULL_BBLAST_TAC
QED

Theorem SET_CPPI_RAM_IS_CPPI_RAM_TRUE:
  ∀ state addr .
     FST ((do SET_ACTIVE_CPPI_RAM addr ; IS_ACTIVE_CPPI_RAM addr od) state) = T
Proof
  NTAC 2 strip_tac
  >> EVAL_TAC
  >> Ho_Rewrite.REWRITE_TAC [ A_BOR_B_BAND_B_EQ_B ]
  >> assume_tac (SPEC “((addr :word32) + (3052396544w :word32)) ≫ (2 :num)” X_AND_Y_LT_Y)
  >> ‘w2n (((addr :word32) + (3052396544w :word32)) ≫ (2 :num) && (31w :word32)) < 32’
      by  (once_rewrite_tac [ SYM (EVAL “w2n ((32w : 32 word))”) ]
           >> once_rewrite_tac [ SYM (SPECL [ “((addr : 32 word) + (3052396544w : 32 word)) ≫ (2 :num) && (31w : 32 word)”
                 , “32w : 32 word”]
                                      (INST_TYPE [ “:'a” |-> “: 32”]  WORD_LO )) ]
           >> asm_rewrite_tac []
           >> blastLib.FULL_BBLAST_TAC
           )
  >> IMP_RES_TAC (SPEC “w2n (((addr :word32) + (3052396544w :word32)) ≫ (2 :num) && (31w :word32))” SHIFTL_1_NOT_0)
QED

Theorem CLEAR_ACTIVE_CPPI_RAM_GIVES_FALSE:
  ∀ state addr .
  FST ((do CLEAR_ACTIVE_CPPI_RAM addr ; IS_ACTIVE_CPPI_RAM addr od) state) = F
Proof
  NTAC 2 strip_tac
  >> EVAL_TAC
  >> Ho_Rewrite.REWRITE_TAC [ WORD_AND_ASSOC ]
  >> Ho_Rewrite.ONCE_REWRITE_TAC [ WORD_AND_COMM ]
  >> ‘(¬((1w :word32) ≪
    w2n ((addr + (3052396544w :word32)) ≫ (2 :num) && (31w :word32))) &&
   (1w :word32) ≪
   w2n ((addr + (3052396544w :word32)) ≫ (2 :num) && (31w :word32)) =
   (0w :word32))’ by Ho_Rewrite.REWRITE_TAC [ WORD_AND_COMM , WORD_AND_COMP ]
  >> asm_rewrite_tac []
  >> fs []
  >> WORD_DECIDE_TAC
QED


Definition update_alpha_effect_def:
  update_alpha_effect bd_ptr b alpha =
    (if b
     then (set_cppi_ram_effect (bd_ptr + 12w)
     (set_cppi_ram_effect (bd_ptr + 8w)
      (set_cppi_ram_effect (bd_ptr + 4w)
       (set_cppi_ram_effect bd_ptr alpha))))
     else (clear_cppi_ram_effect (bd_ptr + 12w)
     (clear_cppi_ram_effect (bd_ptr + 8w)
      (clear_cppi_ram_effect (bd_ptr + 4w)
       (clear_cppi_ram_effect bd_ptr alpha))))
    )
End


Theorem UPDATE_ALPHA_EFFECT_NOT_IN_RANGE_EQ_OLD:
  ∀ p1 p2 add' alpha .
  CPPI_RAM_BYTE_PA p1 ∧ WORD32_ALIGNED p1
  ∧ BD_PTR_ADDRESS p2
  ∧ ¬ IN_ALPHA_RANGE_OF p2 p1
  ⇒ get_alpha_bit p1 (update_alpha_effect p2 add' alpha)
  = get_alpha_bit p1 alpha
Proof
  rpt strip_tac
  >> simp [update_alpha_effect_def]
  >> ‘word_abs(p1 - p2) >= 4w’
      by METIS_TAC [BD_PTR_ADDRESS_WORD_ABS]
  >> ‘word_abs(p1 - (p2 + 4w)) >= 4w’
      by METIS_TAC [BD_PTR_ADDRESS_PLUS_N_WORD_ABS]
  >> ‘word_abs(p1 - (p2 + 8w)) >= 4w’
      by METIS_TAC [BD_PTR_ADDRESS_PLUS_N_WORD_ABS]
  >> ‘word_abs(p1 - (p2 + 12w)) >= 4w’
      by METIS_TAC [BD_PTR_ADDRESS_PLUS_N_WORD_ABS]
  >> Cases_on ‘add'’
  >- ( simp []
       >> METIS_TAC  [SET_AND_GET_DIFFRENT_ALPHA]
     )
  >> simp []
  >> METIS_TAC  [CLEAR_AND_GET_DIFFRENT_ALPHA]
QED



Theorem UPDATE_ALPHA_EFFECT_IN_RANGE_EQ_NEW:
  ∀ p1 p2 add' alpha .
  (CPPI_RAM_BYTE_PA p1 ∧ WORD32_ALIGNED p1)
  ∧ BD_PTR_ADDRESS p2
  ∧ IN_ALPHA_RANGE_OF p2 p1
  ⇒ get_alpha_bit p1 (update_alpha_effect p2 add' alpha)
  = add'
Proof
  rpt strip_tac
  >> simp [update_alpha_effect_def]
  >> ‘CPPI_RAM_BYTE_PA (p2 + 12w)
      ∧ CPPI_RAM_BYTE_PA (p2 + 8w)
      ∧ CPPI_RAM_BYTE_PA (p2 + 4w)
      ∧ CPPI_RAM_BYTE_PA (p2)
      ∧ WORD32_ALIGNED (p2 + 12w)
      ∧ WORD32_ALIGNED (p2 + 8w)
      ∧ WORD32_ALIGNED (p2 + 4w)
      ∧ WORD32_ALIGNED (p2)’
      by (METIS_TAC [BD_PTR_ADDRESS_BYTE_LEMMAS, WORD_ADD_0])
  >> Cases_on ‘add'’
  >> (simp []
  >> Cases_on ‘p1 = p2 + 12w’
  >- ( METIS_TAC [GET_AND_SET_ON_SAME, GET_AND_CLEAR_ON_SAME])
  >> ‘word_abs(p1 - (p2 + 12w)) >= 4w’
      by (METIS_TAC [ BD_PTR_ADDRESS_def, NOT_EQ_DIFF_GT4])
  >> simp [SET_AND_GET_DIFFRENT_ALPHA , CLEAR_AND_GET_DIFFRENT_ALPHA]
  >> Cases_on ‘p1 = p2 + 8w’
  >- ( METIS_TAC [GET_AND_SET_ON_SAME, GET_AND_CLEAR_ON_SAME])
  >> ‘word_abs(p1 - (p2 + 8w)) >= 4w’
      by METIS_TAC [ BD_PTR_ADDRESS_def, NOT_EQ_DIFF_GT4]
  >> simp [SET_AND_GET_DIFFRENT_ALPHA , CLEAR_AND_GET_DIFFRENT_ALPHA]
  >> Cases_on ‘p1 = p2 + 4w’
  >- ( METIS_TAC [GET_AND_SET_ON_SAME, GET_AND_CLEAR_ON_SAME])
  >> ‘word_abs(p1 - (p2 + 4w)) >= 4w’
      by METIS_TAC [ BD_PTR_ADDRESS_def, NOT_EQ_DIFF_GT4]
  >> simp [SET_AND_GET_DIFFRENT_ALPHA , CLEAR_AND_GET_DIFFRENT_ALPHA]
  >> Cases_on ‘p1 = p2’
  >- ( METIS_TAC [GET_AND_SET_ON_SAME, GET_AND_CLEAR_ON_SAME])
  >> ‘word_abs(p1 - (p2)) >= 4w’
      by METIS_TAC [ BD_PTR_ADDRESS_def, NOT_EQ_DIFF_GT4]
  >> METIS_TAC [IN_RANGE_OF_EQS, BD_PTR_ADDRESS_def])
QED

Theorem UPDATE_ALPHA_EFFECT_COMM:
  ∀ p1 p2 b alpha .
  update_alpha_effect p1 b (update_alpha_effect p2 b alpha)
  = update_alpha_effect p2 b (update_alpha_effect p1 b alpha)
Proof
  rpt strip_tac
  >> simp [update_alpha_effect_def]
  >> METIS_TAC [SET_CPPI_RAM_EFFECT_COMM
                , CLEAR_CPPI_RAM_EFFECT_COMM]
QED

Theorem UPDATE_ALPHA_EFFECT:
  ∀ state bd_ptr b .
  update_alpha bd_ptr b state =
    ( ()
    , state with monitor :=
            state.monitor with alpha :=
            update_alpha_effect bd_ptr b state.monitor.alpha
    )
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [update_alpha_def , update_alpha_effect_def]
  >> Cases_on ‘b’
  >> rewrite_tac [IGNORE_BIND_DEF , BIND_DEF , UNCURRY_DEF , o_DEF]
  >> rewrite_tac [SET_ACTIVE_CPPI_RAM_EFFECT , CLEAR_ACTIVE_CPPI_RAM_EFFECT]
  >> simp []
  >> rewrite_tac [ UNIT_DEF ]
  >> simp []
QED


Theorem FOLD_UPDATE_ALPHA_CLEAR_LEFT_BACKWARDS:
  ∀ h q alpha .
    update_alpha_effect h REMOVE
    (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
     alpha q)
    =
    FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
    alpha (q ⧺ [h])
Proof
  Induct_on ‘q’
  >- simp []
  >> simp []
QED

Theorem FOLD_UPDATE_ALPHA_CLEAR_MID_TO_FRONT:
  ∀ q q' h alpha .
  (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha') alpha (q ++ [h] ++ q'))
  = (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha') alpha ([h] ++ q ++ q'))
Proof
  Induct_on ‘q’
  >- (simp [])
  >> rpt strip_tac
  >> simp []
  >> ‘(update_alpha_effect h' REMOVE (update_alpha_effect h REMOVE alpha))
      = (update_alpha_effect h REMOVE (update_alpha_effect h' REMOVE alpha))’
      by (fs [REMOVE_def]
          >> METIS_TAC [UPDATE_ALPHA_EFFECT_COMM]
         )
  >> simp []
QED

Theorem FOLDL_UPDATE_ALPHA_IS_FOLDL:
  ∀ h t alpha .
  (update_alpha_effect h REMOVE
                 (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
                  alpha t))
  = (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
     alpha (h :: t))
Proof
  Induct_on ‘t’
  >- simp []
  >> rpt strip_tac
  >> simp []
  >> simp [REMOVE_def , UPDATE_ALPHA_EFFECT_COMM]
QED

Theorem FOLDL_UPDATE_ALPHA_APPEND_DIST:
  ∀ q q' alpha .
    FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha') alpha (q ⧺ q')
    = FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha')
    (FOLDL (λalpha' p. update_alpha_effect p REMOVE alpha') alpha q) q'
Proof
  Induct
  >- simp []
  >> rpt strip_tac
  >> simp []
QED

Definition state_update_alpha_effect_def:
  state_update_alpha_effect b bd_ptr state =
    state with
        monitor := state.monitor with
            alpha := update_alpha_effect bd_ptr b state.monitor.alpha
End

Theorem UPDATE_ALPHA_QUEUE_UNFOLD_1:
  ∀ loop_n bd_ptr state add' .
  ¬ state.nic.dead
  ∧ BD_PTR_ADDRESS bd_ptr
  ⇒ update_alpha_queue_loop (SUC loop_n) bd_ptr add' state =
    update_alpha_queue_loop loop_n (read_ndp bd_ptr state.nic.regs.CPPI_RAM) add' (state_update_alpha_effect add' bd_ptr state)
Proof
  NTAC 5 strip_tac
  >> ‘CPPI_RAM_BYTE_PA bd_ptr ∧ WORD32_ALIGNED bd_ptr’
      by fs [BD_PTR_ADDRESS_def]
  >> ‘bd_ptr ≠ 0w’
      by (UNDISCH_TAC “BD_PTR_ADDRESS bd_ptr”
          >> EVAL_TAC
          >> WORD_DECIDE_TAC)
  >> once_rewrite_tac [ update_alpha_queue_loop_def ]
  >> simp_st []
  >> Ho_Rewrite.REWRITE_TAC [ UPDATE_ALPHA_EFFECT ]
  >> simp_st []
  >> simp [GET_NEXT_DESCRIPTOR_POINTER_EQ_READ_NDP_X_STATE]
  >> simp [state_update_alpha_effect_def]
QED


Theorem WELL_FORMED_BD_QUEUE_STATE_UPD_ALPHA_EFFECT_INDEPENDANT:
  ∀ bd_queue bd_ptr state bd_ptr' add' .
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr (state_update_alpha_effect add' bd_ptr' state) =
    WELL_FORMED_BD_QUEUE bd_queue bd_ptr state
Proof
  rewrite_tac [ state_update_alpha_effect_def ]
  >> Ho_Rewrite.REWRITE_TAC [ WELL_FORMED_BD_QUEUE_ALPHA_INDEPENDANT]
QED

Theorem UPDATE_ALPHA_QUEUE_EQ_FOLDL:
  ∀ bd_queue add' bd_ptr state n .
  ¬ state.nic.dead
  ∧ WELL_FORMED_BD_QUEUE bd_queue bd_ptr state
  ∧ LENGTH bd_queue <= n
  ⇒
  update_alpha_queue_loop n bd_ptr add' state =
  ( ()
    ,  FOLDL (λ state' bd_ptr'  . state_update_alpha_effect add' bd_ptr' state') state bd_queue)
Proof
  Induct_on ‘n’
  >- (rpt strip_tac
      >> Cases_on ‘bd_queue'’
      >> fs_st [ WELL_FORMED_BD_QUEUE_def , bd_queueTheory.BD_QUEUE_def
               , update_alpha_queue_loop_def
               , update_alpha_queue_loop_def
             ]
     )
  >> rpt strip_tac
  >> Cases_on ‘bd_queue'’
  >- (fs_st [ WELL_FORMED_BD_QUEUE_def , bd_queueTheory.BD_QUEUE_def
               , update_alpha_queue_loop_def
               , update_alpha_queue_loop_def
             ])
  >> ‘LENGTH t <= n’ by fs []
  >> asm_rewrite_tac []
  >> ‘h = bd_ptr’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_HEAD_EQ_START]
  >> pat_x_rewrite_all ‘h = bd_ptr’
  >> ‘WELL_FORMED_BD_QUEUE t (read_ndp bd_ptr state.nic.regs.CPPI_RAM) state’
      by METIS_TAC [WELL_FORMED_BD_QUEUE_TAIL ]
  >> ‘bd_ptr ≠ 0w’
      by (fs [WELL_FORMED_BD_QUEUE_def , bd_queueTheory.BD_QUEUE_def])
  >> ‘BD_PTR_ADDRESS bd_ptr’
      by (fs [  WELL_FORMED_BD_QUEUE_def ])
  >> simp [UPDATE_ALPHA_QUEUE_UNFOLD_1]
  >> ‘update_alpha_queue_loop n (read_ndp bd_ptr state.nic.regs.CPPI_RAM)
          add' (state_update_alpha_effect add' bd_ptr state) =
        ((),
         FOLDL
           (λstate' bd_ptr'. state_update_alpha_effect add' bd_ptr' state')
           (state_update_alpha_effect add' bd_ptr state) t)’
        by  (Q.PAT_X_ASSUM ‘∀ add' bd_ptr state . _’
                 (specl_assume_tac
                  [‘t’ , ‘add'’, ‘(read_ndp bd_ptr state.nic.regs.CPPI_RAM)’
                   , ‘(state_update_alpha_effect add' bd_ptr state)’
                  ])
             >> Q.PAT_UNDISCH_TAC ‘_⇒_’
             >> simp []
             >> ‘¬(state_update_alpha_effect add' bd_ptr state).nic.dead’ by
                 fs [state_update_alpha_effect_def]
             >> simp []
             >> METIS_TAC [WELL_FORMED_BD_QUEUE_STATE_UPD_ALPHA_EFFECT_INDEPENDANT
                           ]
            )
QED

Theorem FOLD_STATE_UPDATE_ALPHA_EFFECT_EQ_ALPHA_FOLD:
    ∀ bd_queue add' state .
    FOLDL (λ state' bd_ptr' . state_update_alpha_effect add' bd_ptr' state') state bd_queue
    = (state with
         monitor :=
           state.monitor with
           alpha := FOLDL (λ state' bd_ptr' . update_alpha_effect bd_ptr' add' state') state.monitor.alpha bd_queue)
Proof
  Induct
  >- fs [ system_state_component_equality
          , nic_monitor_state_component_equality ]
  >> strip_tac
  >> once_rewrite_tac [ FOLDL ]
  >> PAT_ASSUM “_” (fn t => Ho_Rewrite.REWRITE_TAC [t])
  >> fs [state_update_alpha_effect_def]
QED


Theorem UPDATE_ALPHA_EFFECT_FOLDR_MOVE:
  ∀ p queue add' alpha .
  FOLDR (λ bd_ptr' state' . update_alpha_effect bd_ptr' add' state')
        (update_alpha_effect p add' alpha)
        queue
  =
  FOLDR (λ bd_ptr' state' . update_alpha_effect bd_ptr' add' state')
        alpha
        (p::queue)
Proof
  Induct_on ‘queue’
  >- (simp [])
  >> rpt strip_tac
  >> simp [UPDATE_ALPHA_EFFECT_COMM]
QED

Theorem UPDATE_ALPHA_EFFECT_FOLDL_EQ_FOLDR:
  ∀ alpha add' queue .
  FOLDL (λ state' bd_ptr' . update_alpha_effect bd_ptr' add' state') alpha queue
  = FOLDR (λ bd_ptr' state' . update_alpha_effect bd_ptr' add' state') alpha queue
Proof
  Induct_on ‘queue’
  >- (simp [])
  >> rpt strip_tac
  >> ‘FOLDL (λstate' bd_ptr'. update_alpha_effect bd_ptr' add' state')
      alpha queue
      = FOLDR (λ bd_ptr' state'. update_alpha_effect bd_ptr' add' state')
      alpha queue’
      by METIS_TAC []
  >> simp [UPDATE_ALPHA_EFFECT_FOLDR_MOVE]
QED

(* Main use of this theorem is to get rid of the update_alpha term *)
Theorem UPDATE_ALPHA_QUEUE_EFFECT_FOLDL:
  ∀ state queue_start add' bd_queue .
  ((¬ state.nic.dead)
   ∧ (WELL_FORMED_BD_QUEUE bd_queue queue_start state))
   ⇒(update_alpha_queue queue_start add' state
      =
      ( ()
      , (state with
         monitor :=
           state.monitor with
           alpha := FOLDL (λ state' bd_ptr' . update_alpha_effect bd_ptr' add' state') state.monitor.alpha bd_queue))
     )
Proof
  NTAC 5 strip_tac
  >> simp []
  >> rewrite_tac [update_alpha_queue_def]
  >> ‘MAX_QUEUE_LENGTH_NUM >= LENGTH bd_queue'’
      by fs [WELL_FORMED_BD_QUEUE_def , MAX_QUEUE_LENGTH_NUM_def ]
  >> ‘MAX_QUEUE_LENGTH_NUM ≠ 0’ by fs [ MAX_QUEUE_LENGTH_NUM_def ]
  >> Q.SPECL_THEN [‘bd_queue'’
              , ‘add'’
              , ‘queue_start’
               , ‘state’
              , ‘MAX_QUEUE_LENGTH_NUM’
              ]
             assume_tac
             UPDATE_ALPHA_QUEUE_EQ_FOLDL
  >> ‘MAX_QUEUE_LENGTH_NUM > 0’ by EVAL_TAC
  >> fs []
  >> Ho_Rewrite.REWRITE_TAC [FOLD_STATE_UPDATE_ALPHA_EFFECT_EQ_ALPHA_FOLD]
QED

Theorem UPDATE_ALPHA_QUEUE_EFFECT:
  ∀ state queue_start add' bd_queue .
  ((¬ state.nic.dead)
   ∧ (WELL_FORMED_BD_QUEUE bd_queue queue_start state))
   ⇒(update_alpha_queue queue_start add' state
      =
      ( ()
      , (state with
         monitor :=
           state.monitor with
           alpha := FOLDR (λ bd_ptr' state' . update_alpha_effect bd_ptr' add' state') state.monitor.alpha bd_queue))
     )
Proof
  NTAC 5 strip_tac
  >> METIS_TAC [UPDATE_ALPHA_QUEUE_EFFECT_FOLDL
                , UPDATE_ALPHA_EFFECT_FOLDL_EQ_FOLDR]
QED

Theorem NOT_IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_OLD_VALUE:
  ∀ p add' queue alpha .
  ¬IN_QUEUE_RANGE p queue
  ∧ (CPPI_RAM_BYTE_PA p ∧ WORD32_ALIGNED p)
  ∧ EVERY BD_PTR_ADDRESS queue
  ⇒ (get_alpha_bit p (FOLDR (λ bd_ptr' alpha' . update_alpha_effect bd_ptr' add' alpha') alpha queue)
      = get_alpha_bit p alpha)
Proof
  Induct_on ‘queue’
  >- simp [IN_QUEUE_RANGE_def]
  >> rpt strip_tac
  >> fs [IN_QUEUE_RANGE_def]
  >> simp [UPDATE_ALPHA_EFFECT_NOT_IN_RANGE_EQ_OLD]
QED

Theorem IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_EFFECT_EQ_FORAL_MEM_B:
  ∀ p add' queue alpha .
  IN_QUEUE_RANGE p queue
  ∧ EVERY BD_PTR_ADDRESS queue
  ∧ (CPPI_RAM_BYTE_PA p ∧ WORD32_ALIGNED p)
  ⇒ (get_alpha_bit p (FOLDR (λ bd_ptr' alpha' . update_alpha_effect bd_ptr' add' alpha') alpha queue)
      = add')
Proof
  Induct_on ‘queue’
  >- simp [IN_QUEUE_RANGE_def]
  >> rpt strip_tac
  >> Cases_on ‘IN_ALPHA_RANGE_OF h p’
  >- (simp []
      >> METIS_TAC [UPDATE_ALPHA_EFFECT_IN_RANGE_EQ_NEW,EVERY_DEF]
     )
  >> fs [IN_QUEUE_RANGE_def]
  >> simp [UPDATE_ALPHA_EFFECT_NOT_IN_RANGE_EQ_OLD]
QED

Theorem UPDATE_ALPHA_REMOVE_ALL_NOTHING_ALPHA_MARKED:
  ∀ tx0_queue rx0_queue state .
    WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
    ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
    ∧ BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha
    ⇒ BD_QUEUE_ALPHA_MARKED [] []
             (FOLDR
                 (λ bd_ptr' state'. update_alpha_effect bd_ptr' REMOVE state')
                 state.monitor.alpha (tx0_queue ⧺ rx0_queue))

Proof
  rpt strip_tac
  >> fs [BD_QUEUE_ALPHA_MARKED_def]
  >> strip_tac
  >> rewrite_tac [IN_QUEUE_RANGE_def, ALPHA_QUEUE_MARKED_def]
  >> Q.ABBREV_TAC ‘alpha' = ((FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' REMOVE state')
                              state.monitor.alpha (tx0_queue ⧺ rx0_queue)) : 32 word -> 32 word)’
  >> ‘((alpha' (alpha_index bd_ptr))  && alpha_bitmask bd_ptr = 0w)
      = ¬get_alpha_bit bd_ptr alpha'’
      by rewrite_tac  [ get_alpha_bit_def]
  >> pat_x_rewrite_all ‘_=_’
  >> Q.UNABBREV_TAC ‘alpha'’
  >> ‘EVERY BD_PTR_ADDRESS (tx0_queue ⧺ rx0_queue)’
      by( fs [WELL_FORMED_BD_QUEUE_def]
          >> METIS_TAC [ GSYM EVERY_APPEND]
        )
  >> strip_tac
  >> Cases_on ‘IN_QUEUE_RANGE bd_ptr (tx0_queue ⧺ rx0_queue)’
  >-  ( METIS_TAC [IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_EFFECT_EQ_FORAL_MEM_B
                   , REMOVE_def])
  >> simp [NOT_IN_QUEUE_RANGE_UPDATE_ALPHA_QUEUE_OLD_VALUE]
  >> Q.PAT_X_ASSUM ‘∀ bd_ptr . _’ (specl_assume_tac [‘bd_ptr’])
  >> fs [ALPHA_QUEUE_MARKED_def]
  >> simp [get_alpha_bit_def]
  >> METIS_TAC []
QED

Theorem FOLDR_UPDATE_ALPHA_EFFECT_APPEND_DISTR:
  ∀ q1 q2 alpha add .
    FOLDR
    (λbd_ptr' state'.
     update_alpha_effect bd_ptr' add state')
    (FOLDR
     (λbd_ptr' state'.
      update_alpha_effect bd_ptr' add state')
     alpha q2) q1
  = FOLDR (λbd_ptr' state'.
           update_alpha_effect bd_ptr' add state')
  alpha (q1 ++ q2)
Proof
  simp [rich_listTheory.FOLDR_APPEND]
QED

Theorem FOLDR_UPDATE_ALPHA_APPEND_MOVE:
  ∀ h q1 q2 alpha add .
    FOLDR (λbd_ptr' state'.
           update_alpha_effect bd_ptr' add state')
           alpha (h::q1 ++ q2)
    =
    FOLDR (λbd_ptr' state'.
          update_alpha_effect bd_ptr' add state')
          alpha (q1 ++ h::q2)
Proof
  Induct_on ‘q1’
  >- simp []
  >> rpt strip_tac
  >> rewrite_tac [FOLDR, APPEND]
  >> ‘(FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' add' state')
       alpha (q1 ⧺ h'::q2))
      = (FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' add' state')
         alpha (h'::q1 ⧺ q2))’
      by simp []
  >> pat_x_rewrite_all ‘_=_’
  >> Q.PAT_X_ASSUM ‘∀ h q1 alpha add ._’ (fn _=> ALL_TAC)
  >> simp [UPDATE_ALPHA_EFFECT_COMM]
QED

Theorem FOLDR_UPDATE_ALPHA_EFFECT_APPEND_COMM:
  ∀ q1 q2 alpha add .
    FOLDR (λbd_ptr' state'.
           update_alpha_effect bd_ptr' add state')
            alpha (q1 ++ q2)
    = FOLDR (λbd_ptr' state'.
             update_alpha_effect bd_ptr' add state')
             alpha (q2 ++ q1)
Proof
  Induct_on ‘q1’
  >- simp []
  >> rpt strip_tac
  >> simp []
  >> ‘FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' add' state')
      alpha (q2 ⧺ h::q1)
      = FOLDR (λbd_ptr' state'. update_alpha_effect bd_ptr' add' state')
          alpha (h::q2 ⧺ q1)’
          by METIS_TAC [FOLDR_UPDATE_ALPHA_APPEND_MOVE]
  >> pat_x_rewrite_all ‘_=_’
  >> simp []
  >> METIS_TAC []
QED


val _ = export_theory();
