open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open register_writeTheory;
open address_spaceTheory;

open nicInvariantTheory;
open qdInvariantTheory;
open itInvariantTheory;
open rdInvariantTheory;
open rxInvariantTheory;
open tdInvariantTheory;
open txInvariantTheory;

open it_state_definitionsTheory;

open itTheory;

open tx_transition_definitionsTheory
open rx_transition_definitionsTheory
open tx_bd_queueTheory;
open rx_bd_queueTheory;
open schedulerTheory;
open tx_state_definitionsTheory;
open bd_queueTheory;
open bd_listTheory;


open initializationTheory;
open monitorUtils;

open address_lemmasTheory;
open alpha_lemmasTheory;
open invariant_lemmasTheory;

val _ = new_theory "initialization_performed";

(* Doesn't exist in standard proof, should be somewhewre else but i dont care *)

Definition initailization_performed_effect_def:
  initailization_performed_effect tx0_bd_queue rx0_bd_queue state =
    (state with
         monitor :=
         state.monitor with
         <| tx0_active_queue := 0w
          ; rx0_active_queue := 0w
          ; initialized := T
          ; alpha := FOLDR (λ bd_ptr' state' . update_alpha_effect bd_ptr' REMOVE state') state.monitor.alpha (tx0_bd_queue ++ rx0_bd_queue)
          |>
       )
End

Theorem INITIALIZAITON_PERFORMED_EFFECT:
  ∀ state tx0_queue rx0_queue .
    ((¬ state.nic.dead)
     ∧ (WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state)
     ∧ (WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state))
    ⇒ (initialization_performed state =
       ( ()
       , initailization_performed_effect tx0_queue rx0_queue state
       ))
Proof
  NTAC 4 strip_tac
  >> rewrite_tac [initailization_performed_effect_def]
  >> fs []
  >> simp_st [ initialization_performed_def
               , get_tx0_active_queue_def
               , get_rx0_active_queue_def]
  >> ‘(update_alpha_queue state.monitor.tx0_active_queue REMOVE state)
      = ((),
          state with
          monitor :=
            state.monitor with
            alpha :=
              FOLDR
                (λbd_ptr' state'. update_alpha_effect bd_ptr' REMOVE state')
                state.monitor.alpha tx0_queue)’
      by METIS_TAC [UPDATE_ALPHA_QUEUE_EFFECT]
  >> pat_x_rewrite_all ‘_=_’
  >> simp []
  >> Q.ABBREV_TAC ‘state' = (state with
                             monitor :=
                             state.monitor with
                             alpha :=
                             FOLDR
                             (λbd_ptr' state'.
                              update_alpha_effect bd_ptr' REMOVE state')
                             state.monitor.alpha tx0_queue)’
  >> ‘WELL_FORMED_BD_QUEUE rx0_queue  state.monitor.rx0_active_queue state'’
      by ( ‘(state.nic.dead ⇔ state'.nic.dead)’
           by (Q.UNABBREV_TAC ‘state'’ >> fs [])
           >> ‘state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
               by (Q.UNABBREV_TAC ‘state'’ >> fs [])
           >> Q.SPECL_THEN
              [‘state.monitor.rx0_active_queue’, ‘rx0_queue’, ‘state’, ‘state'’]
              imp_res_tac
              WELL_FORMED_CPPI_RAM_DEPENDANT
           >> ‘bds = rx0_queue’ by
               METIS_TAC [WELL_FORMED_BD_QUEUE_def ,EQ_START_EQ_QUEUEs_lemma]
           >> fs [])
  >> ‘¬state'.nic.dead’
      by (Q.UNABBREV_TAC ‘state'’ >> fs [])
  >> ‘(update_alpha_queue state.monitor.rx0_active_queue REMOVE state')
      = ((),
          state' with
          monitor :=
            state'.monitor with
            alpha :=
              FOLDR
                (λbd_ptr' state'. update_alpha_effect bd_ptr' REMOVE state')
                state'.monitor.alpha rx0_queue)’
      by METIS_TAC [UPDATE_ALPHA_QUEUE_EFFECT]
  >> pat_x_rewrite_all ‘_=_’
  >> Q.UNABBREV_TAC ‘state'’
  >> simp_st [set_initialized_def , set_rx0_active_queue_def, set_tx0_active_queue_def]
  >> METIS_TAC [FOLDR_UPDATE_ALPHA_EFFECT_APPEND_COMM, FOLDR_UPDATE_ALPHA_EFFECT_APPEND_DISTR]
QED


Theorem INITIALIZATION_PERFORMED_GIVES_BD_QUEUE_INVARIANT:
  ∀ state .
  ¬ state.nic.dead
  ∧ (∃ tx0_queue rx0_queue .
        WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
     ∧  WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
     ∧  BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha
     )
  ∧ state.nic.tx.sop_bd_pa = 0w
  ∧ state.nic.rx.sop_bd_pa = 0w
  ∧ state.nic.rx.current_bd_pa = 0w
  ⇒ BD_QUEUE_INVARIANT (SND (initialization_performed state))
Proof
  rpt strip_tac
  >> ‘initialization_performed state =
      ((),initailization_performed_effect tx0_queue rx0_queue state)’
      by fs [ INITIALIZAITON_PERFORMED_EFFECT]
  >> asm_rewrite_tac [initailization_performed_effect_def]
  >> rewrite_tac [BD_QUEUE_INVARIANT_def]
  >> Q.EXISTS_TAC ‘[]’
  >> Q.EXISTS_TAC ‘[]’
  >> simp [WELL_FORMED_BD_QUEUE_def, BD_QUEUE_def
           , NON_OVERLAPPING_BD_QUEUES_def]
  >> METIS_TAC [UPDATE_ALPHA_REMOVE_ALL_NOTHING_ALPHA_MARKED]
QED


Theorem INITIALIZATION_PERFORMED_EFFECT_GIVES_BD_QUEUE_INVARIANT:
  ∀ state tx0_queue rx0_queue .
  ¬ state.nic.dead
  ∧ WELL_FORMED_BD_QUEUE tx0_queue state.monitor.tx0_active_queue state
  ∧ WELL_FORMED_BD_QUEUE rx0_queue state.monitor.rx0_active_queue state
  ∧ BD_QUEUE_ALPHA_MARKED tx0_queue rx0_queue state.monitor.alpha
  ∧ state.nic.tx.sop_bd_pa = 0w
  ∧ state.nic.rx.sop_bd_pa = 0w
  ∧ state.nic.rx.current_bd_pa = 0w
  ⇒ BD_QUEUE_INVARIANT (initailization_performed_effect tx0_queue rx0_queue state)
Proof
  rpt strip_tac
  >> ‘(initailization_performed_effect tx0_queue rx0_queue state)
      = SND (initialization_performed state)’
      by ( ‘initialization_performed state =
           ((),initailization_performed_effect tx0_queue rx0_queue state)’
           by fs [ INITIALIZAITON_PERFORMED_EFFECT ]
           >> asm_rewrite_tac []
         )
  >> asm_rewrite_tac []
  >> METIS_TAC [INITIALIZATION_PERFORMED_GIVES_BD_QUEUE_INVARIANT]
QED

val _ = export_theory();
