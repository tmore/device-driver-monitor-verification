open HolKernel wordsLib listTheory boolLib bossLib stateTheory;
open wordsTheory;
open monitor_stateTheory monitor_invariantTheory handlersTheory;

open helperTactics;

open register_writeTheory;
open address_spaceTheory;

open invariant_lemmasTheory;
open address_lemmasTheory;
open alpha_lemmasTheory;

open monitorUtils;

val _ = new_theory "cpdma_soft_reset_handler_proof";



Theorem CPDMA_SOFT_RESET_HANDLER_PRESERVES_INV_lem:
   ∀ state v .
     MONITOR_INVARIANT state
     ⇒ (MONITOR_INVARIANT (SND (cpdma_soft_reset_handler v state)))
Proof
  NTAC 3 strip_tac
  >> rewrite_tac [ cpdma_soft_reset_handler_def ]
  >> Cases_on ‘v && 1w = 0w’
  >- (simp_st [])
  >> simp_st [ get_initialized_def
             , get_tx0_tearingdown_def
             , get_rx0_tearingdown_def
             ]
  >> Cases_on ‘¬state.monitor.initialized ∨ state.monitor.tx0_tearingdown ∨ state.monitor.rx0_tearingdown’
  >- ( Cases_on ‘¬state.monitor.initialized’
       >> Cases_on ‘state.monitor.tx0_tearingdown’
       >> Cases_on ‘state.monitor.rx0_tearingdown’
       >> fs [get_rx0_tearingdown_def])
  >> fs [get_initialized_def
         , get_tx0_tearingdown_def
         , get_rx0_tearingdown_def]
  >> simp_st [ set_initialized_def
               , set_tx0_hdp_initialized_def
               , set_rx0_hdp_initialized_def
               , set_tx0_cp_initialized_def
               , set_rx0_cp_initialized_def
               , write_nic_register_def
             ]
  >> ‘¬ state.nic.dead’ by fs [MON_INV_NIC_NOT_DEAD_lem]
  >> simp [ CPDMA_SOFT_RESET_PA_lemmas
          , register_writeTheory.write_cpdma_soft_reset_def
          ]
  >> Cases_on ‘state.nic.it.state’ >> asm_rewrite_tac [] >> simp []
  >| [ UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp []
       , UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp []
       , UNDISCH_TAC “MONITOR_INVARIANT state” >> EVAL_TAC >> simp []
       , Cases_on ‘state.nic.td.state ≠ td_idle ∨ state.nic.rd.state ≠ rd_idle’
         >| [ ‘( (state.nic.td.state ≠ td_idle) ⇒ state.monitor.tx0_tearingdown)
              ∧ ( (state.nic.rd.state ≠ rd_idle) ⇒ state.monitor.rx0_tearingdown)’ by
                   (UNDISCH_TAC “ MONITOR_INVARIANT state” >> rewrite_tac [MONITOR_INVARIANT_def] >> simp [])
              >> METIS_TAC []
              , fs [register_writeTheory.write_register_def
                    , CPDMA_SOFT_RESET_PA_lemmas
                    , register_writeTheory.write_cpdma_soft_reset_def
                    ]
                >> (Q.ABBREV_TAC ‘ state' = (state with
                                  <|monitor :=
                                      state.monitor with
                                      <|initialized := F; tx0_hdp_initialized := F;
                                        rx0_hdp_initialized := F; tx0_cp_initialized := F;
                                        rx0_cp_initialized := F|>;
                                    nic :=
                                      state.nic with
                                      <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                                        it :=
                                          <|state := it_reset; tx0_hdp_initialized := F;
                                            rx0_hdp_initialized := F; tx0_cp_initialized := F;
                                            rx0_cp_initialized := F|> |> |>)’)
                >> ‘BD_QUEUE_INVARIANT state'’
                    by (‘¬ state'.nic.dead’
                          by (Q.UNABBREV_TAC ‘state'’ >> fs [])
                        >> ‘state.nic.regs.CPPI_RAM = state'.nic.regs.CPPI_RAM’
                            by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.monitor.rx0_active_queue = state'.monitor.rx0_active_queue’
                             by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.monitor.tx0_active_queue = state'.monitor.tx0_active_queue’
                             by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.monitor.alpha = state'.monitor.alpha’
                            by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.nic.tx.sop_bd_pa = state'.nic.tx.sop_bd_pa’
                            by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.nic.rx.sop_bd_pa = state'.nic.rx.sop_bd_pa’
                            by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘state.nic.rx.current_bd_pa = state'.nic.rx.current_bd_pa’
                            by (Q.UNABBREV_TAC ‘state'’ >> EVAL_TAC)
                        >> ‘BD_QUEUE_INVARIANT state’
                            by (fs [MONITOR_INVARIANT_def])
                        >> METIS_TAC [BD_QUEUE_INVARIANT_CPPI_AND_QUEUE_DEPENDANT])

                >> UNDISCH_TAC “MONITOR_INVARIANT state”
                >> rewrite_tac [ MONITOR_INVARIANT_def ]
                >> strip_tac
                >> asm_rewrite_tac []
                >> Q.UNABBREV_TAC ‘state'’
                >> UNDISCH_TAC “NIC_INVARIANT state.nic ARB ARB”
                >> fs [ nicInvariantTheory.NIC_INVARIANT_def ]
                >> strip_tac
                >> ‘NIC_STATE_DEFINED
                       (state.nic with
                        <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                          it :=
                            <|state := it_reset; tx0_hdp_initialized := F;
                              rx0_hdp_initialized := F; tx0_cp_initialized := F;
                              rx0_cp_initialized := F|> |>)’ by
                       (EVAL_TAC >> asm_rewrite_tac [])
                >> ‘QD_INVARIANT
                        (state.nic with
                         <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                           it :=
                             <|state := it_reset; tx0_hdp_initialized := F;
                               rx0_hdp_initialized := F; tx0_cp_initialized := F;
                               rx0_cp_initialized := F|> |>)’ by
                        (UNDISCH_TAC “QD_INVARIANT state.nic” >> EVAL_TAC >> METIS_TAC [])
                >> ‘IT_INVARIANT
                       (state.nic with
                        <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                          it :=
                            <|state := it_reset; tx0_hdp_initialized := F;
                              rx0_hdp_initialized := F; tx0_cp_initialized := F;
                              rx0_cp_initialized := F|> |>)’ by
                       (UNDISCH_TAC “IT_INVARIANT state.nic” >> EVAL_TAC >> METIS_TAC [])
                >> ‘TX_INVARIANT
                       (state.nic with
                        <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                          it :=
                            <|state := it_reset; tx0_hdp_initialized := F;
                              rx0_hdp_initialized := F; tx0_cp_initialized := F;
                              rx0_cp_initialized := F|> |>) ARB ’ by
                       (UNDISCH_TAC “TX_INVARIANT state.nic ARB” >> EVAL_TAC >> METIS_TAC [])
                >> ‘RX_INVARIANT
                       (state.nic with
                        <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                          it :=
                            <|state := it_reset; tx0_hdp_initialized := F;
                              rx0_hdp_initialized := F; tx0_cp_initialized := F;
                              rx0_cp_initialized := F|> |>) ARB’ by
                       (UNDISCH_TAC “RX_INVARIANT state.nic ARB” >> EVAL_TAC >> fs [] >> METIS_TAC [])
                >> ‘TD_INVARIANT
                       (state.nic with
                        <|regs := state.nic.regs with CPDMA_SOFT_RESET := 1w;
                          it :=
                            <|state := it_reset; tx0_hdp_initialized := F;
                              rx0_hdp_initialized := F; tx0_cp_initialized := F;
                              rx0_cp_initialized := F|> |>)’ by
                       (UNDISCH_TAC “TD_INVARIANT state.nic” >> EVAL_TAC >> fs [])
                >> asm_rewrite_tac []
                >> UNDISCH_TAC “RD_INVARIANT state.nic”
                >> fs []
                >> EVAL_TAC >> fs []
            ]
     ]
QED


val _ = export_theory()
