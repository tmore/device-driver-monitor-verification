open HolKernel Parse boolLib bossLib;
open helperTactics;
open rdInvariantTheory;
open rd_transition_invariant_lemmasTheory;
open rd_transition_lemmasTheory;
open tx_invariant_lemmasTheory;
open bd_listTheory;
open txInvariantTheory;
open txInvariantMemoryReadsPreservedTheory;

val _ = new_theory "rd_preserves_tx_invariant";

val RD_AUTONOMOUS_TRANSITION_PRESERVES_TX_BD_QUEUE_CONTENTS_lemma = store_thm (
  "RD_AUTONOMOUS_TRANSITION_PRESERVES_TX_BD_QUEUE_CONTENTS_lemma",
  ``!nic env nic' READABLE.
    RD_AUTONOMOUS_TRANSITION nic env nic' /\
    RD_INVARIANT nic /\
    TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY nic /\
    TX_INVARIANT nic READABLE
    ==>
    EQ_BDs (tx_bd_queue nic) nic.regs.CPPI_RAM nic'.regs.CPPI_RAM``,
  REPEAT GEN_TAC THEN
  DISCH_TAC THEN
  SPLIT_ASM_TAC THEN
  ASM_CASES_TAC ``RD_WRITE_CURRENT_BD_PA nic`` THENL
  [
   RW_ASM_TAC ``RD_INVARIANT nic`` RD_INVARIANT_def THEN
   SPLIT_ASM_TAC THEN
   RW_ASM_TAC ``RD_INVARIANT_CURRENT_BD_PA nic`` RD_INVARIANT_CURRENT_BD_PA_def THEN
   ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL RD_AUTONOMOUS_TRANSITION_WRITE_CURRENT_BD_PA_IMP_RD_STATE_WRITE_CPPI_RAM_lemma)) THEN
   PAT_ASSUM ``P ==> Q`` (fn thm => ASSUME_TAC (CONJ_ANT_TO_HYP thm)) THEN
   SPLIT_ASM_TAC THEN
   ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL TX_INVARIANT_IMP_TX_BD_QUEUE_IN_CPPI_RAM_lemma)) THEN
   ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL rd_transition_PRESERVES_TX_BD_QUEUE_CONTENTS_lemma)) THEN
   ASSUME_TAC (UNDISCH (SPEC_ALL RD_AUTONOMOUS_TRANSITION_IMP_RD_TRANSITION_lemma)) THEN
   ASM_REWRITE_TAC []
   ,
   ASSUME_TAC (UNDISCH (SPEC_ALL rd_transition_CPPI_RAM_EQ_lemma)) THEN
   RW_ASM_TAC ``CPPI_RAM_EQ nic nic'`` CPPI_RAM_EQ_def THEN
   ASSUME_TAC (UNDISCH (SPEC_ALL RD_AUTONOMOUS_TRANSITION_IMP_RD_TRANSITION_lemma)) THEN
   ASM_REWRITE_TAC [EQ_BDs_SYM_lemma]
  ]);

val RD_AUTONOMOUS_TRANSITION_PRESERVES_TX_INVARIANT_lemma = store_thm (
  "RD_AUTONOMOUS_TRANSITION_PRESERVES_TX_INVARIANT_lemma",
  ``!nic env nic' READABLE.
    RD_AUTONOMOUS_TRANSITION nic env nic' /\
    RD_INVARIANT nic /\
    TX_INVARIANT nic READABLE
    ==>
    TX_INVARIANT nic' READABLE``,
  REPEAT GEN_TAC THEN
  DISCH_TAC THEN
  SPLIT_ASM_TAC THEN
  ASSUME_TAC (UNDISCH (SPEC_ALL RD_AUTONOMOUS_TRANSITION_IMP_RD_TRANSITION_lemma)) THEN
  ASSUME_TAC (UNDISCH (SPEC_ALL RD_AUTONOMOUS_TRANSITION_IMP_RD_STATE_NOT_IDLE_lemma)) THEN
  ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL rd_transition_RD_STATE_NOT_IDLE_PRESERVES_DEAD_lemma)) THEN
  ASSUME_TAC (REWRITE_RULE [TX_EQ_def] (SPEC_ALL rd_transition_TX_EQ_lemma)) THEN
  REFLECT_ASM_TAC ``nic' = rd_transition env nic`` THEN
  ASM_RW_ASM_TAC ``rd_transition env nic = nic'`` ``nic.tx = (rd_transition env nic).tx`` THEN
  REFLECT_ASM_TAC ``(nic : nic_state).tx = nic'.tx`` THEN
  REWRITE_TAC [TX_INVARIANT_def] THEN
  DISCH_TAC THEN
  ASSUME_TAC (UNDISCH (SPEC_ALL RD_AUTONOMOUS_TRANSITION_IMP_RD_TRANSITION_lemma)) THEN
  ASSUME_TAC (UNDISCH (SPEC_ALL rd_transition_EQ_TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY_lemma)) THEN
  ASM_RW_ASM_TAC ``f nic' = f nic`` ``f nic'`` THEN
  ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL RD_AUTONOMOUS_TRANSITION_PRESERVES_TX_BD_QUEUE_CONTENTS_lemma)) THEN
  ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL TX_STATE_AUTONOMOUS_TRANSITION_ENABLE_OR_PROCESS_MEMORY_READ_REPLY_TX_INVARIANT_IMP_TX_INVARIANT_MEMORY_lemma)) THEN
  ASSUME_TAC (CONJ_ANT_TO_HYP (SPEC_ALL TX_INVARIANT_MEMORY_PRESERVED_lemma)) THEN
  ASM_REWRITE_TAC []);

val _ = export_theory();

