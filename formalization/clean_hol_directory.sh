#!/bin/bash
Holmake -r clean;
find -type d -name ".hollogs" -prune -exec rm -rf {} \;
find -type d -name ".HOLMK" -prune -exec rm -rf {} \;
find -type d -name "*Theory.*" -prune -exec rm -rf {} \;
find . -name "*~" -type f -delete;
