\documentclass[conference]{IEEEtran}

\usepackage{tikz}
\usepackage{amsthm,amsmath}

%-----
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{centernot}
\usepackage{amssymb}
\usepackage{amscd}
\usepackage{float}
\usepackage{alltt}
\usepackage{url}
\usepackage{balance}
\usepackage{paralist}
\usepackage{mathtools}
\usepackage{listings}
\usepackage{transparent}
\usepackage{textcomp}
\usetikzlibrary{arrows}
\usepackage{paralist}
\usepackage{soul}
\usepackage{multirow}
\usepackage{listings}
\usepackage{tcolorbox}
\lstset{
  basicstyle=\ttfamily,
  mathescape
}

\usepackage[all,cmtip]{xy}
\usetikzlibrary{positioning}


\usepackage[disable,colorinlistoftodos,prependcaption,textsize=tiny, textwidth=3.7cm]{todonotes}
\usepackage{regexpatch}
%\tracingxpatches%for debugging

\allowdisplaybreaks

\makeatletter
\xpatchcmd{\@todo}{\setkeys{todonotes}{#1}}{\setkeys{todonotes}{inline,#1}}{}{}
\makeatother


\begin{document}
\title{Trustworthy Isolation of a Network Interface Controller}
\author{\IEEEauthorblockN{Jonas Haglund}
\IEEEauthorblockA{KTH Royal Institute of Technology\\
Stockholm, Sweden\\
Email: jhagl@kth.se}
}

\maketitle




\setlength{\jot}{1pt}

Formally verified execution platforms (microkernels, hypervisors and separation
kernels) provide the basic infrastructure to implement secure IoT devices. By
guaranteeing memory isolation and controlling communication between software
components, they prevent faults of non-critical software (like HTTP interfaces,
optimizations based on machine learning, software providing complex
functionality and with short life cycle) from affecting the components that must
support strict security and safety requirements. This enables verification or
certification of critical software without considering untrusted software.

One of the main issues with several of these platforms is that the verification
does not take I/O devices into account. Current systems either disable the I/O
devices (thus making the IoT device useless), or the device drivers are trusted
(whose code is comparable in complexity and size to the verified kernel) and
which may exploit an I/O device with Direct Memory Access (DMA) to bypass
process isolation.

We designed a secure IoT system that relies on component isolation and the
principle of complete mediation: the device driver of the Network Interface
Controller (NIC) is part of untrusted software and monitored by a customized
piece of software, which verifies that the configurations built by the device
driver cannot enable the NIC to access disallowed memory locations. In practice
the monitor preserves a security policy which can be modeled by a system
invariant. The rationale is that the monitor can be substantially simpler than
the device driver and therefore it is easier to analyze, verify, or certify. For
this design, security depends on three main properties: (1) The monitor is
correctly isolated from the other, possibly corrupted, parts of the system (e.g.
the execution platform is formally verified or vulnerabilities are unlikely due
to the small code base of the kernel); (2) The monitor is functionally correct
and denies configurations that violate the invariant (e.g. the monitor is
verified or its small code reduces the number of critical bugs); (3) The
security policy (i.e. the invariant) is strong enough to guarantee that the NIC
does not violate isolation.

We accomplished the first formal verification of (3) for a real hardware I/O
device of significant complexity. As a demonstrating platform we use Beaglebone
Black. We formalized the model of the NIC and we defined the security policy as
an invariant of the NIC state. Then we demonstrated that this policy is sound:
the invariant is preserved by the NIC and it restricts DMA accesses to
address only a predetermined memory region. The analysis is implemented in the
HOL4 interactive theorem prover, which guarantees soundness of our reasoning.

The invariant provides a blueprint for securing the NIC: either the device
driver ensures preservation of the invariant, or a run-time monitor is used to
prevent a potentially compromised driver to violate the invariant. We
demonstrate applicability of the second method by implementing a demonstrator.
This demonstrator consists of a secure hypervisor that hosts a paravirtualized
Linux, a secure service, and the NIC monitor. The untrusted Linux is in charge
of controlling the NIC, but each NIC reconfiguration is intercepted and analyzed
by the monitor, which forbids DMA requests addressing memory locations not part
of the memory region of Linux. This enables the secure service to access the
Internet, using Linux as an untrusted intermediary, and to reduce the trusted
computing base.

The verification identified some properties of secure NIC configurations that
are not explicitly stated by the specification and that may be overlooked by
developers. For example, configurations are based on linked list of buffer
descriptors, which should never be overlapping. We also identified several bugs
in the Linux  driver while testing the monitor, including a possible deadlock
and a write after free.

Our verification relies on the correctness of the NIC model. Hardware
specifications are often incomplete, unclear, ambiguous, and self contradictory.
To get a clear understanding of the NIC, in addition to using the specification,
we inspected the source code of the Linux device driver and we tested on
hardware some unclear configurations. We also exercised the model by means of
several lemmas, each one representing a large set of test cases for one usage
scenario. Finally we developed an independent and simplified NIC model and
device driver in NuSMV. This allowed us to identify an error in the model
related to the order of execution of some operations.

We believe that advances are needed to provide and analyze models of I/O
devices. Namely, a unified framework containing theories and tools used to: Relate models with designs written in hardware description languages; formalize specifications of hardware; and analyze non-functional properties at the system
level taking both software and hardware into account.

\end{document}
