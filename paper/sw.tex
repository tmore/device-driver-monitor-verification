\section{Prevention of Code-Injection in IoT System}\label{sec:sw}
To demonstrate the applicability of our approach we developed a software
platform to prevent code injection in an embedded connected Linux system
running on BeagleBone Black.
%\vskip -40pt
\subsubsection{Existing Platform}
MProsper~\cite{chfouka2015trustworthy} is a formally verified platform that
guarantees component isolation and absence of code injection. The latter is
based on  Virtual Machine Introspection (VMI) and code hashing: MProsper
prevents execution of code (i.e. memory page) whose hash value is not in the
database of trusted program hashes, referred to as the ``golden image''.

MProsper and Linux are guests of the Prosper
hypervisor~\cite{nemati2015trustworthy}, which has been formally verified to
isolate itself and its guests. Linux is paravirtualized, implying that both
Linux and the applications are executed in user mode. Only the hypervisor is
executed in privileged mode and which is invoked via hypercalls. In order to
guarantee isolation, the hypervisor is in control of the MMU and virtualizes
the memory subsystem via direct paging: Linux allocates the page tables inside
its own memory area and can directly modify them while the tables are not in
active use by the MMU; once the page tables are in active use by the MMU, the
hypervisor guarantees that those page tables can only be modified via
hypercalls.

Since the hypervisor supervises all modifications of the page tables, MProsper
can intercept all modifications of the virtual memory layout. Whenever Linux
requests to change a page table, MProsper identifies the physical pages that
are requested to be made executable (if the request involves executable
permissions), computes the hash values of those pages, and checks that the hash
values are in the golden image.
% This policy is sufficient to prevent code
% injection that are caused by changes of the virtual memory layout.
% In order to prevent code injections performed by writing malicious code in an
% executable memory area,
Additionally, MProsper forces Linux to obey the executable space protection
policy: A memory page can be either executable or writable, but not both.
These policies guarantee that the hash values of the code have been checked by
MProsper before the code is executed and that executable code remains
unmodified after validation.

\subsubsection{Attacker Model}
Concerning the Linux guest it is not realistic to restrict the attacker model,
since it has been repeatedly demonstrated that software vulnerabilities have
enabled complete Linux systems to be overtaken via privilege escalation. For
this reason we assume that the attacker has complete control of the Linux
guest. The attacker can force applications and the kernel to execute arbitrary
code and access arbitrary data. It is assumed that the goal of the attacker is
to run an arbitrary binary program.
% In order to detect code injection, we assume
% the attacker is computationally bound: The attacker cannot create code with an
% arbitrary behavior and with a hash value in the golden image.

% MProsper prevents all sort of code injections.
% The verification relies on three main principles:  (1) the hypervisor
% mediates all changes of the virtual memory setup by controlling access
% permissions of page tables;
% (2) the hypervisor prevents
%  virtual memory setup that enable Linux to modify the hypervisor or
%  the VMI memory;
%  (3) the VMI prevents virtual memory setup that enable execution of
%  pages having untrusted hashes;
%  (4) the W $\oplus$ X policy makes impossible to update executable
% code.
%
% Note that the identification of the
% executable code  does not rely on any information
% provided by the untrusted Linux. Instead, the VMI only depends on HW
% information, which can not be tampered by an attacker.
% Also, there is no assumption that Linux actually 
% respects the W $\oplus$ X policy: if a wrong configuration is
% requested, Linux execution is suspended. \todo{EMulation layer}


% \todo{
% As for the hypervisor, a naive run-time check of the executable space protection is not efficient.
% Instead, we reuse the hypervisor reference counters: we accept an hypercall that
% makes a block executable (writable) only if the writable (executable) reference
% counter of the block is zero.
% }
% \todo{Similarly to
% MProsper, other proposals (including Livewire [?], VMWatcher [?] and Patagonix [?]) use VMI,
% code signing and executable space protection to prevent binary code injection in
% commodity OSs. However, all existing proposals rely on untrusted hypervisors
% and their designs have not been subject of formal verification.}

\subsubsection{Secure Network Connectivity via Monitoring}
\begin{figure}[t]
  \center
  \includegraphics[width=0.8\linewidth]{architecture3}
  \caption{The hypervisor prevents Linux from directly modifying the trusted
    components, page tables, and NIC registers (gray elements). Mprosper
    intercepts all changes to page tables, guaranteeing that executable code is
    read only (dashed gray elements) and that the hash values of the executable
    pages (e.g. code of App 2) are in the golden image. The NIC monitor
    intercepts all attempted NIC reconfigurations, guaranteeing that the
    invariant is preserved.}
  \label{img:design}
\end{figure}

MProsper prevents code injection if the CPU is the only hardware component that
can modify memory~\cite{chfouka2015trustworthy}. However, if Linux can control
a DMA device then Linux can indirectly perform arbitrary memory accesses with
catastrophic consequences: modify page tables, enabling Linux to escape its
memory confinement; inject code and data into the hypervisor or other guests
(providing e.g. secure services); modify the golden image; or inject code into
Linux executable memory.

We extend the system with secure Internet connectivity while preventing Linux
from abusing the DMAC of the NIC. We deploy a new monitor within the hypervisor
that validates all NIC reconfigurations (see Figure~\ref{img:design}). The
hypervisor forces Linux to map the NIC registers with read-only access (NIC
register reads have no side effects). When the Linux NIC driver attempts to
configure the NIC, by writing a NIC register, an exception is raised. The
hypervisor catches the exception and, in case of a NIC register write attempt,
invokes the monitor. The monitor checks whether the write preserves the NIC
invariant, and if so re-executes the write, and otherwise blocks it.

\begin{figure}[t]
\begin{lstlisting}[escapeinside={(*}{*)}]
bool nic_ram_handler(a: word, v: word)
  update_q_heads();
  for op in [tx, rx]:
    case q_overlap(a, op) of
    LAST_NDP:
      if (not queue_secure(v, op))
        return false;
    ILLEGAL: return false;
    NO: continue
  (*$\cppi[a] := v$*);
  return true;
\end{lstlisting}
  \caption{Pseudo-code of the NIC Monitor handling writes to internal NIC memory.}
  \label{fig:code}
\end{figure}

Figure~\ref{fig:code} presents the pseudo-code of the monitor that checks
writes (of the value $v$) to (the address $a$ of) the NIC memory. The monitor
uses the variables \txhdvar\ and \rxhdvar\ to store the addresses of the heads
of the transmission and reception queues, respectively. Initially (line 2), the
monitor updates \txhdvar\ and \rxhdvar\ by traversing the queues until a BD
with a cleared OWN-flag is encountered (i.e. the monitor updates its view of
which BDs are in use by the NIC). The monitor then checks the write with
respect to both transmission and reception queues (line 3). If Linux is
attempting to overwrite a BD of a queue then the queue is only allowed to be
extended: The updated BD must be last in the queue and the address $a$ must be
the corresponding $NDP$ field (line 5). In this case the appended queue (i.e.
the one starting from address $v$) must be secure: All BDs are properly
initialized, it is not circular and does not point to BDs of the existing
nor have overlapping BDs, it does not overlap the other queue, and all
addressed buffers belong to Linux memory. Moreover, reception buffers must
not be executable. Any other update (line 8) of an existing queue is
prohibited. Finally, if the queue extension is secure or no queue is modified
then the monitor performs the write (line 10). The actual code of the monitor
is slightly more complex, due to data structures recording which BDs are
potentially in use by the NIC, in order to speed up the checks of whether a
request attempts to modify a BD that can affect the operation of the NIC.

In addition to the NIC monitor, we extended the checks of MProsper to ensure
that page tables and executable code are not allocated in buffers address by
BDs in the reception queue, since those buffers are written when frames are
received.

\subsubsection{Secure Remote Upgrade}
In addition to enabling Internet connectivity to Linux applications, the new
system design also enables connectivity for the secure components, which can
use Linux as an untrusted ``virtual'' gateway. We used this feature to
implement secure remote upgrade of Linux applications. New binary code and
corresponding hash values are signed using the administration private key and
published by a remote host. Linux downloads the new binaries, hash values, and
signatures and requests an update of the golden image via a hypercall. The
hypervisor forwards the request to MProsper. The signature is checked by
MProsper using the administration public key, and if it is valid, the golden
image is updated with the new hash values. The use of digital signatures makes
the upgrade trustworthy, even though Linux acts as a network intermediary, and
furthermore, even if Linux is compromised. A similar approach is used to revoke
hash values from the golden image.

% The formally verified parts of this secure remote upgrade mechanism are:
% \begin{itemize}
% 	\item The hypervisor isolates Linux, MProsper and the NIC monitor: Verified at
% 	the abstraction level where each transition describes the execution of one CPU
% 	instruction.
% 	\item MProsper ensures that no unsigned Linux code is executed: Verified at
% 	the abstraction level where each transition describes one invocation of
% 	MProsper.
% 	\item The invariant of the NIC ensures that the NIC only accesses readable and
% 	writable memory: Verified at the abstraction level where each transition
% 	describes one NIC register/memory update.
% \end{itemize}

% To completely formally verify the secure remote upgrade mechanism the NIC
% monitor must be formally verified to preserve the NIC invariant (instantiated
% with the memory regions that are readable and writable), and that no
% other code of the hypervisor nor any guests write NIC registers. (To get a
% rigourous verification, the verification of MProsper should be refined to the
% CPU instruction level.)

Having the NIC driver in Linux in contrast to developing a NIC driver for the
hypervisor has several advantages. It keeps the code of the hypervisor small,
avoiding verification of code that manages initialization, power management,
routing tables and statistics of the NIC. It makes the interface of the NIC
independent of the guest OS, since the monitor code does not depend on the
Linux networking stack. It also enables the same hypervisor and monitor to
be used with different OSs, OS versions, and device driver versions. Finally,
it demonstrates a general mechanism to secure DMACs that are configured via
linked lists and can easily be adapted to support other DMACs. 

% Third, fewer
% context switches between Linux and the hypervisor are performed,
% avoiding unnecessary performance hits. 



\subsubsection{Evaluation}
Network performance was evaluated for BeagleBone Black (BBB), Linux 3.10, with
netperf 2.7.0. BBB was connected with a 100 Mbit Ethernet point-to-point link
to a PC, with netperf 2.6.0. The benchmarks are: TCP\_STREAM and TCP\_MAERTS
transfer data with TCP from BBB to the PC and vice versa; UDP\_STREAM transfers
data with UDP from BBB to the PC; and TCP\_RR and UDP\_RR use TCP and UDP,
respectively, to send requests from BBB and replies from the PC. Each benchmark
lasted for ten seconds and was performed five times. Table~\ref{tbl:benchmark}
reports the average value for each test.
\begin{table}[]
  \center
\begin{tabular}{|l|r|r|r|r|r|}
 \hline
 \multirow{2}{*}{Configuration} & \multicolumn{5}{c|}{Benchmark} \\ \cline{2-6}
  & TCP\_STREAM & TCP\_MAERTS & UDP\_STREAM & TCP\_RR & UDP\_RR \\ \hline
 Native			& 94.1	& 93.9	& 96.2	& 3365.1	& 3403.4 \\ \hline
 Native+Monitor		& 94.1	& 93.9	& 96.2	& 3317.6	& 3402.2 \\ \hline
 Hyper			& 16.2	& 45.6	& 29.3	& 924.9		& 1009.0 \\ \hline
 Hyper+Monitor		& 15.3	& 41.0	& 27.6	& 891.3		& 982.6 \\ \hline
 %  2			& 94.1	& 93.9	& 96.2	& 3365.1	& 3403.4 \\ \hline
 % 2+M		& 94.1	& 93.9	& 96.2	& 3317.6	& 3402.2 \\ \hline
 % 1			& 16.2	& 45.6	& 29.3	& 924.9		& 1009.0 \\ \hline
 % 1+RXM		& 14.6	& 44.4	& 28.5	& 905.1		& 1003.0 \\ \hline
 % 1+BDM		& 14.1	& 38.8	& 27.9	& 864.8		& 979.2 \\ \hline
 %  1+M		& 15.3	& 41.0	& 27.6	& 891.3		& 982.6 \\ \hline
\end{tabular}
\vskip 5pt
  \caption{Netperf benchmarks. TCP\_STREAM, TCP\_MAERTS and UDP\_STREAM are measured in Mbit/second, and TCP\_RR and UDP\_RR are measured in transactions/second.}
  \label{tbl:benchmark}
\end{table}

We compare the network performance of the system (Hyper+Monitor) shown in
Figure~\ref{img:design} with the MProsper system (Hyper) where Linux is free
to directly configure the NIC, and therefore being able to violate all security
properties. The performance of the new system are between 89.9\% and 97.4\% of
the original system. This performance loss is expected due to the additional
context switches caused by the Linux NIC driver attempting to write NIC
registers.

To validate the monitor design we also experimented with a different system
setup. In this case we consider a trusted Linux kernel that is executed without
the hypervisor but with a potentially compromised NIC driver (Native). This is
typically the case when the driver is a binary blob. In order to prevent the
driver from abusing the NIC DMA the monitor is added to the Linux kernel
(Native+Monitor). The Linux NIC driver has been modified to not directly write
NIC registers but instead to invoke the monitor function when a NIC register
write is required. The monitor is similar to the one in the hypervisor, and the
C file containing the monitor code is located in the same directory as the
Linux NIC driver. The overhead introduced by this configuration is negligible,
as demonstrated by the first two lines of Table 1. The same approach can be
used to monitor an untrusted device driver that is executed in user mode on top
of a microkernel (e.g. seL4 and Minix).

% The monitor \cite{holformalizationandmonitor} is installed in Linux as follows.
% The C-file containing the code of the monitor is located in the Linux directory
% containing the NIC driver. 36 lines in four files of the NIC driver that writes
% NIC registers or are macro definitions writing NIC registers, are replaced by a
% call to a wrapper of the monitor. The arguments of the wrapper are a physical
% address and a 32-bit value with the meaning that the driver requests to write
% the given value to the addressed NIC register. Since the NIC driver uses
% virtual addresses, and the monitor uses physical addresses, code was added to
% the monitor for converting between these two types of addresses, resulting in
% an additional 230 lines of code (excluding comments; the logic behind this code
% is simple but for readability reasons a significant amount of macros were added
% to name registers and sets of registers). To make this conversion code work,
% the NIC driver provides information about the mapping between virtual and
% physical addresses. Hence, the checks the monitor performs work only if the
% driver does not provide false information.

In addition to being OS and device driver version independent, the monitor
minimizes the trusted computing base controlling the NIC. In fact, the
monitor consists of 900 lines of C code (excluding address conversion) while
the Linux device driver consists of 4650 lines of C.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
