\section{Formal Verification of Isolation}
The main verification goal is to identify a NIC configuration that isolates the
NIC from certain memory regions. This means that the NIC can only read and
write certain memory regions, denoted by $\readable$ and $\writable$,
respectively. We identify such a configuration by an invariant $\nicinvariant$
that is preserved by internal NIC transitions
($l \neq \lnread{\address}{v}$) and that restricts the set of accessed memory
addresses:

\begin{theorem}
  \label{thm:main}
  If
  $\nicinvariantA{\nstate}{\readable}{\writable} \land
   \nstate \xrightarrow{l} \nstate' \land
   l \neq \lnread{\address}{v}$, then
  \begin{enumerate}
    \item $\nicinvariantA{\nstate'}{\readable}{\writable}$,
    \item $l = \ldread{\address} \implies \address \in \readable$, and
    \item $l = \lnwrite{\address}{v} \implies \address \in \writable$.
  \end{enumerate}
\end{theorem}

% \todo{This is probably clear from the introduction
% Theorem~\ref{thm:main} means that if such isolation is
% to be formally verified at system level, the main necessary work is to verify
% that the device driver establishes and preserves the invariant. Hence, the NIC
% invariant and Theorem~\ref{thm:main} serves as a base for ensuring that the NIC
% is isolated from certain memory regions in a computer using this NIC \cite{nicspec}.
% }

\subsection{Definition of the Invariant}
The invariant states that the NIC state is not undefined and that the
transmission and reception queues are not overlapping, and restricts the values
of the state components of each automaton (stored in $\nstate.\nsaut.\autovar$,
$\autovar \in \automataset$) and the contents of the BDs in the BD queues:
\begin{alignat*}{3}
\nicinvariant  & \triangleq
\invariantnotdead \land\ 
\qsinvariant{\nstate} \land\ 
\itinvariant{\nstate} \land\ 
\txinvariant{\nstate} \land\ 
\rxinvariant{\nstate}
\end{alignat*}


\subsubsection{Disjoint Queues}
$\qsinvariantname$ states that when the transmission and reception automata are
active, their queues do not overlap (no byte in $\nstate.\nsreg.\cppi$ is used
by both a BD in the transmission queue and a BD in the reception queue):
\begin{alignat*}{2}
	\qsinvariant{\nstate}
	\triangleq
	\nstate.\nsaut.tx.s \neq \txstatenameidle \land
	\nstate.\nsaut.rx.s \neq \rxstatenameidle
	\implies
	\disjointqueues{\txbdqueue{\nstate}}{\rxbdqueue{\nstate}}
\end{alignat*}

The functions $\txbdqueuename$ and $\rxbdqueuename$ return the list of the
addresses of the BDs in transmission  and reception queues respectively. A
queue is considered empty when the corresponding automaton is idle.

\subsubsection{Initialization} 
$\itinvariantname$ states that during
initialization the transmission and reception automata are idle:
\begin{alignat*}{2}
	\itinvariant{\nstate}
	\triangleq
	\nstate.\nsaut.it.s \neq \textit{idle}
	\implies
	\nstate.\nsaut.tx.s = \textit{idle} \land
	\nstate.\nsaut.rx.s = \textit{idle}
\end{alignat*}

This implies that when initialization finishes, the transmission and
reception automata are idle. Therefore, after initialization
$\txinvariantname$ and $\rxinvariantname$ hold vacously (see the definition
of $\txinvariantname$ in the next paragraph).

\subsubsection{Transmission} 
$\txinvariantname$ consists of two conjuncts:
\[
  \begin{array}{ll}
  \txinvariant{\nstate} \triangleq
	& (\nstate.\nsaut.tx.s \neq \txstatenameidle \implies
	   \txinvariantwelldefined{\nstate} \land \txinvariantmemoryreads{\nstate})\ \land \\
	& (\nstate.\nsaut.tx.s = \txstatenameidle \implies \\
	&  \ \nstate.\nsaut.tx.\currentbdpa \neq 0 \implies
	   \disjointqueues{[\nstate.\nsaut.tx.\currentbdpa]}{\rxbdqueue{\nstate}})
  \end{array}
\]

The first conjunct ensures that the transmission automaton cannot cause the NIC
to enter $\stateundefined$ ($\txinvariantwelldefined{\nstate}$) and that only
readable memory is read ($\txinvariantmemoryreads{\nstate}$).

$\txinvariantwelldefinedname$ states for example that the transmission queue
is acyclic; no pair of BDs overlap; all BDs are appropriately configured (e.g.
the OWN-flag is cleared); the queue is not empty while a BD is processed
($\nstate.\nsaut.tx.s \neq \txstatenameidle \land \nstate.\nsaut.tx.s \neq \txstatenamewritecp$); and the currently processed
BD ($\nstate.\nsaut.tx.\currentbdpa$) is the head of the queue.

$\txinvariantmemoryreadsname$ states that the buffers addressed by the BDs in
the queue are located in $\readable$. $\txinvariantmemoryreadsname$ also states
that if the transmission automaton is in the DMA loop, then the state
components used to compute the memory addresses do not cause overflow, and the
addresses of the future memory read requests issued during the processing of
the current BD are in $\readable$; that is, if
$\nstate.\nsaut.tx.s = \txstatenameissuenextmemoryreadrequest\ \lor\
 \nstate.\nsaut.tx.s = \txstatenameprocessmemoryreadreply$ then
$\forall 0 \leq i < \nstate.\nsaut.tx.left\ (\nstate.\nsaut.tx.\txmemoryaddress + i \in \readable)$, where $\nstate.\nsaut.tx.left$ records the number of bytes left to read of the buffer addressed by the current BD, and
$\nstate.\nsaut.tx.\txmemoryaddress$ records the address of the next memory
read request (see Figure~\ref{fig:tx}).

The second conjunct ensures that the transmission tear down automaton does not
modify the reception queue when writing the NIC memory $\nstate.\nsreg.\cppi$.
This prevents the tear down automaton from affecting the reception automaton to
cause the NIC to enter $\stateundefined$ or issue a memory write request
outside $\writable$.

\subsubsection{Reception}
The invariant for reception is similar to the invariant for transmission. The
main difference is the definition of $\invariant_{\textit{rx-wd}}$, since
reception BDs specify different properties than transmission BDs. Also, the
invariant states that BDs in the reception queue address buffers located in
$\writable$, and that $\nstate.\nsaut.rx.\currentbdpa$ is
disjoint from the transmission queue.

\subsection{Proof of Theorem~\ref{thm:main}}
Consider Theorem~\ref{thm:main}.2. Transitions of the form
$\nstate \xrightarrow{\ldread{\address}} \nstate'$ occur only when
$\nstate.\nsaut.tx.s = \txstatenameissuenextmemoryreadrequest$, where
$\address = \nstate.\nsaut.tx.\txmemoryaddress$. 
$\txinvariantmemoryreads{\nstate}$ implies
$\nstate.\nsaut.tx.s = \txstatenameissuenextmemoryreadrequest \implies
 \nstate.\nsaut.tx.\txmemoryaddress \in \readable$. Hence, the requested
address is readable: $\address \in \readable$. The proof of
Theorem~\ref{thm:main}.3 has the same structure but follows from
$\rxinvariant{\nstate}$.

Defining the invariant in terms of conjuncts specialized for each automaton
gives a natural structure to the proof of Theorem~\ref{thm:main}.1. The proof
is therefore described in terms of the three actions the NIC performs:
initialization, transmission and reception, $\opvar \in \{it, tx, rx\}$. The
labels of the transitions describing one of the three actions are
identified by $L(\opvar)$, where
$L(it) \triangleq \{\lnempty_{it}\}$,
$L(tx) \triangleq \{\lnempty_{tx}, \lnempty_{td}\} \cup \bigcup_{\address,v}\{\ldread{\address}, \ldwrite{\address}{v}\}$, and
$L(rx) \triangleq \{\lnempty_{rx}, \lnempty_{rd}\} \cup
\bigcup_{\address,v}\{\lnwrite{\address}{v}\}$.

The following two lemmas formalize properties of the NIC model: Transitions of
an action do not modify state components of other actions; and an automaton can
leave the idle state only when the CPU writes a NIC register.
\begin{lemma}
  \label{lem:auto:state}
  For every $\opvar$ if  
  $\nstate \xrightarrow{l} \nstate'$ and
   $l \not \in L(\opvar)$
  then
  $\nstate'.\nsaut.\opvar = \nstate.\nsaut.\opvar$.
\end{lemma}

\begin{lemma}
  \label{lem:auto:leaveidle}
  For every $\autovar$ if
  $\nstate \xrightarrow{l} \nstate'$, 
   $\nstate.\nsaut.\autovar.s = \mathit{idle}$, and $
   \nstate'.\nsaut.\autovar.s \neq \mathit{idle}$
  then
  $l = \lnread{\address}{v}$.
\end{lemma}

Lemma~\ref{lem:auto:invariant} states that all transitions of each
action, $\opvar \in \{it, tx, rx\}$, preserve the corresponding invariant:
\begin{lemma}
  \label{lem:auto:invariant}
  For every $\opvar$ if
  $\nicinvariant $, $\nstate \xrightarrow{l} \nstate'$, and  $l \in L(\opvar)$
  then
  $\opinvariant{\opvar}{\nstate'} $ and $ \nstate' \neq \stateundefined$.
\end{lemma}

\begin{proof}
We sketch the proof for $\opvar = tx$, since reception is analogous and
initialization is straightforward. The transition $l$ belongs to the transmission or the transmission tear down automaton. There are four cases
depending on whether $\nstate.\nsaut.tx.s$ and $\nstate'.\nsaut.tx.s$ are equal
to $\txstatenameidle$ or not.

The case $\nstate.\nsaut.tx.s = \txstatenameidle \land \nstate'.\nsaut.tx.s \neq \txstatenameidle$ cannot occur by Lemma~\ref{lem:auto:leaveidle}.

If
$\nstate.\nsaut.tx.s \neq \txstatenameidle \land \nstate'.\nsaut.tx.s \neq \txstatenameidle$
then the transition is performed by the transmission automaton. We first
analyze modifications of the transmission queue. The transmission automaton
can only modify the flags OWN and EOQ of the currently processed BD and advance
the head of the transmission queue (but not atomically).
$\txinvariantwelldefined{\nstate}$ implies that the current BD is the head of
$\txbdqueue{\nstate}$ and that the BDs in $\txbdqueue{\nstate}$ do not overlap.
Therefore, the two flag modifications do not alter the 	NDP-fields of the
current BD nor the following BDs in $\txbdqueue{\nstate}$. For this reason the
queue is only either \emph{unmodified or shrinked}, thereby implying
$\txinvariantwelldefined{\nstate'}$. Moreover, the buffers addressed by the BDs
in $\txbdqueue{\nstate'}$ are still located in $\readable$, therefore
$\txinvariantmemoryreads{\nstate'}$ holds. The modifications of OWN and
EOQ of the current BD do not violate the invariant, since the queue is acyclic,
implying that the current BD is not part of the queue when the head is
advanced.

We now analyze modifications of the state components that are used for address
calculations and the DMA read requests, which are restricted by
$\txinvariantmemoryreads{\nstate}$. If the transition is from
$\txstatenamefetchnextbd$, then the automaton fetches the current BD from the
NIC memory, and assigns certain state components.
$\txinvariantmemoryreads{\nstate}$ ensures that the overflow restrictions are
satisfied by the relevant state components in $\nstate'$ and that the buffer of the fetched BD is in readable memory.
These properties are preserved by transitions from
$\txstatenameprocessmemoryreadreply$ and
$\txstatenameissuenextmemoryreadrequest$.
	
If
$\nstate.\nsaut.tx.s \neq \txstatenameidle \land \nstate'.\nsaut.tx.s = \txstatenameidle$
then the transition is performed by the transmission automaton and
$\nstate.\nsaut.tx.s = \txstatenamewritecp$. Such a transition does not modify
$\nstate.\nsaut.tx.\currentbdpa$, $\nstate.\nsaut.\nsreg.\cppi$,
nor $\nstate.\nsaut.rx$. The BD at $\nstate'.\nsaut.tx.\currentbdpa$ does not
overlap any BD in $\rxbdqueue{\nstate'}$ due to $\qsinvariant{\nstate}$,
$\txinvariantwelldefined{\nstate}$, and the fact that $\rxbdqueuename$ is unmodified since neither $\nstate.\nsaut.\nsreg.\cppi$ nor $\nstate.\nsaut.rx$ is modified.

The last case is
$\nstate.\nsaut.tx.s = \txstatenameidle \land \nstate'.\nsaut.tx.s = \txstatenameidle$. These transitions are performed by the transmission tear
down automaton, and only assign fields of the BD at
$\nstate.\nsaut.tx.\currentbdpa$ (provided
$\nstate.\nsaut.tx.\currentbdpa \neq 0$) and set
$\nstate.\nsaut.tx.\currentbdpa$ to 0. The second conjunct of
$\txinvariant{\nstate}$ implies that the BD at $\nstate.\nsaut.tx.\currentbdpa$
does not overlap $\rxbdqueue{\nstate}$, therefore
$\rxbdqueue{\nstate} = \rxbdqueue{\nstate'}$ and $\txinvariant{\nstate'}$
holds.
\qed
\end{proof}

The following definitions, lemmas and corollary are used to prove that
each action preserves the invariant of other actions and
it does not make the queue overlapping.
First, for each action $\opvar$, we introduce a relation on NIC states,
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$, with the meaning
that the invariant $\opinvariantname{\opvar}$ is preserved from $\nstate$ to
$\nstate'$. For initialization, the relation
$\automatastatespreserveinvariant{it}{\nstate}{\nstate'}$ requires that the
state components of the initialization automaton are equal
($\nstate.\nsaut.it = \nstate'.\nsaut.it$) and that the transmission and
reception automata remain in their idle states
($\wedge_{\autovar \in \{tx, rx\}}
(\nstate.\nsaut.\autovar.s = \textit{idle} \implies \nstate'.\nsaut.\autovar.s = \textit{idle})$). For $\opvar \in \{tx, rx\}$,
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$ states that the:
\begin{itemize}
	\item
	state components of the corresponding automaton are equal
	($\nstate.\nsaut.\opvar = \nstate'.\nsaut.\opvar$).
	\item
	locations of the corresponding queues are equal
	($\opvarqueue{\opvar}{\nstate} = \opvarqueue{\opvar}{\nstate'}$).
	\item
	content of the corresponding queues are equal
	($\forall \address \in \opvarqueue{\opvar}{\nstate} . \ 
	 \getbd{\nstate}{\address} = \getbd{\nstate'}{\address}$,
	 where $\in$ denotes list membership and $\getbd{\nstate}{\address}$ is a record with its fields set to the
	 values of the corresponding fields of the BD at address $\address$ in the
	 state $\nstate$).
	\item
	other queue is not expanded
	($\forall \address.\ \address \in
      \opvarqueue{\opvar'}{\nstate'} \implies \address \in
      \opvarqueue{\opvar'}{\nstate}$, where $\opvar' = tx$ if $\opvar=rx$ and
     $\opvar' = rx$ if $\opvar=tx$).
\end{itemize}

The following Lemma states that
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$ indeed preserves
the corresponding invariant $\opinvariantname{\opvar}$:
\begin{lemma}
  \label{lem:inv:pres:automaton}
  For every $\opvar$ if
  $\opinvariant{\opvar}{\nstate}$ and $
   \automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}
   $ then $ 
   \opinvariant{\opvar}{\nstate'}$.
\end{lemma}

To complete the proof we introduce a relation for every action $\opvar$, $\subqueue{\opvar}{\nstate}{\nstate'}$,
which formalizes that the location of the corresponding queue is unmodified and
that all bytes outside the queue are unmodified:
\[
  \begin{array}{ll}
    \subqueue{\opvar}{\nstate}{\nstate'} \triangleq
    & (\forall \address \in \bdqueue{\opvar}{\nstate}
       (\getbd{\nstate}{\address}.ndp = \getbd{\nstate'}{\address}.ndp))\ \land\\
    & (\forall \address \not \in \queuebyteaddresses{\bdqueue{\opvar}{\nstate}}
       (\nstate.\nsreg.\cppi(\address) = \nstate'.\nsreg.\cppi(\address)))
  \end{array}
\]
(where
$\queuebyteaddresses{\bdqueue{\opvar}{\nstate}}$ is the set of byte addresses
of the BDs in $\bdqueue{\opvar}{\nstate}$,
and the imaginary ``initialization-queue'' is
defined to be empty: $\bdqueue{it}{\nstate} \triangleq [\ ]$).
The following Lemma states that each action preserves this relation, provided that the corresponding invariant holds in the pre-state:
\begin{lemma}
  \label{lem:queue:equal}
    For every $\opvar$ if
  $\opinvariant{\opvar}{\nstate}$, $\nstate \xrightarrow{l} \nstate'$
  and $l \in L(\opvar)$
   then
   $\subqueue{\opvar}{\nstate}{\nstate'}$.
\end{lemma}
\begin{proof}
The lemmas follows immediately for initialization since the initialization automaton does not modify $\nstate.\nsreg.\cppi$.

For transmission and reception, the first conjunct of
$\subqueue{\opvar}{\nstate}{\nstate'}$ holds since the corresponding  automaton does not modify
the NDP-fields of the BDs in $\bdqueue{\opvar}{\nstate}$, and
$\bdqueue{\opvar}{\nstate}$ contains no overlapping BDs (by
$\opinvariant{\opvar}{\nstate}$). The second conjunct holds since the automaton assigns only fields of BDs in $\bdqueue{\opvar}{\nstate}$ (by
$\opinvariant{\opvar}{\nstate}$).
\qed
\end{proof}

The next Lemma states that each action either shrinks the corresponding queue or does not modify its location (the symbol $@$ denotes concatenation):
\begin{lemma}
  \label{lem:queue:head}
    For every $\opvar$ if
  $\opinvariant{\opvar}{\nstate}$, $\nstate \xrightarrow{l} \nstate'$,
  and 
  $
  l \in L(\opvar)$
  then $\exists q\ (\bdqueue{\opvar}{\nstate} = q @ \bdqueue{\opvar}{\nstate'})$.
\end{lemma}

\begin{proof}
$\opvarqueue{\opvar}{\nstate}$ is non-overlapping
(by $\opinvariant{\opvar}{\nstate}$) and no automaton assigns an NDP-field of a
BD. Therefore, no automaton can change the location of the BDs
in its queue. If the state component identifying the head of
$\opvarqueuename{\opvar}$ is not modified, the location of the queue is not
modified; and if it is modified, then it is set to either zero (emptying the queue) or to a member of $\opvarqueue{\opvar}{\nstate}$ (by
$\opinvariant{\opvar}{\nstate}$; shrinking the queue).
\qed
\end{proof}

We finally show that each action preserves the invariant of the other actions:
\begin{corollary}
  \label{cor:automaton:equal}
   For every $\opvar \neq \opvar'$ if
  $\nicinvariant$,
   $\nstate \xrightarrow{l} \nstate'$, and $
   l \in L(\opvar)$ then
   $\automatastatespreserveinvariant{\opvar'}{\nstate}{\nstate'}$.
\end{corollary}
\begin{proof}
  For $\opvar' = it$, $\opvar \in \{tx, rx\}$.
  Lemma~\ref{lem:auto:state} gives
  $\automatastatespreserveinvariant{\opvar'}{\nstate}{\nstate'}$, and
  Lemma~\ref{lem:auto:leaveidle} gives
  $\nstate.\nsaut.\autovar.s = \textit{idle} \implies
  \nstate'.\nsaut.\autovar.s = \textit{idle}$ for $\autovar \in
  \{tx, rx\}$.
  Therefore,
  $\automatastatespreserveinvariant{it}{\nstate}{\nstate'}$ holds.

  If $\opvar = it$ then the transition is performed by the
  initialization automaton, which does not modify $\nstate.\nsaut.tx$,
  $\nstate.\nsaut.rx$ (by Lemma~\ref{lem:auto:state}), nor
  $\nstate.\nsreg.\cppi$. Therefore the $\txbdqueuename$ and $\rxbdqueuename$
  are unchanged.
  
  If $\opvar = tx$ and $\opvar' = rx$, then
  Lemma~\ref{lem:auto:state}, Lemma~\ref{lem:queue:equal}, and
  Lemma~\ref{lem:queue:head} imply
  $\automatastatespreserveinvariant{rx}{\nstate}{\nstate'}$. The same reasoning  
  applies for $\opvar = rx$ and $\opvar' = tx$.
\qed
\end{proof}

\begin{corollary}
  \label{cor:automaton:inv}
   For every $\opvar \neq \opvar'$ if
  $\nicinvariant$, 
   $\nstate \xrightarrow{l} \nstate'$, and 
   $l \in L(\opvar)$ then
   $\opinvariant{\opvar'}{\nstate'}$.
\end{corollary}
\begin{proof}
Follows from Corollary~\ref{cor:automaton:equal} and
Lemma~\ref{lem:inv:pres:automaton}.
\qed
\end{proof}

\begin{corollary}
  \label{cor:queue:head}
   For every $\opvar$ if
  $\nicinvariant$, 
   $\nstate \xrightarrow{l} \nstate'$, and 
   $l \in L(\opvar)$ then
   $\qsinvariant{\nstate'}$.
\end{corollary}
\begin{proof}
Lemma~\ref{lem:queue:equal}, Lemma~\ref{lem:auto:state} and
$\qsinvariant{\nstate}$ imply that an action
cannot modify the queue of another action. This property, Lemma~\ref{lem:queue:head}, and
$\qsinvariant{\nstate}$, imply that the queues remain disjoint.
\qed
\end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: