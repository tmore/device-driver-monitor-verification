\section{HOL4 Implementation}
The model and the proof have been implemented with the HOL4 interactive
theorem prover~\cite{slind2008brief}. Hereafter we briefly summarize some 
details of the implementation.

The HOL4 model uses an oracle to decide which automaton shall perform the next
NIC transition and to identify properties of received frames (e.g. when a frame is
received, its content, and presence of CRC errors). The oracle is also used to
resolve some of the ambiguities in the NIC specification \cite{nicspec}.
%
The NIC transition relation is defined in terms of several functions, one for each
automaton state:
$\nstate \xrightarrow{l} \nstate'$ is represented in HOL4 as
$\nstate' = \delta^{\autovar}_{\nstate.\nsaut.\autovar.s}(\nstate)$,
where $\autovar$ is the automaton causing transition $l$ and
$\delta^{\autovar}_{\nstate.\nsaut.\autovar.s}$ is the transition
function of $\autovar$ from the state $\nstate.\nsaut.\autovar.s$.

The implementation of the proof of Lemma~\ref{lem:queue:equal} is based on the
following strategy:
\begin{enumerate}
	\item
	For each BD-field $f$ we introduce a HOL4 function,
	$\modBD_i(\cppi, \address, v)$, which updates the NIC memory $\cppi$ by
	writing the BD-field $f$ of the BD at address $\address$ with the value
	$v$.
    \item The HOL4 function $\writecppiram$ performs several field writes
    sequentially:
\[
  \begin{array}{l}
    \writecppiram([], \cppi) \triangleq \cppi\\
    
    \writecppiram([(\modBD_1, a_1, v_1), \dots, (\modBD_k, a_k, v_k)], \cppi) \triangleq\\
	\qquad \writecppiram([(\modBD_2, a_2, v_2), \dots, (\modBD_k, a_k, v_k)],
	 \modBD_1(\cppi, a_1, v_1)) 
  \end{array}
\]
%	\item
%	For each transition function $\delta^{\autovar}_s$, we define a (possibly empty) list
%	$\updBD^{\autovar}_s = [(\modBD_1, a_1, v_1), \dots, (\modBD_n,
%        a_n, v_n)]$, where each $a_i$ and $v_i$ is a function from NIC
%        state to address and value respectively.
%        We prove that $\delta^{\autovar}_s$ assigns
%        $\cppi$ as the field updates $\updBD^{\autovar}_s$ assign $\cppi$: i.e.
%        $\delta^{\autovar}_s (\nstate).\cppi = \writecppiram
%        (\updBD^{\autovar}_s, \nstate.\nsreg.\cppi)$. 
%	For $tx$ and $rx$, we also show that the updated BDs are in the appropriate
%	queue ($\{a_1, \dots, a_n\} \subseteq q_{\autovar}(\nstate)$), and for $td$
%	and $rd$ that the written BD is the BD following the last processed BD
%	($\{a_1, \dots, a_n\} \subseteq
%        \{\nstate.\nsaut.tx.\currentbdpa\}$ and $\{a_1, \dots, a_n\} \subseteq
%        \{\nstate.\nsaut.rx.\currentbdpa\}$ respectively).\todo{Where
%          is the value we use for the update?}

	\item
	For each transition function $\delta^{\autovar}_s$, we define a (possibly
	empty) list
	$\updBD^{\autovar}_s(\nstate) = [t_1(\nstate), \dots, t_k(\nstate)]$, where
	$t_i(\nstate)$ is a triple of the form $(w, a, v)$ (denoting a field
	writer, address and value, respectively) and $w$, $a$ and $v$ depend on the
	state $\nstate$. We prove that $\delta^{\autovar}_s$ and
	$\updBD^{\autovar}_s$ update $\nstate.\nsreg.\cppi$ identically:
	$\delta^{\autovar}_s (\nstate).\nsreg.\cppi = \writecppiram (\updBD^{\autovar}_s(\nstate), \nstate.\nsreg.\cppi)$. 
	For $tx$ and $rx$, we also prove that the written BDs are in the
	corresponding queue
	($\{t_1.a, \dots, t_k.a\} \subseteq q_{\autovar}(\nstate)$), and for $td$
	and $rd$ that the written BD is the BD following the last processed BD
	($\{t_1.a, \dots, t_k.a\} \subseteq \{\nstate.\nsaut.tx.\currentbdpa\}$ and
	 $\{t_1.a, \dots, t_k.a\} \subseteq \{\nstate.\nsaut.rx.\currentbdpa\}$
	 respectively).

	\item
	We prove that each $\modBD_i$ writes only the BD at the given address and
	preserves the NDP-field:
\[
	\begin{array}{l}
		(\forall \address' \not \in \queueAddresses{[\address]}\
		 (\cppi(\address') = \modBD_i(\cppi, \address, v)(\address')))\ \land \\
		\getbd{\cppi}{\address}.ndp = \getbd{\modBD_i(\cppi, \address, v)}{\address}.ndp
	\end{array}
\]

	\item
	Finally, we prove Lemma~\ref{lem:queue:equal} for every update
	$\writecppiram (\updBD^{\autovar}_s(\nstate), \nstate.\nsreg.\cppi)$,
	provided that all possible pairs of BDs at the addresses in
	$\updBD^{\autovar}_s(\nstate)$ are non-overlapping
	(that is, $t_i.a$ and $t_j.a$ are not
	overlapping for $\{t_i, t_j\} \subseteq
        \updBD^{\autovar}_s(\nstate)$). The non-overlapping is
        guaranteed by $\nicinvariant$.
\end{enumerate}

HOL4 requires a termination proof for every function definition. For this
reason the function $\txbdqueuename$ returning the list of BDs in the
transmission queue cannot be implemented by recursively traversing the
NDP-fields of the BDs, since in general the (linked) list can be cyclic
and therefore the queue can be infinite. This problem is solved as follows.
We introduce a predicate $\bdqueuepredicate{q}{\address}{\cppi}$ that holds if
 the queue $q$ is the list (which is finite by definition in HOL4)
of BDs in NIC memory $\cppi$ starting at address $\address$, linked via the NDP-fields,
and containing a BD with a zero NDP-field (the last BD). This predicate is
defined by structural induction on $q$ and its termination proof is therefore
trivial. We show that the queue starting from a given address in a given NIC
memory $\cppi$ is unique:
$$\forall q\ q'\ \address\ \cppi\ . (\bdqueuepredicate{q}{\address}{\cppi}\ \land
\bdqueuepredicate{q'}{\address}{\cppi}) \implies q' = q$$
$\txinvariantwelldefinedname$ includes a conjunct stating that there
exists a list $q$ satisfying \linebreak[4] 
$\bdqueuepredicate{q}{\nstate.\nsaut.tx.\sopbdpa}{\nstate.\nsreg.\cppi}$
($\nstate.\nsaut.tx.\sopbdpa$ denotes the address of the first BD in the
transmission queue, which is defined to be empty if
\linebreak[4]
$\nstate.\nsaut.tx.\sopbdpa = 0$). This enables to define
$\txbdqueue{\nstate}$ using Hilbert's choice operator applied on the set
$\{q \mid
\bdqueuepredicate{q}{\nstate.\nsaut.tx.\sopbdpa}{\nstate.\nsreg.\cppi}\}$,
returning the unique queue satisfying the predicate. The same approach is used for the reception queue.

The model of the NIC consists of 1500 lines of HOL4 code. Understanding the NIC
specification, experimenting with hardware, and implementing the model required
(roughly) three man-months of work. The NIC invariant consists of 650 lines of
HOL4 code and the proof consists of approximately 55000 lines of HOL4 code
(including comments). Identifying the invariant, formalizing it HOL4, defining
a suitable proof strategy, and implementing the proof in HOL4 required
(roughly) one man-year of work. Executing the proof scripts take approximately
45 minutes on a 2.93GHz Xeon(R) CPU X3470 with
16GB RAM.

%Model LOC (excluding comments): 1500
%Invariant LOC (excluding comments): 650
%Proof LOC (excluding model and invariant, but including comments): 55000
%Number of lemmas: 2200
%Manhours (rough estimation): One year or 2200 hours.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
