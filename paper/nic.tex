\section{Hardware Platform and Formal Model}
\label{sec:nic}
Our analysis concerns the development board BeagleBone Black.
We take into account only the Ethernet NIC and assume that the
other DMACs of the SoC (e.g. USB) are disabled.

Our formal model of the SoC uses the device model framework by Schwarz et
al.~\cite{schwarz2014formal}, which describes executions of computers
consisting of one ARMv7 CPU, memory and a number of I/O devices. The state of
the CPU-memory subsystem~\cite{fox2010trustworthy} is represented by a pair
$\mainstate = (\cstate, \mstate)$, where $\cstate$ is a record representing the
contents of the CPU registers, and $\mstate$ is a function from 32-bit words to
8-bit bytes representing the memory.

The state of the NIC is described by a pair $\nstate=(\nsreg, \nsaut)$. The
first component describes the interface between the CPU and the NIC, consisting
of the memory-mapped NIC registers: Ten 32-bit registers
$\nsreg.\nsregisterword$ and an 8-kB memory $\nsreg.\cppi$.
The
second component, $\nsaut = (it, tx, rx, td, rd)$, describes the internal state
of the NIC, consisting of five records storing values of five automata. Each
automaton describes the behavior of one of the five NIC functions:
\emph{initialization} (\emph{it}), \emph{transmission} (\emph{tx}) and
\emph{reception} (\emph{rx}) of frames, and \emph{tear down} of
\emph{transmission} (\emph{td}) and \emph{reception} (\emph{rd}).

The NIC specification~\cite{nicspec} describes a programming guideline
for device drivers, % (e.g. that a specific register should be written with a
% specific value when the NIC is in a specific state)
 but it does not describe
the behavior of the NIC when this guideline is not followed.
A NIC transitions results in the undefined state $\stateundefined$ if
the transition models a NIC operation that is either:
(1) not consistent with the device driver guideline,
(2) not described by the specification (e.g. the behavior in the case
of a DMA request that does not address RAM is unspecified),
or (3) not supported by our formal NIC model.


The execution of the system is described by a transition
relation
$(\mainstate, \nstate) \xrightarrow{} (\mainstate',
\nstate')$,
which is the smallest relation satisfying the following rules
\begin{mathpar}
  \inferrule {
    \mainstate \xrightarrow{\lcempty_{cpu}} \mainstate'
  }
  {
    (\mainstate, \nstate) \xrightarrow{}
    (\mainstate', \nstate)
  }
  \qquad
\inferrule {
  \mainstate \xrightarrow{\ldread{\address}} \mainstate''
  \qquad
  \mainstate'' \xrightarrow{\ldwrite{\address}{\nstate.\nsreg[\address]}} \mainstate' \\
} {
  (\mainstate, \nstate) \xrightarrow{} (\mainstate', \nstate)
}
\\
\inferrule{
  \mainstate \xrightarrow{\lnwrite{\address}{v}} \mainstate' \\
  \nstate \xrightarrow{\lnread{\address}{v}} \nstate' \\
} {
  (\mainstate, \nstate) \xrightarrow{} (\mainstate', \nstate')}
%
\\
  \inferrule {
    \nstate \xrightarrow{\lcempty_{\autovar}} \nstate'
  }
  {
    (\mainstate, \nstate) \xrightarrow{}
    (\mainstate, \nstate')
  }
\hspace{10pt}
\inferrule{
  \nstate \xrightarrow{\lnwrite{\address}{v}} \nstate' \\
} {
  (\cstate, \mstate, \nstate) \xrightarrow{} (\cstate, \mstate[\address:=v], \nstate')}
\\
\inferrule {
  \nstate' \xrightarrow{\ldread{\address}} \nstate''
  \qquad
  \nstate'' \xrightarrow{\ldwrite{\address}{\mstate[\address]}} \nstate' \\
} {
  % (\cstate, \mstate, \nstate)
  % \xrightarrow{\mathit{read}(\address,\mstate[\address])} (\cstate,
  % \mstate, \nstate')
  (\cstate, \mstate, \nstate) \xrightarrow{} (\cstate, \mstate, \nstate')
}
\end{mathpar}
where $\mainstate \xrightarrow{l} \mainstate'$ and
$\nstate \xrightarrow{l} \nstate'$ denote the transition relations of the CPU-memory subsystem
and the NIC, respectively.
Notice that these rules are general enough to handle other types of DMACs.
To include fine-grained interleavings of the
operations of the CPU and the NIC, each NIC transition describes one single
observable hardware operation: One register read or write, or one single memory
access of one byte.

The first two rules do not affect the NIC: The CPU can execute an instruction
that (1) does not access a memory mapped NIC register
($\mainstate \xrightarrow{\lcempty_{cpu}} \mainstate'$), or (2) that reads the
NIC register at address $\address$
($\mainstate \xrightarrow{\ldread{\address}} \mainstate''$) and processes the
result
($\mainstate'' \xrightarrow{\ldwrite{\address}{\nstate.\nsreg[\address]}} \mainstate'$).
The third rule describes executions of CPU instructions writing a value $v$ to
the NIC register at address $\address$
($\mainstate \xrightarrow{\lnwrite{\address}{v}} \mainstate'$). Register writes
configure the NIC and may activate an automaton
($\nstate \xrightarrow{\lnread{\address}{v}} \nstate'$).

The other three rules involve transitions of active automata. An internal
transition of an automaton $\autovar \in \{it, tx, rx, td, rd\}$
($\nstate \xrightarrow{\lcempty_{\autovar}} \nstate'$) does not affect the CPU.
Memory write requests of writing a byte value $v$ to a location with the
address $\address$ ($\nstate \xrightarrow{\lnwrite{\address}{v}} \nstate'$) are
issued only by the transmission automaton $tx$. Memory read requests of reading
the memory byte at an address $\address$
($\nstate' \xrightarrow{\ldread{\address}} \nstate''$) are issued only by the
reception automaton $rx$, and the byte value at the addressed memory location
($\mstate[\address]$) is immedietaly processed by the NIC
($\nstate'' \xrightarrow{\ldwrite{\address}{\mstate[\address]}} \nstate'$).

The remainder of this section describes the five automata.

\paragraph{\textbf{Initialization}}
\begin{figure}[t]
  \center
  \includegraphics[scale=0.25]{automata/it}
  \caption{
  Initialization automaton: $r$ is the address of the reset register. $p$
  ranges over the addresses of those NIC registers that are cleared to
  complete the initialization of the NIC.
  }
  \label{fig:init}
\end{figure}

Figure~\ref{fig:init} depicts the initialization automaton. Initially, the
automaton is in the state $\itstatenamepoweron$
($\nstate.\nsaut.it.s = \itstatenamepoweron$). Initialization is activated by
writing 1 to the reset register, causing the automaton to transition to the
state $\itstatenamereset$. Once the reset is performed, the automaton
transitions to the state $\itstatenameinitializehdpcp$. The CPU completes the
initialization by clearing some registers, causing the automaton to enter the
state $\itstatenameidle$. The NIC can now be used to transmit and receive
frames. If any register is written with a different value or when the
initialization automaton is in a different state than described, then the NIC
enters $\stateundefined$ (i.e. $n' =\ \stateundefined$).

\paragraph{\textbf{Transmission and Reception}}

\begin{figure}[t]
	\begin{center}
	\center
    \includegraphics[scale=0.6]{automata/bdqueue}
    \end{center}
	\caption{
	A buffer descriptor queue consisting of three BDs located in the memory of the
	NIC. The queue starts with the topmost BD, which addresses the first buffer of
	the first frame (SOP = 1) and is linked to the middle BD. The middle BD
	addresses the last (and second) buffer of the first frame (EOP = 1) and is
	linked to the bottom BD. The bottom BD is last in the queue (NDP = 0) and
	addresses the only buffer of the second frame (SOP = EOP = 1).
	}
	\label{fig:bdqueue}
\end{figure}
The NIC is configured via linked lists of BDs.
One frame to transmit (receive) can be stored in several buffers scattered in
memory, the concatenation of which forms the frame. The properties of a frame
and the associated buffers are described by a 16-byte BD.
Differently than the example of Figure~\ref{fig:dmac}, the lists of
BDs are located in the private NIC memory $\nstate.\nsreg.\cppi$.
There is one queue (list) for transmission and one for reception,
which are traversed by the NIC during transmission and reception of frames.
Each BD contains among others the following fields: Buffer Pointer (BP) identifies the start address of the associated
buffer in memory; Buffer Length (BL) identifies the byte size of the buffer;
Next Descriptor Pointer (NDP) identifies the start address of the next BD in
the queue (or zero if the BD is last in the queue); Start/End Of Packet
(SOP/EOP) indicates whether the BD addresses the first/last buffer of the
associated frame; Ownership (OWN) specifies whether the NIC has completed the
processing of the BD or not; End Of Queue (EOQ) indicates whether the NIC
considered the BD to be last in the queue when the NIC processed that BD (i.e.
NDP was equal to zero). Figure~\ref{fig:bdqueue} shows an example of a BD
queue.

\begin{figure}[t]
  \center
  \includegraphics[scale=0.25]{automata/txc}
  \caption{Transmission automaton: \textit{tx-add} is the address of the
  transmission head descriptor pointer register, which is written to trigger
  transmission of the frames addressed by the BDs in the queue whose head is at
  \textit{bd-add}. The address \txmemoryaddress\ is the memory location
  requested to read, and $v$ is the byte value in memory at that location.}
  \label{fig:tx}
\end{figure}
%\begin{figure}[t]
%  \center
%  \includegraphics[width=0.3\linewidth]{automata/eoq}
%  \caption{Transmission of a queue. The figure depicts OWN and EOQ
%    fields of BD: (a) start transmission, (b) after first frame, (c)
%    after second frame, (d) after third frame in state $own$, (e)
%    after third frame}
%  \label{fig:queue}
%\end{figure}

The initial state of the transmission automaton (Figure~\ref{fig:tx}) is
$\nstate.\nsaut.tx.s = \txstatenameidle$. The CPU activates transmission by
writing the transmission head descriptor pointer register with the address of
the first BD in the queue addressing the frames to transmit. Such a NIC register
write causes $\nstate.\nsaut.tx.\currentbdpa$ to be assigned the written
address, recording the address of the currently processed BD, and the next
state to be $\txstatenamefetchnextbd$.

The transition from $\txstatenamefetchnextbd$  reads the current BD from
$\nstate.\nsreg.\cppi$ located at $\nstate.\nsaut.tx.\currentbdpa$, assigns
fields of the record $\nstate.\nsaut.tx$ to values identifying the memory
location of the buffer addressed by the current BD, and sets the state to
$\txstatenameissuenextmemoryreadrequest$.

As long as there are bytes of the buffer left to read, the automaton transitions between $\txstatenameissuenextmemoryreadrequest$ and
$\txstatenameprocessmemoryreadreply$, fetching and processing in each cycle one
byte via DMA. When the last byte of the buffer addressed by the current BD has
been processed, and if the currently transmitted frame consists of additional
buffers (i.e. the EOP-flag is not set of the current BD) then the automaton
moves from $\txstatenameprocessmemoryreadreply$ to $\txstatenamefetchnextbd$
and sets $\nstate.\nsaut.tx.\currentbdpa$ to the address of next BD. Once all
bytes of the currently transmitted frame have been processed (i.e. the EOP-flag
is set of the current BD), the automaton moves to $\txstatenamepostprocess$.


Once in state $\txstatenamepostprocess$, if the current BD is not last in the queue (i.e. the NDP-field of the current BD is not 0) then the automaton clears
the OWN-flag of the SOP-BD (the BD of the currently transmitted frame with the SOP-flag set; signaling to a device driver that the NIC memory area of the BDs of the transmitted frame can be reused), sets $\nstate.\nsaut.tx.\currentbdpa$
to the address of next the BD, and enters the state $\txstatenamewritecp$. If
the current BD is last in the queue then the automaton sets the EOQ-flag of the
current BD (used by a device driver to check whether a BD was appended just
after the NIC processed a BD, which would result in the NIC not processing the
appended BD, meaning that a device driver must restart transmission) and enters
the state $\txstatenameclearownerandhdp$.

The transition from $\txstatenameclearownerandhdp$ clears the OWN-flag of the
SOP-BD and the head descriptor pointer register (the latter register clear
signals to a device driver that transmission is complete).

The transition from $\txstatenamewritecp$ writes the address of the processed
BD to a register to inform a device driver of which is the last processed BD.
Furthermore, if all BDs in the BD queue have now been processed, or
initialization or transmission teardown was requested during the processing of
the BDs of the last transmitted frame, then the next state is
$\txstatenameidle$. Otherwise the next state is $\txstatenamefetchnextbd$ to
begin the processing of the first BD of the next frame.

The structure of the reception automaton is similar to the structure of the
transmission automaton but with four notable differences:
(1) After the reception head descriptor pointer has been written with a BD
	address to enable reception, it is non-deterministically decided when a
	frame is received to activate the reception automaton.
(2) The BDs in the reception queue address the buffers used to store received
	frames. Since reception do not get memory read replies there is only one
	state related to memory accesses.
(3) The transmission automaton has two states ($\txstatenamepostprocess$ and
	$\txstatenameclearownerandhdp$) to describe BD writes (of the flags EOQ and
	OWN). Reception writes sixteen BD fields (e.g. the length of a frame and
	the result of a CRC check), leading to fourteen additional states.
(4) Since content of received frames are unknown, values written to memory
	and some BD fields are selected non-deterministically.

\paragraph{\textbf{Tear Down}}
Transmission and reception tear down are similar to each other, and are
activated by writing 0 to the associated tear down register. First, the NIC
finishes the processing of the currently transmitted (received) frame (the
corresponding automaton enters the state $idle$). Then the tear down automaton
performs four (six) transitions, each one describing one observable hardware
operation (for the CPU). If the corresponding queue has not been completely
processed ($\nstate.\nsaut.tx.\currentbdpa \neq 0$), then certain fields are
written of the BD ($\nstate.\nsaut.tx.\currentbdpa$) that follows the last
processed BD. The last two transitions clear the head descriptor pointer register and $\nstate.\nsaut.tx.\currentbdpa$, and writes a specific value to a
register to signal to the CPU that the tear down is complete, respectively.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
