\section{Hardware Platform and Formal Model}
\label{sec:nic}
Our analysis concerns the development board BeagleBone Black.
We take into account only the Ethernet NIC and assume that the
other DMACs of the SoC (e.g., USB) are disabled.

Our formal model of the SoC uses the device model framework by Schwarz et
al.~\cite{schwarz2014formal}, which describes executions of computers
consisting of one ARMv7 CPU, memory and a number of I/O devices. The state of
the CPU-memory subsystem~\cite{fox2010trustworthy} is represented by a pair
$\mainstate = (\cstate, \mstate)$, where $\cstate$ is a record representing the
contents of the CPU registers, and $\mstate$ is a function from 32-bit words to
8-bit bytes representing the memory.

The state of the NIC is described by a record
$\nstate = (\nsreg, it, tx, rx, td, rd)$. The
first component describes the interface between the CPU and the NIC, consisting
of the memory-mapped NIC registers: Ten 32-bit registers
$\nsreg.\nsregisterword$ and an 8-kB memory $\nsreg.\textit{\regbdram}$. The
other components ($it, tx, rx, td, rd$), describe the internal state
of the NIC, consisting of five records representing components of five automata.
Each automaton describes the behavior of one of the five NIC functions:
\emph{initialization} (\emph{it}), \emph{transmission} (\emph{tx}) and
\emph{reception} (\emph{rx}) of frames, and \emph{tear down} of
\emph{transmission} (\emph{td}) and \emph{reception} (\emph{rd}).

A NIC transition enters an undefined state $\stateundefined$ if any of the
following conditions hold: (1) the transition results due to a NIC register
write that does not follow the NIC specification~\cite{nicspec} (e.g., that a
specific register should be written with a specific value when the NIC is in a
specific state), (2) the transition occurs from a state from which operations
are not described by the specification (e.g., the result of issuing a DMA
request not addressing RAM), or (3) the operation is not included in the formal
NIC model (e.g., not relevant for memory accesses).

The execution of the system is described by a transition relation
$(\mainstate, \nstate) \xrightarrow{} (\mainstate', \nstate')$, which is the
smallest relation satisfying the following rules
\begin{mathpar}
  \inferrule {
    \mainstate \xrightarrow{\lcempty_{cpu}} \mainstate'
  }
  {
    (\mainstate, \nstate) \xrightarrow{}
    (\mainstate', \nstate)
  }
  \qquad
\inferrule {
  \mainstate \xrightarrow{\ldread{\address}} \mainstate''
  \qquad
  \mainstate'' \xrightarrow{\ldwrite{\address}{\nstate.\nsreg[\address]}} \mainstate' \\
} {
  (\mainstate, \nstate) \xrightarrow{} (\mainstate', \nstate)
}
\\
\inferrule{
  \mainstate \xrightarrow{\lnwrite{\address}{v}} \mainstate' \\
  \nstate \xrightarrow{\lnread{\address}{v}} \nstate' \\
} {
  (\mainstate, \nstate) \xrightarrow{} (\mainstate', \nstate')}
%
\\
  \inferrule {
    \nstate \xrightarrow{\lcempty_{\autovar}} \nstate'
  }
  {
    (\mainstate, \nstate) \xrightarrow{}
    (\mainstate, \nstate')
  }
\hspace{10pt}
\inferrule{
  \nstate \xrightarrow{\lnwrite{\address}{v}} \nstate' \\
} {
  (\cstate, \mstate, \nstate) \xrightarrow{} (\cstate, \mstate[\address:=v], \nstate')}
\\
\inferrule {
  \nstate' \xrightarrow{\ldread{\address}} \nstate''
  \qquad
  \nstate'' \xrightarrow{\ldwrite{\address}{\mstate[\address]}} \nstate' \\
} {
  % (\cstate, \mstate, \nstate)
  % \xrightarrow{\mathit{read}(\address,\mstate[\address])} (\cstate,
  % \mstate, \nstate')
  (\cstate, \mstate, \nstate) \xrightarrow{} (\cstate, \mstate, \nstate')
}
\end{mathpar}
where $\mainstate \xrightarrow{l} \mainstate'$ and
$\nstate \xrightarrow{l} \nstate'$ denote the transition relations of the
CPU-memory subsystem and the NIC, respectively. Notice that these rules are
general enough to handle other types of DMACs. To include fine-grained
interleavings of the operations of the CPU and the NIC, each NIC transition
describes one single observable hardware operation: One register read or write,
one BD read, one BD field write, or one single memory access of one byte.

The first two rules do not affect the NIC: The CPU can execute an instruction
that (1) does not access a memory mapped NIC register
($\mainstate \xrightarrow{\lcempty_{cpu}} \mainstate'$), or (2) that reads the
NIC register at address $\address$
($\mainstate \xrightarrow{\ldread{\address}} \mainstate''$) and processes the
result
($\mainstate'' \xrightarrow{\ldwrite{\address}{\nstate.\nsreg[\address]}} \mainstate'$).
The third rule describes executions of CPU instructions writing a value $v$ to
the NIC register at address $\address$
($\mainstate \xrightarrow{\lnwrite{\address}{v}} \mainstate'$). Register writes
configure the NIC and may activate an automaton
($\nstate \xrightarrow{\lnread{\address}{v}} \nstate'$).

The other three rules involve transitions of active automata. An internal
transition of an automaton $\autovar \in \{it, tx, rx, td, rd\}$
($\nstate \xrightarrow{\lcempty_{\autovar}} \nstate'$) does not affect the CPU.
Memory write requests of writing a byte value $v$ to a location with the
address $\address$ ($\nstate \xrightarrow{\lnwrite{\address}{v}} \nstate'$) are
issued only by the transmission automaton $tx$. Memory read requests of reading
the memory byte at an address $\address$
($\nstate' \xrightarrow{\ldread{\address}} \nstate''$) are issued only by the
reception automaton $rx$, and the byte value at the addressed memory location
($\mstate[\address]$) is immedietaly processed by the NIC
($\nstate'' \xrightarrow{\ldwrite{\address}{\mstate[\address]}} \nstate'$).

The remainder of this section describes the five automata.

\paragraph{\textbf{Initialization}}

\begin{figure}[t]
\center
\includegraphics[scale=0.25]{automata/it}
\caption{Initialization automaton: $r$ is the address of the \regcpdmasoftreset\
register. $p$ ranges over the addresses of the HDP and CP registers that must be
cleared to complete the initialization of the NIC.}
\label{fig:init}
\end{figure}

Figure~\ref{fig:init} depicts the initialization automaton. Initially, the
automaton is in the state \linebreak[4] $\itstatenamepoweron$
($\nstate.it.s = \itstatenamepoweron$). Initialization is activated by
writing \pcone\ to the register \regcpdmasoftreset\
($\nstate.\nsreg.\textit{\regcpdmasoftreset}$), causing the automaton to
transition to the state $\itstatenamereset$. The transition from
$\itstatenamereset$ to $\itstatenameidle$ is inhibited until the transmission
and reception automaton have reach the $\itstatenameidle$ state. When the
automaton reach the transitions to the state $\itstatenameinitializehdpcp$ it
sets \regcpdmasoftreset\ to \pczero. The CPU completes the initialization by
clearing the transmission and reception HDP and CP registers (explained below),
causing the automaton to enter the state $\itstatenameidle$. The NIC can now be
used to transmit and receive frames. If the CPU does not initialize registers as
described, then the NIC enters $\stateundefined$ (i.e., $\nstatenext =\ \stateundefined$),
since any other behavior is unspecified.

\paragraph{\textbf{Transmission and Reception}}

\begin{figure}[t]
\begin{center}
\center
\includegraphics[scale=0.62]{automata/bdqueue}
\end{center}
\caption{A buffer descriptor queue consisting of three BDs located in the memory
of the NIC. The queue starts with the topmost BD, which addresses the first
buffer of the first frame (SOP = \pcone) and is linked to the middle BD. The
middle BD addresses the last (and second) buffer of the first frame (EOP =
 \pcone) and is linked to the bottom BD. The bottom BD is last in the queue (NDP
= \pczero) and addresses the only buffer of the second frame (SOP = EOP =
\pcone).}
\label{fig:bdqueue}
\end{figure}

The NIC is configured via linked lists of BDs. One frame to transmit (receive)
can be stored in several buffers scattered in memory, the concatenation of which
forms the frame. The properties of a frame and the associated buffers are
described by a 16-byte BD. In contrast to the example illustrated in
Figure~\ref{fig:dmac}, the lists of BDs are located in the internal NIC memory
($\nstate.\nsreg.\textit{\regbdram}$). There is one queue (linked
list) for transmission and one for reception, which are traversed by the NIC
during transmission and reception of frames. Each BD contains among others the
following fields: Buffer Pointer (BP) identifies the start address of the
associated buffer in memory; Buffer Length (BL) identifies the byte size of the
buffer; Next Descriptor Pointer (NDP) identifies the start address of the next
BD in the queue (or \pcone if the BD is last in the queue); Start/End Of Packet
(SOP/EOP) indicates whether the BD addresses the first/last buffer of the
associated frame; Ownership (OWN) specifies that the NIC has not completed the
processing of the BD; End Of Queue (EOQ) indicates whether the NIC considered
the BD to be last in the queue when the NIC processed that BD (i.e., NDP was
equal to \pczero). Figure~\ref{fig:bdqueue} shows an example of a BD queue.

\begin{figure}[t]
  \center
  \includegraphics[scale=0.25]{automata/txc}
  \caption{Transmission automaton: \textit{tx-a} is the address of the
  transmission head descriptor pointer register, which is written to trigger
  transmission of the frames addressed by the BDs in the queue whose head is at
  \textit{bd-a}. The address \textit{mem-a} is the memory location
  requested to read, and $v$ is the byte value in memory at that location.}
  \label{fig:tx}
\end{figure}

The state of the transmission automaton consists of the following fields:
\begin{itemize}
\item $\nstate.tx.s$: The state of the
transmission automaton.
\item $\nstate.tx.\currentbdpa$: The address of the currently processed
BD (can be a SOP, EOP or neither), or \pczero\ if no BD is currently processed.
\item $\nstate.tx.\sopbdpa$: The address of the first BD in the
transmission queue (always a SOP), or \pczero\ if no queue is currently
processed.
\item $\nstate.tx.\eopbdpa$: The address of the EOP BD currently
processed.
\item $\nstate.tx.\currentbd$: A record containing values of the fields
of the currently processed BD.
\item $\nstate.tx.\txmemoryaddress$: The address of the next byte of the
frame to transmit.
\item $\nstate.tx.\numberofbufferbyteslefttorequest$: The number of bytes
of the frame to transmit that are left.
\end{itemize}

The initial state of the transmission automaton (Figure~\ref{fig:tx}) is
$\nstate.tx.s = \txstatenameidle$. The CPU activates transmission by
writing the transmission head descriptor pointer register ($\nstate.\nsreg.\textit{\regtxzerohdp}$) with the address of the first BD in the
queue addressing the frames to transmit. Such a NIC register write causes
$\nstatenext.tx.\currentbdpa$ and $\nstatenext.tx.\sopbdpa$ to
contain the written address, and the next state to be
$\nstatenext.tx.s = \txstatenamefetchnextbd$. If \regtxzerohdp\ is
written when it is not \pczero, or when transmission teardown is active,
then $\nstatenext =\ \stateundefined$.

The transition from $\txstatenamefetchnextbd$ reads
$\nstate.\nsreg.\textit{\regbdram}$ to decode the fields of the
current BD located at $\nstate.tx.\currentbdpa$ and sets
$\nstatenext.tx.\currentbd$ to the values of the read fields. If the BD
is well-formed, then $\nstatenext.tx.\txmemoryaddress$ and
$\nstatenext.tx.\numberofbufferbyteslefttorequest$ are set to values
computed from fields of the read BD, and
the resulting automaton state is $\txstatenameissuenextmemoryreadrequest$. If
the BD is
not well-formed (e.g., the fetched BD is outside \regbdram\ or its location is
not 4-byte aligned, or certain BD fields are not properly initialized), then the
NIC enters $\stateundefined$.

As long as there are bytes of the buffer left to read and transmit
($\nstate.tx.s = \txstatenameprocessmemoryreadreply \land \nstate.tx.\numberofbufferbyteslefttorequest > 0$ or $\nstate.tx.s = \txstatenameissuenextmemoryreadrequest$),
the automaton transitions between $\txstatenameissuenextmemoryreadrequest$ and
$\txstatenameprocessmemoryreadreply$, fetching and processing in each cycle one
byte from memory via DMA. The transition from
$\txstatenameprocessmemoryreadreply$ that processes the last byte of the buffer
addressed by the current BD
($\nstate.tx.\numberofbufferbyteslefttorequest = \pczero$) enters either
$\txstatenamefetchnextbd$ or
$\txstatenamepostprocess$. The transition to $\txstatenamefetchnextbd$ is
performed if the currently transmitted frame consists of additional buffers that
need to be fetched from memory (i.e., the EOP flag is not set of the current BD:
$\nstate.tx.\currentbd.eop = \pcfalse$), and which sets the address of
the current BD to the address of next BD
($\nstatenext.tx.\currentbdpa = \nstate.tx.\currentbd.ndp$).
$\txstatenamepostprocess$ is entered if the all bytes of the current frame has
been fetched from memory (i.e., the EOP flag is set of the current BD:
$\nstate.tx.\currentbd.eop = \pctrue$), and which saves the address of the
current (EOP) BD
($\nstatenext.tx.\eopbdpa = \nstate.tx.\currentbdpa$; this assignment is made
since the value $\nstate.tx.\currentbdpa$ is needed by the later transitions
from $\txstatenamewritecp$ to $\txstatenameidle$ and $\txstatenamefetchnextbd$,
but $\nstate.tx.\currentbdpa$ is overwritten by either the transition from
$\txstatenamepostprocess$ to $\txstatenamewritecp$ or by the transition
from $\txstatenameclearownerandhdp$ to $\txstatenamewritecp$).

Once in the state $\txstatenamepostprocess$, if the
current BD is not last in the queue (i.e., the NDP field of the current BD is
not \pczero: $\nstate.tx.\currentbd.ndp \neq \pczero$), then the OWN flag is
cleared in \regbdram\ ($\nstatenext.\nsreg.\textit{\regbdram}$) of the SOP BD at
$\nstate.tx.\sopbdpa$ (indicating to a device driver that the memory area
in \regbdram\ of the BDs of the transmitted frame can be reused), and
$\nstatenext.tx.\currentbdpa$ and $\nstatenext.tx.\sopbdpa$ are
set to the address of next the BD
( $\nstatenext.tx.\currentbdpa = \nstatenext.tx.\sopbdpa = \nstate.tx.\currentbd.ndp$;
identifying the next BD to process and advancing the transmission queue to start
from the next BD, respectively), and enters the state
$\txstatenamewritecp$.
If the current BD is last in the queue (i.e., the NDP field of the current BD is
\pczero: $\nstate.tx.\currentbd.ndp \neq \pczero$) then the EOQ flag is
cleared in $\nstatenext.\nsreg.\textit{\regbdram}$ of the current
BD located at $\nstatenext.tx.\currentbdpa$ (used by a device driver to
check whether a BD was appended just after the NIC processed a BD, which would
result in the NIC not processing the appended BD, meaning that a device driver
must restart transmission) and enters the state $\txstatenameclearownerandhdp$.
The transition from $\txstatenameclearownerandhdp$ clears
the OWN flag in $\nstatenext.\nsreg.\textit{\regbdram}$ of the
SOP BD at $\nstate.tx.\sopbdpa$, \regtxzerohdp\
($\nstatenext.\nsreg.\textit{\regtxzerohdp} = \pczero$;
$\regtxzerohdp = \pczero$ indicates to a device driver that all frames have been
transmitted), and sets $\nstatenext.tx.\currentbdpa = \pczero$ and
$\nstatenext.tx.\sopbdpa = \pczero$ (indicating that there is no current
BD to process and that the transmission queue is empty).

The transition from $\txstatenamewritecp$ writes the
address of the just processed (EOP) BD to the transmission completion pointer
register ($\nstatenext.\nsreg.\textit{\regtxzerocp} = \nstate.tx.\eopbdpa$) to
inform a device driver of which is the last processed BD (this raises a frame
transmission completion interrupt which a device driver can acknowledge by
writing \regtxzerocp\ with the address of the last processed BD). Furthermore,
if all BDs in the BD queue have now been processed
($\nstate.tx.\currentbdpa = \pczero$), or initialization or
transmission teardown was requested during the processing of the BDs of the last
transmitted frame
($\nstate.it.s \neq \itstatenameidle \lor \nstate.td.s \neq \tdstatenameidle$),
then the next state is $\txstatenameidle$. Otherwise
the next state is $\txstatenamefetchnextbd$ to begin
the processing of the first BD of the next frame.

The structure of the reception automaton is similar to the structure of the
transmission automaton but with four notable differences:
(1) After the reception head descriptor pointer has been written with a BD
	address to enable reception, it is non-deterministically decided when a
	frame is received to activate the reception automaton.
(2) The BDs in the reception queue address the buffers used to store received
	frames. Since reception do not get memory read replies there is only one
	state related to memory accesses.
(3) The transmission automaton has two states ($\txstatenamepostprocess$ and
	$\txstatenameclearownerandhdp$) to describe BD writes (of the flags EOQ and
	OWN). Reception writes sixteen BD fields (e.g., the length of a frame and
	the result of a CRC check), leading to fourteen additional states.
(4) Since content of received frames are unknown, values written to memory
	and some BD fields are selected non-deterministically.

\paragraph{\textbf{Tear Down}}

\begin{figure}[t]
  \center
  \includegraphics[scale=0.25]{automata/td}
  \caption{Transmission teardown automaton: \textit{td-add} is the address of
  \regtxteardown\ which is written with \pczero\ to trigger teardown of
  transmission.}
  \label{fig:td}
\end{figure}

The initial state of the transmission teardown automaton (Figure~\ref{fig:td})
is $\nstate.td.s = \tdstatenameidle$. When the CPU writes \pczero\ to the
transmission teardown register ($\nstate.\nsreg.\textit{\regtxteardown}$), the state $\tdstatenameseteoq$ is
entered. However, if \regtxteardown\ is written when the NIC is not initialized
($\nstate.it.s \neq \itstatenameidle$), transmission teardown is in
progress ($\nstate.td.s \neq \tdstatenameidle$), or a non-zero value is
written, then the NIC enters $\stateundefined$.

Before a transition can be performed from $\tdstatenameseteoq$, the transmission
automaton must first complete the processing of the currently transmitted frame
($\nstate.tx.s = \txstatenameidle$). Then there are two cases depending on
whether all BDs in the transmission queue were processed or not. If all BDs were
processed ($\nstate.tx.\currentbdpa = \pczero$), then the transition from
$\tdstatenameseteoq$ to $\tdstatenamewritecp$ is performed, clearing
\regtxzerohdp\ ($\nstatenext.\nsreg.\textit{\regtxzerohdp} = \pczero$) and
$\nstatenext.tx.\currentbdpa = \pczero$. Otherwise
($\nstate.tx.\currentbdpa \neq \pczero$), there are two non-deterministic
cases of either setting the EOQ flag (the transition to $\tdstatenamesettd$) or
the teardown flag (the transition to $\tdstatenameclearownerandhdp$) of the BD
(at address $\nstate.tx.\currentbdpa$) that follows the last processed BD. 
The reason for this non-deterministic behavior is because the NIC specification
does not state that this operation is performed but tests on the hardware shows
that this is indeed the cases, making the model cover both cases. The transition
from $\tdstatenamesettd$ clears the teardown flag of the BD at
$\nstate.tx.\currentbdpa$. The transition from
$\tdstatenameclearownerandhdp$ clears \regtxzerohdp\, the OWN flag of the BD at
$\nstate.tx.\currentbdpa$, $\nstatenext.tx.\currentbdpa = \pczero$
and $\nstatenext.tx.\sopbdpa = \pczero$. Finally, the transition from
$\tdstatenamewritecp$ writes the teardown completion code \pctdiacnumber\ to
$\nstatenext.\nsreg.\textit{\regtxzerocp}$ to
signal to the CPU that the teardown is complete%  (raising an interrupt which the
% CPU can acknowledge by writing \pctdiacnumber\ to \regtxzerocp)
.

Reception teardown works in a similar way but has two more states for
writing additional BD
fields.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
