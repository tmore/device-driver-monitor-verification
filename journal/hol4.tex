\section{HOL4 Implementation}\label{sec:hol4}
Verifying correctness of the invariant requires handling a large
state space, since it depends on the actual binary content of the BDs.
This prevents the usage of model checkers, because they
cannot enumerate all possible values of the \regbdram. 
For this reason, the model and the proof have been implemented using the HOL4 interactive theorem
prover~\cite{slind2008brief}.
Hereafter we briefly summarize some details of the
implementation.

The HOL4 model uses an oracle to decide which automaton shall perform the next
NIC transition and to identify properties of received frames (e.g., when a frame
is received, its content, and presence of CRC errors). The oracle is also used
to resolve some of the ambiguities in the NIC specification \cite{nicspec}.

The NIC transition relation is defined in terms of several functions, one for
each automaton state. In HOL4 $\nstate \xrightarrow{l} \nstate'$ is represented
as $\nstate' = \delta^{\autovar}_{\nstate.\autovar.s}(\nstate)$, where
$\autovar$ is the automaton performing the transition $l$ and
$\delta^{\autovar}_{\nstate.\autovar.s}$ is the transition function of
$\autovar$ from the state $\nstate.\autovar.s$.

The implementation of the proof of Lemma~\ref{lem:queue:equal} is based on the
following strategy:
\begin{enumerate}
\item For each BD field $f$ we introduce a HOL4 function,
$\modBD_i(\textit{\regbdram}, \address, v)$, which writes the value $v$ to the
BD field $f$ in $\textit{\regbdram}$ of the BD at address $\address$, and
returning the resulting representation of $\textit{\regbdram}$.

\item The HOL4 function $\writecppiram$ performs several BD field writes
sequentially:
\begin{align*}
&\writecppiram([], \textit{\regbdram}) \coloneqq \textit{\regbdram}\\
&\writecppiram([(\modBD_1, a_1, v_1), \dots, (\modBD_k, a_k, v_k)], \textit{\regbdram}) \coloneqq\\
&	\qquad \writecppiram([(\modBD_2, a_2, v_2), \dots, (\modBD_k, a_k, v_k)],\\
&	\qquad \qquad \qquad \modBD_1(\textit{\regbdram}, a_1, v_1))
\end{align*}

\item For each transition function $\delta^{\autovar}_s$, we define a (possibly
empty) list
$\updBD^{\autovar}_s(\nstate) = [t_1(\nstate), \dots, t_k(\nstate)]$, whose
elements $t_i(\nstate)$ are triples of the form $(w, a, v)$, depending on the
state $\nstate$, and in which $w$, $a$ and $v$ denote, respectively, a function
writing a BD field, an address, and a value. We prove that $\delta^{\autovar}_s$
and $\updBD^{\autovar}_s$ update $\nstate.\nsreg.\textit{\regbdram}$
identically:
\begin{align*}
&\delta^{\autovar}_s (\nstate).\nsreg.\textit{\regbdram} =\\
&\qquad \writecppiram (\updBD^{\autovar}_s(\nstate), \nstate.\nsreg.\textit{\regbdram})
\end{align*}
For $tx$ and $rx$, we also prove that the written BDs are in the corresponding
queue \linebreak[4]
($\{t_1.a, \dots, t_k.a\} \subseteq q_{\autovar}(\nstate)$), and for $td$
and $rd$ that the written BD is the BD following the last processed BD
($\{t_1.a, \dots, t_k.a\} \subseteq \{\nstate.tx.\currentbdpa\}$ and
\linebreak[4]
$\{t_1.a, \dots, t_k.a\} \subseteq \{\nstate.rx.\currentbdpa\}$ respectively).

\item We prove that each $\modBD_i$ writes only the BD at the given address
$\address$ and preserves the NDP field:
\begin{align*}
&(\forall \address' \not \in \queueAddresses{[\address]}.\\
& \qquad \textit{\regbdram}(\address') = \modBD_i(\textit{\regbdram}, \address, v)(\address'))\ \land \\
&\getbd{\textit{\regbdram}}{\address}.ndp =\\
&\qquad \getbd{\modBD_i(\textit{\regbdram}, \address, v)}{\address}.ndp
\end{align*}
\item Finally, we prove Lemma~\ref{lem:queue:equal} for every update
\linebreak[4]
$\writecppiram(\updBD^{\autovar}_s(\nstate), \nstate.\nsreg.\textit{\regbdram})$,
provided that all possible pairs of BDs at the addresses in
$\updBD^{\autovar}_s(\nstate)$ are non-overlapping (that is, the BDs at
locations $t_i.a$ and $t_j.a$ do not overlap for $\{t_i, t_j\} \subseteq \updBD^{\autovar}_s(\nstate)$).
The non-overlapping is implied by $\nicinvariant$.
\end{enumerate}

HOL4 requires a termination proof for every function definition. For this reason
the function $\bdqueuestateaddress{\nstate}{a}$ (i.e., the list of
addresses of reachable BDs from address $a$ in the NIC state $\nstate$)
cannot be implemented by recursively traversing the BDs by reading their NDP. In
general the linked list can be cyclic and therefore the queue can be infinite.
This problem is solved as follows. We introduce a predicate
$\bdqueuepredicate{q}{\address}{\textit{\regbdram}}$ that holds if the queue $q$
is the list (which is finite by definition in HOL4) of addresses of BDs in
$\textit{\regbdram}$ starting at address $\address$, linked via the NDP fields,
and containing a BD with an NDP field equal to \pczero\ (the last BD). This
predicate is defined by structural induction on the list $q$ and its termination
proof is therefore trivial. We show that the queue starting from a given address
in a given $\textit{\regbdram}$ is unique:
\begin{align*}
& \forall q\ q'\ \address\ \textit{\regbdram}.\\
& \qquad \bdqueuepredicate{q}{\address}{\textit{\regbdram}}\ \land
  \bdqueuepredicate{q'}{\address}{\textit{\regbdram}}\\
& \qquad \implies q' = q
\end{align*}

$\txinvariantwelldefinedname$ includes a conjunct stating that the transmission
queue is not circular. That conjunct is phrased in HOL4 as there exists a list
$q$ satisfying \linebreak[4]
$\bdqueuepredicate{q}{\nstate.tx.\sopbdpa}{\nstate.\nsreg.\textit{\regbdram}}$.
This enables a definition of $\bdqueuentxname$ by means of Hilbert's choice
operator applied on the set
$$\{q \mid
\bdqueuepredicate{q}{\nstate.tx.\sopbdpa}{\nstate.\nsreg.\textit{\regbdram}}\}$$
(the choice operator returns an arbitrary element of the set satisyfing the
predicate). Since this set contains only one element, a unique queue is returned
satisfying the predicate. The same approach is used for the reception queue.

The model of the NIC consists of 1500 lines of HOL4 code. Understanding the NIC
specification, experimenting with hardware, and implementing the model required
(roughly) three man-months of work. The NIC invariant consists of 650 lines of
HOL4 code and the proof consists of approximately 55000 lines of HOL4 code
(including comments). Identifying the invariant, formalizing it HOL4, defining
a suitable proof strategy, and implementing the proof in HOL4 required
(roughly) one man-year of work. Executing the proof scripts take approximately
45 minutes on a 2.93GHz Xeon(R) CPU X3470 with
16GB RAM.

%Model LOC (excluding comments): 1500
%Invariant LOC (excluding comments): 650
%Proof LOC (excluding model and invariant, but including comments): 55000
%Number of lemmas: 2200
%Manhours (rough estimation): One year or 2200 hours.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
