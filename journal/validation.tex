\section{Model Validation}\label{sec:validation}
This section considers the correctness of the NIC model. There are three
components that affect correctness: The model, the specification and the
hardware implementation of the NIC. If any of these components do not describe
the intended behavior, then the NIC model is most likely incorrect. For
instance, there could be a typo, logical error or ambiguity in the model or
specification, or there could be an error in the hardware.

To minimize inconsistency between the model and the specification, both the
model and the specification have been reviewed several times. In addition,
we studied the Linux NIC driver to clarify vague statements in the
specification. Still, there are some unknowns.
%
An example of an inconcistency in the specification is: One section states that
a certain BD flag is set of the SOP BD while another section states that the
flag is set in the EOP BD. To include all possibilities, the model is
non-deterministic, causing it to either set the flag in the SOP BD, in the EOP
BD or in both.

The effects of some operations are not completely specified. For instance, the
specification states that \linebreak[4] \regtxzerohdp\ becomes \pczero\ after the complete
transmission queue has been processed, but nothing is stated about how
\regtxzerohdp\ changes its value during transmission. The model describes this
behavior by setting \regtxzerohdp\ to a non-deterministic value distinct from
\pczero. The internal state component $\nstate.tx.\sopbdpa$, not accessible to
the CPU, is therefore introduced to record the head of the queue. If
\regtxzerohdp\ always contains the address of the head of the queue, then this
non-determinism and the state component $\nstate.tx.\sopbdpa$ would not be
needed.

Moreover, the specification includes instructions of how the NIC should be
configured. For instance, \linebreak[4] \regtxzerohdp\ shall be written with the address of
the first BD of a queue to be processed for transmission, but \regtxzerohdp\
should not be written when not \pczero. The NIC model enters an undefined
state (i.e., $\stateundefined$) when these
instructions are not followed. The model also enters an undefined state when
operations shall be performed that are unspecified or unclear. For example, the
specification does not state the effect of activating transmission while
transmission teardown is in progress.

To minimize inconcistency between the model and the actual hardware, the NIC has
been tested to observe how the NIC updates its registers and BD fields. For
example we discovered that the NIC sets the EOQ flag of the first unprocessed BD
during teardown, which is unstated in the specification. This behavior is
described non-deterministically by the model (the transitions from
$\tdstatenameseteoq$). In order to not inadvertently omit possible interleaving
between NIC and CPU operations, the NIC transitions are fine-grained: each NIC
transition describes a single  BD field write or memory byte access. Finally,
the verification exercised the model by means of a significant number of lemmas
(e.g., the queues shrink during transmission and reception).

Despite this conservative definition of the model, some inconsistencies were
found. The teardown automata does not check whether the BD to write is in
\regbdram. For our verification, this error is not critical since the NIC
invariant (c.f. Section~\ref{def:invariant}) guarantees that every BD is in
\regbdram. We also identified an inconsistency by analyzing a simplified model
of transmission with the NuSMV~\cite{cavada2005nusmv} model checker (a model
with a small address space and few BD fields to make the analysis feasible). The
order of the operations of transmission differs from the order that can be
inferred, via non-trivial reasoning, from the specification. This inconsistency
and non-trivial reasoning illustrate the challenge of manual modeling based on
informal specifications. This error is also not critical for the verification of
the invariant of Section~\ref{sec:proof} since it only affects the order of
transitions. The error may affect the formal verification of the NIC monitor,
since the order of transitions affect the synchronization between the CPU and
the NIC.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
