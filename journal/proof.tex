\section{Formal Verification of NIC Isolation}\label{sec:proof}
Our main verification goal is to identify a NIC configuration (state) that
isolates the NIC from certain memory regions. This means that the NIC can only
read and write certain memory regions, denoted by $\readable$ and $\writable$
respectively. We identify such a configuration by means of an invariant
$\nicinvariantname$ that is preserved by internal NIC transitions
($l \neq \lnread{\address}{v}$) and that restricts the set of accessed memory
locations:
\begin{theorem}
  \label{thm:main}
  $\nicinvariant \land
   \nstate \xrightarrow{l} \nstate' \land
   l \neq \lnread{\address}{v}$ implies
  \begin{enumerate}
    \item $\nicinvariantstatearg{\nstate'}$,
    \item $l = \ldread{\address} \implies \address \in \readable$, and
    \item $l = \lnwrite{\address}{v} \implies \address \in \writable$.
  \end{enumerate}
\end{theorem}

% \todo{This is probably clear from the introduction
% Theorem~\ref{thm:main} means that if such isolation is
% to be formally verified at system level, the main necessary work is to verify
% that the device driver establishes and preserves the invariant. Hence, the NIC
% invariant and Theorem~\ref{thm:main} serves as a base for ensuring that the NIC
% is isolated from certain memory regions in a computer using this NIC \cite{nicspec}.
% }

\subsection{Definition of the Invariant}
\label{def:invariant}
In order to facilitate the definition, the invariant of the NIC model is
split into several sub-invariants:
\begin{align*}
\nicinvariant \coloneqq\
\bigwedge_{i \in \{wd, qs, it, tx, rx\}} \invariant_i(\nstate, \readable, \writable)
\end{align*}

\subsubsection{Well-Defined State}
$\statedefinedinvariant{\nstate} \coloneqq \nstate \neq\ \stateundefined$ states that the NIC is in a defined state.
This ensures that the NIC cannot perform unspecified (arbitrary) operations
(transitions) that would potentially violate memory isolation.

\subsubsection{Disjoint Queues}
$\qsinvariantname$ states that when the transmission and reception automata are
active, no BD in the transmission queue overlaps a BD in the reception queue,
and vice versa (no byte in $\nstate.\nsreg.\textit{\regbdram}$ is used by both a
BD in the transmission queue and a BD in the reception queue):
\begin{alignat*}{2}
\qsinvariant{\nstate} \coloneqq\
&
	\nstate.tx.s \neq \txstatenameidle \land
	\nstate.rx.s \neq \rxstatenameidle
	\implies\\
&	\disjointqueues{\bdqueuentxstate{\nstate}}{\bdqueuenrxstate{\nstate}}
\end{alignat*}

The functions $\bdqueuentxname$ and $\bdqueuenrxname$ denote the list of the
addresses of the BDs in the transmission and reception queues of the NIC,
respectively: Let $\bdqueuestateaddress{\nstate}{a}$ denote the list of
addresses of the BDs in the queue starting at address $a$ in the state
$\nstate$ and $\bdqueuestateaddress{\nstate}{\pczero} = []$,  then
$\bdqueuentxstate{\nstate} = \bdqueuestateaddress{\nstate}{\nstate.tx.\sopbdpa}$,
and $\bdqueuenrxstate{\nstate} = \bdqueuestateaddress{\nstate}{\nstate.rx.\sopbdpa}$.

This invariant ensures that transmission and transmission teardown do not affect
nor are affected by the state components of the reception and reception teardown
automata, and vice versa. In particular this property guarantees that when the
transmission (reception) automaton writes into the transmission queue, it cannot
modify the content of the reception (transmission) queue, which would otherwise
potentially cause the reception automaton to violate memory isolation.

\subsubsection{Initialization}
$\itinvariantname$ implies that when initialization is complete (the
initialization automaton transitions from $\itstatenameinitializehdpcp$ to
$\itstatenameidle$), the transmission and reception automata are idle.
\begin{align*}
\itinvariant{\nstate} \coloneqq\
&	\nstate.it.s = \itstatenameinitializehdpcp
	\implies\\
&	\nstate.tx.s = \rxstatenameidle \land \nstate.rx.s = \rxstatenameidle
\end{align*}
Only the transmission and reception automata can perform internal transition
that may cause the NIC model to enter an undefined state or access memory.
Hence, $\itinvariantname$ implies that when initialization is complete, the
transmission and reception automata cannot perform such transitions, and that
$\txinvariantname$ and $\rxinvariantname$ hold vacously (see below for the
definition of $\txinvariantname$).

\subsubsection{Transmission} 
$\txinvariantname$ is split in into two conjuncts:

\begin{align*}
\txinvariant{\nstate} \coloneqq\
& (\nstate.tx.s \neq \txstatenameidle
   \implies\\
&  \ \ \txinvariantwelldefined{\nstate} \land \txinvariantmemoryreads{\nstate})\ \land \\
& (\nstate.tx.s = \txstatenameidle \land \nstate.tx.\currentbdpa \neq \pczero
   \implies\\
&  \ \ \disjointqueues{[\nstate.tx.\currentbdpa]}{\bdqueuenrxstate{\nstate}})
\end{align*}

The first conjunct applies when the transmission automaton is in a state from
which it can perform an internal transition and prevents these
transition from entering an undefined state or reading unreadable memory (to prevent the problems mentioned
in the first paragraph of Section~\ref{sec:overview}).

To prevent the transmission automaton from causing the NIC to enter
$\stateundefined$, $\txinvariantwelldefinedname$ consists of a number of
constraints, including:
\begin{itemize}
  \item The location of each BD in the transmission queue has a 4-byte aligned
   address in \regbdram.
  \item Each BD in the transmission queue is properly initialized. For instance,
   the OWN flag is set and the buffer length field is greater than zero.
  \item Each transmission BD is both a SOP and an EOP. To prevent the NIC model
   from entering $\stateundefined$, each SOP BD must have a matching EOP
   BD. Since Linux configures each transmission BD to be both a SOP and an EOP,
   this statement is stronger than necessary, but simplifies the proof of that
   $\txinvariantwelldefinedname$ is preserved.
  \item The currently processed BD is the head of the transmission queue
   ($\nstate.tx.\currentbdpa = \nstate.tx.\sopbdpa$). This
   statement is an invariant as a consequence of the previous statement of BDs
   being both SOP and EOP. This is mainly used to simplify the proof.
  \item No pair of BDs in the transmission queue overlap each other. This
   prevents the NIC from writing BD fields such that other BDs, processed in the
   future, get modified. This has two implications with respect to preventing
   transitions to $\stateundefined$ and reading unreadable memory: The NIC
   cannot modify (properly initialized) BDs fields that can cause transitions to
   $\stateundefined$ (e.g., SOP, EOP, buffer length), nor the buffer pointer
   field (initialized to address readable memory) to address unreadable memory.
  \item The transmission queue is not circular. If the queue is circular and the
   transmission automaton modifies fields of a BD, then that modified BD remains
   in the queue and may be processed again. That modification may cause the BD
   to violate $\txinvariantname$ (c.f. the second bullet of this list).
  \item The transmission queue is not empty if the transmission automaton is in
   a state where a BD is currently being processed or will be processed in the
   future
   ($\nstate.tx.s \neq \txstatenameidle \land
    \neg(\nstate.tx.s = \txstatenamewritecp \land
    \nstate.tx.\sopbdpa = \pczero)$). This is an example of a statement
   that is included in $\txinvariantname$ for the purpose of proving that
   $\txinvariantname$ is preserved.
\end{itemize}

To ensure that the transmission automaton only reads readable memory,
$\txinvariantmemoryreadsname$ requires that:
\begin{itemize}
\item Each BD in the transmission queue addresses the memory region $\readable$.
\item If the transmission automaton is in the frame fetching loop
($\nstate.tx.s = \txstatenameissuenextmemoryreadrequest\ \lor\
 \nstate.tx.s = \txstatenameprocessmemoryreadreply$), then the
state components used to compute the memory addresses do not cause overflow, and
the addresses of future memory read requests issued during the processing of the
current BD are in $\readable$
($\forall 0 \leq i < \nstate.tx.\numberofbufferbyteslefttorequest.\ \nstate.tx.\txmemoryaddress + i \in \readable$,
where \linebreak[4] $\nstate.tx.\numberofbufferbyteslefttorequest$ records the
number
of bytes left to read of the buffer addressed by the current BD, and
$\nstate.tx.\txmemoryaddress$ records the address of the next memory
read request; see Figure~\ref{fig:tx}).
\end{itemize}

The second conjunct of $\txinvariantname$ applies when the transmission
automaton is in a state from which it cannot perform an internal
transition. In these cases, the reception and the transmission
teardown automata may be in states  with enabled internal transitions. However, only the
reception automaton can perform internal transitions that potentially cause the
NIC model to enter an undefined state or writes unwritable memory (the reception
automaton does not read memory). Therefore, the transmission teardown automaton
must be restricted from affecting the reception automaton. Notice that it is sufficient to
restrict the transmission teardown automaton when the transmission
automaton is idle, since the former cannot perform transitions when the latter
is active. The transmission teardown automaton may affect the
reception automaton when it writes fields of the BD at
$\nstate.tx.\currentbdpa$ in \regbdram, because \regbdram contains the reception
queue. The transmission teardown automaton writes \regbdram\ only when
$\nstate.tx.\currentbdpa \neq \pczero$. Therefore, the second part of
$\txinvariantname$ requires that
the BD at address $\nstate.tx.\currentbdpa \neq \pczero$ is separated from each
(does not overlap any) BD in the reception queue (they do not share a byte
location in \regbdram).

\subsubsection{Reception}
The invariant for reception is similar to the invariant for transmission. The
main difference is the definition of $\invariant_{\textit{rx-wd}}$, since
reception BDs specify different properties than transmission BDs. Also, the
invariant states that BDs in the reception queue address buffers in $\writable$,
and that $\nstate.rx.\currentbdpa$ is disjoint from the transmission
queue.

\subsection{Proof of Theorem~\ref{thm:main}}
The proof of Theorem~\ref{thm:main}.2 and Theorem~\ref{thm:main}.2 are
straightforward. Transitions of the form
$\nstate \xrightarrow{\ldread{\address}} \nstate'$ occur only when
$\nstate.tx.s = \txstatenameissuenextmemoryreadrequest$, where
$\address = \nstate.tx.\txmemoryaddress$.
$\nstate.tx.s = \txstatenameissuenextmemoryreadrequest \implies
 \nstate.tx.\txmemoryaddress \in \readable$ is implied by
$\txinvariantmemoryreads{\nstate}$. Hence, the requested
address is readable: $\address \in \readable$. The proof of
Theorem~\ref{thm:main}.3 has the same structure but follows from
$\rxinvariant{\nstate}$.

Defining the invariant in terms of sub-invariants stating properties of
initialization, transmission or reception naturally leads the proof of
Theorem~\ref{thm:main}.1 to be described in terms of these three types of
actions the NIC performs: $\opvar \in \{it, tx, rx\}$. The labels of the
transitions describing one of these three types of actions are identified by
$L(\opvar)$, where:
\begin{itemize}
\item $L(it) \coloneqq \{\lnempty_{it}\}$
\item $L(tx) \coloneqq \{\lnempty_{tx}, \lnempty_{td}\} \cup \bigcup_{\address,v}\{\ldread{\address}, \ldwrite{\address}{v}\}$
\item $L(rx) \coloneqq \{\lnempty_{rx}, \lnempty_{rd}\} \cup
\bigcup_{\address,v}\{\lnwrite{\address}{v}\}$.
\end{itemize}

The following two lemmas formalize properties of the NIC model: Transitions of
an action do not modify state components of other actions; and an automaton can
leave the idle state only when the CPU writes a NIC register.
\begin{lemma}
  \label{lem:auto:state}
  For every $\opvar$, if  
  $\nstate \xrightarrow{l} \nstate'$ and
   $l \not \in L(\opvar)$
  then
  $\nstate'.\opvar = \nstate.\opvar$.
\end{lemma}

\begin{lemma}
  \label{lem:auto:leaveidle}
  For every $\autovar$, if
  $\nstate \xrightarrow{l} \nstate'$, 
   $\nstate.\autovar.s = \mathit{idle}$, and $
   \nstate'.\autovar.s \neq \mathit{idle}$
  then
  $l = \lnread{\address}{v}$.
\end{lemma}

Lemma~\ref{lem:auto:invariant} states that all transitions of each
action, $\opvar \in \{it, tx, rx\}$, preserve the corresponding invariant:
\begin{lemma}
  \label{lem:auto:invariant}
  For every $\opvar$, if
  $\nicinvariant$, $\nstate \xrightarrow{l} \nstate'$, and  $l \in L(\opvar)$
  then
  $\opinvariant{\opvar}{\nstate'} $ and $ \nstate' \neq\ \stateundefined$.
\end{lemma}

\begin{proof}
We sketch the proof for $\opvar = tx$, since reception is analogous and
initialization is straightforward. The transition $l$ belongs to the
transmission or the transmission tear down automaton. There are four cases
depending on whether $\nstate.tx.s$ and $\nstate'.tx.s$ are equal
to $\txstatenameidle$ or not.

\paragraph{Case 1}
$\nstate.tx.s = \txstatenameidle \land \nstate'.tx.s \neq \txstatenameidle$
cannot occur by Lemma~\ref{lem:auto:leaveidle}.

\paragraph{Case 2}
$\nstate.tx.s \neq \txstatenameidle \land \nstate'.tx.s \neq \txstatenameidle$
implies that the transition is performed by the transmission automaton
($l = \tau_{tx}$). We first analyze modifications of the transmission queue. The
transmission automaton can only modify the flags OWN and EOQ of the currently
processed BD (at $\nstate.tx.\currentbdpa$) and advance the head of the
transmission queue ($\nstatenext.tx.\sopbdpa = \nstate.tx.\currentbd.ndp$;
although not atomically). $\txinvariantwelldefined{\nstate}$ implies that the
current BD is the head of $\bdqueuentxstate{\nstate}$
($\bdqueuentxstate{\nstate} = [\nstate.tx.\currentbdpa] \cdot t$ for some
possibly empty tail $t$, where $\cdot$ denotes concatenation) and that the BDs
in $\bdqueuentxstate{\nstate}$ do not overlap. Therefore, the two flag
modifications do not alter the NDP fields of the current BD (at
$\nstate.tx.\currentbdpa$) nor the following BDs (at the addresses listed by
$t$) in $\bdqueuentxstate{\nstate}$. For this reason the transmission queue is
only either \emph{unmodified}
($\bdqueuentxstate{\nstatenext} = \bdqueuentxstate{\nstate}$) or \emph{shrinked}
($\bdqueuentxstate{\nstatenext} = t$), thereby implying
$\txinvariantwelldefined{\nstate'}$. Moreover, the BP fields are not modified
meaning that the buffers addressed by the BDs in $\bdqueuentxstate{\nstate'}$
are still located in $\readable$. Therefore $\txinvariantmemoryreads{\nstate'}$
holds. The modifications of OWN and EOQ of the current BD do not violate the
invariant, since the queue is acyclic, implying that the current BD (at
$\nstate.tx.\currentbdpa$) is not part of the new queue
($\bdqueuentxstate{\nstatenext} = t$).

We now analyze modifications of the state components that are used for address
calculations of the memory read requests, which are restricted by
$\txinvariantmemoryreads{\nstate}$. If the transition is from
$\txstatenamefetchnextbd$, then the automaton reads the current BD from
$\nstate.\nsreg.\textit{\regbdram}$, and assigns the read values to the record
$\nstatenext.tx.\currentbd$. $\txinvariantwelldefined{\nstate}$ implies that the
overflow restrictions are satisfied by the fetched BD and hence by the relevant
state components in $\nstate'$, and that the buffer addressed by the fetched BD
is in readable memory. These properties are preserved by transitions from
$\txstatenameprocessmemoryreadreply$ and
$\txstatenameissuenextmemoryreadrequest$.
	
\paragraph{Case 3}
$\nstate.tx.s \neq \txstatenameidle \land \nstate'.tx.s = \txstatenameidle$.
It must be shown that if $\nstatenext.tx.\currentbdpa \neq \pczero$, then
$\nstatenext.tx.\currentbdpa$ does not overlap any BD in
$\bdqueuenrxstate{\nstatenext}$. The only possible transition in this case is
made by the transmission automaton when $\nstate.tx.s = \txstatenamewritecp$.
Such transitions do not modify $\nstate.tx.\currentbdpa$, $\nstate.tx.\sopbdpa$,
$\nstate.\nsreg.\textit{\regbdram}$, nor $\nstate.rx$. Hence,
$\bdqueuentxstate{\nstatenext} = \bdqueuentxstate{\nstate}$ and
$\bdqueuenrxstate{\nstatenext} = \bdqueuenrxstate{\nstate}$, which are disjoint
by $\qsinvariant{\nstate}$. Since
$\nstatenext.tx.\sopbdpa = \nstatenext.tx.\currentbdpa \neq \pczero$ and
$\bdqueuentxstate{\nstatenext} = \bdqueuentxstate{\nstate}$, 
$\nstatenext.tx.\currentbdpa$ is the first element of
$\bdqueuentxstate{\nstatenext}$. Hence, $\nstatenext.tx.\currentbdpa$ does not
overlap any BD in $\bdqueuenrxstate{\nstatenext}$.

\paragraph{Case 4}
$\nstate.tx.s = \txstatenameidle \land \nstate'.tx.s = \txstatenameidle$. These
transitions are performed by the transmission tear down automaton
($l = \tau_{td}$), and only write fields of the BD at $\nstate.tx.\currentbdpa$
(provided $\nstate.tx.\currentbdpa \neq \pczero$) and set
$\nstate.tx.\currentbdpa$ to \pczero. The second conjunct of
$\txinvariant{\nstate}$ implies that the BD at $\nstate.tx.\currentbdpa$
does not overlap $\bdqueuenrxstate{\nstate}$, therefore
$\bdqueuenrxstate{\nstate} = \bdqueuenrxstate{\nstate'}$ and
$\txinvariant{\nstate'}$ holds.
\qed
\end{proof}

The following definitions, lemmas and corollaries are used to prove that each
action preserves the invariant of other actions and it does not cause the queues
to overlap. First, for each action $\opvar$, we introduce a relation on NIC
states, $\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$, with the
meaning that the invariant $\opinvariantname{\opvar}$ is preserved from
$\nstate$ to $\nstate'$. For initialization, the relation
$\automatastatespreserveinvariant{it}{\nstate}{\nstate'}$ states that the state
components of the initialization automaton are equal
($\nstate.it = \nstate'.it$) and that the transmission and reception automata
remain in their idle states
($\wedge_{\autovar \in \{tx, rx\}}
(\nstate.\autovar.s = \textit{idle} \implies \nstate'.\autovar.s = \textit{idle})$).
For $\opvar \in \{tx, rx\}$,
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$ states that the:
\begin{itemize}
\item state components of the corresponding automaton are equal:
$\nstate.\opvar = \nstate'.\opvar$.
\item locations of the corresponding queues are equal: \linebreak[4]
$\bdqueuenactstate{\opvar}{\nstate} = \bdqueuenactstate{\opvar}{\nstate'}$.
\item content of the corresponding queues are equal:
$\forall \address \in \bdqueuenactstate{\opvar}{\nstate} . \ 
 \getbd{\nstate}{\address} = \getbd{\nstate'}{\address}$,
where $\in$ denotes list membership and $\getbd{\nstate}{\address}$ is a record
with its fields set to the values of the corresponding fields of the BD at
address $\address$ in the state $\nstate$.
\item other queue is not expanded:
$\forall \address.\ \address \in
\bdqueuenactstate{\opvar'}{\nstate'} \implies \address \in
\bdqueuenactstate{\opvar'}{\nstate}$, where $\opvar' = tx$ if $\opvar=rx$ and
$\opvar' = rx$ if $\opvar=tx$.
\end{itemize}

The following Lemma states that
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}$ indeed preserves
the corresponding invariant $\opinvariantname{\opvar}$:
\begin{lemma}
  \label{lem:inv:pres:automaton}
  For every $\opvar$, if
  $\opinvariant{\opvar}{\nstate}$ and $
   \automatastatespreserveinvariant{\opvar}{\nstate}{\nstate'}
   $ then $ 
   \opinvariant{\opvar}{\nstate'}$.
\end{lemma}

To complete the proof we introduce a relation for every action $\opvar$,
$\subqueue{\opvar}{\nstate}{\nstate'}$, which formalizes that the location of
the corresponding queue is unmodified and that all bytes outside the queue are
unmodified:
\begin{align*}
& \subqueue{\opvar}{\nstate}{\nstate'} \coloneqq\\
& \qquad (\forall \address \in \bdqueuenactstate{\opvar}{\nstate}.\ 
   \getbd{\nstate}{\address}.ndp = \getbd{\nstate'}{\address}.ndp)\ \land\\
& \qquad (\forall \address \not \in \queuebyteaddresses{\bdqueuenactstate{\opvar}{\nstate}}.\\
& \qquad \qquad \nstate.\nsreg.\textit{\regbdram}(\address) = \nstate'.\nsreg.\textit{\regbdram}(\address))
\end{align*}
(where $\queuebyteaddresses{\bdqueuenactstate{\opvar}{\nstate}}$ is the set of
byte addresses of the BDs in $\bdqueuenactstate{\opvar}{\nstate}$, and the
imaginary ``initialization-queue'' is defined to be empty:
$\bdqueuenitstate{\nstate} \coloneqq []$). The following Lemma states that
each action preserves this relation, provided that the corresponding invariant
holds in the pre-state:
\begin{lemma}
  \label{lem:queue:equal}
    For every $\opvar$, if
  $\opinvariant{\opvar}{\nstate}$, $\nstate \xrightarrow{l} \nstate'$
  and $l \in L(\opvar)$
   then
   $\subqueue{\opvar}{\nstate}{\nstate'}$.
\end{lemma}
\begin{proof}
This is immediate for initialization since the initialization automaton does not
modify $\nstate.\nsreg.\textit{\regbdram}$.

For transmission and reception, the first conjunct of
$\subqueue{\opvar}{\nstate}{\nstate'}$ holds since the corresponding automaton
does not modify the NDP fields of the BDs in
$\bdqueuenactstate{\opvar}{\nstate}$, and $\bdqueuenactstate{\opvar}{\nstate}$
contains no overlapping BDs (by $\opinvariant{\opvar}{\nstate}$). The second
conjunct holds since the automaton assigns only fields of BDs in
$\bdqueuenactstate{\opvar}{\nstate}$ (by $\opinvariant{\opvar}{\nstate}$).
\qed
\end{proof}

The next Lemma states that each action either shrinks the corresponding queue or
does not modify its location:
\begin{lemma}
  \label{lem:queue:head}
    For every $\opvar$, if
  $\opinvariant{\opvar}{\nstate}$, $\nstate \xrightarrow{l} \nstate'$,
  and 
  $
  l \in L(\opvar)$
  then $\exists q.\ \bdqueuenactstate{\opvar}{\nstate} = q \cdot \bdqueuenactstate{\opvar}{\nstate'}$.
\end{lemma}

\begin{proof}
$\opinvariant{\opvar}{\nstate}$ implies that
$\bdqueuenactstate{\opvar}{\nstate}$ contains no overlapping BDs. In addition,
no automaton assigns an NDP field of a BD. Therefore, no automaton can change
the location of the BDs in its queue. If the state component identifying the
head of $\bdqueuenactname{\opvar}$ ($\nstate.tx.\sopbdpa$ in the case
$\opvar = tx$) is not modified, the location of the queue is not modified; and
if that state component is modified, then it is set to either \pczero\ (emptying
the queue), or to the next BD which is a member of
$\bdqueuenactstate{\opvar}{\nstate}$ (by $\opinvariant{\opvar}{\nstate}$;
shrinking the queue).
\qed
\end{proof}

We finally show that each action preserves the invariant of the other actions
via a corollary:
\begin{corollary}
  \label{cor:automaton:equal}
   For every $\opvar \neq \opvar'$, if
   $\nicinvariant$,
   $\nstate \xrightarrow{l} \nstate'$, and $
   l \in L(\opvar)$ then
   $\automatastatespreserveinvariant{\opvar'}{\nstate}{\nstate'}$.
\end{corollary}
\begin{proof}
  Assume $\opvar \in \{tx, rx\}$ and $\opvar' = it$.
  Lemma~\ref{lem:auto:state} gives
  $\automatastatespreserveinvariant{\opvar'}{\nstate}{\nstate'}$, and
  Lemma~\ref{lem:auto:leaveidle} gives
  $\nstate.\autovar.s = \textit{idle} \implies
  \nstate'.\autovar.s = \textit{idle}$ for $\autovar \in
  \{tx, rx\}$.
  Therefore,
  $\automatastatespreserveinvariant{it}{\nstate}{\nstate'}$ holds.

  If $\opvar = it$ then the transition is performed by the initialization
  automaton, which does not modify $\nstate.tx$, $\nstate.rx$ (by
  Lemma~\ref{lem:auto:state}), nor $\nstate.\nsreg.\textit{\regbdram}$.
  Therefore $\bdqueuentxname$ and $\bdqueuenrxname$ are unchanged.
  
  If $\opvar = tx$ and $\opvar' = rx$, then
  Lemma~\ref{lem:auto:state}, Lemma~\ref{lem:queue:equal}, and
  Lemma~\ref{lem:queue:head} imply
  $\automatastatespreserveinvariant{rx}{\nstate}{\nstate'}$. The same reasoning  
  applies for $\opvar = rx$ and $\opvar' = tx$.
\qed
\end{proof}

\begin{corollary}
  \label{cor:automaton:inv}
   For every $\opvar \neq \opvar'$, if
   $\nicinvariant$, 
   $\nstate \xrightarrow{l} \nstate'$, and 
   $l \in L(\opvar)$ then
   $\opinvariant{\opvar'}{\nstate'}$.
\end{corollary}
\begin{proof}
Follows from Corollary~\ref{cor:automaton:equal} and
Lemma~\ref{lem:inv:pres:automaton}.
\qed
\end{proof}

\begin{corollary}
  \label{cor:queue:head}
   For every $\opvar$, if
   $\nicinvariant$, 
   $\nstate \xrightarrow{l} \nstate'$, and 
   $l \in L(\opvar)$ then
   $\qsinvariant{\nstate'}$.
\end{corollary}
\begin{proof}
Lemma~\ref{lem:queue:equal}, Lemma~\ref{lem:auto:state} and
$\qsinvariant{\nstate}$ imply that an action
cannot modify the queue of another action. This property, Lemma~\ref{lem:queue:head}, and
$\qsinvariant{\nstate}$, imply that the queues remain disjoint.
\qed
\end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: