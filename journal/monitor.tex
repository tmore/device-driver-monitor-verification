\section{NIC Monitor}
\label{sec:monitor}
%The explanations of the handlers are based on the code in
%https://bitbucket.org/jhagl/indented_sth_comp2.6_exec3.10/src/j_monitor/core/hw/soc/am33xx/soc_cpsw.c

This section describes the NIC monitor of Figure~\ref{fig:monitor}. The top-level
function $\monitorfunction$ of the monitor is invoked when Linux
attempts to write the 4-byte word value $\monitorargvalue$ to the NIC register
at the physical address $\monitorargpa$. Physical addresses are used
instead of virtual addresses to make the monitor independent of the virtual
address map. Function $\monitorfunctionname$ enforces 4-byte alignment
of the address, to ensure that exactly one register is accessed. The function checks which NIC register is located at
$\monitorargpa$ and invokes the corresponding handler.
Each handler performs the write if it preserves the NIC invariant
$\nicinvariantname$. Each handler returns $\pcfalse$ if the write can
violate the NIC invariant. The handler's return value
is used by the hypervisor to take a suitable action in case Linux does
something suspicious.

The monitor uses the following data structures to track the state of the NIC:
\begin{itemize}
\item $\minitialized$ is a boolean variable indicating that the NIC
  is initialized.
\item $\mclearedinitreg[p]$ is an array of booleans indicating if
  the register $p$ has been cleared during the initialization procedure, where
  $p$ ranges over the four transmission and reception HDP and CP registers.
\item $\mtxzerotearingdown$, $\mrxzerotearingdown$ are booleans
  indicating if the NIC is performing a teardown operation.
\item $\mtxactivequeue$, $\mrxactivequeue$ are pointers to the head of
  the NIC queues.
\item $\mactivecppiram[a]$ is a mask indicating if the
  word of \linebreak[4] \regbdram\ at address $a$ stores is part of a BD that
  reachable from $\mtxactivequeue$ or $\mrxactivequeue$. This masks is used to
  optimize checks of writes to \regbdram.
\end{itemize}

Hereafter we describe the main functions of the monitor.

\subsection{Subroutine $\monitornameupdateactivequeue$}

\begin{figure}[t]
\centering
\includegraphics[scale=0.605]{monitor_data_structures_new3}
\caption{BDs in \regbdram\ are marked in grey. Each black square of
  $\mactivecppiram$ represents a location that the monitor considers in
  use for BDs.
}
\label{fig:activecppiram}
\end{figure}

The datastructures $\mtxactivequeue$, $\mrxactivequeue$ and $\mactivecppiram$ must be
periodically updated by the monitor to ``release'' the BDs that have been
processed by the NIC. This ``garbage collection'' is performed by the subroutine
$\monitornameupdateactivequeue$. The argument of the subroutine can be
\pctx\ or \pcrx\ to indicate with queue must be analyzed. We describe
the behavior for \pctx, since the case for \pcrx\ is analogous.
If the register \regtxzerocp\ is \pctdiacnumber, then the NIC has finished transmission
teardown, meaning that transmission is idle and the
corresponding queue is empty. In this case,
$\monitornameupdateactivequeue$ traverses the BDs starting at
$\mtxactivequeue$/$\mrxactivequeue$, unmarks each corresponding
entries in $\mactivecppiram$, and sets $\mtxactivequeue$
to \pczero. Otherwise, this traversal is done up to the first BD whose OWN
flag is set, and $\mtxactivequeue$ is set to the address of that BD.

Figure~\ref{fig:activecppiram} illustrates this process.
Each state is represented by two columns, one column for the NIC state
and one column for the monitor's state.
In the first state (columns 1 and 2), the NIC has transmission and
reception queues, whose start addresses are identified by the
internal NIC variables $tx\_p$ and $rx\_p$ (denoted in
the NIC model by $\nstate.tx.\sopbdpa$ and
$\nstate.rx.\sopbdpa$).
The start locations of the queues are recorded by the monitor variables
$\mtxactivequeue$ and $\mrxactivequeue$ and the addresses used by
these queues marked in $\mactivecppiram$.
%
The
second state (columns 3 and 4) shows the result of the NIC
transmitting the first two BDs. The internal NIC variable $tx\_p$ is
advanced to address the third BD in the
transmission queue. The monitor's variable $\mtxactivequeue$ is now lagging behind the
transmission queue and $\mactivecppiram$ marks some BDs that have been
already processed. However, $\mtxactivequeue$
still identifies a queue of which the transmission queue is a suffix.
The execution of $\monitornameupdateactivequeue$ collects these BDs,
by updating $\mtxactivequeue$ to point to the
current head of the transmission queue and unmaking from
$\mactivecppiram$ the traversed BDs.

\subsection{Handler $\monitorfunctionnamereset$ (Figure~\ref{fig:resetcode})}

\begin{figure}[t]
\begin{algorithmic}
\Function{bool : $\monitorfunctionnamereset$}{$\monitorargvalue$ : \pcwordtype}
	\If{$\monitorargvalue = \pczero$}
		\State return \pctrue
	\ElsIf {$\neg \minitialized\ \lor \mtxzerotearingdown\ \lor \mrxzerotearingdown$}
		\State return \pcfalse
	\Else
		\For{$p \in \{\pctxhdpid, \pcrxhdpid, \pctxcpid, \pcrxcpid\}$} \State $\mclearedinitreg[p] \coloneqq \pcfalse$
		\EndFor
        \State $\minitialized \coloneqq \pcfalse$
		\State $\regcpdmasoftreset \coloneqq \pcone$
		\State return \pctrue
	\EndIf
\EndFunction
\end{algorithmic}
\caption{Pseudocode of the handler for writes to \regcpdmasoftreset.}
\label{fig:resetcode}
\end{figure}

This handler is normally invoked when Linux attempts to trigger the reset operation of the
NIC by writing \pcone\ to \regcpdmasoftreset.

If the value to write to \regcpdmasoftreset\ is \pczero, then the monitor
accepts the request because this operation has no
effect. Otherwise, the monitor checks whether the NIC is being currently initialized
($\neg \minitialized$), is tearing down an operation ($\mtxzerotearingdown$ or
$\mrxzerotearingdown$). If so, the request is rejected, since the
effect of writing \regcpdmasoftreset\ while the NIC is performing any of these
operations is unspecified. If the checks succeed, \pcone\ is written to
\regcpdmasoftreset\ to start the reset operation. In addition, the data
structures tracking the initialization procedure of the NIC are set to \pcfalse.

\subsection{Handlers $\monitorfunctionnametxhdp$ (Figure~\ref{fig:txhdpcode}) and $\monitorfunctionnamerxhdp$ }

\begin{figure}[t]
\begin{algorithmic}
\Function{bool : $\monitorfunctionnametxhdp$}{$\monitorargvalue$ : \pcwordtype}
	\If{$\neg \minitialized$}
		\If{$\regcpdmasoftreset = \pcone \lor \monitorargvalue \neq \pczero$}
			\State return \pcfalse
		\Else
			\State $\regtxzerohdp \coloneqq \pczero$
			\State $\mclearedinitreg[\pctxhdpid] \coloneqq \pctrue$
            \State $\monitorupdateactivequeue{\pctx}$
			\If{$\bigwedge_p \mclearedinitreg[p]$}
				\State $\minitialized \coloneqq \pctrue$
			\EndIf
			\State return \pctrue
		\EndIf
	\Else
		\If{\regtxzerohdp\ $\neq \pczero \lor \mtxzerotearingdown$}
			\State return \pcfalse
		\Else
			\State $\monitorupdateactivequeue{\pctx}$
			\If{$\monitorisqueuesecure{\monitorargvalue}{\pctx}$}
				\State $\monitorpreparequeue{\monitorargvalue}{\pctx}$
				\State $\monitordecrementrhonicupdatealphaqueue{\monitorargvalue}{\pctx}$
				\State $\regtxzerohdp \coloneqq \monitorargvalue$
				\State return \pctrue
			\Else
				\State return \pcfalse
			\EndIf
		\EndIf
	\EndIf
\EndFunction
\end{algorithmic}
\caption{Pseudocode of the handler for writes to \regtxzerohdp.}
\label{fig:txhdpcode}
\end{figure}

The handler $\monitorfunctionnametxhdp$ is invoked either during
initialization (to clear \regtxzerohdp\ by writing \pczero) or to start
transmission (by writing the address of the first BD of the new queue).

Clearing \regtxzerohdp\ is allowed only if the NIC is currently being
initialized ($\neg \minitialized$), the internal reset operation has been
completed ($\regcpdmasoftreset = \pczero$), and the attempted write clears
the register ($\monitorargvalue = \pczero$). If these conditions are satisfied,
it is recorded that \regtxzerohdp\ has been initialized. If all HDP and CP
registers have been cleared then the initialization is complete. Therefore
$\monitornameinitializationperformed$ sets $\minitialized$ to \pctrue, and
clears $\mtxactivequeue$, $\mrxactivequeue$ and $\mactivecppiram$ to records
that there is no BD in use by the NIC.

Starting transmission is allowed only if the NIC has been initialized
($\minitialized$), transmission teardown is not being performed
($\neg \mtxzerotearingdown$), and \regtxzerohdp\ is \pczero. This means that the
NIC is not transmitting and the transmission queue is empty. If these conditions
are satisfied, $\monitornameupdateactivequeue$ is invoked to garbage
collect old transmission BDs. Notice that
$\monitornameupdateactivequeue$ 
sets $\mtxactivequeue$ to \pczero\ since \regtxzerohdp\ is \pczero. Then $\monitornameisqueuesecure$ checks that
the transmission queue starting at $\monitorargvalue$ is secure. Namely the
following conditions must be satisfied:
\begin{enumerate}
\item BDs are located at 4-byte aligned addresses in \linebreak[4] \regbdram, and do not
overlap the transmission or reception queues (the former is empty since \regtxzerohdp\ = \pczero). The latter condition is checked by a lookup in
$\mactivecppiram$.
\item No pairs of BDs overlap.
\item Each BD is well-formed (e.g., each BD is both SOP and EOP, and the OWN
flag is cleared).
\item BDs address only readable memory.
\end{enumerate}

If these conditions are satisfied, $\monitornamepreparequeue$ sets the
OWN flag and clears the EOQ flag of each BD of the new queue. Then
$\monitornamedecrementrhonicupdatealphaqueue$ sets $\mtxactivequeue$ to the
address of the first BD of the new queue ($\monitorargvalue$) and marks the
entries of the new BDs in $\mactivecppiram$. Finally, the monitor starts
transmission by writing the address of the first BD to \regtxzerohdp.

\subsection{Handler $\monitorfunctionnamenicmemory$ (Figure~\ref{fig:nicmemorycode})}

\begin{figure}[t]
\begin{algorithmic}
\Function{bool : $\monitorfunctionnamenicmemory$}{$\monitorargpa$ : \pcwordtype, $\monitorargvalue$ : \pcwordtype}
    \State $\monitorupdateactivequeue{\pctx}$
    \State $\monitorupdateactivequeue{\pcrx}$
	\State
	\If{$\neg \mactivecppiram[\monitorargpa]$}
		\State $\monitoraddressspacemonitorargpa \coloneqq \monitorargvalue$
		\State return \pctrue
	\EndIf
	\State
	\State $q\_access\_type \coloneqq \typeofcppiramaccess{\monitorargpa}$
	\If{$q\_access\_type \in \{\pctx, \pcrx\}$}
		\If{$\monitorisqueuesecure{\monitorargvalue}{q\_access\_type}$}
			\State $\monitorpreparequeue{\monitorargvalue}{q\_access\_type}$
			\State $\monitordecrementrhonicupdatealphaqueue{\monitorargvalue}{q\_access\_type}$
			\State $\monitoraddressspacemonitorargpa \coloneqq \monitorargvalue$
			\State return \pctrue
		\Else
			\State return \pcfalse
		\EndIf
	\Else
		\State return \pcfalse
	\EndIf
\EndFunction
\end{algorithmic}
\caption{Pseudocode of the handler for writes to \regbdram.}
\label{fig:nicmemorycode}
\end{figure}

The handler $\monitorfunctionnamenicmemory$ is normally invoked in two
situations. The first case is when Linux is initializing some fields
of a new BD that will be later given to the NIC (either as an extension of an
existing queue, or as a new queue by writing \regtxzerohdp\ or \regrxzerohdp).
The second situation is when Linux is attempting to extend a queue by writing the NDP field
of the last BD of the queue.
The handler always uses $\monitornameupdateactivequeue(\pctx)$ and
$\monitornameupdateactivequeue(\pcrx)$ to garbage collect transmission
and reception BDs.

In the first situation, the address of the BD to initialize cannot be already in use
by the NIC, hence $\mactivecppiram[pa]$ must
be unmarked. In this case, the monitor updates \regbdram, by writing
$\monitorargvalue$ in the address $\monitorargpa$ ($\monitornameaddressspace[pa] \coloneqq v$ in Figure~\ref{fig:nicmemorycode}).

In the second situation ($\mactivecppiram[\monitorargpa]$) the
attempted write targets a BD in use by the NIC.
Function $\typeofcppiramaccessname$ traverses the
queues starting at $\mtxactivequeue$ and $\mrxactivequeue$ to \linebreak[4]
check if the attempted write addresses the NDP field of the last BD of the
transmission or reception queue
($\typeofcppiramaccess{\monitorargpa} \in \{\pctx, \pcrx\}$). In this
case, the monitor performs the same operations as in the handlers
$\monitorfunctionnametxhdp$ and $\monitorfunctionnamerxhdp$ (depending on
whether the transmission or reception queue is to be extended), with
the exception that \regbdram\ is written instead of \regtxzerohdp\ and \regrxzerohdp.
If the attempted write targets any other part of the queues (i.e., the NDP field
of the corresponding BD is not \pczero\ or $\monitorargpa$ points to
other fields of an existing BD), then Linux is attempting to
modify a BD that is currently in use by the
NIC. This operation is forbidden by the monitor, irrespectively of whether it preserves the security conditions.

\subsection{Handlers $\monitorfunctionnametxcp$ (Figure~\ref{fig:txzerocpcode}) and $\monitorfunctionnamerxcp$}

\begin{figure}[t]
\begin{algorithmic}
\Function{bool : $\monitorfunctionnametxcp$}{$\monitorargvalue$ : \pcwordtype}
	\If{$\neg \minitialized$}
		\If{$\regcpdmasoftreset = \pcone \lor \monitorargvalue \neq \pczero$}
			\State return \pcfalse
		\Else
			\State $\regtxzerocp \coloneqq \pczero$
			\State $\mclearedinitreg[\pctxcpid] \coloneqq \pctrue$
			\If{$\bigwedge_p \mclearedinitreg[p]$}
				\State $\minitialized \coloneqq \pctrue$
			\EndIf			
			\State return \pctrue
		\EndIf
	\Else
		\State $\monitorupdateactivequeue{\pctx}$
		\If{$\regtxzerocp = \pctdiacnumber$}
			\State $\mtxzerotearingdown \coloneqq \pcfalse$
			\State return \pctrue
		\Else
			\State return \pcfalse
		\EndIf
	\EndIf
\EndFunction
\end{algorithmic}
\caption{Pseudocode of the handler for writes to \regtxzerocp.}
\label{fig:txzerocpcode}
\end{figure}

The handler is invoked in two situations.
In the first case Linux is attempting to 
clear and initialize \regtxzerocp\ and
the monitor behave analogously to the case of clearing 
\regtxzerohdp\ for  $\monitorfunctionnametxhdp$.
In the second case, the handler is only used to detect the 
completion of  a  transmission teardown. The monitor releases 
the transmitted BDs and updates $\mtxzerotearingdown$ if transmission teardown has been
completed.

The handler $\monitorfunctionnamerxcp$ operates in the same way, but with respect to
reception instead of transmission.

\subsection{Handlers $\monitorfunctionnametxtd$ (Figure~\ref{fig:txtdcode}) and $\monitorfunctionnamerxtd$}
\label{sec:txtd}
\begin{figure}[t]
\begin{algorithmic}
\Function{bool : $\monitorfunctionnametxtd$}{$\monitorargvalue$ : \pcwordtype}
	\If{$\neg \minitialized \lor \mtxzerotearingdown$}
		\State return \pcfalse
	\EndIf
		\State $\mtxzerotearingdown \coloneqq \pctrue$
		\State $\text{\regtxteardown} \coloneqq \pczero$
		\State return \pctrue
\EndFunction
\end{algorithmic}
\caption{Pseudocode of the handler for writes to \regtxteardown.}
\label{fig:txtdcode}
\end{figure}

This handler is invoked when Linux attempts to teardown transmission by writing
\pczero\ to \regtxteardown. It is unspecified to initiate a transmission teardown operation while
the NIC is currently being initialized ($\minitialized$) or is already performing
a transmission teardown operation ($\mtxzerotearingdown$).
If the teardown request is accepted, \pczero\ is written to \regtxteardown\ to activate a teardown and
$\mtxzerotearingdown$ is set to \pctrue.
The handler $\monitorfunctionnamerxtd$ is identical but handles to reception instead of
transmission.

\subsection{Default handler}
The default handler simply prevents writes to all other NIC registers,
which are not used by the Linux NIC driver.

%\subsubsection{$\monitorfunctionnamedmacontrol$}
%\regdmacontrol\ is used to configure how the NIC updates certain BD fields. A
%value of \pczero\ is the "standard" behavior of the NIC, and which is the only
%value that is permitted to be written (which is consistent with how the Linux
%NIC driver uses \regdmacontrol) to simplify verification of the monitor. Hence,
%this handler does not write any NIC register.
%
%\subsubsection{$\monitorfunctionnamerxbufferoffset$}
%\regrxbufferoffset\ is used to configure how many leading bytes the NIC shall
%skip when writing a received frame to a reception data buffer. For simplicity,
%only \pczero\ is allowed to be written (consistent with the Linux NIC driver).
%Hence, this handler does not write any NIC register.
%
%\subsubsection{$\monitorfunctionnamestateram$}
%The NIC has eight transmission DMA channels, and eight reception DMA channels.
%The Linux NIC driver uses only one transmission and one reception DMA
%channel, but does initialize the HDP and CP registers for all channels. Since
%the hypervisor initializes the NIC during boot, and these channels are never
%used, writes to these HDP and CP registers are not necessary. Therefore, this
%handler does not write any register.
%
%\subsubsection{Non-Critical Register Accesses}
%If Linux attempts to write a register that does not affect which memory accesses
%the NIC performs (but the address of the register belongs to the same page that
%other critical registers belong to, meaning that the non-critical register is
%mapped as non-writable), a special handler is invoked that performs the
%attempted write without performing any checks.

\section{Correctness of the NIC Monitor}\label{sec:monproof}
This section presents a semi-formal analysis of the correctness of the monitor.
Independently of the argument values $\monitorargvalue$ and $\monitorargpa$
given to $\monitorfunction$, $\monitorfunctionname$ should preserve
$\nicinvariantname$. We analyze each handler individually. Since $\nicinvariantname$ only depends on
the NIC state, the monitor can only violate $\nicinvariantname$ by writing the
NIC registers (c.f. rule in Section~\ref{sec:nic} involving NIC
transitions with the label $\lnreadname$).

\subsection{Monitor Invariant}
Clearly correctness of the monitor depends on its data structures correctly tracking the state of the NIC. This
property is formulated as an invariant
$\monitorinvariantname(\monitorstate,\nstate)$, which relates the state (data
structures) of the monitor $\monitorstate$ with the state of the NIC. The most
important parts of $\monitorinvariantname$ are:
\begin{itemize}
\item $\minitializedinvariant{\monitorstate}{\nstate}$:
 $\minitialized = \pctrue$ if and only if the NIC is initialized.
\item $\mclearedinitreg[p] = \pctrue$ if and only if the corresponding HDP/CP
 register $p$ has been initialized/cleared during the current initialization.
\item $\mtxzerotearingdowninvariant{\monitorstate}{\nstate}$: If the NIC is
 performing a transmission teardown then $\mtxzerotearingdown = \pctrue$. A
 corresponding invariant holds for reception teardown.
\item $\mtxactivequeueinvariant{\monitorstate}{\nstate}$: The transmission \linebreak[4] queue
of the NIC $\bdqueuentxstate{\nstate}$ is a suffix of the transmission queue of the monitor $\bdqueuemtxstatestate{\monitorstate}{\nstate}$, where \linebreak[4]
$\bdqueuemtxstatestate{\monitorstate}{\nstate} \coloneqq \bdqueuestateaddress{\nstate}{\monitorstate.\mtxactivequeue}$.
 A corresponding invariant holds for the reception queue.
\item $\forall a \in
  \queuebyteaddresses{
    \bdqueuemtxstatestate{\monitorstate}{\nstate}
  } \cup
  \queuebyteaddresses{
    \bdqueuemrxstatestate{\monitorstate}{\nstate}
  }
  .\\
\ \ \ \ \monitorstate.\mactivecppiram[\textit{word}(a)]$:\todo{fix?} $\mactivecppiram$
 marks which words of \regbdram\ that store BDs reachable from
 $\mtxactivequeue$ or $\mrxactivequeue$. $\queuebyteaddresses{bds}$ denotes the
 set of byte addresses of the bytes of the BDs in the queue $bds$, and
 $\textit{word}(a)$ denotes the word-aligned address of the word
 containing the byte located at address $a$.
\end{itemize}

In the following we do consider each handler to be executed
atomically. A formal proof for the monitor would require to show that
transitions of the NIC interleaved with the monitor operation can be
reordered without affecting the preservation of the invariant.
\subsection{Subroutine $\monitornameupdateactivequeue$ preserves $\nicinvariantname \land \monitorinvariantname$}
The subroutine $\monitornameupdateactivequeue$ does not write any
NIC register, hence 
$\nstate = \nstatenext$. The subroutine only affects $\mtxactivequeue$
(if argument is \pctx), $\mrxactivequeue$ (if argument is \pcrx) and
$\mactivecppiram$. As usual we analyze the case for transmission,
since the reception case is similar. There are two
possible scenarios depending on whether $\monitornameupdateactivequeue$ reads
\pctdiacnumber\ from \regtxzerocp.

\paragraph{Case 1} If \regtxzerocp = \pctdiacnumber\ then 
all BDs in \linebreak[4] $\bdqueuemtxstatestate{\monitorstate}{\nstate}$ are unmarked
and $\mtxactivequeue$ is set to \pczero, implying
$\bdqueuemtxstatestate{\monitorstatenext}{\nstate} = []$. This case
can only happen after \linebreak[4] that transmission teardown automaton has performed the transition from
$\tdstatenamewritecp$ to $\tdstatenameidle$ which means \linebreak[4]
$\nstate.tx.\sopbdpa = \pczero$, hence $\bdqueuentxstate{\nstate} =
[]$.

\paragraph{Case 2} If \regtxzerocp $\neq$ \pctdiacnumber\ then $\monitornameupdateactivequeue$ sets $\mtxactivequeue$ to
the address of the first BD reachable from $\monitorstate.\mtxactivequeue$ and
whose OWN flag is set. $\txinvariantwelldefinedname$ states that all BDs in
$\bdqueuentxstate{\nstate}$ have their OWN flag set. Since
$\monitornameupdateactivequeue$ advances $\monitorstate'.\mtxactivequeue$ to the
first BD in the queue with the OWN flag set, $\bdqueuentxstate{\nstatenext}$
remains a suffix of
$\bdqueuemtxstatestate{\monitorstatenext}{\nstate}$. Also, by the
separation of the transmission and reception queue, we can infer that
the traversed BDs are not part of the reception queue. Therefore they
can be safely be unmarked in  $\mactivecppiram$.

\subsection{Handler $\monitorfunctionnamereset$ preserves $\nicinvariantname \land \monitorinvariantname$}
Note that this function affects the data structures of the monitor or the NIC
state only if $\monitorargvalue \neq \pczero$, $minitialized$,
$\neg \mtxzerotearingdown$, and $\neg \mrxzerotearingdown$. In this case,
$\monitorinvariantprestate$ ensures that the initialization and tear down
automata are in the states $\itstatenameidle$. Writing \pcone\ to
\regcpdmasoftreset\ in this case causes the initialization automaton to enter
the state $\itstatenamereset$. Since $\minitialized$ and $\mclearedinitreg$ are
set to $\pcfalse$ (NIC nor any HDP or CP registers are initialized),
$\monitorinvariantname$ is preserved. Regarding $\nicinvariantname$, only
$\statedefinedinvariantname$ and $\itinvariantname$ are relevant.
$\itinvariantname$ is preserved since
$\nstatenext.it.s = \itstatenamereset \neq \itstatenameinitializehdpcp$.
$\statedefinedinvariantname$ is preserved since writing \regcpdmasoftreset\
causes the NIC model to enter an undefined state only when the teardown or
initialization automata are not idle.

\subsection{Handler $\monitorfunctionnametxhdp$ preserves $\nicinvariantname \land \monitorinvariantname$}
There are two cases where $\monitorfunctionnametxhdp$ affects the states of the
monitor data structures or the NIC state:
\begin{enumerate}
\item $\neg \minitialized$, $\text{\regcpdmasoftreset} = \pczero$, and
 $\monitorargvalue = \pczero$: \regtxzerohdp\ is cleared during initialization.
\item $init$, $\regtxzerohdp = \pczero$, $\neg \mtxzerotearingdown$, and
 $\monitorisqueuesecure{\monitorargvalue}{\pctx}$: \regtxzerohdp\ is written with
 $\monitorargvalue$ to start transmission of a secure queue with the first BD at
 address $\monitorargvalue$.
\end{enumerate}

\paragraph{Case 1}
$\neg \monitorstate.\minitialized$,
$\nstate.\nsreg.\text{\regcpdmasoftreset} = \pczero$ and
$\monitorinvariantprestate$ imply $\itstateeqinitializehdpcpprestate$. Writing
\pczero\ to \regtxzerohdp\ \linebreak[4] when the initialization automaton is in this state
\linebreak[4] means that \regtxzerohdp\ has been initialized, and therefore
$\mclearedinitreg[\pctxhdpid]$ is set to \pctrue. If all other HDP \linebreak[4] and CP
registers have aalso been initialized (i.e., \linebreak[4]
$\bigwedge_{p \neq \pctxhdpid} \mclearedinitreg[p]$ as implied by
$\monitorinvariantprestate$) then initialization is complete. Hence,
$\nstatenext.it.s = \itstatenameidle$, and \minitialized\ is therefore set
to \pctrue. $\monitorinvariantname$ is therefore preserved.

Regarding $\nicinvariantname$, $\statedefinedinvariantname$ is preserved
because clearing \regtxzerohdp\ when the initialization automaton is in
$\itstatenameinitializehdpcp$ do not cause the NIC to enter an undefined state.
All other sub-invariants of $\txinvariantname$ hold vacuously since
$\bdqueuentxstate{\nstate'}$ is empty (since clearing \regtxzerohdp\ causes the
NIC model to also clear $\nstatenext.\sopbdpa$).

\paragraph{Case 2}
$\regtxzerohdp = \pczero$ implies that $\bdqueuentxstate{\nstate}$ is empty, and
$\monitorisqueuesecure{\monitorargvalue}{\pctx}$ implies that the new
transmission queue $\bdqueuentxstate{\nstatenext}$ starting at
$\monitorargvalue$, is not overlapping with the reception queue
$\bdqueuenrxstate{\nstate}$. For this reason, the writes of the OWN and EOQ
fields by $\monitorpreparequeue{\monitorargvalue}{\pctx}$ does not affect the
sub-invariants of $\nicinvariantname$ that depend on the reception queue.
$\monitorisqueuesecure{\monitorargvalue}{\pctx}$ implies that
$\bdqueuentxstate{\nstatenext}$ satisfies all security requirements, and thus
also all sub-invariants of $\nicinvariantname$ that depend on the transmission
queue. Regarding $\monitorinvariantname$,
$\monitordecrementrhonicupdatealphaqueue{\monitorargvalue}{\pctx}$ marks all
entries of $\mactivecppiram$ of the BDs in $\bdqueuentxstate{\nstatenext}$ and
sets $\mtxactivequeue$ to $\monitorargvalue$, thereby preserving their
associated invariants, and thus also $\monitorinvariantname$.

\subsection{Handler $\monitorfunctionnamenicmemory$ preserves
  $\nicinvariantname \land \monitorinvariantname$}
There are two cases in which the execution of $\monitorfunctionnamenicmemory$
affects the data structures of the monitor or the NIC state:
\begin{enumerate}
\item $\monitorargpa$ does not address a BD reachable from $\mtxactivequeue$ or
$\mtxactivequeue$ ($\neg \mactivecppiram[\monitorargpa]$).
\item $\monitorargpa$ addresses an NDP field equal to zero of a BD in the
transmission or reception queue.
\end{enumerate}

\paragraph{Case 1} $\monitorinvariantname(\monitorstate, \nstate)$ implies that
the transmission and reception queues of the NIC ($\bdqueuentxstate{\nstate}$
and $\bdqueuenrxstate{\nstate}$) are suffixes of the corresponding queues as
viewed by the monitor 
($\bdqueuemtxstatestate{\monitorstate}{\nstate}$ and
$\bdqueuemrxstatestate{\monitorstate}{\nstate}$). Since $\monitorargpa$ does not
address a 4-byte word of a BD reachable from $\mtxactivequeue$ or
$\mtxactivequeue$, the addressed location is not a part of a BD in use by the
NIC. The write does therefore not affect the NIC queues nor the NIC automata.
That is, the write satisfies
$\automatastatespreserveinvariant{\opvar}{\nstate}{\nstatenext}$, keeps the
queues disjoint and does not cause the NIC to enter an undefined state, thereby
preserving each sub-invariant of $\nicinvariantname$ by
Lemma~\ref{lem:inv:pres:automaton}. In addition, no data structure of the
monitor is written, thereby preserving $\monitorinvariantname$.

\paragraph{Case 2} Only the case for transmission is considered since the case
for reception is similar, for which the reasoning is nearly identical to Case 2
of the handler $\monitorfunctionnametxhdp$. The difference is that there is an
existing transmission queue, which the appended queue is checked to not overlap.

\subsection{Handler $\monitorfunctionnametxcp$ preserves $\nicinvariantname \land \monitorinvariantname$}
\label{sec:txcpcorrect}
The operations of $\monitorfunctionnametxcp$ and $\monitorfunctionnametxhdp$
when $\minitialized = \pctrue$ are analogous and the correctness reasoning of
$\monitorfunctionnametxcp$ is therefore analogous to the correctness reasoning
of $\monitorfunctionnametxhdp$.

Otherwise
($\neg \minitialized \land \mtxzerotearingdown \land \regtxzerocp = \pctdiacnumber$),
for $\monitorinvariantname$ to be preserved, the transmission teardown automaton must be in the state
$\tdstatenameidle$. The only transition of the NIC model that writes
\pctdiacnumber\ to \regtxzerocp\ is the last transition of the transmission
teardown operation. Hence, if
$\text{\regtxzerocp} = \pctdiacnumber$, the transmission teardown automaton is
idle.

\subsection{Handler $\monitorfunctionnametxtd$ preserves $\nicinvariantname \land \monitorinvariantname$}
Writing \regtxteardown\ has two possible outcomes depending on the state of the
NIC and the value written. If either the NIC is not initialized
($\nstate.it.s \neq \itstatenameinitializehdpcp$), the transmission
teardown automaton is not idle ($\nstate.td.s \neq \tdstatenameidle$), or
the value written is not \pczero, then the NIC model enters an undefined state.
Otherwise, the transmission teardown automaton is activated.

The former outcome cannot occur since only \pczero\ is written to
\regtxteardown\, and that write only occurs when $\minitialized$ and
$\neg \mtxzerotearingdown$. The values of the latter two monitor data structures
and $\monitorinvariantname(\monitorstate, \nstate)$ imply that the NIC is
initialized and that the transmission teardown automaton is in the state
$\tdstatenameidle$. Therefore the NIC does not enter an undefined state. Hence,
only the latter outcome is relevant, causing activation of the transmission
teardown automaton, which does not affect $\nicinvariantname$ and thus
$\nicinvariantname$ is preserved. Since $\mtxzerotearingdown$ is set to \pctrue,
$\monitorinvariantname$ is preserved.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
