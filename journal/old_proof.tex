

The last two conjuncts are used to ensure that when initialization is complete, the reception queue is empty. An empty reception queue means that the reception automaton cannot perform a transition and therefore its invariant holds vacously when initialization is complete.

The invariant of the intitialization automaton is satisfied when the NIC is in an initial state nic, which satisfies \(\itstatepoweron \land \txstateidle \land \rxstateidle\). The second and third conjuncts imply the first conjunct of the invariant, while the first conjunct implies the last two conjuncts of the invariant. Furthermore, the first and the third conjuncts imply that the reception automaton cannot perform a transition from an initial state. The implications of these properties is that the NIC invariant is satisfied in an initial state.

Remaining parts of a NIC invariant are concerned with how the the CPU executing the NIC device driver configures the NIC.


\rxinvariantnoargmath\ and \rdinvariantnoargmath\ are similar to \txinvariantnoargmath\ and \tdinvariantnoargmath\ but with respect to reception instead of transmission, respectively, and one main difference. \txinvariantnoargmath\ states that the transmission automaton always process the current head of the transsmission buffer descriptor queue. \rxinvariantnoargmath\ states instead that the reception automaton always process some buffer descriptor in the reception queue \rxbdqueuemath\ but not a buffer descriptor in the queue that that has not been fetched yet \rxunseenbdqueuemath\ (\(\Vert\) denotes concatenation):
\begin{align*}
&(\exists q_{rx-seen}.\ \rxbdqueue = q_{rx-seen}\ \Vert\ \rxunseenbdqueue)\ \land\\
&(\rxcurrentbdpa \neq 0 \implies \rxcurrentbdpa \in \rxbdqueue \setminus \rxunseenbdqueue)
\end{align*}

This difference between \rxinvariantnoargmath\ and \txinvariantnoargmath\ occurs due to each buffer descriptor being both a SOP and an EOP for transmission while this is not necessarily the case for reception, causing the currently processed buffer descriptor to not being the head of the queue (the head of a queue is advanced to the buffer descriptor following the EOP buffer descriptor of the currently processed frame after that frame has been processed).






This section describes the main ideas for proving that autonomous and memory read reply transitions preserve the invariant of the NIC (transitions  register accesses are not considered since they are only relevant if a CPU/device driver is taken into account):
\begin{alignat*}{1}
&\forall nic\ mr\ nic'\ mr'\ R\ W.\\
&((nic', mr') = \nictransitionautonomous\ \lor nic' = \nictransitionprocessmemoryrequest)\ \land\\
&\nicinvariantarg{nic}\\
&\implies\\
&\nicinvariantarg{nic'}
\end{alignat*}

The preservation proofs of the invariants (e.g. \txinvariantnoargmath) with respect to the associated automata (transmission automaton) are decomposed by proving preservation of each subinvariant (\txinvariantbdqueuelocationdefinednoargmath) for each subtransition function of the associated automaton (\txfetchnextbdnoargmath) assuming that the automaton can make a transition (\txstatetransitionenableorprocessmemoryreadreplymath) and the invariant (\txinvariantnoargmath, which imply \txinvariantsuccedentmath). There are three kinds of reasonings behind these preservation proofs depending on which state components the subinvariant depends on hand how the subtransition function generates the next state.

The first kind of proof is when the considered subtransition function \textbf{does not assign a new value to the state components that the considered subinvariant depends on}. For instance, \txinvariantsuccedentmath, depends on ten state components, seven of which are assigned by at most two subtransition functions, and two of which are assigned by at most three. Therefore, a common preservation proof strategy is to prove a preservation and non-assignment lemmas for the considered subinvariant and subtransition functions. The preservation lemma states that for two states \prestatevar\ and \poststatevar\ whose state components the subinvariant depends on are equal, if \prestatevar\ satisfies the subinvariant, then \poststatevar\ also satisfies it. The non-assignment lemma is proved for each transition function, and states that the subtransition function does not assign the state components the subinvariant depends on. The preservation and non-assignment lemmas are then combined to prove that the subtransition function preserves the subinvariant.
%\begin{itemize}
%	\item nic.dead (assigned by 1): NIC is in dead state.
%	\item nic.tx.current\_bd.ndp (1): Address of the buffer descriptor following the currently processed one.
%	\item nic.tx.current\_bd.eop (1): Read buffer descriptor is an EOP.
%	\item nic.tx.pa\_of\_next\_memory\_buffer\_byte (1,2): Address of the next memory byte to fetch.
%	\item nic.tx.number\_of\_buffer\_bytes\_left\_to\_request (1, 2): Number of memory bytes left to fetch of the currently processed buffer descriptor.
%	\item nic.tx.sop\_bd\_pa (4, 5): Head of the buffer descriptor queue.
%	\item nic.regs.\cppiram (4, 5): \cppiram memory.
%	\item nic.tx.current\_bd\_pa (3, 4, 5): Currently processed buffer descriptor.
%	\item nic.tx.expects\_sop (3, 4, 5): Next buffer descriptor is expected to be a SOP.
%	\item nic.tx.state (1, 2, 3, 4, 5, 6): State of transmission automaton.
%\end{itemize}
%The first 7 and 2 second last state components are assigned by at most 2, and 3, out of 6 subtransition functions of the transmission automaton, respectively. Therefore, one common approach to prove that an invariant I is preserved is to prove a preservation lemma for I. That preservation lemma states that for two arbitrary related NIC states nic and nic' (they are related if the state components I depends on are equal between nic and nic'), if I nic holds, then I nic' holds. For each transition function, a non-assignment lemma is proved stating which state components are not assigned by that transition function. The preservation and non-assignment lemmas are then combined to prove that each transition function with the required non-assignment properties preserves I.

The second kind of proof is when the considered subtransition function \textbf{establishes the subinvariant and does not assign \cppiramstatevar}. This situation occurs when the subinvariant is an implication where the pre-state does not satisfy the antecedent but the post-state does. In such a situation, a subtransition function either establishes the invariant directly, or indirectly by means of subinvariants assumed to be satisfied by the pre-state. An example of the former case is the subinvariant

\(\txinvariantcurrentbdpaeqsopbdpa \triangleq \txcurrentbdpa = \txsopbdpa\),

and the subtransition function \txclearownerandhdp\ returning a state \poststatevar\ satisfying 

\(\txcurrentbdpaarg{nic'} = \txcurrentbdndp\ \land\ \txsopbdpaarg{nic'} = \txcurrentbdndp\).

An example of the latter case is the proof of that \txinvariantmemoryreadablestatenoargmath (stating that the pointer and counter state components identifying the next memory byte address to read is readable) is preserved by tx\_1fetch\_next\_bd (setting the pointer and counter state components by reading \cppiramstatevar\ at \txcurrentbdpavar). That proofs is derived by means of the following subinvariants:
\begin{itemize}
	\item \txinvariantmemoryreadablebdqueuenoargmath: All buffer descriptors in the transmission queue address only readable memory.
	\item \txinvarianttxstatenotbdqueueemptynoargmath: \txbdqueuemath\ is not empty, implying \(\txsopbdpa \in \txbdqueue\).
	\item \txinvariantcurrentbdpaeqsopbdpanoargmath: \(\txcurrentbdpa = \txsopbdpa\), implying by means of the previously listed subinvariant \(\txcurrentbdpa \in \txbdqueue\), and hence that the buffer descriptor at address \txcurrentbdpavar\ addresses only readable memory by means of the first listed subinvariant.
\end{itemize}

This gives the lemma:
\begin{alignat*}{1}
&\forall nic\ nic'.\\
&nic' = \txfetchnextbd\ \land nic'.tx.state = \txstatefetchnextbd\ \land\\
&\txinvariantmemoryreadablebdqueue\ \land \txinvarianttxstatenotbdqueueempty\ \land \txinvariantcurrentbdpaeqsopbdpa\\
&\implies\\
&\txinvariantmemoryreadablestate
\end{alignat*}

which can then be used in the proof of that \nictransitionautonomousnoargmath\ preserves \nicinvariantnoargmath.

The third kind of proof is when the considered subtransition function \textbf{assigns \textit{nic.regs.\cppiram}}. Most of the subinvariants depend on the location of a buffer descriptor queue and a few on the contents of the buffer descriptors. This means that the preservation proofs of these subinvarants depend on how a subtransition function assigns \cppiramstatevar. These preservation proofs rely on lemmas stating the following properties of the subtransition functions:
\begin{itemize}
	\item the assigned byte of \cppiramstatevar\ belongs to a buffer descriptor in the queue,
	\item the next descriptor pointer field is unchanged of the assigned buffer descriptor, and
	\item if the subinvariant depends on the new value, then that value does not falsify the subinvariant, or the assigned buffer descriptor is outside the part of the queue that may be processed later (unless a teardown operation is pending).
\end{itemize}

These properties hold for all subtransition functions, and are encoded by the predicate \nicdeltawritesfieldsnotndpofbdsinbdqueuemath\ having the arguments:
\begin{itemize}
	\item \(\delta\): a subtransition function from NIC states to NIC states.
	\item \textit{ws}: A list of pairs, \([(w_0, a_0), ..., (w_{n-1}, a_{n-1})]\), where \(w_i\) is a function from \cppiramstatevar and 13-bit addresses to \cppiramstatevar, and \(a_i\) is an address of a buffer descriptor.
	\item \textit{q}: A list of addresses of buffer descriptors.
\end{itemize}

\nicdeltawritesfieldsnotndpofbdsinbdqueuemath\ encode four statements:
\begin{enumerate}
	\item \(\delta\) assigns \cppiramvar\ by first assigning \cppiramstatevar\ at the buffer descriptor located at address \(a_0\) by means of the function \(w_0\), then assigning the resulting \cppiramvar\ at the buffer descriptor at \(a_1\) and so on: \begin{alignat*}{1}
	&(\delta(nic)).regs.\cppiram = w_{n-1}(...w_0(nic.regs.\cppiram, a_0)..., a_{n-1})
	\end{alignat*}
	\item Each assigned buffer descriptor is located in the list \(q\): \begin{alignat*}{1}
	&\forall a.\ a \in \left\{a_0, ..., a_{n-1}\right\} \implies a \in q
	\end{alignat*}
	
	\item Each function assigning \cppiramvar\ does not modify the next descriptor pointer of the buffer descriptor assigned at address \(a\): \begin{alignat*}{1}	
	&\forall w_i\ \cppiram\ a.\ (w_i(\cppiram,a)).ndp = (\cppiram(a)).ndp
	\end{alignat*}
	
	\item No \(w_i\) modifies a location outside the buffer descriptor \(w_i\) assigns: \begin{alignat*}{1}
&\forall w_i\ \cppiram\ w\ r.\\
&\neg BDs\_OVERLAP(w,r) \implies (w_i(\cppiram, w))(r) = \cppiram(r))
\end{alignat*}
\end{enumerate}

%These properties are used to infer that the subtransition functions \(\delta\) of the automata do not modify the location of buffer descriptors in the transmission and reception queues (under the assumption that such a queue does not overlap itself), nor any buffer descriptor outside the queue (implying no modification of the other queue under the assumption that the transmission and reception queues do not overlap).

The lemmas that the subtransition functions satisfy \nicdeltawritesfieldsnotndpofbdsinbdqueuemath\ is proved in three steps. First, statements 3 and 4 are proved for each \(w_i\) in the definition of the NIC model. Second, statement 1 is proved for each subtransition function by defining a list describing how that subtransition assigns \cppiramvar\ (denoted \(ws\) above). Third, by assuming the invariant for the pre-state \nicvar, statement 2 is proved for \(q\) instantiated with the relevant queue (e.g. \rxbdqueuemath). The resulting lemmas are then be used to prove properties of the subtransition functions like:
\begin{itemize}
	\item The location of the queue \(q\) is preserved (e.g. \rxbdqueuemath).
	\item The queue \(q\) is still finite.
	\item The contents of a buffer descriptor queue not overlapping \(q\) (e.g. \txbdqueuemath) is not modified.
	\item The queue \(q\) is not expanded.
\end{itemize}

These properties are then used in preservation proofs of subinvariants, e.g. \txinvariantbdqueuefinitenoargmath, and the invariant \txrxbdqueuesdisjointinvariantnoargmath.

The predicate \nicdeltawritesfieldsnotndpofbdsinbdqueuenoargmath\ is used to reduce the amount of redundancy in the proof compared to if it was not used, and implies lemmas necessary to prove for many subtransition functions.

The automata must not only preserve their associated invariant but also the invariants associated to the other automata. Each invariant must be preserved by the automata as follows (only the transmission and reception automata can cause transitions to a dead state or issue disallowed memory requests):
\begin{itemize}
	\item \txinvariantnoargmath: Reception and reception teardown automata. The transmission teardown and the initialization automata do not need to preserve \txinvariantnoargmath\ since they cannot perform transitions from a state from which the transmission automaton can perform a transition.
	\item \rxinvariantnoargmath: Transmission and transmission teardown automata.
	\item \txrxbdqueuesdisjointinvariantnoargmath: Transmission, reception and teardown automata since they all assign \cppiramstatevar.
	\item \tdinvariantnoargmath: Transmission teardown and reception automata since the transmission teardown automaton must not write into the reception queue. 
	\item \rdinvariantnoargmath: Reception teardown and transmission automata.
\end{itemize}

These preservation proofs are also based on \nicdeltawritesfieldsnotndpofbdsinbdqueuenoargmath.

To complete the verification of that the NIC invariant is preserved, it must be proved that the CPU and all other I/O devices of the computer preserve the NIC invariant. This requires an invariant for the CPU that ensures that the NIC register writes the CPU performs preserve the NIC invariant (which among other things requires that the NIC cannot write into the memory region storing CPU instructions, and that certain data structures track the state of the NIC to prevent the CPU from writing NIC registers such that the NIC enters an undefined state), and that the CPU configures the other I/O devices such that they cannot write the NIC registers that affect which memory accesses the NIC performs.




In fact, the DMA requests raised by 

\subsection{Proof of Theorem~\ref{thm:main}.2 and~\ref{thm:main}.3}


The proofs of that the NIC only issues read and write memory requests to readable and writable memory follows immediately from the two subinvariants \txinvariantmemoryreadablestatenoargmath\ and \rxinvariantmemorywritablestatenoargmath\ of the transmission and reception automata, defined as follows (\rxinvariantmemorywritablestatenoargmath\ is defined similarly but for the reception automaton):
\begin{alignat*}{1}	
	&\txinvariantmemoryreadablestate \triangleq\\
	&nic.tx.state = \txstateissuememoryreadrequest\ \lor\\
	&(nic.tx.state = \txstateprocessmemoryreadreply\ \land\ \txnobytesleft > 0)\\
	&\implies\\
	&\forall a.\ nic.tx.dma\_a \leq a \leq \txdmapointer + (\txnobytesleft - 1) \implies R(a)
\end{alignat*}

\txnobytesleftmath\ specifies the number of bytes left to fetch from memory, and \txdmapointermath\ identifies the address of the next byte to fetch from memory.

This leads to the theorem:
\begin{alignat*}{1}
&\forall nic\ nic'\ mr'\ a\ v\ R\ W.\\
&((nic', mr') = \nictransitionautonomous\ \land \nicinvariantarg{nic}\\
&\implies\\
&(mr' = (a, \bot) \implies R(a))\ \land\\
&(mr' = (a, v)\ \land\ v \neq \bot \implies W(a))
\end{alignat*}

The predicate \nicdeltawritesfieldsnotndpofbdsinbdqueuenoargmath is
used to reduce the amount of redundancy in the proof compared to if it
was not used. It implies lemmas necessary to prove for each
subtransition function.

If \(mr' \neq \bot\), then the transition issued a memory request. If \(mr = (a, \bot\), then a request is issued to read the byte in system memory at address \(a\). If \(mr = (a, v) \land v \neq \bot\), then a request is issued to write the byte value \(v\) to system memory at address \(a\).

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
