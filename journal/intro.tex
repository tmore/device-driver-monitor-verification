\section{Introduction}
Formally verified execution platforms (microkernels~\cite{klein2009sel4},
hypervisors~\cite{leinenbach2009verifying} and separation
kernels~\cite{dam2013formal}) constitute key software infrastructure for implementing
secure IoT devices. By guaranteeing memory isolation and controlling
communication between software components, they prevent faults of non-critical
software (e.g., HTTP interfaces, optimizations based on machine learning, and
software providing complex functionality or with short life cycle) from
affecting software that must fulfill strict security and safety requirements.
This enables verification of critical software without considering untrusted
software.

A problem with these platforms is that the verification does not consider
I/O devices with direct memory access (DMA). Current systems either disable
them, use a special System or Input/Output MMU (SMMU or IOMMU; usually
unavailable in embedded systems) to isolate potentially misconfigured devices,
or trust the (usually large) controlling software.
% (in case of microkernels, this consists of unpriviledged drivers that are comparable 
% in complexity and size to the microkernel itself;
% in case of Xen, this consists of the entire Linux Dom0)

In order to address this issue we advocate designing secure IoT and embedded
systems using component isolation and the principle of complete mediation: The
reconfigurations of the I/O device defined by a device driver are checked by a
secure monitor to enable the device to only access certain memory regions.
This monitor preserves a security policy which is described by an
invariant. The rationale is that the monitor is substantially simpler, and
therefore easier to analyze and verify, than the untrusted software. In
this context, security depends mainly on three properties: (1) The security
policy (i.e., the invariant) implies that the I/O device cannot violate memory
isolation; (2) The monitor is correctly isolated from the other, possibly
corrupted, components of the system (i.e., the execution platform is formally
verified or vulnerabilities are unlikely due to the small code base of the
kernel); (3) The monitor is functionally correct and denies configurations that
violate the invariant (i.e., the monitor is verified or its small code minimizes
the number of critical bugs).

We contribute with the first formal verification of (1) for a real I/O device of
significant complexity. As a demonstrating platform we use the embedded system
Beaglebone Black (a commonly available development board) and its Network
Interface Controller (NIC). We provide a formal model of the NIC and we define
the security policy as an invariant in terms of the state of the NIC. We then
demonstrate that this policy is sound: The invariant is preserved by the NIC and
it restricts memory accesses to predetermined memory regions. The analysis is
machine-checked by means of the interactive theorem prover HOL4, which makes
our reaosning trustworthy.

To demonstrate the applicability of this approach we implemented a secure
connected system. Real systems often: need complex network stacks and
application frameworks, have short time to market, require support of legacy
features, and adopt binary blobs. For these reasons many applications are
dependent on commodity OSs. Our goal is to provide a system that
satisfies some desired security properties (e.g., absence of malware), even if
the commodity software is completely compromised.
We extend the Prosper hypervisor~\cite{dam2013formal}, which has been
previously verified to guarantee property (2), with secure support for network
connectivity by deploying a NIC monitor, and analyze the correctness (i.e.,
property (3)) of the monitor.

The paper is organized as follows. In Section~\ref{sec:controllers} we provide a
high level description of common DMA controllers in order to demonstrate that
the majority of devices are configured similarly to the NIC under analysis.
Section~\ref{sec:overview} presents the security threats posed
by the untrusted and potentially compromised device driver of the NIC.
%
The following four sections~\ref{sec:nic}--\ref{sec:hol4} describe the
contributions for verification of property (1). Section~\ref{sec:nic} introduces
the hardware platform and its formal model, Section~\ref{sec:validation}
discusses the correctness of the NIC model, Section~\ref{sec:proof} describes
the invariant and the structure of the corresponding proof, and
Section~\ref{sec:hol4} describes the implementation of the model and proof in
HOL4. The next four sections~\ref{sec:sw}--\ref{sec:upgrade} describe a secure
connected system implemented with our design approach.
Section~\ref{sec:sw} presents the extension of the existing hypervisor with the
NIC monitor and evaluates the resulting overhead, Section~\ref{sec:monitor}
describes the monitor, Section~\ref{sec:monproof} motivates the correctness of
the monitor, and Section~\ref{sec:upgrade} describes an application of the
resulting software platform, which supports remote software upgrade and prevents
code injection in a connected (and potentially vulnerable) Linux system.
Finally, Section~\ref{sec:related} and~\ref{sec:conclusions} present related
work and concluding remarks.

% This demonstrator consists of a secure hypervisor that hosts a paravirtualized
% Linux, a secure service, and the NIC monitor. The untrusted Linux is in charge
% of controlling the NIC, but each NIC reconfiguration is intercepted and
% analyzed by the monitor, which forbids DMA requests addressing memory locations
% not part of the memory region of Linux. This enables the secure service to
% access the Internet, using Linux as an untrusted intermediary, and to reduce
% the trusted computing base.


% Some modern platform provide hardware support to handle this problem.
% A System-MMU (or IO-MMU) is placed to mediate all device memory
% accesses and can be configured by the kernel in a similar fashion of
% the CPU MMU. Once the System-MMU is correctly configured, the kernel
% can forget to care about the connected devices. SystemMMU are
% attractive because they provide a similar abstration to the one
% provided by the MMU, simplifying kernel development, and their
% comfiguration is independent of the type of connected device, enabling
% a general kernel module to secure all possible devices.
% However, SystemMMUs have some limitations that make the unsuitable for
% several applications.
% First, not all CPUs, expecially in the embedded domain, support System
% MMUs. Also, in some platforms the SystemMMU control only a subset of
% the available devices, while others are left unrestricted. Finally,
% SMMU introduce latency and unpredictability of accessing time, since every memory access
% performed by a device must be translated, requiring potentially
% multiple page table walks.
% This is not desirable when high-performance must be guaranteed of
% jitters can cause problems and side channels.

% Multiplexing need device knowledge.








%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
%  LocalWords:  upgradable
