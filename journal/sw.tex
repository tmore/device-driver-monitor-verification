\section{Isolating Secure Partitions in an IoT System}\label{sec:sw}

% \section{Prevention of Code-Injection in IoT System}\label{sec:sw}
To demonstrate the applicability of our design we developed a software
platform to isolate security critical components from a connected
Linux system. BeagleBone Black is used for evaluation.
%\vskip -40pt
\subsection{Existing Platform}
\begin{figure}[t]
  \center
  \subfloat[Disabled peripherals.\label{fig:prosper}]{
    \includegraphics[width=0.45\linewidth]{arch01}
  }
 ~
  \subfloat[Secure peripherals via monitoring.\label{fig:monitor}]{
    \includegraphics[width=0.45\linewidth]{arch02}
  }
  \caption{Prosper hypervisor.}
  \label{img:design}
\end{figure}

Prosper (c.f. Figure~\ref{fig:prosper}) is a
hypervisor~\cite{nemati2015trustworthy} for ARMv7 that is capable of isolating a
Linux guest from itself and other guests. The latter can be used to deploy
security critical software and isolate it from faults in Linux. Linux is
paravirtualized (modified) to be executed in user mode alongside its
applications. Only the hypervisor is executed in privileged mode and which is
invoked via hypercalls. In order to guarantee isolation, the hypervisor is in
control of the MMU and virtualizes the memory subsystem via direct paging: Linux
allocates the page tables inside its own memory area and can directly modify
them while the tables are not in active use by the MMU; once the page tables are
in active use by the MMU, the hypervisor guarantees that those page tables can
be modified only via hypercalls. The isolation properties of Prosper have been
formally verified. However, the existing proofs disregard devices and assume
that memory can be changed only by the CPU whose accesses are mediated by the
MMU.

\subsection{Attacker Model}
Concerning the Linux guest it is not realistic to restrict the attacker model,
since it has been repeatedly demonstrated that software vulnerabilities have
enabled overtaking complete Linux systems via privilege escalation. For this
reason we assume that the attacker has complete control of the Linux guest. The
attacker can force Linux to execute and access arbitrary code and data. It is
assumed that the goal of the attacker is to escape isolation, i.e., reading or
writing arbitrary memory of a secure guest.

Prosper guarantees isolation (i.e., prevents direct information flow between
Linux and a secure guest) if the CPU is the only hardware component that can
access memory~\cite{chfouka2015trustworthy}. However, if Linux can configure a
DMA device then Linux can indirectly perform arbitrary memory accesses with
catastrophic consequences: for example it can configure BDs to address
hypervisor code, page tables, secure guest memory, and confidential memory
regions, all of which will be written or read by the DMA device.

\subsection{Secure Network Connectivity via Monitoring}

We extend the system with Internet connectivity while preventing Linux from
abusing the DMAC of the NIC. We deploy a NIC monitor (c.f.
Section~\ref{sec:monitor}) within the hypervisor that validates all NIC
reconfigurations (c.f. Figure~\ref{fig:monitor}). The hypervisor forces Linux to
map the NIC registers with read-only access (NIC register reads have no side
effects). When the Linux NIC driver attempts to configure the NIC, by writing a
NIC register, an exception is raised. The hypervisor catches the exception and,
in case of a NIC register write attempt, invokes the monitor. The monitor checks
whether the write preserves the NIC invariant, and if so re-executes the write,
and otherwise blocks it. In addition to the NIC monitor, we extended the checks
of the hypervisor to ensure that page tables are not allocated in buffers
address by BDs in the reception queue, since those buffers are written when
frames are received.

% \subsubsection{Secure Remote Upgrade}
% \todo{New section, architecture details / new system call to update
%   and verify signatures/ etc}
% In addition to enabling Internet connectivity to Linux applications, the new
% system design also enables connectivity for the secure components, which can
% use Linux as an untrusted ``virtual'' gateway. We used this feature to
% implement secure remote upgrade of Linux applications. New binary code and
% corresponding hash values are signed using the administration private key and
% published by a remote host. Linux downloads the new binaries, hash values, and
% signatures and requests an update of the golden image via a hypercall. The
% hypervisor forwards the request to MProsper. The signature is checked by
% MProsper using the administration public key, and if it is valid, the golden
% image is updated with the new hash values. The use of digital signatures makes
% the upgrade trustworthy, even though Linux acts as a network intermediary, and
% furthermore, even if Linux is compromised. A similar approach is used to revoke
% hash values from the golden image.

% The formally verified parts of this secure remote upgrade mechanism are:
% \begin{itemize}
% 	\item The hypervisor isolates Linux, MProsper and the NIC monitor: Verified at
% 	the abstraction level where each transition describes the execution of one CPU
% 	instruction.
% 	\item MProsper ensures that no unsigned Linux code is executed: Verified at
% 	the abstraction level where each transition describes one invocation of
% 	MProsper.
% 	\item The invariant of the NIC ensures that the NIC only accesses readable and
% 	writable memory: Verified at the abstraction level where each transition
% 	describes one NIC register/memory update.
% \end{itemize}

% To completely formally verify the secure remote upgrade mechanism the NIC
% monitor must be formally verified to preserve the NIC invariant (instantiated
% with the memory regions that are readable and writable), and that no
% other code of the hypervisor nor any guests write NIC registers. (To get a
% rigourous verification, the verification of MProsper should be refined to the
% CPU instruction level.)

Having the NIC driver in Linux in contrast to a specialized NIC driver in the
hypervisor has several advantages. It keeps the code of the hypervisor small,
and avoids verification of code that manages power management, routing tables
and statistics of the NIC. Furthermore, in this design the interface between the
OS and the NIC is OS independent. The monitor provides a NIC interface that
closely mimics that of the NIC, with the difference that security violating
reconfigurations are blocked. Hence, the hypervisor and the monitor can be used
with different OSs, OS versions, and device driver versions. Finally, the design
demonstrates a general approach to secure DMACs that are configured via linked
lists of BDs and can easily be adapted to support other DMACs.

% Third, fewer
% context switches between Linux and the hypervisor are performed,
% avoiding unnecessary performance hits. 

\subsection{Evaluation}
We evaluated network performance with netperf for the system in
Figure~\ref{fig:monitor}, involving Linux 3.10 and BeagleBone Black (BBB).
Linux was running netperf 2.7.0 on BBB, which was connected with a 100 Mbit
Ethernet point-to-point link to a PC running netperf 2.6.0. The benchmarks are:
TCP\_STREAM and TCP\_MAERTS transfer data with TCP from BBB to the PC and vice
versa; UDP\_STREAM transfers data with UDP from BBB to the PC; and TCP\_RR and
UDP\_RR use TCP and UDP, respectively, to send requests from BBB and replies
from the PC. Each benchmark lasted for ten seconds and was performed five times.
Table~\ref{tbl:benchmark} lists the average value for each test.
\begin{table}[]
\center
\begin{tabular}{|l|r|r|r|r|r|}
\hline
\multirow{2}{*}{Configuration} & \multicolumn{5}{c|}{Benchmark} \\ \cline{2-6}
& TS & TM & US & TR & UR \\ \hline
Native			& 94.1	& 93.9	& 96.2	& 3365.1	& 3403.4 \\ \hline
Native+Monitor		& 94.1	& 93.9	& 96.2	& 3317.6	& 3402.2 \\ \hline
Hyper			& 16.2	& 45.6	& 29.3	& 924.9		& 1009.0 \\ \hline
Hyper+Monitor		& 15.3	& 41.0	& 27.6	& 891.3		& 982.6 \\ \hline
%  2			& 94.1	& 93.9	& 96.2	& 3365.1	& 3403.4 \\ \hline
% 2+M		& 94.1	& 93.9	& 96.2	& 3317.6	& 3402.2 \\ \hline
% 1			& 16.2	& 45.6	& 29.3	& 924.9		& 1009.0 \\ \hline
% 1+RXM		& 14.6	& 44.4	& 28.5	& 905.1		& 1003.0 \\ \hline
% 1+BDM		& 14.1	& 38.8	& 27.9	& 864.8		& 979.2 \\ \hline
%  1+M		& 15.3	& 41.0	& 27.6	& 891.3		& 982.6 \\ \hline
\end{tabular}
\vskip 5pt
\caption{Netperf benchmarks. TS (TCP\_STREAM), TM (TCP\_MAERTS) and US
(UDP\_STREAM) are measured in Mbit/second, and TR (TCP\_RR) and UR (UDP\_RR) are
measured in transactions/second.}
\label{tbl:benchmark}
\end{table}

We compare the network performance of the system (Hyper+Monitor) shown in
Figure~\ref{fig:monitor} with the system (Hyper) where Linux
is executed on top of the Prosper hypervisor but is free to directly configure
the NIC, and therefore being able to violate all security properties. The
performance of the Hyper+Monitor system is between 89.9\% and 97.4\% of
the Hyper system. This performance loss
is expected due to the additional context switches caused by the Linux NIC
driver attempting to write NIC registers.

To validate the monitor design we also experimented with a different system. In
this case we consider a trusted Linux kernel that is executed without the
hypervisor but with a potentially compromised NIC driver (Native). This is
typically the case when the driver is a binary blob. In order to prevent the
driver from abusing the NIC DMA the monitor is added to the Linux kernel
(Native+Monitor). The Linux NIC driver has been modified to not directly write
NIC registers but instead to invoke the monitor when it needs to write a NIC
register. The monitor is similar to the one in the hypervisor, and the C file
containing the monitor code is located in the same directory as the Linux NIC
driver. The overhead introduced by this configuration is negligible, as
demonstrated by the first two lines of Table 1. The same approach can for
instance be used to monitor an untrusted device driver that is executed in user
mode on top of a microkernel (e.g., seL4 and Minix).

In addition to being OS and NIC driver independent, the monitor minimizes the
trusted computing base configuring the NIC. In fact, the monitor consists of 900
lines of C code while the Linux NIC driver consists of 4650 lines. Moreover, the
monitor is independent of the specific version of the Linux kernel and the NIC
driver, the latter of which has grown to 6500 lines of C in Linux 5.2.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
