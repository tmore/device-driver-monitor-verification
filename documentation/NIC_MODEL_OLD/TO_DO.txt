THESE LISTS SHALL BE PUT IN NIC_STATE.txt

The lists are of infinite length. Mention this in the appendix as well.

t1[]: l := [0, 1, 2, 3...]

Accessing elements are done as l[index].

Lists for non-determinism:
-NIC_REGISTER_READS.txt:
	*read_nic_register: word32.
-NIC_REGISTER_WRITES.txt:
	*write_tx0_hdp: word32 + 1.
	*write_rx0_hdp: word32 + 1.
	*write_tx0_cp: bool.
	*write_rx0_cp: bool.
-NIC_SCHEDULER.txt:
	*nic_execute: {init, transmit, receive, transmit_teardown, receive_teardown}.
-NIC_TRANSMISSION.txt:
	*transmit_step8: bool.
-NIC_RECEPTION.txt:
	*receive_step0: nat + 1.
	*receive_step4: word8.
	*receive_step5: word2.
	*receive_step6: word1.
	*receive_step7: word3.
	*receive_step15: word1.
	*receive_step16: word1.
	*receive_step17: word1.
	*receive_step18: word1.
	*receive_step20: {0, 1, 2}.
	*receive_step22: bool.
-NIC_TRANSMISSION_TEARDOWN:
	*transmit_teardown_step1: bool.
	*transmit_teardown_step4: bool.
-NIC_RECEPTION_TEARDOWN.txt:
	*receive_teardown_step1: bool.
	*receive_teardown_step2: bool.
	*receive_teardown_step3: bool.
	*receive_teardown_step6: bool.

//////////////////////////////////////////////////////////////////
//Lists and indexes used for making non-deterministic decisions.//
//////////////////////////////////////////////////////////////////

non-determistic_lists ≝ (
	register_read_lists register_read,
	register_write_lists register_write,
	schedule_lists schedule,
	tx_lists tx,
	rx_lists rx,
	tx_td_lists tx_td,
	rx_td_lists rx_td,
)

register_read_lists ≝ (
	word32[] read_nic_register_list,		//The list with words that are returned if a non-modeled NIC register is read.
	nat read_nic_register_index,			//The index of the word to return if a non-modeled NIC register is read.
)

register_write_lists ≝ (
	word32[] write_tx0_hdp_list,			//The list with words to set TX0_HDP to when TX0_HDP is written.
	nat write_tx0_hdp_index,				//The index of the word to set TX0_HDP to when TX0_HDP is written.
	word32[] write_rx0_hdp_list,			//The list with words to set RX0_HDP to when RX0_HDP is written.
	nat write_rx0_hdp_index,				//The index of the word to set RX0_HDP to when RX0_HDP is written.
	bool[] write_tx0_cp_list,				//The list with decisions of whether transmission completion interrupts should be deasserted when TX0_CP is initialized.
	nat write_tx0_cp_index,					//The index of the decision of whether the next initialization of TX0_CP should cause a deassertion of a transmission completion interrupt.
	bool[] write_rx0_cp_list,				//The list with decisions of whether reception completion interrupts should be deasserted when RX0_CP is initialized.
	nat write_rx0_cp_index,					//The index of the decision of whether the next initialization of RX0_CP should cause a deassertion of a reception completion interrupt.
)

schedule_lists ≝ (
	{0, 1, 2, 3, 4}[] nic_execute_list,		//The list with identifiers (0=i,1=tx,2=rx,3=tx_td,4=rx_td) for selecting the next automaton transition.
	nat nic_execute_index,					//The index of the automaton that shall perform the next autonomous transition of the NIC model.
)

tx_lists ≝ (
	bool[] transmit_step8_list,				//The list with decisions of whether transmission completion interrupts shall be asserted after frame transmissions.
	nat transmit_step8_index,				//The index of the decision of whether the next transmission of a frame shall cause an interrupt.
)

rx_lists ≝ (
	nat[] receive_step0_list,				//The list with lengths of all received frames.
	nat receive_step0_index,				//The index of the length of the next received frame.
	word8[] receive_step4_list,				//The list with values of all bytes of received frames.
	nat receive_step4_index,				//The index of the next byte value to transfer to memory of the currently processed received frame.
	word2[] receive_step5_list,				//The list with values of the packet error field for reception buffer descriptors.
	nat receive_step5_index,				//The index of the value to set for the packet error field of the currently processed reception buffer descriptor.
	word1[] receive_step6_list,				//The list with values of the rx_vlan_encap field for reception buffer descriptors.
	nat receive_step6_index,				//The index of the value to set for the rx_vlan_encap field of the currently processed reception buffer descriptor.
	word3[] receive_step7_list,				//The list with values of the "from port" field for reception buffer descriptors.
	nat receive_step7_index,				//The index of the value to set for the "from port" field of the currently processed reception buffer descriptor.
	word1[] receive_step15_list,			//The list with values of the "Pass CRC" field for reception buffer descriptors.
	nat receive_step15_index,				//The index of the value to set for the "Pass CRC" field of the currently processed reception buffer descriptor.
	word1[] receive_step16_list,			//The list with values of the long field for reception buffer descriptors.
	nat receive_step16_index,				//The index of the value to set for the long field of the currently processed reception buffer descriptor.
	word1[] receive_step17_list,			//The list with values of the short field for reception buffer descriptors.
	nat receive_step17_index,				//The index of the value to set for the short field of the currently processed reception buffer descriptor.
	word1[] receive_step18_list,			//The list with values of the mac_ctl field for reception buffer descriptors.
	nat receive_step18_index,				//The index of the value to set for the mac_ctl field of the currently processed reception buffer descriptor.
	{0, 1, 2}[] receive_step20_list,		//The list with decisions of how to set the overrun field for reception buffer descriptors.
	nat receive_step20_index,				//The index of the decision for how to to set the overrun field for the buffer descriptors of the just received frame.
	bool[] receive_step22_list,				//The list with decisions of whether reception completion interrupts shall be asserted when a frame has been received.
	nat receive_step22_index,				//The index of the decision of whether the next reception of a frame shall cause an interrupt.
)

tx_td_lists ≝ (
	bool[] transmit_teardown_step1_list,	//The list with decisions of whether the EOQ bit should be set in the first unused buffer descriptor after a tear down.
	nat transmit_teardown_step1_index,		//The decision index of whether the EOQ bit should be set in the first unused buffer descriptor after the next tear down.
	bool[] transmit_teardown_step4_list,	//The list with decisions of whether transmission completion interrupts shall be asserted after tear down operations.
	nat transmit_teardown_step4_index,		//The decision index of whether the next transmission tear down operation shall cause an interrupt.
)

rx_td_lists ≝ (
	bool[] receive_teardown_step1_list,		//The list with decisions of whether the SOP bit should be set in the first unused buffer descriptor after a tear down.
	nat receive_teardown_step1_index,		//The decision index of whether the first unused buffer descriptor shall have its SOP bit set after the next tear down.
	bool[] receive_teardown_step2_list,		//The list with decisions of whether the EOP bit should be set in the first unused buffer descriptor after a tear down.
	nat receive_teardown_step2_index,		//The decision index of whether the first unused buffer descriptor shall have its EOP bit set after the next tear down.
	bool[] receive_teardown_step3_list,		//The list with decisions of whether the EOQ bit should be set in the first unused buffer descriptor after a tear down.
	nat receive_teardown_step3_index,		//The decision index of whether the first unused buffer descriptor shall have its EOQ bit set after the next tear down.
	bool[] receive_teardown_step6_list,		//The list with decisions of whether reception completion interrupts shall be asserted after tear down operations.
	nat receive_teardown_step6_index		//The index of the decision of whether the next reception tear down operation shall cause an interrupt.
)
